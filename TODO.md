# Pendências

## TODO
- melhorar o tratamento do header
  - permitir copiar alguns headers da página (listar quais headers quer na página options - e qual nome)
  - permitir colocar tags no header (no popup)
- colocar o suporte do cleanup
  - colocar dependende de URL (testar)
  - fazer o editor em tela popup/listar readonly o resultado (resumido)
  - tratamento dos erros do YAML
  - permitir salvar/carregar e salvar

- colocar as opções de salvar no google drive
- usar o clipboard do chrome
- limpeza minify, lint e uglify
- limpar libs não usadas e colocar para usar todas minificadas

## Done

- colocar no context menu
- colocar as opções mais espertas (habilitar os botões de copy só quando houver seleção)
- colocar no ícone que conseguiu fazer copy&paste
- consertar bug de meta e result nos metadados
- criar uma tela de opções e salvar essas opções
- baixar também as imagens
- Usar o miniy/browserify
- Colocar uma tela de opções (1)
- colocar em background.js
- usar o snowdown
- melhorar a interface do popup
- colocar atalhor para o teclado
- colocar as opções de salvar localmente ou clipboard https://stackoverflow.com/questions/4845215/making-a-chrome-extension-download-a-file
  - desabilitar o editor quando o cleanup não estiver habilitado
- mudar o processamento para tratar todo o HTML em sequencia (convertes/index.js)

## Referencias

Cleanup:

    host: (regexp de URL)
    action: [remove, change tag]
    order: number
    selector: jQuery style
