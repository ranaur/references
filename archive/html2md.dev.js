/* html2md / (c) 2014 Beneath the Ink, Inc. / MIT License / Version 0.1.1 */
(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.html2md = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * # HTML2MD
 * HTML2MD attempts to convert HTML into Markdown by reducing an HTML document into simple, Markdown-compatible parts. This library is compatible with both browsers and Node.js.
 *
 * To use, pass a string of HTML to the function.
 * 
 * ```javascript
 * var markdown = html2md("<h1>Hello World</h1>");
 * console.log(markdown); // -> # Hello World
 * ```
 */

var _ = require("underscore"),
	Entities = require("special-entities"),
	DOMUtils = require("bti-dom-utils");

function html2md(doc, options) {
	return html2md.toMarkdown(html2md.parse(doc, options));
}

module.exports = html2md;

var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;

var block_elements = [ "article", "aside", "blockquote", "body", "button", "canvas", "caption", "col", "colgroup", "dd", "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", "li", "map", "object", "ol", "output", "p", "pre", "progress", "section", "table", "tbody", "textarea", "tfoot", "th", "thead", "tr", "ul", "video" ];

var empty_tags = ([ "hr", "br", "img", "video", "audio" ]).join(", ");

var markdown_block_tags = [ "blockquote", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "li", "pre", "p" ];

var markdown_empty_blocks = [ "hr" ];

var markdown_inline_tags = {
	"b, strong": "bold",
	"i, em": "italic",
	"code": "code"
};

var markdown_syntax = {
	hr:         "- - -",
	br:         "  \n",
	h1:         "# ",
	h2:         "## ",
	h3:         "### ",
	h4:         "#### ",
	h5:         "##### ",
	h6:         "###### ",
	ul:         "* ",
	ol:         "1. ",
	blockquote: "> ",
	pre:        "  ",
	p:          "",
	bold:       "**",
	italic:     "_",
	code:       "`"
}

/**
 * ## parse()
 *
 * This is where the magic happens. This method takes a string of HTML (or an HTML Document) and returns a Markdown abstract syntax tree.
 * 
 * #### Arguments
 * 
 * - **html** _string | Document_ - A string of HTML or a Document instance provided by the DOM.
 * - **options** _object; optional_ - An object of options.
 *   - **options.window** _object_ - The window object to use while parsing. This is to gain access to some global methods needed while parsing. Usually this is not needed because its value can be inferred.
 */
html2md.parse = function(doc, options) {
	options = options || {};

	if (typeof doc === "string") {
		doc = html2md.toDOM(doc);
	}

	var win = options.window ? options.window :
		typeof window !== "undefined" ? window :
		doc.parentWindow || doc.defaultView;

	if (win == null) {
		throw new Error("Missing window reference.");
	}

	if (typeof DOMUtils == "function") {
		var utils = DOMUtils(win);
	} else { 
		var utils = DOMUtils;
	}
	/*
	 * #### STEP 1: Convert HTML into a set of basic blocks.
	 *
	 * Markdown organizes a document into a set of blocks and unlike HTML
	 * blocks, these cannot contain other blocks, only inline elements. This is
	 * accomplished by reducing the HTML to its smallest block parts, with the
	 * assumption that block elements usually define separate bodies of
	 * information within an HTML document.
	 *
	 * Each block is given a type based on the Markdown types. This type is
	 * determined from the closest ancestor with one of the following tags:
	 * `blockquote`, `pre`, `li`, `hr`, `h1-6`. All other blocks become
	 * paragraphs.
	 *
	 * This operates on a few assumptions, which outline its limitations:
	 *   - Inline elements do not contain block elements.
	 *   - Standard HTML block elements are used to define and separate content.
	 */

	var blocks = compactBlocks(extractBlocks(doc.body));
	
	function extractBlocks(node) {
		var currentBlock, blocks;

		blocks = [];
		extract(node);
		return blocks;

		function addInline(el) {
			if (currentBlock != null) {
				currentBlock.nodes.push(el);
			} else {
				blocks.push({ type: "p", nodes: [ el ] });
			}

			return blocks;
		}

		function extract(el) {
			if (el.nodeType !== 1) return addInline(el);

			var tag = el.tagName.toLowerCase();
			if (!_.contains(block_elements, tag)) return addInline(el);

			// remove the current block if it's empty
			if (currentBlock != null &&
				!currentBlock.nodes.length &&
				!_.contains(markdown_empty_blocks, currentBlock.type) &&
				_.last(blocks) === currentBlock) blocks.pop();

			// add a new block
			blocks.push(currentBlock = {
				type: _.contains(markdown_block_tags, tag) ? tag : "p",
				nodes: []
			});

			// process children
			_.each(el.childNodes, function(child) {
				extract(child);
			});

			// reset current block
			currentBlock = null;
		}
	}

	function compactBlocks(blocks) {
		return blocks.filter(function(b) {
			var emptyBlock = _.contains(markdown_empty_blocks, b.type);

			// delete nodes array if this is an empty block
			if (emptyBlock) delete b.nodes;

			// make sure the block isn't empty
			return emptyBlock || _.some(b.nodes, function(n) {
				return utils.getTextContent(n).trim() != "" ||
					(n.nodeType === 1 && (utils.matchesSelector(n, empty_tags) || n.querySelector(empty_tags)));
			});
		});
	}

	/*
	 * #### STEP 2: Convert inline HTML into inline Markdown.
	 *
	 * Basically we push each text node onto a stack, accounting for specific
	 * styling like italics and bold. Other inline elements like `br` and `img`
	 * are preserved, but everything else is thrown out.
	 */

	blocks.forEach(function(b) {
		if (_.contains(markdown_empty_blocks, b.type)) return;
		
		b.content = cleanInlines(b.nodes.reduce(function(m, n) {
			return extractInlines(n, m);
		}, []));
	});

	function extractInlines(el, inlines) {
		var lastInline, styles, content;

		if (inlines == null) inlines = [];

		switch (el.nodeType) {
			case 1:
				switch (el.tagName.toLowerCase()) {
					case "br":
						inlines.push({ type: "br" });
						break;

					case "img":
						inlines.push({
							type: "img",
							src: el.getAttribute("src")
						});
						break;

					default:
						_.each(el.childNodes, function(n) {
							extractInlines(n, inlines);
						});
						break;
				}

				break;

			case 3:
				lastInline = _.last(inlines);
				content = Entities.normalizeXML(utils.getTextContent(el), "html");
				styles = _.filter(markdown_inline_tags, function(s, sel) {
					return !!closest(el, sel);
				});

				if (lastInline && lastInline.content != null && _.isEqual(lastInline.styles, styles)) {
					lastInline.content += content;
				} else {
					inlines.push({
						type: "text",
						content: content,
						styles: styles
					});
				}

				break;
		}

		return inlines;
	}

	function cleanInlines(inlines) {
		// clean up whitespace and drop empty inlines
		inlines = inlines.reduce(function(m, inline, i) {
			if (inline.type !== "text") m.push(inline);
			else {
				var prev = i > 0 ? inlines[i-1] : null;

				// reduce multiple spaces to one
				inline.content = inline.content.replace(/\s+/g, " ");

				// remove leading space if previous inline has trailing space
				if (inline.content[0] === " " && prev && (
					prev.type === "br" || (
					prev.type === "text" &&
					prev.content[prev.content.length - 1] === " ")
				)) {
					inline.content = inline.content.substr(1);
				}

				// only add if this has real content
				if (inline.content) m.push(inline);
			}

			return m;
		}, []);

		// trim leading whitespace
		while (inlines.length && inlines[0].type === "text") {
			inlines[0].content = inlines[0].content.replace(/^\s+/, "");
			if (inlines[0].content) break;
			inlines.shift();
		}

		// trim trailing whitespace
		var lastInline;
		while (inlines.length && (lastInline = _.last(inlines)).type === "text") {
			lastInline.content = lastInline.content.replace(/\s+$/, "");
			if (lastInline.content) break;
			inlines.pop();
		}

		return inlines;
	}

	function closest(el, selector) {
		while (el != null) {
			if (el.nodeType === 1 && utils.matchesSelector(el, selector)) return el;
			el = el.parentNode;
		}

		return null;
	}

	/*
	 * #### STEP 3: Clean up
	 *
	 * The last step is to clean up the resulting AST before returning it.
	 */

	// cannot be empty unless otherwise specified
	blocks = blocks.filter(function(b) {
		return _.contains(markdown_empty_blocks, b.type) || b.content.length;
	});

	// remove DOM nodes reference to keep it clean
	blocks.forEach(function(b) { delete b.nodes; });

	return blocks;
}

/**
 * ## toMarkdown()
 *
 * This methods converts the output of `.parse()` into a string of Markdown.
 * 
 * #### Arguments
 * 
 * - **tree** _object_ - A Markdown AST object returned from `.parse()`.
 */
html2md.toMarkdown = function(tree) {
	return tree.map(function(block) {
		var activeStyles = [],
			content = "";

		if (block.content != null) {
			block.content.forEach(function(inline) {
				switch (inline.type) {
					case "text":
						updateStyles(inline.styles);
						content += inline.content;
						break;

					case "br":
						content += markdown_syntax.br;
						break;

					case "img":
						content += "![](" + inline.src + ")";
						break;
				}
			});
		
			updateStyles();
		}

		switch (block.type) {
			case "blockquote":
			case "pre":
			case "p":
			case "hr":
			case "h1":
			case "h2":
			case "h3":
			case "h4":
			case "h5":
			case "h6":
				return (markdown_syntax[block.type] || "") + content;
		}

		function updateStyles(styles) {
			if (styles == null) styles = [];

			// close active styles
			var close = _.difference(activeStyles, styles);
			activeStyles = _.without.apply(_, [ activeStyles ].concat(close));
			close.reverse().forEach(function(style) {
				content += markdown_syntax[style] || "";
			});

			// open new styles
			_.difference(styles, activeStyles).forEach(function(style) {
				activeStyles.push(style);
				content += markdown_syntax[style] || "";
			});
		}
	}).join("\n\n");	
}

/**
 * ## toDOM()
 *
 * A small utility that takes a string of HTML and returns a new HTMLDocument instance. In Node.js, `jsdom` is used to simulate the DOM.
 * 
 * #### Arguments
 * 
 * - **html** _string_ - A string of HTML.
 */
html2md.toDOM = function(html) {
	var doc;

	// clean html before we parse
	html = html.replace(SCRIPT_REGEX, '');
	html = Entities.normalizeXML(html, 'xhtml');

	// browsers
	if (typeof window !== "undefined" && window.document) {
		var doc = window.document.implementation.createHTMLDocument();
		doc.documentElement.innerHTML = html;
	}

	// nodejs
	//else {
	//	doc = require("jsdom").jsdom(html);
	//}

	return doc;
}

},{"bti-dom-utils":2,"special-entities":4,"underscore":5}],2:[function(require,module,exports){
(function (global){
/**
 * # DOM Utilities
 *
 * This is a collection of common utility methods for the DOM. While similar in nature to libraries like jQuery, this library aims to provide methods for unique and odd features.
 */

var Node = global.Node;
var Element = global.Element;

/**
 * ## isNode()
 *
 * Determines if a value is a DOM node.
 *
 * #### Arguments
 *
 * - **value** _mixed_ - A value to test as a DOM node.
 */
var isNode =
exports.isNode = function(node) {
	return node != null && typeof node.nodeType === "number";
}

/**
 * ## matchesSelector()
 *
 * A cross browser compatible solution to testing a DOM element against a CSS selector.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to test.
 * - **selector** _string_ - A CSS selector.
 */
var matchesSelector = typeof Element !== "undefined" ?
	Element.prototype.matches ||
	Element.prototype.webkitMatchesSelector ||
	Element.prototype.mozMatchesSelector ||
	Element.prototype.msMatchesSelector :
	function() { return false; };

exports.matchesSelector = function(node, selector) {
	return matchesSelector.call(node, selector)
}

/**
 * ## matches()
 *
 * Similar to `matchesSelector()`, this method will test a DOM node against CSS selectors, other DOM nodes and functions.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to test.
 * - **selector** _string | function | Node | Array_ - A CSS selector, a function (called with one argument, the node) or a DOM node. Pass an array of selectors to match any of them.
 */
var matches =
exports.matches = function(node, selector) {
	if (Array.isArray(selector)) return selector.some(function(s) {
		return matches(node, s);
	});

	if (isNode(selector)) {
		return node === selector;
	}
	
	if (typeof selector === "function") {
		return !!selector(node);
	}
	
	if (node.nodeType === Node.ELEMENT_NODE) {
		return matchesSelector.call(node, selector);
	}

	return false;
};

/**
 * ## closest()
 *
 * Stating at `elem`, this method traverses up the parent nodes and returns the first one that matches `selector`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to test.
 * - **selector** _string | function | Node_ - A CSS selector, a function (called with one argument, the node) or a DOM node.
 */
exports.closest = function(elem, selector) {
	while (elem != null) {
		if (elem.nodeType === 1 && matches(elem, selector)) return elem;
		elem = elem.parentNode;
	}

	return null;
};

/**
 * ## requestAnimationFrame()
 *
 * A cross-browser requestAnimationFrame. Fall back on `setTimeout()` if requestAnimationFrame isn't defined.
 *
 * #### Arguments
 *
 * - **fn** _function_ - A funciton to call on the next animation frame.
 */
exports.requestAnimationFrame =
	window.requestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	function (f) { setTimeout(f, 16); };

/**
 * ## getFirstLeafNode()
 *
 * Returns the first descendant node without children or `null` if doesn't exist.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to find the first leaf of.
 */
var getFirstLeafNode =
exports.getFirstLeafNode = function(node) {
	while (node.hasChildNodes()) node = node.firstChild;
	return node;
}

/**
 * ## getLastLeafNode()
 *
 * Returns the last descendant node without children or `null` if doesn't exist.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to find the last leaf of.
 */
var getLastLeafNode =
exports.getLastLeafNode = function(node) {
	while (node.hasChildNodes()) node = node.lastChild;
	return node;
}

/**
 * ## getNextExtendedSibling()
 *
 * Returns the next sibling of this node, a direct ancestor node's next sibling, or `null`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the next extended sibling of.
 */
var getNextExtendedSibling =
exports.getNextExtendedSibling = function(node) {
	while (node != null) {
		if (node.nextSibling != null) return node.nextSibling;
		node = node.parentNode;
	}

	return null;
}

/**
 * ## getPreviousExtendedSibling()
 *
 * Returns the previous sibling of this node, a direct ancestor node's previous sibling, or `null`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the previous extended sibling of.
 */
var getPreviousExtendedSibling =
exports.getPreviousExtendedSibling = function(node) {
	while (node != null) {
		if (node.previousSibling != null) return node.previousSibling;
		node = node.parentNode;
	}

	return null;
}

/**
 * ## getNextNode()
 *
 * Gets the next node in the DOM tree. This is either the first child node, the next sibling node, a direct ancestor node's next sibling, or `null`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the next node of.
 */
var getNextNode =
exports.getNextNode = function(node) {
	return node.hasChildNodes() ? node.firstChild : getNextExtendedSibling(node);
}

/**
 * ## getPreviousNode()
 *
 * Gets the previous node in the DOM tree. This will return the previous extended sibling's last, deepest leaf node or `null` if doesn't exist. This returns the exact opposite result of `getNextNode` (ie `getNextNode(getPreviousNode(node)) === node`).
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the previous node of.
 */
var getPreviousNode =
exports.getPreviousNode = function(node) {
	return node.previousSibling == null ? node.parentNode : getLastLeafNode(node.previousSibling);
}

/**
 * ## getTextContent()
 *
 * Gets the text content of a node and its descendants. This is the text content that is visible to a user viewing the HTML from browser. Hidden nodes, such as comments, are not included in the output.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the text content of.
 */
var getTextContent =
exports.getTextContent = function(node) {
	if (Array.isArray(node)) return node.map(getTextContent).join("");

	switch(node.nodeType) {
		case Node.DOCUMENT_NODE:
		case Node.DOCUMENT_FRAGMENT_NODE:
			return getTextContent(Array.prototype.slice.call(node.childNodes, 0));

		case Node.ELEMENT_NODE:
			if (typeof node.innerText === "string") return node.innerText;		// webkit
			if (typeof node.textContent === "string") return node.textContent;	// firefox
			return getTextContent(Array.prototype.slice.call(node.childNodes, 0));// other
		
		case Node.TEXT_NODE:
			return node.nodeValue || "";

		default:
			return "";
	}
}

/**
 * ## getRootNode()
 *
 * Returns the root node of a DOM tree.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node in the DOM tree you need the root of.
 */
var getRootNode =
exports.getRootNode = function(node) {
	while (node.parentNode != null) {
		node = node.parentNode
	}
	
	return node;
}

/**
 * ## contains()
 *
 * Determines if a node is a direct ancestor of another node. This is the same syntax as jQuery's `$.contains()`.
 *
 * #### Arguments
 *
 * - **parent** _Node_ - The ancestor node.
 * - **node** _Node_ - The node which may or may not be a descendant of the parent.
 */
var contains =
exports.contains = function(parent, node) {
	while (node != null) {
		if (matches(node, parent)) return true;
		node = node.parentNode;
	}
	
	return false;
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],3:[function(require,module,exports){
module.exports={
	"Aacute": [ 193 ],
	"aacute": [ 225 ],
	"Abreve": [ 258 ],
	"abreve": [ 259 ],
	"ac": [ 8766 ],
	"acd": [ 8767 ],
	"acE": [ 8766, 819 ],
	"Acirc": [ 194 ],
	"acirc": [ 226 ],
	"acute": [ 180 ],
	"Acy": [ 1040 ],
	"acy": [ 1072 ],
	"AElig": [ 198 ],
	"aelig": [ 230 ],
	"af": [ 8289 ],
	"Afr": [ 120068 ],
	"afr": [ 120094 ],
	"Agrave": [ 192 ],
	"agrave": [ 224 ],
	"alefsym": [ 8501 ],
	"aleph": [ 8501 ],
	"Alpha": [ 913 ],
	"alpha": [ 945 ],
	"Amacr": [ 256 ],
	"amacr": [ 257 ],
	"amalg": [ 10815 ],
	"AMP": [ 38 ],
	"amp": [ 38 ],
	"And": [ 10835 ],
	"and": [ 8743 ],
	"andand": [ 10837 ],
	"andd": [ 10844 ],
	"andslope": [ 10840 ],
	"andv": [ 10842 ],
	"ang": [ 8736 ],
	"ange": [ 10660 ],
	"angle": [ 8736 ],
	"angmsd": [ 8737 ],
	"angmsdaa": [ 10664 ],
	"angmsdab": [ 10665 ],
	"angmsdac": [ 10666 ],
	"angmsdad": [ 10667 ],
	"angmsdae": [ 10668 ],
	"angmsdaf": [ 10669 ],
	"angmsdag": [ 10670 ],
	"angmsdah": [ 10671 ],
	"angrt": [ 8735 ],
	"angrtvb": [ 8894 ],
	"angrtvbd": [ 10653 ],
	"angsph": [ 8738 ],
	"angst": [ 197 ],
	"angzarr": [ 9084 ],
	"Aogon": [ 260 ],
	"aogon": [ 261 ],
	"Aopf": [ 120120 ],
	"aopf": [ 120146 ],
	"ap": [ 8776 ],
	"apacir": [ 10863 ],
	"apE": [ 10864 ],
	"ape": [ 8778 ],
	"apid": [ 8779 ],
	"apos": [ 39 ],
	"ApplyFunction": [ 8289 ],
	"approx": [ 8776 ],
	"approxeq": [ 8778 ],
	"Aring": [ 197 ],
	"aring": [ 229 ],
	"Ascr": [ 119964 ],
	"ascr": [ 119990 ],
	"Assign": [ 8788 ],
	"ast": [ 42 ],
	"asymp": [ 8776 ],
	"asympeq": [ 8781 ],
	"Atilde": [ 195 ],
	"atilde": [ 227 ],
	"Auml": [ 196 ],
	"auml": [ 228 ],
	"awconint": [ 8755 ],
	"awint": [ 10769 ],
	"backcong": [ 8780 ],
	"backepsilon": [ 1014 ],
	"backprime": [ 8245 ],
	"backsim": [ 8765 ],
	"backsimeq": [ 8909 ],
	"Backslash": [ 8726 ],
	"Barv": [ 10983 ],
	"barvee": [ 8893 ],
	"Barwed": [ 8966 ],
	"barwed": [ 8965 ],
	"barwedge": [ 8965 ],
	"bbrk": [ 9141 ],
	"bbrktbrk": [ 9142 ],
	"bcong": [ 8780 ],
	"Bcy": [ 1041 ],
	"bcy": [ 1073 ],
	"bdquo": [ 8222 ],
	"becaus": [ 8757 ],
	"Because": [ 8757 ],
	"because": [ 8757 ],
	"bemptyv": [ 10672 ],
	"bepsi": [ 1014 ],
	"bernou": [ 8492 ],
	"Bernoullis": [ 8492 ],
	"Beta": [ 914 ],
	"beta": [ 946 ],
	"beth": [ 8502 ],
	"between": [ 8812 ],
	"Bfr": [ 120069 ],
	"bfr": [ 120095 ],
	"bigcap": [ 8898 ],
	"bigcirc": [ 9711 ],
	"bigcup": [ 8899 ],
	"bigodot": [ 10752 ],
	"bigoplus": [ 10753 ],
	"bigotimes": [ 10754 ],
	"bigsqcup": [ 10758 ],
	"bigstar": [ 9733 ],
	"bigtriangledown": [ 9661 ],
	"bigtriangleup": [ 9651 ],
	"biguplus": [ 10756 ],
	"bigvee": [ 8897 ],
	"bigwedge": [ 8896 ],
	"bkarow": [ 10509 ],
	"blacklozenge": [ 10731 ],
	"blacksquare": [ 9642 ],
	"blacktriangle": [ 9652 ],
	"blacktriangledown": [ 9662 ],
	"blacktriangleleft": [ 9666 ],
	"blacktriangleright": [ 9656 ],
	"blank": [ 9251 ],
	"blk12": [ 9618 ],
	"blk14": [ 9617 ],
	"blk34": [ 9619 ],
	"block": [ 9608 ],
	"bne": [ 61, 8421 ],
	"bnequiv": [ 8801, 8421 ],
	"bNot": [ 10989 ],
	"bnot": [ 8976 ],
	"Bopf": [ 120121 ],
	"bopf": [ 120147 ],
	"bot": [ 8869 ],
	"bottom": [ 8869 ],
	"bowtie": [ 8904 ],
	"boxbox": [ 10697 ],
	"boxDL": [ 9559 ],
	"boxDl": [ 9558 ],
	"boxdL": [ 9557 ],
	"boxdl": [ 9488 ],
	"boxDR": [ 9556 ],
	"boxDr": [ 9555 ],
	"boxdR": [ 9554 ],
	"boxdr": [ 9484 ],
	"boxH": [ 9552 ],
	"boxh": [ 9472 ],
	"boxHD": [ 9574 ],
	"boxHd": [ 9572 ],
	"boxhD": [ 9573 ],
	"boxhd": [ 9516 ],
	"boxHU": [ 9577 ],
	"boxHu": [ 9575 ],
	"boxhU": [ 9576 ],
	"boxhu": [ 9524 ],
	"boxminus": [ 8863 ],
	"boxplus": [ 8862 ],
	"boxtimes": [ 8864 ],
	"boxUL": [ 9565 ],
	"boxUl": [ 9564 ],
	"boxuL": [ 9563 ],
	"boxul": [ 9496 ],
	"boxUR": [ 9562 ],
	"boxUr": [ 9561 ],
	"boxuR": [ 9560 ],
	"boxur": [ 9492 ],
	"boxV": [ 9553 ],
	"boxv": [ 9474 ],
	"boxVH": [ 9580 ],
	"boxVh": [ 9579 ],
	"boxvH": [ 9578 ],
	"boxvh": [ 9532 ],
	"boxVL": [ 9571 ],
	"boxVl": [ 9570 ],
	"boxvL": [ 9569 ],
	"boxvl": [ 9508 ],
	"boxVR": [ 9568 ],
	"boxVr": [ 9567 ],
	"boxvR": [ 9566 ],
	"boxvr": [ 9500 ],
	"bprime": [ 8245 ],
	"Breve": [ 728 ],
	"breve": [ 728 ],
	"brvbar": [ 166 ],
	"Bscr": [ 8492 ],
	"bscr": [ 119991 ],
	"bsemi": [ 8271 ],
	"bsim": [ 8765 ],
	"bsime": [ 8909 ],
	"bsol": [ 92 ],
	"bsolb": [ 10693 ],
	"bsolhsub": [ 10184 ],
	"bull": [ 8226 ],
	"bullet": [ 8226 ],
	"bump": [ 8782 ],
	"bumpE": [ 10926 ],
	"bumpe": [ 8783 ],
	"Bumpeq": [ 8782 ],
	"bumpeq": [ 8783 ],
	"Cacute": [ 262 ],
	"cacute": [ 263 ],
	"Cap": [ 8914 ],
	"cap": [ 8745 ],
	"capand": [ 10820 ],
	"capbrcup": [ 10825 ],
	"capcap": [ 10827 ],
	"capcup": [ 10823 ],
	"capdot": [ 10816 ],
	"CapitalDifferentialD": [ 8517 ],
	"caps": [ 8745, 65024 ],
	"caret": [ 8257 ],
	"caron": [ 711 ],
	"Cayleys": [ 8493 ],
	"ccaps": [ 10829 ],
	"Ccaron": [ 268 ],
	"ccaron": [ 269 ],
	"Ccedil": [ 199 ],
	"ccedil": [ 231 ],
	"Ccirc": [ 264 ],
	"ccirc": [ 265 ],
	"Cconint": [ 8752 ],
	"ccups": [ 10828 ],
	"ccupssm": [ 10832 ],
	"Cdot": [ 266 ],
	"cdot": [ 267 ],
	"cedil": [ 184 ],
	"Cedilla": [ 184 ],
	"cemptyv": [ 10674 ],
	"cent": [ 162 ],
	"CenterDot": [ 183 ],
	"centerdot": [ 183 ],
	"Cfr": [ 8493 ],
	"cfr": [ 120096 ],
	"CHcy": [ 1063 ],
	"chcy": [ 1095 ],
	"check": [ 10003 ],
	"checkmark": [ 10003 ],
	"Chi": [ 935 ],
	"chi": [ 967 ],
	"cir": [ 9675 ],
	"circ": [ 710 ],
	"circeq": [ 8791 ],
	"circlearrowleft": [ 8634 ],
	"circlearrowright": [ 8635 ],
	"circledast": [ 8859 ],
	"circledcirc": [ 8858 ],
	"circleddash": [ 8861 ],
	"CircleDot": [ 8857 ],
	"circledR": [ 174 ],
	"circledS": [ 9416 ],
	"CircleMinus": [ 8854 ],
	"CirclePlus": [ 8853 ],
	"CircleTimes": [ 8855 ],
	"cirE": [ 10691 ],
	"cire": [ 8791 ],
	"cirfnint": [ 10768 ],
	"cirmid": [ 10991 ],
	"cirscir": [ 10690 ],
	"ClockwiseContourIntegral": [ 8754 ],
	"CloseCurlyDoubleQuote": [ 8221 ],
	"CloseCurlyQuote": [ 8217 ],
	"clubs": [ 9827 ],
	"clubsuit": [ 9827 ],
	"Colon": [ 8759 ],
	"colon": [ 58 ],
	"Colone": [ 10868 ],
	"colone": [ 8788 ],
	"coloneq": [ 8788 ],
	"comma": [ 44 ],
	"commat": [ 64 ],
	"comp": [ 8705 ],
	"compfn": [ 8728 ],
	"complement": [ 8705 ],
	"complexes": [ 8450 ],
	"cong": [ 8773 ],
	"congdot": [ 10861 ],
	"Congruent": [ 8801 ],
	"Conint": [ 8751 ],
	"conint": [ 8750 ],
	"ContourIntegral": [ 8750 ],
	"Copf": [ 8450 ],
	"copf": [ 120148 ],
	"coprod": [ 8720 ],
	"Coproduct": [ 8720 ],
	"COPY": [ 169 ],
	"copy": [ 169 ],
	"copysr": [ 8471 ],
	"CounterClockwiseContourIntegral": [ 8755 ],
	"crarr": [ 8629 ],
	"Cross": [ 10799 ],
	"cross": [ 10007 ],
	"Cscr": [ 119966 ],
	"cscr": [ 119992 ],
	"csub": [ 10959 ],
	"csube": [ 10961 ],
	"csup": [ 10960 ],
	"csupe": [ 10962 ],
	"ctdot": [ 8943 ],
	"cudarrl": [ 10552 ],
	"cudarrr": [ 10549 ],
	"cuepr": [ 8926 ],
	"cuesc": [ 8927 ],
	"cularr": [ 8630 ],
	"cularrp": [ 10557 ],
	"Cup": [ 8915 ],
	"cup": [ 8746 ],
	"cupbrcap": [ 10824 ],
	"CupCap": [ 8781 ],
	"cupcap": [ 10822 ],
	"cupcup": [ 10826 ],
	"cupdot": [ 8845 ],
	"cupor": [ 10821 ],
	"cups": [ 8746, 65024 ],
	"curarr": [ 8631 ],
	"curarrm": [ 10556 ],
	"curlyeqprec": [ 8926 ],
	"curlyeqsucc": [ 8927 ],
	"curlyvee": [ 8910 ],
	"curlywedge": [ 8911 ],
	"curren": [ 164 ],
	"curvearrowleft": [ 8630 ],
	"curvearrowright": [ 8631 ],
	"cuvee": [ 8910 ],
	"cuwed": [ 8911 ],
	"cwconint": [ 8754 ],
	"cwint": [ 8753 ],
	"cylcty": [ 9005 ],
	"Dagger": [ 8225 ],
	"dagger": [ 8224 ],
	"daleth": [ 8504 ],
	"Darr": [ 8609 ],
	"dArr": [ 8659 ],
	"darr": [ 8595 ],
	"dash": [ 8208 ],
	"Dashv": [ 10980 ],
	"dashv": [ 8867 ],
	"dbkarow": [ 10511 ],
	"dblac": [ 733 ],
	"Dcaron": [ 270 ],
	"dcaron": [ 271 ],
	"Dcy": [ 1044 ],
	"dcy": [ 1076 ],
	"DD": [ 8517 ],
	"dd": [ 8518 ],
	"ddagger": [ 8225 ],
	"ddarr": [ 8650 ],
	"DDotrahd": [ 10513 ],
	"ddotseq": [ 10871 ],
	"deg": [ 176 ],
	"Del": [ 8711 ],
	"Delta": [ 916 ],
	"delta": [ 948 ],
	"demptyv": [ 10673 ],
	"dfisht": [ 10623 ],
	"Dfr": [ 120071 ],
	"dfr": [ 120097 ],
	"dHar": [ 10597 ],
	"dharl": [ 8643 ],
	"dharr": [ 8642 ],
	"DiacriticalAcute": [ 180 ],
	"DiacriticalDot": [ 729 ],
	"DiacriticalDoubleAcute": [ 733 ],
	"DiacriticalGrave": [ 96 ],
	"DiacriticalTilde": [ 732 ],
	"diam": [ 8900 ],
	"Diamond": [ 8900 ],
	"diamond": [ 8900 ],
	"diamondsuit": [ 9830 ],
	"diams": [ 9830 ],
	"die": [ 168 ],
	"DifferentialD": [ 8518 ],
	"digamma": [ 989 ],
	"disin": [ 8946 ],
	"div": [ 247 ],
	"divide": [ 247 ],
	"divideontimes": [ 8903 ],
	"divonx": [ 8903 ],
	"DJcy": [ 1026 ],
	"djcy": [ 1106 ],
	"dlcorn": [ 8990 ],
	"dlcrop": [ 8973 ],
	"dollar": [ 36 ],
	"Dopf": [ 120123 ],
	"dopf": [ 120149 ],
	"Dot": [ 168 ],
	"dot": [ 729 ],
	"DotDot": [ 8412 ],
	"doteq": [ 8784 ],
	"doteqdot": [ 8785 ],
	"DotEqual": [ 8784 ],
	"dotminus": [ 8760 ],
	"dotplus": [ 8724 ],
	"dotsquare": [ 8865 ],
	"doublebarwedge": [ 8966 ],
	"DoubleContourIntegral": [ 8751 ],
	"DoubleDot": [ 168 ],
	"DoubleDownArrow": [ 8659 ],
	"DoubleLeftArrow": [ 8656 ],
	"DoubleLeftRightArrow": [ 8660 ],
	"DoubleLeftTee": [ 10980 ],
	"DoubleLongLeftArrow": [ 10232 ],
	"DoubleLongLeftRightArrow": [ 10234 ],
	"DoubleLongRightArrow": [ 10233 ],
	"DoubleRightArrow": [ 8658 ],
	"DoubleRightTee": [ 8872 ],
	"DoubleUpArrow": [ 8657 ],
	"DoubleUpDownArrow": [ 8661 ],
	"DoubleVerticalBar": [ 8741 ],
	"DownArrow": [ 8595 ],
	"Downarrow": [ 8659 ],
	"downarrow": [ 8595 ],
	"DownArrowBar": [ 10515 ],
	"DownArrowUpArrow": [ 8693 ],
	"DownBreve": [ 785 ],
	"downdownarrows": [ 8650 ],
	"downharpoonleft": [ 8643 ],
	"downharpoonright": [ 8642 ],
	"DownLeftRightVector": [ 10576 ],
	"DownLeftTeeVector": [ 10590 ],
	"DownLeftVector": [ 8637 ],
	"DownLeftVectorBar": [ 10582 ],
	"DownRightTeeVector": [ 10591 ],
	"DownRightVector": [ 8641 ],
	"DownRightVectorBar": [ 10583 ],
	"DownTee": [ 8868 ],
	"DownTeeArrow": [ 8615 ],
	"drbkarow": [ 10512 ],
	"drcorn": [ 8991 ],
	"drcrop": [ 8972 ],
	"Dscr": [ 119967 ],
	"dscr": [ 119993 ],
	"DScy": [ 1029 ],
	"dscy": [ 1109 ],
	"dsol": [ 10742 ],
	"Dstrok": [ 272 ],
	"dstrok": [ 273 ],
	"dtdot": [ 8945 ],
	"dtri": [ 9663 ],
	"dtrif": [ 9662 ],
	"duarr": [ 8693 ],
	"duhar": [ 10607 ],
	"dwangle": [ 10662 ],
	"DZcy": [ 1039 ],
	"dzcy": [ 1119 ],
	"dzigrarr": [ 10239 ],
	"Eacute": [ 201 ],
	"eacute": [ 233 ],
	"easter": [ 10862 ],
	"Ecaron": [ 282 ],
	"ecaron": [ 283 ],
	"ecir": [ 8790 ],
	"Ecirc": [ 202 ],
	"ecirc": [ 234 ],
	"ecolon": [ 8789 ],
	"Ecy": [ 1069 ],
	"ecy": [ 1101 ],
	"eDDot": [ 10871 ],
	"Edot": [ 278 ],
	"eDot": [ 8785 ],
	"edot": [ 279 ],
	"ee": [ 8519 ],
	"efDot": [ 8786 ],
	"Efr": [ 120072 ],
	"efr": [ 120098 ],
	"eg": [ 10906 ],
	"Egrave": [ 200 ],
	"egrave": [ 232 ],
	"egs": [ 10902 ],
	"egsdot": [ 10904 ],
	"el": [ 10905 ],
	"Element": [ 8712 ],
	"elinters": [ 9191 ],
	"ell": [ 8467 ],
	"els": [ 10901 ],
	"elsdot": [ 10903 ],
	"Emacr": [ 274 ],
	"emacr": [ 275 ],
	"empty": [ 8709 ],
	"emptyset": [ 8709 ],
	"EmptySmallSquare": [ 9723 ],
	"emptyv": [ 8709 ],
	"EmptyVerySmallSquare": [ 9643 ],
	"emsp": [ 8195 ],
	"emsp13": [ 8196 ],
	"emsp14": [ 8197 ],
	"ENG": [ 330 ],
	"eng": [ 331 ],
	"ensp": [ 8194 ],
	"Eogon": [ 280 ],
	"eogon": [ 281 ],
	"Eopf": [ 120124 ],
	"eopf": [ 120150 ],
	"epar": [ 8917 ],
	"eparsl": [ 10723 ],
	"eplus": [ 10865 ],
	"epsi": [ 949 ],
	"Epsilon": [ 917 ],
	"epsilon": [ 949 ],
	"epsiv": [ 1013 ],
	"eqcirc": [ 8790 ],
	"eqcolon": [ 8789 ],
	"eqsim": [ 8770 ],
	"eqslantgtr": [ 10902 ],
	"eqslantless": [ 10901 ],
	"Equal": [ 10869 ],
	"equals": [ 61 ],
	"EqualTilde": [ 8770 ],
	"equest": [ 8799 ],
	"Equilibrium": [ 8652 ],
	"equiv": [ 8801 ],
	"equivDD": [ 10872 ],
	"eqvparsl": [ 10725 ],
	"erarr": [ 10609 ],
	"erDot": [ 8787 ],
	"Escr": [ 8496 ],
	"escr": [ 8495 ],
	"esdot": [ 8784 ],
	"Esim": [ 10867 ],
	"esim": [ 8770 ],
	"Eta": [ 919 ],
	"eta": [ 951 ],
	"ETH": [ 208 ],
	"eth": [ 240 ],
	"Euml": [ 203 ],
	"euml": [ 235 ],
	"euro": [ 8364 ],
	"excl": [ 33 ],
	"exist": [ 8707 ],
	"Exists": [ 8707 ],
	"expectation": [ 8496 ],
	"ExponentialE": [ 8519 ],
	"exponentiale": [ 8519 ],
	"fallingdotseq": [ 8786 ],
	"Fcy": [ 1060 ],
	"fcy": [ 1092 ],
	"female": [ 9792 ],
	"ffilig": [ 64259 ],
	"fflig": [ 64256 ],
	"ffllig": [ 64260 ],
	"Ffr": [ 120073 ],
	"ffr": [ 120099 ],
	"filig": [ 64257 ],
	"FilledSmallSquare": [ 9724 ],
	"FilledVerySmallSquare": [ 9642 ],
	"fjlig": [ 102, 106 ],
	"flat": [ 9837 ],
	"fllig": [ 64258 ],
	"fltns": [ 9649 ],
	"fnof": [ 402 ],
	"Fopf": [ 120125 ],
	"fopf": [ 120151 ],
	"ForAll": [ 8704 ],
	"forall": [ 8704 ],
	"fork": [ 8916 ],
	"forkv": [ 10969 ],
	"Fouriertrf": [ 8497 ],
	"fpartint": [ 10765 ],
	"frac12": [ 189 ],
	"frac13": [ 8531 ],
	"frac14": [ 188 ],
	"frac15": [ 8533 ],
	"frac16": [ 8537 ],
	"frac18": [ 8539 ],
	"frac23": [ 8532 ],
	"frac25": [ 8534 ],
	"frac34": [ 190 ],
	"frac35": [ 8535 ],
	"frac38": [ 8540 ],
	"frac45": [ 8536 ],
	"frac56": [ 8538 ],
	"frac58": [ 8541 ],
	"frac78": [ 8542 ],
	"frasl": [ 8260 ],
	"frown": [ 8994 ],
	"Fscr": [ 8497 ],
	"fscr": [ 119995 ],
	"gacute": [ 501 ],
	"Gamma": [ 915 ],
	"gamma": [ 947 ],
	"Gammad": [ 988 ],
	"gammad": [ 989 ],
	"gap": [ 10886 ],
	"Gbreve": [ 286 ],
	"gbreve": [ 287 ],
	"Gcedil": [ 290 ],
	"Gcirc": [ 284 ],
	"gcirc": [ 285 ],
	"Gcy": [ 1043 ],
	"gcy": [ 1075 ],
	"Gdot": [ 288 ],
	"gdot": [ 289 ],
	"gE": [ 8807 ],
	"ge": [ 8805 ],
	"gEl": [ 10892 ],
	"gel": [ 8923 ],
	"geq": [ 8805 ],
	"geqq": [ 8807 ],
	"geqslant": [ 10878 ],
	"ges": [ 10878 ],
	"gescc": [ 10921 ],
	"gesdot": [ 10880 ],
	"gesdoto": [ 10882 ],
	"gesdotol": [ 10884 ],
	"gesl": [ 8923, 65024 ],
	"gesles": [ 10900 ],
	"Gfr": [ 120074 ],
	"gfr": [ 120100 ],
	"Gg": [ 8921 ],
	"gg": [ 8811 ],
	"ggg": [ 8921 ],
	"gimel": [ 8503 ],
	"GJcy": [ 1027 ],
	"gjcy": [ 1107 ],
	"gl": [ 8823 ],
	"gla": [ 10917 ],
	"glE": [ 10898 ],
	"glj": [ 10916 ],
	"gnap": [ 10890 ],
	"gnapprox": [ 10890 ],
	"gnE": [ 8809 ],
	"gne": [ 10888 ],
	"gneq": [ 10888 ],
	"gneqq": [ 8809 ],
	"gnsim": [ 8935 ],
	"Gopf": [ 120126 ],
	"gopf": [ 120152 ],
	"grave": [ 96 ],
	"GreaterEqual": [ 8805 ],
	"GreaterEqualLess": [ 8923 ],
	"GreaterFullEqual": [ 8807 ],
	"GreaterGreater": [ 10914 ],
	"GreaterLess": [ 8823 ],
	"GreaterSlantEqual": [ 10878 ],
	"GreaterTilde": [ 8819 ],
	"Gscr": [ 119970 ],
	"gscr": [ 8458 ],
	"gsim": [ 8819 ],
	"gsime": [ 10894 ],
	"gsiml": [ 10896 ],
	"GT": [ 62 ],
	"Gt": [ 8811 ],
	"gt": [ 62 ],
	"gtcc": [ 10919 ],
	"gtcir": [ 10874 ],
	"gtdot": [ 8919 ],
	"gtlPar": [ 10645 ],
	"gtquest": [ 10876 ],
	"gtrapprox": [ 10886 ],
	"gtrarr": [ 10616 ],
	"gtrdot": [ 8919 ],
	"gtreqless": [ 8923 ],
	"gtreqqless": [ 10892 ],
	"gtrless": [ 8823 ],
	"gtrsim": [ 8819 ],
	"gvertneqq": [ 8809, 65024 ],
	"gvnE": [ 8809, 65024 ],
	"Hacek": [ 711 ],
	"hairsp": [ 8202 ],
	"half": [ 189 ],
	"hamilt": [ 8459 ],
	"HARDcy": [ 1066 ],
	"hardcy": [ 1098 ],
	"hArr": [ 8660 ],
	"harr": [ 8596 ],
	"harrcir": [ 10568 ],
	"harrw": [ 8621 ],
	"Hat": [ 94 ],
	"hbar": [ 8463 ],
	"Hcirc": [ 292 ],
	"hcirc": [ 293 ],
	"hearts": [ 9829 ],
	"heartsuit": [ 9829 ],
	"hellip": [ 8230 ],
	"hercon": [ 8889 ],
	"Hfr": [ 8460 ],
	"hfr": [ 120101 ],
	"HilbertSpace": [ 8459 ],
	"hksearow": [ 10533 ],
	"hkswarow": [ 10534 ],
	"hoarr": [ 8703 ],
	"homtht": [ 8763 ],
	"hookleftarrow": [ 8617 ],
	"hookrightarrow": [ 8618 ],
	"Hopf": [ 8461 ],
	"hopf": [ 120153 ],
	"horbar": [ 8213 ],
	"HorizontalLine": [ 9472 ],
	"Hscr": [ 8459 ],
	"hscr": [ 119997 ],
	"hslash": [ 8463 ],
	"Hstrok": [ 294 ],
	"hstrok": [ 295 ],
	"HumpDownHump": [ 8782 ],
	"HumpEqual": [ 8783 ],
	"hybull": [ 8259 ],
	"hyphen": [ 8208 ],
	"Iacute": [ 205 ],
	"iacute": [ 237 ],
	"ic": [ 8291 ],
	"Icirc": [ 206 ],
	"icirc": [ 238 ],
	"Icy": [ 1048 ],
	"icy": [ 1080 ],
	"Idot": [ 304 ],
	"IEcy": [ 1045 ],
	"iecy": [ 1077 ],
	"iexcl": [ 161 ],
	"iff": [ 8660 ],
	"Ifr": [ 8465 ],
	"ifr": [ 120102 ],
	"Igrave": [ 204 ],
	"igrave": [ 236 ],
	"ii": [ 8520 ],
	"iiiint": [ 10764 ],
	"iiint": [ 8749 ],
	"iinfin": [ 10716 ],
	"iiota": [ 8489 ],
	"IJlig": [ 306 ],
	"ijlig": [ 307 ],
	"Im": [ 8465 ],
	"Imacr": [ 298 ],
	"imacr": [ 299 ],
	"image": [ 8465 ],
	"ImaginaryI": [ 8520 ],
	"imagline": [ 8464 ],
	"imagpart": [ 8465 ],
	"imath": [ 305 ],
	"imof": [ 8887 ],
	"imped": [ 437 ],
	"Implies": [ 8658 ],
	"in": [ 8712 ],
	"incare": [ 8453 ],
	"infin": [ 8734 ],
	"infintie": [ 10717 ],
	"inodot": [ 305 ],
	"Int": [ 8748 ],
	"int": [ 8747 ],
	"intcal": [ 8890 ],
	"integers": [ 8484 ],
	"Integral": [ 8747 ],
	"intercal": [ 8890 ],
	"Intersection": [ 8898 ],
	"intlarhk": [ 10775 ],
	"intprod": [ 10812 ],
	"InvisibleComma": [ 8291 ],
	"InvisibleTimes": [ 8290 ],
	"IOcy": [ 1025 ],
	"iocy": [ 1105 ],
	"Iogon": [ 302 ],
	"iogon": [ 303 ],
	"Iopf": [ 120128 ],
	"iopf": [ 120154 ],
	"Iota": [ 921 ],
	"iota": [ 953 ],
	"iprod": [ 10812 ],
	"iquest": [ 191 ],
	"Iscr": [ 8464 ],
	"iscr": [ 119998 ],
	"isin": [ 8712 ],
	"isindot": [ 8949 ],
	"isinE": [ 8953 ],
	"isins": [ 8948 ],
	"isinsv": [ 8947 ],
	"isinv": [ 8712 ],
	"it": [ 8290 ],
	"Itilde": [ 296 ],
	"itilde": [ 297 ],
	"Iukcy": [ 1030 ],
	"iukcy": [ 1110 ],
	"Iuml": [ 207 ],
	"iuml": [ 239 ],
	"Jcirc": [ 308 ],
	"jcirc": [ 309 ],
	"Jcy": [ 1049 ],
	"jcy": [ 1081 ],
	"Jfr": [ 120077 ],
	"jfr": [ 120103 ],
	"jmath": [ 567 ],
	"Jopf": [ 120129 ],
	"jopf": [ 120155 ],
	"Jscr": [ 119973 ],
	"jscr": [ 119999 ],
	"Jsercy": [ 1032 ],
	"jsercy": [ 1112 ],
	"Jukcy": [ 1028 ],
	"jukcy": [ 1108 ],
	"Kappa": [ 922 ],
	"kappa": [ 954 ],
	"kappav": [ 1008 ],
	"Kcedil": [ 310 ],
	"kcedil": [ 311 ],
	"Kcy": [ 1050 ],
	"kcy": [ 1082 ],
	"Kfr": [ 120078 ],
	"kfr": [ 120104 ],
	"kgreen": [ 312 ],
	"KHcy": [ 1061 ],
	"khcy": [ 1093 ],
	"KJcy": [ 1036 ],
	"kjcy": [ 1116 ],
	"Kopf": [ 120130 ],
	"kopf": [ 120156 ],
	"Kscr": [ 119974 ],
	"kscr": [ 120000 ],
	"lAarr": [ 8666 ],
	"Lacute": [ 313 ],
	"lacute": [ 314 ],
	"laemptyv": [ 10676 ],
	"lagran": [ 8466 ],
	"Lambda": [ 923 ],
	"lambda": [ 955 ],
	"Lang": [ 10218 ],
	"lang": [ 10216 ],
	"langd": [ 10641 ],
	"langle": [ 10216 ],
	"lap": [ 10885 ],
	"Laplacetrf": [ 8466 ],
	"laquo": [ 171 ],
	"Larr": [ 8606 ],
	"lArr": [ 8656 ],
	"larr": [ 8592 ],
	"larrb": [ 8676 ],
	"larrbfs": [ 10527 ],
	"larrfs": [ 10525 ],
	"larrhk": [ 8617 ],
	"larrlp": [ 8619 ],
	"larrpl": [ 10553 ],
	"larrsim": [ 10611 ],
	"larrtl": [ 8610 ],
	"lat": [ 10923 ],
	"lAtail": [ 10523 ],
	"latail": [ 10521 ],
	"late": [ 10925 ],
	"lates": [ 10925, 65024 ],
	"lBarr": [ 10510 ],
	"lbarr": [ 10508 ],
	"lbbrk": [ 10098 ],
	"lbrace": [ 123 ],
	"lbrack": [ 91 ],
	"lbrke": [ 10635 ],
	"lbrksld": [ 10639 ],
	"lbrkslu": [ 10637 ],
	"Lcaron": [ 317 ],
	"lcaron": [ 318 ],
	"Lcedil": [ 315 ],
	"lcedil": [ 316 ],
	"lceil": [ 8968 ],
	"lcub": [ 123 ],
	"Lcy": [ 1051 ],
	"lcy": [ 1083 ],
	"ldca": [ 10550 ],
	"ldquo": [ 8220 ],
	"ldquor": [ 8222 ],
	"ldrdhar": [ 10599 ],
	"ldrushar": [ 10571 ],
	"ldsh": [ 8626 ],
	"lE": [ 8806 ],
	"le": [ 8804 ],
	"LeftAngleBracket": [ 10216 ],
	"LeftArrow": [ 8592 ],
	"Leftarrow": [ 8656 ],
	"leftarrow": [ 8592 ],
	"LeftArrowBar": [ 8676 ],
	"LeftArrowRightArrow": [ 8646 ],
	"leftarrowtail": [ 8610 ],
	"LeftCeiling": [ 8968 ],
	"LeftDoubleBracket": [ 10214 ],
	"LeftDownTeeVector": [ 10593 ],
	"LeftDownVector": [ 8643 ],
	"LeftDownVectorBar": [ 10585 ],
	"LeftFloor": [ 8970 ],
	"leftharpoondown": [ 8637 ],
	"leftharpoonup": [ 8636 ],
	"leftleftarrows": [ 8647 ],
	"LeftRightArrow": [ 8596 ],
	"Leftrightarrow": [ 8660 ],
	"leftrightarrow": [ 8596 ],
	"leftrightarrows": [ 8646 ],
	"leftrightharpoons": [ 8651 ],
	"leftrightsquigarrow": [ 8621 ],
	"LeftRightVector": [ 10574 ],
	"LeftTee": [ 8867 ],
	"LeftTeeArrow": [ 8612 ],
	"LeftTeeVector": [ 10586 ],
	"leftthreetimes": [ 8907 ],
	"LeftTriangle": [ 8882 ],
	"LeftTriangleBar": [ 10703 ],
	"LeftTriangleEqual": [ 8884 ],
	"LeftUpDownVector": [ 10577 ],
	"LeftUpTeeVector": [ 10592 ],
	"LeftUpVector": [ 8639 ],
	"LeftUpVectorBar": [ 10584 ],
	"LeftVector": [ 8636 ],
	"LeftVectorBar": [ 10578 ],
	"lEg": [ 10891 ],
	"leg": [ 8922 ],
	"leq": [ 8804 ],
	"leqq": [ 8806 ],
	"leqslant": [ 10877 ],
	"les": [ 10877 ],
	"lescc": [ 10920 ],
	"lesdot": [ 10879 ],
	"lesdoto": [ 10881 ],
	"lesdotor": [ 10883 ],
	"lesg": [ 8922, 65024 ],
	"lesges": [ 10899 ],
	"lessapprox": [ 10885 ],
	"lessdot": [ 8918 ],
	"lesseqgtr": [ 8922 ],
	"lesseqqgtr": [ 10891 ],
	"LessEqualGreater": [ 8922 ],
	"LessFullEqual": [ 8806 ],
	"LessGreater": [ 8822 ],
	"lessgtr": [ 8822 ],
	"LessLess": [ 10913 ],
	"lesssim": [ 8818 ],
	"LessSlantEqual": [ 10877 ],
	"LessTilde": [ 8818 ],
	"lfisht": [ 10620 ],
	"lfloor": [ 8970 ],
	"Lfr": [ 120079 ],
	"lfr": [ 120105 ],
	"lg": [ 8822 ],
	"lgE": [ 10897 ],
	"lHar": [ 10594 ],
	"lhard": [ 8637 ],
	"lharu": [ 8636 ],
	"lharul": [ 10602 ],
	"lhblk": [ 9604 ],
	"LJcy": [ 1033 ],
	"ljcy": [ 1113 ],
	"Ll": [ 8920 ],
	"ll": [ 8810 ],
	"llarr": [ 8647 ],
	"llcorner": [ 8990 ],
	"Lleftarrow": [ 8666 ],
	"llhard": [ 10603 ],
	"lltri": [ 9722 ],
	"Lmidot": [ 319 ],
	"lmidot": [ 320 ],
	"lmoust": [ 9136 ],
	"lmoustache": [ 9136 ],
	"lnap": [ 10889 ],
	"lnapprox": [ 10889 ],
	"lnE": [ 8808 ],
	"lne": [ 10887 ],
	"lneq": [ 10887 ],
	"lneqq": [ 8808 ],
	"lnsim": [ 8934 ],
	"loang": [ 10220 ],
	"loarr": [ 8701 ],
	"lobrk": [ 10214 ],
	"LongLeftArrow": [ 10229 ],
	"Longleftarrow": [ 10232 ],
	"longleftarrow": [ 10229 ],
	"LongLeftRightArrow": [ 10231 ],
	"Longleftrightarrow": [ 10234 ],
	"longleftrightarrow": [ 10231 ],
	"longmapsto": [ 10236 ],
	"LongRightArrow": [ 10230 ],
	"Longrightarrow": [ 10233 ],
	"longrightarrow": [ 10230 ],
	"looparrowleft": [ 8619 ],
	"looparrowright": [ 8620 ],
	"lopar": [ 10629 ],
	"Lopf": [ 120131 ],
	"lopf": [ 120157 ],
	"loplus": [ 10797 ],
	"lotimes": [ 10804 ],
	"lowast": [ 8727 ],
	"lowbar": [ 95 ],
	"LowerLeftArrow": [ 8601 ],
	"LowerRightArrow": [ 8600 ],
	"loz": [ 9674 ],
	"lozenge": [ 9674 ],
	"lozf": [ 10731 ],
	"lpar": [ 40 ],
	"lparlt": [ 10643 ],
	"lrarr": [ 8646 ],
	"lrcorner": [ 8991 ],
	"lrhar": [ 8651 ],
	"lrhard": [ 10605 ],
	"lrm": [ 8206 ],
	"lrtri": [ 8895 ],
	"lsaquo": [ 8249 ],
	"Lscr": [ 8466 ],
	"lscr": [ 120001 ],
	"Lsh": [ 8624 ],
	"lsh": [ 8624 ],
	"lsim": [ 8818 ],
	"lsime": [ 10893 ],
	"lsimg": [ 10895 ],
	"lsqb": [ 91 ],
	"lsquo": [ 8216 ],
	"lsquor": [ 8218 ],
	"Lstrok": [ 321 ],
	"lstrok": [ 322 ],
	"LT": [ 60 ],
	"Lt": [ 8810 ],
	"lt": [ 60 ],
	"ltcc": [ 10918 ],
	"ltcir": [ 10873 ],
	"ltdot": [ 8918 ],
	"lthree": [ 8907 ],
	"ltimes": [ 8905 ],
	"ltlarr": [ 10614 ],
	"ltquest": [ 10875 ],
	"ltri": [ 9667 ],
	"ltrie": [ 8884 ],
	"ltrif": [ 9666 ],
	"ltrPar": [ 10646 ],
	"lurdshar": [ 10570 ],
	"luruhar": [ 10598 ],
	"lvertneqq": [ 8808, 65024 ],
	"lvnE": [ 8808, 65024 ],
	"macr": [ 175 ],
	"male": [ 9794 ],
	"malt": [ 10016 ],
	"maltese": [ 10016 ],
	"Map": [ 10501 ],
	"map": [ 8614 ],
	"mapsto": [ 8614 ],
	"mapstodown": [ 8615 ],
	"mapstoleft": [ 8612 ],
	"mapstoup": [ 8613 ],
	"marker": [ 9646 ],
	"mcomma": [ 10793 ],
	"Mcy": [ 1052 ],
	"mcy": [ 1084 ],
	"mdash": [ 8212 ],
	"mDDot": [ 8762 ],
	"measuredangle": [ 8737 ],
	"MediumSpace": [ 8287 ],
	"Mellintrf": [ 8499 ],
	"Mfr": [ 120080 ],
	"mfr": [ 120106 ],
	"mho": [ 8487 ],
	"micro": [ 181 ],
	"mid": [ 8739 ],
	"midast": [ 42 ],
	"midcir": [ 10992 ],
	"middot": [ 183 ],
	"minus": [ 8722 ],
	"minusb": [ 8863 ],
	"minusd": [ 8760 ],
	"minusdu": [ 10794 ],
	"MinusPlus": [ 8723 ],
	"mlcp": [ 10971 ],
	"mldr": [ 8230 ],
	"mnplus": [ 8723 ],
	"models": [ 8871 ],
	"Mopf": [ 120132 ],
	"mopf": [ 120158 ],
	"mp": [ 8723 ],
	"Mscr": [ 8499 ],
	"mscr": [ 120002 ],
	"mstpos": [ 8766 ],
	"Mu": [ 924 ],
	"mu": [ 956 ],
	"multimap": [ 8888 ],
	"mumap": [ 8888 ],
	"nabla": [ 8711 ],
	"Nacute": [ 323 ],
	"nacute": [ 324 ],
	"nang": [ 8736, 8402 ],
	"nap": [ 8777 ],
	"napE": [ 10864, 824 ],
	"napid": [ 8779, 824 ],
	"napos": [ 329 ],
	"napprox": [ 8777 ],
	"natur": [ 9838 ],
	"natural": [ 9838 ],
	"naturals": [ 8469 ],
	"nbsp": [ 160 ],
	"nbump": [ 8782, 824 ],
	"nbumpe": [ 8783, 824 ],
	"ncap": [ 10819 ],
	"Ncaron": [ 327 ],
	"ncaron": [ 328 ],
	"Ncedil": [ 325 ],
	"ncedil": [ 326 ],
	"ncong": [ 8775 ],
	"ncongdot": [ 10861, 824 ],
	"ncup": [ 10818 ],
	"Ncy": [ 1053 ],
	"ncy": [ 1085 ],
	"ndash": [ 8211 ],
	"ne": [ 8800 ],
	"nearhk": [ 10532 ],
	"neArr": [ 8663 ],
	"nearr": [ 8599 ],
	"nearrow": [ 8599 ],
	"nedot": [ 8784, 824 ],
	"NegativeMediumSpace": [ 8203 ],
	"NegativeThickSpace": [ 8203 ],
	"NegativeThinSpace": [ 8203 ],
	"NegativeVeryThinSpace": [ 8203 ],
	"nequiv": [ 8802 ],
	"nesear": [ 10536 ],
	"nesim": [ 8770, 824 ],
	"NestedGreaterGreater": [ 8811 ],
	"NestedLessLess": [ 8810 ],
	"NewLine": [ 10 ],
	"nexist": [ 8708 ],
	"nexists": [ 8708 ],
	"Nfr": [ 120081 ],
	"nfr": [ 120107 ],
	"ngE": [ 8807, 824 ],
	"nge": [ 8817 ],
	"ngeq": [ 8817 ],
	"ngeqq": [ 8807, 824 ],
	"ngeqslant": [ 10878, 824 ],
	"nges": [ 10878, 824 ],
	"nGg": [ 8921, 824 ],
	"ngsim": [ 8821 ],
	"nGt": [ 8811, 8402 ],
	"ngt": [ 8815 ],
	"ngtr": [ 8815 ],
	"nGtv": [ 8811, 824 ],
	"nhArr": [ 8654 ],
	"nharr": [ 8622 ],
	"nhpar": [ 10994 ],
	"ni": [ 8715 ],
	"nis": [ 8956 ],
	"nisd": [ 8954 ],
	"niv": [ 8715 ],
	"NJcy": [ 1034 ],
	"njcy": [ 1114 ],
	"nlArr": [ 8653 ],
	"nlarr": [ 8602 ],
	"nldr": [ 8229 ],
	"nlE": [ 8806, 824 ],
	"nle": [ 8816 ],
	"nLeftarrow": [ 8653 ],
	"nleftarrow": [ 8602 ],
	"nLeftrightarrow": [ 8654 ],
	"nleftrightarrow": [ 8622 ],
	"nleq": [ 8816 ],
	"nleqq": [ 8806, 824 ],
	"nleqslant": [ 10877, 824 ],
	"nles": [ 10877, 824 ],
	"nless": [ 8814 ],
	"nLl": [ 8920, 824 ],
	"nlsim": [ 8820 ],
	"nLt": [ 8810, 8402 ],
	"nlt": [ 8814 ],
	"nltri": [ 8938 ],
	"nltrie": [ 8940 ],
	"nLtv": [ 8810, 824 ],
	"nmid": [ 8740 ],
	"NoBreak": [ 8288 ],
	"NonBreakingSpace": [ 160 ],
	"Nopf": [ 8469 ],
	"nopf": [ 120159 ],
	"Not": [ 10988 ],
	"not": [ 172 ],
	"NotCongruent": [ 8802 ],
	"NotCupCap": [ 8813 ],
	"NotDoubleVerticalBar": [ 8742 ],
	"NotElement": [ 8713 ],
	"NotEqual": [ 8800 ],
	"NotEqualTilde": [ 8770, 824 ],
	"NotExists": [ 8708 ],
	"NotGreater": [ 8815 ],
	"NotGreaterEqual": [ 8817 ],
	"NotGreaterFullEqual": [ 8807, 824 ],
	"NotGreaterGreater": [ 8811, 824 ],
	"NotGreaterLess": [ 8825 ],
	"NotGreaterSlantEqual": [ 10878, 824 ],
	"NotGreaterTilde": [ 8821 ],
	"NotHumpDownHump": [ 8782, 824 ],
	"NotHumpEqual": [ 8783, 824 ],
	"notin": [ 8713 ],
	"notindot": [ 8949, 824 ],
	"notinE": [ 8953, 824 ],
	"notinva": [ 8713 ],
	"notinvb": [ 8951 ],
	"notinvc": [ 8950 ],
	"NotLeftTriangle": [ 8938 ],
	"NotLeftTriangleBar": [ 10703, 824 ],
	"NotLeftTriangleEqual": [ 8940 ],
	"NotLess": [ 8814 ],
	"NotLessEqual": [ 8816 ],
	"NotLessGreater": [ 8824 ],
	"NotLessLess": [ 8810, 824 ],
	"NotLessSlantEqual": [ 10877, 824 ],
	"NotLessTilde": [ 8820 ],
	"NotNestedGreaterGreater": [ 10914, 824 ],
	"NotNestedLessLess": [ 10913, 824 ],
	"notni": [ 8716 ],
	"notniva": [ 8716 ],
	"notnivb": [ 8958 ],
	"notnivc": [ 8957 ],
	"NotPrecedes": [ 8832 ],
	"NotPrecedesEqual": [ 10927, 824 ],
	"NotPrecedesSlantEqual": [ 8928 ],
	"NotReverseElement": [ 8716 ],
	"NotRightTriangle": [ 8939 ],
	"NotRightTriangleBar": [ 10704, 824 ],
	"NotRightTriangleEqual": [ 8941 ],
	"NotSquareSubset": [ 8847, 824 ],
	"NotSquareSubsetEqual": [ 8930 ],
	"NotSquareSuperset": [ 8848, 824 ],
	"NotSquareSupersetEqual": [ 8931 ],
	"NotSubset": [ 8834, 8402 ],
	"NotSubsetEqual": [ 8840 ],
	"NotSucceeds": [ 8833 ],
	"NotSucceedsEqual": [ 10928, 824 ],
	"NotSucceedsSlantEqual": [ 8929 ],
	"NotSucceedsTilde": [ 8831, 824 ],
	"NotSuperset": [ 8835, 8402 ],
	"NotSupersetEqual": [ 8841 ],
	"NotTilde": [ 8769 ],
	"NotTildeEqual": [ 8772 ],
	"NotTildeFullEqual": [ 8775 ],
	"NotTildeTilde": [ 8777 ],
	"NotVerticalBar": [ 8740 ],
	"npar": [ 8742 ],
	"nparallel": [ 8742 ],
	"nparsl": [ 11005, 8421 ],
	"npart": [ 8706, 824 ],
	"npolint": [ 10772 ],
	"npr": [ 8832 ],
	"nprcue": [ 8928 ],
	"npre": [ 10927, 824 ],
	"nprec": [ 8832 ],
	"npreceq": [ 10927, 824 ],
	"nrArr": [ 8655 ],
	"nrarr": [ 8603 ],
	"nrarrc": [ 10547, 824 ],
	"nrarrw": [ 8605, 824 ],
	"nRightarrow": [ 8655 ],
	"nrightarrow": [ 8603 ],
	"nrtri": [ 8939 ],
	"nrtrie": [ 8941 ],
	"nsc": [ 8833 ],
	"nsccue": [ 8929 ],
	"nsce": [ 10928, 824 ],
	"Nscr": [ 119977 ],
	"nscr": [ 120003 ],
	"nshortmid": [ 8740 ],
	"nshortparallel": [ 8742 ],
	"nsim": [ 8769 ],
	"nsime": [ 8772 ],
	"nsimeq": [ 8772 ],
	"nsmid": [ 8740 ],
	"nspar": [ 8742 ],
	"nsqsube": [ 8930 ],
	"nsqsupe": [ 8931 ],
	"nsub": [ 8836 ],
	"nsubE": [ 10949, 824 ],
	"nsube": [ 8840 ],
	"nsubset": [ 8834, 8402 ],
	"nsubseteq": [ 8840 ],
	"nsubseteqq": [ 10949, 824 ],
	"nsucc": [ 8833 ],
	"nsucceq": [ 10928, 824 ],
	"nsup": [ 8837 ],
	"nsupE": [ 10950, 824 ],
	"nsupe": [ 8841 ],
	"nsupset": [ 8835, 8402 ],
	"nsupseteq": [ 8841 ],
	"nsupseteqq": [ 10950, 824 ],
	"ntgl": [ 8825 ],
	"Ntilde": [ 209 ],
	"ntilde": [ 241 ],
	"ntlg": [ 8824 ],
	"ntriangleleft": [ 8938 ],
	"ntrianglelefteq": [ 8940 ],
	"ntriangleright": [ 8939 ],
	"ntrianglerighteq": [ 8941 ],
	"Nu": [ 925 ],
	"nu": [ 957 ],
	"num": [ 35 ],
	"numero": [ 8470 ],
	"numsp": [ 8199 ],
	"nvap": [ 8781, 8402 ],
	"nVDash": [ 8879 ],
	"nVdash": [ 8878 ],
	"nvDash": [ 8877 ],
	"nvdash": [ 8876 ],
	"nvge": [ 8805, 8402 ],
	"nvgt": [ 62, 8402 ],
	"nvHarr": [ 10500 ],
	"nvinfin": [ 10718 ],
	"nvlArr": [ 10498 ],
	"nvle": [ 8804, 8402 ],
	"nvlt": [ 60, 8402 ],
	"nvltrie": [ 8884, 8402 ],
	"nvrArr": [ 10499 ],
	"nvrtrie": [ 8885, 8402 ],
	"nvsim": [ 8764, 8402 ],
	"nwarhk": [ 10531 ],
	"nwArr": [ 8662 ],
	"nwarr": [ 8598 ],
	"nwarrow": [ 8598 ],
	"nwnear": [ 10535 ],
	"Oacute": [ 211 ],
	"oacute": [ 243 ],
	"oast": [ 8859 ],
	"ocir": [ 8858 ],
	"Ocirc": [ 212 ],
	"ocirc": [ 244 ],
	"Ocy": [ 1054 ],
	"ocy": [ 1086 ],
	"odash": [ 8861 ],
	"Odblac": [ 336 ],
	"odblac": [ 337 ],
	"odiv": [ 10808 ],
	"odot": [ 8857 ],
	"odsold": [ 10684 ],
	"OElig": [ 338 ],
	"oelig": [ 339 ],
	"ofcir": [ 10687 ],
	"Ofr": [ 120082 ],
	"ofr": [ 120108 ],
	"ogon": [ 731 ],
	"Ograve": [ 210 ],
	"ograve": [ 242 ],
	"ogt": [ 10689 ],
	"ohbar": [ 10677 ],
	"ohm": [ 937 ],
	"oint": [ 8750 ],
	"olarr": [ 8634 ],
	"olcir": [ 10686 ],
	"olcross": [ 10683 ],
	"oline": [ 8254 ],
	"olt": [ 10688 ],
	"Omacr": [ 332 ],
	"omacr": [ 333 ],
	"Omega": [ 937 ],
	"omega": [ 969 ],
	"Omicron": [ 927 ],
	"omicron": [ 959 ],
	"omid": [ 10678 ],
	"ominus": [ 8854 ],
	"Oopf": [ 120134 ],
	"oopf": [ 120160 ],
	"opar": [ 10679 ],
	"OpenCurlyDoubleQuote": [ 8220 ],
	"OpenCurlyQuote": [ 8216 ],
	"operp": [ 10681 ],
	"oplus": [ 8853 ],
	"Or": [ 10836 ],
	"or": [ 8744 ],
	"orarr": [ 8635 ],
	"ord": [ 10845 ],
	"order": [ 8500 ],
	"orderof": [ 8500 ],
	"ordf": [ 170 ],
	"ordm": [ 186 ],
	"origof": [ 8886 ],
	"oror": [ 10838 ],
	"orslope": [ 10839 ],
	"orv": [ 10843 ],
	"oS": [ 9416 ],
	"Oscr": [ 119978 ],
	"oscr": [ 8500 ],
	"Oslash": [ 216 ],
	"oslash": [ 248 ],
	"osol": [ 8856 ],
	"Otilde": [ 213 ],
	"otilde": [ 245 ],
	"Otimes": [ 10807 ],
	"otimes": [ 8855 ],
	"otimesas": [ 10806 ],
	"Ouml": [ 214 ],
	"ouml": [ 246 ],
	"ovbar": [ 9021 ],
	"OverBar": [ 8254 ],
	"OverBrace": [ 9182 ],
	"OverBracket": [ 9140 ],
	"OverParenthesis": [ 9180 ],
	"par": [ 8741 ],
	"para": [ 182 ],
	"parallel": [ 8741 ],
	"parsim": [ 10995 ],
	"parsl": [ 11005 ],
	"part": [ 8706 ],
	"PartialD": [ 8706 ],
	"Pcy": [ 1055 ],
	"pcy": [ 1087 ],
	"percnt": [ 37 ],
	"period": [ 46 ],
	"permil": [ 8240 ],
	"perp": [ 8869 ],
	"pertenk": [ 8241 ],
	"Pfr": [ 120083 ],
	"pfr": [ 120109 ],
	"Phi": [ 934 ],
	"phi": [ 966 ],
	"phiv": [ 981 ],
	"phmmat": [ 8499 ],
	"phone": [ 9742 ],
	"Pi": [ 928 ],
	"pi": [ 960 ],
	"pitchfork": [ 8916 ],
	"piv": [ 982 ],
	"planck": [ 8463 ],
	"planckh": [ 8462 ],
	"plankv": [ 8463 ],
	"plus": [ 43 ],
	"plusacir": [ 10787 ],
	"plusb": [ 8862 ],
	"pluscir": [ 10786 ],
	"plusdo": [ 8724 ],
	"plusdu": [ 10789 ],
	"pluse": [ 10866 ],
	"PlusMinus": [ 177 ],
	"plusmn": [ 177 ],
	"plussim": [ 10790 ],
	"plustwo": [ 10791 ],
	"pm": [ 177 ],
	"Poincareplane": [ 8460 ],
	"pointint": [ 10773 ],
	"Popf": [ 8473 ],
	"popf": [ 120161 ],
	"pound": [ 163 ],
	"Pr": [ 10939 ],
	"pr": [ 8826 ],
	"prap": [ 10935 ],
	"prcue": [ 8828 ],
	"prE": [ 10931 ],
	"pre": [ 10927 ],
	"prec": [ 8826 ],
	"precapprox": [ 10935 ],
	"preccurlyeq": [ 8828 ],
	"Precedes": [ 8826 ],
	"PrecedesEqual": [ 10927 ],
	"PrecedesSlantEqual": [ 8828 ],
	"PrecedesTilde": [ 8830 ],
	"preceq": [ 10927 ],
	"precnapprox": [ 10937 ],
	"precneqq": [ 10933 ],
	"precnsim": [ 8936 ],
	"precsim": [ 8830 ],
	"Prime": [ 8243 ],
	"prime": [ 8242 ],
	"primes": [ 8473 ],
	"prnap": [ 10937 ],
	"prnE": [ 10933 ],
	"prnsim": [ 8936 ],
	"prod": [ 8719 ],
	"Product": [ 8719 ],
	"profalar": [ 9006 ],
	"profline": [ 8978 ],
	"profsurf": [ 8979 ],
	"prop": [ 8733 ],
	"Proportion": [ 8759 ],
	"Proportional": [ 8733 ],
	"propto": [ 8733 ],
	"prsim": [ 8830 ],
	"prurel": [ 8880 ],
	"Pscr": [ 119979 ],
	"pscr": [ 120005 ],
	"Psi": [ 936 ],
	"psi": [ 968 ],
	"puncsp": [ 8200 ],
	"Qfr": [ 120084 ],
	"qfr": [ 120110 ],
	"qint": [ 10764 ],
	"Qopf": [ 8474 ],
	"qopf": [ 120162 ],
	"qprime": [ 8279 ],
	"Qscr": [ 119980 ],
	"qscr": [ 120006 ],
	"quaternions": [ 8461 ],
	"quatint": [ 10774 ],
	"quest": [ 63 ],
	"questeq": [ 8799 ],
	"QUOT": [ 34 ],
	"quot": [ 34 ],
	"rAarr": [ 8667 ],
	"race": [ 8765, 817 ],
	"Racute": [ 340 ],
	"racute": [ 341 ],
	"radic": [ 8730 ],
	"raemptyv": [ 10675 ],
	"Rang": [ 10219 ],
	"rang": [ 10217 ],
	"rangd": [ 10642 ],
	"range": [ 10661 ],
	"rangle": [ 10217 ],
	"raquo": [ 187 ],
	"Rarr": [ 8608 ],
	"rArr": [ 8658 ],
	"rarr": [ 8594 ],
	"rarrap": [ 10613 ],
	"rarrb": [ 8677 ],
	"rarrbfs": [ 10528 ],
	"rarrc": [ 10547 ],
	"rarrfs": [ 10526 ],
	"rarrhk": [ 8618 ],
	"rarrlp": [ 8620 ],
	"rarrpl": [ 10565 ],
	"rarrsim": [ 10612 ],
	"Rarrtl": [ 10518 ],
	"rarrtl": [ 8611 ],
	"rarrw": [ 8605 ],
	"rAtail": [ 10524 ],
	"ratail": [ 10522 ],
	"ratio": [ 8758 ],
	"rationals": [ 8474 ],
	"RBarr": [ 10512 ],
	"rBarr": [ 10511 ],
	"rbarr": [ 10509 ],
	"rbbrk": [ 10099 ],
	"rbrace": [ 125 ],
	"rbrack": [ 93 ],
	"rbrke": [ 10636 ],
	"rbrksld": [ 10638 ],
	"rbrkslu": [ 10640 ],
	"Rcaron": [ 344 ],
	"rcaron": [ 345 ],
	"Rcedil": [ 342 ],
	"rcedil": [ 343 ],
	"rceil": [ 8969 ],
	"rcub": [ 125 ],
	"Rcy": [ 1056 ],
	"rcy": [ 1088 ],
	"rdca": [ 10551 ],
	"rdldhar": [ 10601 ],
	"rdquo": [ 8221 ],
	"rdquor": [ 8221 ],
	"rdsh": [ 8627 ],
	"Re": [ 8476 ],
	"real": [ 8476 ],
	"realine": [ 8475 ],
	"realpart": [ 8476 ],
	"reals": [ 8477 ],
	"rect": [ 9645 ],
	"REG": [ 174 ],
	"reg": [ 174 ],
	"ReverseElement": [ 8715 ],
	"ReverseEquilibrium": [ 8651 ],
	"ReverseUpEquilibrium": [ 10607 ],
	"rfisht": [ 10621 ],
	"rfloor": [ 8971 ],
	"Rfr": [ 8476 ],
	"rfr": [ 120111 ],
	"rHar": [ 10596 ],
	"rhard": [ 8641 ],
	"rharu": [ 8640 ],
	"rharul": [ 10604 ],
	"Rho": [ 929 ],
	"rho": [ 961 ],
	"rhov": [ 1009 ],
	"RightAngleBracket": [ 10217 ],
	"RightArrow": [ 8594 ],
	"Rightarrow": [ 8658 ],
	"rightarrow": [ 8594 ],
	"RightArrowBar": [ 8677 ],
	"RightArrowLeftArrow": [ 8644 ],
	"rightarrowtail": [ 8611 ],
	"RightCeiling": [ 8969 ],
	"RightDoubleBracket": [ 10215 ],
	"RightDownTeeVector": [ 10589 ],
	"RightDownVector": [ 8642 ],
	"RightDownVectorBar": [ 10581 ],
	"RightFloor": [ 8971 ],
	"rightharpoondown": [ 8641 ],
	"rightharpoonup": [ 8640 ],
	"rightleftarrows": [ 8644 ],
	"rightleftharpoons": [ 8652 ],
	"rightrightarrows": [ 8649 ],
	"rightsquigarrow": [ 8605 ],
	"RightTee": [ 8866 ],
	"RightTeeArrow": [ 8614 ],
	"RightTeeVector": [ 10587 ],
	"rightthreetimes": [ 8908 ],
	"RightTriangle": [ 8883 ],
	"RightTriangleBar": [ 10704 ],
	"RightTriangleEqual": [ 8885 ],
	"RightUpDownVector": [ 10575 ],
	"RightUpTeeVector": [ 10588 ],
	"RightUpVector": [ 8638 ],
	"RightUpVectorBar": [ 10580 ],
	"RightVector": [ 8640 ],
	"RightVectorBar": [ 10579 ],
	"ring": [ 730 ],
	"risingdotseq": [ 8787 ],
	"rlarr": [ 8644 ],
	"rlhar": [ 8652 ],
	"rlm": [ 8207 ],
	"rmoust": [ 9137 ],
	"rmoustache": [ 9137 ],
	"rnmid": [ 10990 ],
	"roang": [ 10221 ],
	"roarr": [ 8702 ],
	"robrk": [ 10215 ],
	"ropar": [ 10630 ],
	"Ropf": [ 8477 ],
	"ropf": [ 120163 ],
	"roplus": [ 10798 ],
	"rotimes": [ 10805 ],
	"RoundImplies": [ 10608 ],
	"rpar": [ 41 ],
	"rpargt": [ 10644 ],
	"rppolint": [ 10770 ],
	"rrarr": [ 8649 ],
	"Rrightarrow": [ 8667 ],
	"rsaquo": [ 8250 ],
	"Rscr": [ 8475 ],
	"rscr": [ 120007 ],
	"Rsh": [ 8625 ],
	"rsh": [ 8625 ],
	"rsqb": [ 93 ],
	"rsquo": [ 8217 ],
	"rsquor": [ 8217 ],
	"rthree": [ 8908 ],
	"rtimes": [ 8906 ],
	"rtri": [ 9657 ],
	"rtrie": [ 8885 ],
	"rtrif": [ 9656 ],
	"rtriltri": [ 10702 ],
	"RuleDelayed": [ 10740 ],
	"ruluhar": [ 10600 ],
	"rx": [ 8478 ],
	"Sacute": [ 346 ],
	"sacute": [ 347 ],
	"sbquo": [ 8218 ],
	"Sc": [ 10940 ],
	"sc": [ 8827 ],
	"scap": [ 10936 ],
	"Scaron": [ 352 ],
	"scaron": [ 353 ],
	"sccue": [ 8829 ],
	"scE": [ 10932 ],
	"sce": [ 10928 ],
	"Scedil": [ 350 ],
	"scedil": [ 351 ],
	"Scirc": [ 348 ],
	"scirc": [ 349 ],
	"scnap": [ 10938 ],
	"scnE": [ 10934 ],
	"scnsim": [ 8937 ],
	"scpolint": [ 10771 ],
	"scsim": [ 8831 ],
	"Scy": [ 1057 ],
	"scy": [ 1089 ],
	"sdot": [ 8901 ],
	"sdotb": [ 8865 ],
	"sdote": [ 10854 ],
	"searhk": [ 10533 ],
	"seArr": [ 8664 ],
	"searr": [ 8600 ],
	"searrow": [ 8600 ],
	"sect": [ 167 ],
	"semi": [ 59 ],
	"seswar": [ 10537 ],
	"setminus": [ 8726 ],
	"setmn": [ 8726 ],
	"sext": [ 10038 ],
	"Sfr": [ 120086 ],
	"sfr": [ 120112 ],
	"sfrown": [ 8994 ],
	"sharp": [ 9839 ],
	"SHCHcy": [ 1065 ],
	"shchcy": [ 1097 ],
	"SHcy": [ 1064 ],
	"shcy": [ 1096 ],
	"ShortDownArrow": [ 8595 ],
	"ShortLeftArrow": [ 8592 ],
	"shortmid": [ 8739 ],
	"shortparallel": [ 8741 ],
	"ShortRightArrow": [ 8594 ],
	"ShortUpArrow": [ 8593 ],
	"shy": [ 173 ],
	"Sigma": [ 931 ],
	"sigma": [ 963 ],
	"sigmaf": [ 962 ],
	"sigmav": [ 962 ],
	"sim": [ 8764 ],
	"simdot": [ 10858 ],
	"sime": [ 8771 ],
	"simeq": [ 8771 ],
	"simg": [ 10910 ],
	"simgE": [ 10912 ],
	"siml": [ 10909 ],
	"simlE": [ 10911 ],
	"simne": [ 8774 ],
	"simplus": [ 10788 ],
	"simrarr": [ 10610 ],
	"slarr": [ 8592 ],
	"SmallCircle": [ 8728 ],
	"smallsetminus": [ 8726 ],
	"smashp": [ 10803 ],
	"smeparsl": [ 10724 ],
	"smid": [ 8739 ],
	"smile": [ 8995 ],
	"smt": [ 10922 ],
	"smte": [ 10924 ],
	"smtes": [ 10924, 65024 ],
	"SOFTcy": [ 1068 ],
	"softcy": [ 1100 ],
	"sol": [ 47 ],
	"solb": [ 10692 ],
	"solbar": [ 9023 ],
	"Sopf": [ 120138 ],
	"sopf": [ 120164 ],
	"spades": [ 9824 ],
	"spadesuit": [ 9824 ],
	"spar": [ 8741 ],
	"sqcap": [ 8851 ],
	"sqcaps": [ 8851, 65024 ],
	"sqcup": [ 8852 ],
	"sqcups": [ 8852, 65024 ],
	"Sqrt": [ 8730 ],
	"sqsub": [ 8847 ],
	"sqsube": [ 8849 ],
	"sqsubset": [ 8847 ],
	"sqsubseteq": [ 8849 ],
	"sqsup": [ 8848 ],
	"sqsupe": [ 8850 ],
	"sqsupset": [ 8848 ],
	"sqsupseteq": [ 8850 ],
	"squ": [ 9633 ],
	"Square": [ 9633 ],
	"square": [ 9633 ],
	"SquareIntersection": [ 8851 ],
	"SquareSubset": [ 8847 ],
	"SquareSubsetEqual": [ 8849 ],
	"SquareSuperset": [ 8848 ],
	"SquareSupersetEqual": [ 8850 ],
	"SquareUnion": [ 8852 ],
	"squarf": [ 9642 ],
	"squf": [ 9642 ],
	"srarr": [ 8594 ],
	"Sscr": [ 119982 ],
	"sscr": [ 120008 ],
	"ssetmn": [ 8726 ],
	"ssmile": [ 8995 ],
	"sstarf": [ 8902 ],
	"Star": [ 8902 ],
	"star": [ 9734 ],
	"starf": [ 9733 ],
	"straightepsilon": [ 1013 ],
	"straightphi": [ 981 ],
	"strns": [ 175 ],
	"Sub": [ 8912 ],
	"sub": [ 8834 ],
	"subdot": [ 10941 ],
	"subE": [ 10949 ],
	"sube": [ 8838 ],
	"subedot": [ 10947 ],
	"submult": [ 10945 ],
	"subnE": [ 10955 ],
	"subne": [ 8842 ],
	"subplus": [ 10943 ],
	"subrarr": [ 10617 ],
	"Subset": [ 8912 ],
	"subset": [ 8834 ],
	"subseteq": [ 8838 ],
	"subseteqq": [ 10949 ],
	"SubsetEqual": [ 8838 ],
	"subsetneq": [ 8842 ],
	"subsetneqq": [ 10955 ],
	"subsim": [ 10951 ],
	"subsub": [ 10965 ],
	"subsup": [ 10963 ],
	"succ": [ 8827 ],
	"succapprox": [ 10936 ],
	"succcurlyeq": [ 8829 ],
	"Succeeds": [ 8827 ],
	"SucceedsEqual": [ 10928 ],
	"SucceedsSlantEqual": [ 8829 ],
	"SucceedsTilde": [ 8831 ],
	"succeq": [ 10928 ],
	"succnapprox": [ 10938 ],
	"succneqq": [ 10934 ],
	"succnsim": [ 8937 ],
	"succsim": [ 8831 ],
	"SuchThat": [ 8715 ],
	"Sum": [ 8721 ],
	"sum": [ 8721 ],
	"sung": [ 9834 ],
	"Sup": [ 8913 ],
	"sup": [ 8835 ],
	"sup1": [ 185 ],
	"sup2": [ 178 ],
	"sup3": [ 179 ],
	"supdot": [ 10942 ],
	"supdsub": [ 10968 ],
	"supE": [ 10950 ],
	"supe": [ 8839 ],
	"supedot": [ 10948 ],
	"Superset": [ 8835 ],
	"SupersetEqual": [ 8839 ],
	"suphsol": [ 10185 ],
	"suphsub": [ 10967 ],
	"suplarr": [ 10619 ],
	"supmult": [ 10946 ],
	"supnE": [ 10956 ],
	"supne": [ 8843 ],
	"supplus": [ 10944 ],
	"Supset": [ 8913 ],
	"supset": [ 8835 ],
	"supseteq": [ 8839 ],
	"supseteqq": [ 10950 ],
	"supsetneq": [ 8843 ],
	"supsetneqq": [ 10956 ],
	"supsim": [ 10952 ],
	"supsub": [ 10964 ],
	"supsup": [ 10966 ],
	"swarhk": [ 10534 ],
	"swArr": [ 8665 ],
	"swarr": [ 8601 ],
	"swarrow": [ 8601 ],
	"swnwar": [ 10538 ],
	"szlig": [ 223 ],
	"Tab": [ 9 ],
	"target": [ 8982 ],
	"Tau": [ 932 ],
	"tau": [ 964 ],
	"tbrk": [ 9140 ],
	"Tcaron": [ 356 ],
	"tcaron": [ 357 ],
	"Tcedil": [ 354 ],
	"tcedil": [ 355 ],
	"Tcy": [ 1058 ],
	"tcy": [ 1090 ],
	"tdot": [ 8411 ],
	"telrec": [ 8981 ],
	"Tfr": [ 120087 ],
	"tfr": [ 120113 ],
	"there4": [ 8756 ],
	"Therefore": [ 8756 ],
	"therefore": [ 8756 ],
	"Theta": [ 920 ],
	"theta": [ 952 ],
	"thetasym": [ 977 ],
	"thetav": [ 977 ],
	"thickapprox": [ 8776 ],
	"thicksim": [ 8764 ],
	"ThickSpace": [ 8287, 8202 ],
	"thinsp": [ 8201 ],
	"ThinSpace": [ 8201 ],
	"thkap": [ 8776 ],
	"thksim": [ 8764 ],
	"THORN": [ 222 ],
	"thorn": [ 254 ],
	"Tilde": [ 8764 ],
	"tilde": [ 732 ],
	"TildeEqual": [ 8771 ],
	"TildeFullEqual": [ 8773 ],
	"TildeTilde": [ 8776 ],
	"times": [ 215 ],
	"timesb": [ 8864 ],
	"timesbar": [ 10801 ],
	"timesd": [ 10800 ],
	"tint": [ 8749 ],
	"toea": [ 10536 ],
	"top": [ 8868 ],
	"topbot": [ 9014 ],
	"topcir": [ 10993 ],
	"Topf": [ 120139 ],
	"topf": [ 120165 ],
	"topfork": [ 10970 ],
	"tosa": [ 10537 ],
	"tprime": [ 8244 ],
	"TRADE": [ 8482 ],
	"trade": [ 8482 ],
	"triangle": [ 9653 ],
	"triangledown": [ 9663 ],
	"triangleleft": [ 9667 ],
	"trianglelefteq": [ 8884 ],
	"triangleq": [ 8796 ],
	"triangleright": [ 9657 ],
	"trianglerighteq": [ 8885 ],
	"tridot": [ 9708 ],
	"trie": [ 8796 ],
	"triminus": [ 10810 ],
	"TripleDot": [ 8411 ],
	"triplus": [ 10809 ],
	"trisb": [ 10701 ],
	"tritime": [ 10811 ],
	"trpezium": [ 9186 ],
	"Tscr": [ 119983 ],
	"tscr": [ 120009 ],
	"TScy": [ 1062 ],
	"tscy": [ 1094 ],
	"TSHcy": [ 1035 ],
	"tshcy": [ 1115 ],
	"Tstrok": [ 358 ],
	"tstrok": [ 359 ],
	"twixt": [ 8812 ],
	"twoheadleftarrow": [ 8606 ],
	"twoheadrightarrow": [ 8608 ],
	"Uacute": [ 218 ],
	"uacute": [ 250 ],
	"Uarr": [ 8607 ],
	"uArr": [ 8657 ],
	"uarr": [ 8593 ],
	"Uarrocir": [ 10569 ],
	"Ubrcy": [ 1038 ],
	"ubrcy": [ 1118 ],
	"Ubreve": [ 364 ],
	"ubreve": [ 365 ],
	"Ucirc": [ 219 ],
	"ucirc": [ 251 ],
	"Ucy": [ 1059 ],
	"ucy": [ 1091 ],
	"udarr": [ 8645 ],
	"Udblac": [ 368 ],
	"udblac": [ 369 ],
	"udhar": [ 10606 ],
	"ufisht": [ 10622 ],
	"Ufr": [ 120088 ],
	"ufr": [ 120114 ],
	"Ugrave": [ 217 ],
	"ugrave": [ 249 ],
	"uHar": [ 10595 ],
	"uharl": [ 8639 ],
	"uharr": [ 8638 ],
	"uhblk": [ 9600 ],
	"ulcorn": [ 8988 ],
	"ulcorner": [ 8988 ],
	"ulcrop": [ 8975 ],
	"ultri": [ 9720 ],
	"Umacr": [ 362 ],
	"umacr": [ 363 ],
	"uml": [ 168 ],
	"UnderBar": [ 95 ],
	"UnderBrace": [ 9183 ],
	"UnderBracket": [ 9141 ],
	"UnderParenthesis": [ 9181 ],
	"Union": [ 8899 ],
	"UnionPlus": [ 8846 ],
	"Uogon": [ 370 ],
	"uogon": [ 371 ],
	"Uopf": [ 120140 ],
	"uopf": [ 120166 ],
	"UpArrow": [ 8593 ],
	"Uparrow": [ 8657 ],
	"uparrow": [ 8593 ],
	"UpArrowBar": [ 10514 ],
	"UpArrowDownArrow": [ 8645 ],
	"UpDownArrow": [ 8597 ],
	"Updownarrow": [ 8661 ],
	"updownarrow": [ 8597 ],
	"UpEquilibrium": [ 10606 ],
	"upharpoonleft": [ 8639 ],
	"upharpoonright": [ 8638 ],
	"uplus": [ 8846 ],
	"UpperLeftArrow": [ 8598 ],
	"UpperRightArrow": [ 8599 ],
	"Upsi": [ 978 ],
	"upsi": [ 965 ],
	"upsih": [ 978 ],
	"Upsilon": [ 933 ],
	"upsilon": [ 965 ],
	"UpTee": [ 8869 ],
	"UpTeeArrow": [ 8613 ],
	"upuparrows": [ 8648 ],
	"urcorn": [ 8989 ],
	"urcorner": [ 8989 ],
	"urcrop": [ 8974 ],
	"Uring": [ 366 ],
	"uring": [ 367 ],
	"urtri": [ 9721 ],
	"Uscr": [ 119984 ],
	"uscr": [ 120010 ],
	"utdot": [ 8944 ],
	"Utilde": [ 360 ],
	"utilde": [ 361 ],
	"utri": [ 9653 ],
	"utrif": [ 9652 ],
	"uuarr": [ 8648 ],
	"Uuml": [ 220 ],
	"uuml": [ 252 ],
	"uwangle": [ 10663 ],
	"vangrt": [ 10652 ],
	"varepsilon": [ 1013 ],
	"varkappa": [ 1008 ],
	"varnothing": [ 8709 ],
	"varphi": [ 981 ],
	"varpi": [ 982 ],
	"varpropto": [ 8733 ],
	"vArr": [ 8661 ],
	"varr": [ 8597 ],
	"varrho": [ 1009 ],
	"varsigma": [ 962 ],
	"varsubsetneq": [ 8842, 65024 ],
	"varsubsetneqq": [ 10955, 65024 ],
	"varsupsetneq": [ 8843, 65024 ],
	"varsupsetneqq": [ 10956, 65024 ],
	"vartheta": [ 977 ],
	"vartriangleleft": [ 8882 ],
	"vartriangleright": [ 8883 ],
	"Vbar": [ 10987 ],
	"vBar": [ 10984 ],
	"vBarv": [ 10985 ],
	"Vcy": [ 1042 ],
	"vcy": [ 1074 ],
	"VDash": [ 8875 ],
	"Vdash": [ 8873 ],
	"vDash": [ 8872 ],
	"vdash": [ 8866 ],
	"Vdashl": [ 10982 ],
	"Vee": [ 8897 ],
	"vee": [ 8744 ],
	"veebar": [ 8891 ],
	"veeeq": [ 8794 ],
	"vellip": [ 8942 ],
	"Verbar": [ 8214 ],
	"verbar": [ 124 ],
	"Vert": [ 8214 ],
	"vert": [ 124 ],
	"VerticalBar": [ 8739 ],
	"VerticalLine": [ 124 ],
	"VerticalSeparator": [ 10072 ],
	"VerticalTilde": [ 8768 ],
	"VeryThinSpace": [ 8202 ],
	"Vfr": [ 120089 ],
	"vfr": [ 120115 ],
	"vltri": [ 8882 ],
	"vnsub": [ 8834, 8402 ],
	"vnsup": [ 8835, 8402 ],
	"Vopf": [ 120141 ],
	"vopf": [ 120167 ],
	"vprop": [ 8733 ],
	"vrtri": [ 8883 ],
	"Vscr": [ 119985 ],
	"vscr": [ 120011 ],
	"vsubnE": [ 10955, 65024 ],
	"vsubne": [ 8842, 65024 ],
	"vsupnE": [ 10956, 65024 ],
	"vsupne": [ 8843, 65024 ],
	"Vvdash": [ 8874 ],
	"vzigzag": [ 10650 ],
	"Wcirc": [ 372 ],
	"wcirc": [ 373 ],
	"wedbar": [ 10847 ],
	"Wedge": [ 8896 ],
	"wedge": [ 8743 ],
	"wedgeq": [ 8793 ],
	"weierp": [ 8472 ],
	"Wfr": [ 120090 ],
	"wfr": [ 120116 ],
	"Wopf": [ 120142 ],
	"wopf": [ 120168 ],
	"wp": [ 8472 ],
	"wr": [ 8768 ],
	"wreath": [ 8768 ],
	"Wscr": [ 119986 ],
	"wscr": [ 120012 ],
	"xcap": [ 8898 ],
	"xcirc": [ 9711 ],
	"xcup": [ 8899 ],
	"xdtri": [ 9661 ],
	"Xfr": [ 120091 ],
	"xfr": [ 120117 ],
	"xhArr": [ 10234 ],
	"xharr": [ 10231 ],
	"Xi": [ 926 ],
	"xi": [ 958 ],
	"xlArr": [ 10232 ],
	"xlarr": [ 10229 ],
	"xmap": [ 10236 ],
	"xnis": [ 8955 ],
	"xodot": [ 10752 ],
	"Xopf": [ 120143 ],
	"xopf": [ 120169 ],
	"xoplus": [ 10753 ],
	"xotime": [ 10754 ],
	"xrArr": [ 10233 ],
	"xrarr": [ 10230 ],
	"Xscr": [ 119987 ],
	"xscr": [ 120013 ],
	"xsqcup": [ 10758 ],
	"xuplus": [ 10756 ],
	"xutri": [ 9651 ],
	"xvee": [ 8897 ],
	"xwedge": [ 8896 ],
	"Yacute": [ 221 ],
	"yacute": [ 253 ],
	"YAcy": [ 1071 ],
	"yacy": [ 1103 ],
	"Ycirc": [ 374 ],
	"ycirc": [ 375 ],
	"Ycy": [ 1067 ],
	"ycy": [ 1099 ],
	"yen": [ 165 ],
	"Yfr": [ 120092 ],
	"yfr": [ 120118 ],
	"YIcy": [ 1031 ],
	"yicy": [ 1111 ],
	"Yopf": [ 120144 ],
	"yopf": [ 120170 ],
	"Yscr": [ 119988 ],
	"yscr": [ 120014 ],
	"YUcy": [ 1070 ],
	"yucy": [ 1102 ],
	"Yuml": [ 376 ],
	"yuml": [ 255 ],
	"Zacute": [ 377 ],
	"zacute": [ 378 ],
	"Zcaron": [ 381 ],
	"zcaron": [ 382 ],
	"Zcy": [ 1047 ],
	"zcy": [ 1079 ],
	"Zdot": [ 379 ],
	"zdot": [ 380 ],
	"zeetrf": [ 8488 ],
	"ZeroWidthSpace": [ 8203 ],
	"Zeta": [ 918 ],
	"zeta": [ 950 ],
	"Zfr": [ 8488 ],
	"zfr": [ 120119 ],
	"ZHcy": [ 1046 ],
	"zhcy": [ 1078 ],
	"zigrarr": [ 8669 ],
	"Zopf": [ 8484 ],
	"zopf": [ 120171 ],
	"Zscr": [ 119989 ],
	"zscr": [ 120015 ],
	"zwj": [ 8205 ],
	"zwnj": [ 8204 ]
}
},{}],4:[function(require,module,exports){
// the raw mapping of special names to unicode values
var by_name =
exports.by_name = require("../data/entities.json");

// index the reverse mappings immediately
var by_code = exports.by_code = {};
Object.keys(by_name).forEach(function(n) {
	by_name[n].forEach(function(c) {
		if (by_code[c] == null) by_code[c] = [];
		by_code[c].push(n);
	});
});

// a few regular expressions for parsing entities
var hex_value_regex = /^0?x([a-f0-9]+)$/i,
	entity_regex = /&(?:#x[a-f0-9]+|#[0-9]+|[a-z0-9]+);?/ig,
	dec_entity_regex = /^&#([0-9]+);?$/i,
	hex_entity_regex = /^&#x([a-f0-9]+);?$/i,
	spec_entity_regex = /^&([a-z0-9]+);?$/i;

// converts all entities found in string to specific format
var normalizeEntities =
exports.normalizeEntities = function(str, format) {
	return str.replace(entity_regex, function(entity) {
		return convert(entity, format) || entity;
	});
}

// converts all entities and higher order utf-8 chars to format
exports.normalizeXML = function(str, format) {
	var i, code, res;
	
	// convert entities first
	str = normalizeEntities(str, format);

	// find all characters above ASCII and replace, backwards
	for (i = str.length - 1; i >= 0; i--) {
		code = str.charCodeAt(i);
		if (code > 127) {
			res = convert(str[i], format);
			str = str.substr(0, i) + (res != null ? res : str[i]) + str.substr(i + 1);
		}
	}

	return str;
}

// converts single value into utf-8 character or entity equivalent
var convert =
exports.convert = function(s, format) {
	var code, name;
	if (format == null) format = "html";

	code = toCharCode(s);
	if (code === false) return null;

	switch(format) {
		// only decimal entities
		case "xml":
		case "xhtml":
		case "num":
		case "numeric":
			return toEntity(code);

		// only hex entities
		case "hex":
			return toEntity(code, true);

		// first special, then decimal
		case "html":
			return toEntity((name = cton(code)) != null ? name : code);

		// only special entities
		case "name":
		case "special":
			return (name = cton(code)) != null ? toEntity(name) : null;

		// utf-8 character
		case "char":
		case "character":
		case "utf-8":
		case "UTF-8":
			return String.fromCharCode(code);

		// regular number
		case "code":
			return code;
	}

	return null;
}

// code to name
var cton =
exports.codeToName = function(c) {
	var n = by_code[c.toString()];
	return n != null ? n[0] : null;
}

// name to code
var ntoc =
exports.nameToCode = function(n) {
	var c = by_name[n]
	return c != null ? c[0] : null;
}

// parse an array of inputs and returns the equivalent
// unicode decimal or false if invalid
var toCharCode =
exports.toCharCode = function(s) {
	var m, code;

	if (typeof s === "string") {
		if (s === "") return false;

		// regular char
		if (s.length === 1) return s.charCodeAt(0);

		// special entity
		if (m = spec_entity_regex.exec(s)) {
			return ntoc(m[1]) || false;
		}

		// decimal entity
		if (m = dec_entity_regex.exec(s)) {
			return parseInt(m[1], 10);
		}

		// hex entity
		if (m = hex_entity_regex.exec(s)) {
			return parseInt(m[1], 16);
		}

		// hex value
		if (m = hex_value_regex.exec(s)) {
			return parseInt(m[1], 16);
		}

		// otherwise look it up as a special entity name
		return ntoc(s) || false;
	}

	if (typeof s === "number") return s;

	return false;
}

// converts string or number to valid entity
var toEntity =
exports.toEntity = function(n, hex) {
	return "&" + (
		typeof n === "number" ?
		"#" + (hex ? "x" : "") +
		n.toString(hex ? 16 : 10).toUpperCase() : n
	) + ";";
}
},{"../data/entities.json":3}],5:[function(require,module,exports){
(function (global){
//     Underscore.js 1.9.1
//     http://underscorejs.org
//     (c) 2009-2018 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` (`self`) in the browser, `global`
  // on the server, or `this` in some virtual machines. We use `self`
  // instead of `window` for `WebWorker` support.
  var root = typeof self == 'object' && self.self === self && self ||
            typeof global == 'object' && global.global === global && global ||
            this ||
            {};

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype;
  var SymbolProto = typeof Symbol !== 'undefined' ? Symbol.prototype : null;

  // Create quick reference variables for speed access to core prototypes.
  var push = ArrayProto.push,
      slice = ArrayProto.slice,
      toString = ObjProto.toString,
      hasOwnProperty = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var nativeIsArray = Array.isArray,
      nativeKeys = Object.keys,
      nativeCreate = Object.create;

  // Naked function reference for surrogate-prototype-swapping.
  var Ctor = function(){};

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for their old module API. If we're in
  // the browser, add `_` as a global object.
  // (`nodeType` is checked to ensure that `module`
  // and `exports` are not HTML elements.)
  if (typeof exports != 'undefined' && !exports.nodeType) {
    if (typeof module != 'undefined' && !module.nodeType && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.9.1';

  // Internal function that returns an efficient (for current engines) version
  // of the passed-in callback, to be repeatedly applied in other Underscore
  // functions.
  var optimizeCb = function(func, context, argCount) {
    if (context === void 0) return func;
    switch (argCount == null ? 3 : argCount) {
      case 1: return function(value) {
        return func.call(context, value);
      };
      // The 2-argument case is omitted because we’re not using it.
      case 3: return function(value, index, collection) {
        return func.call(context, value, index, collection);
      };
      case 4: return function(accumulator, value, index, collection) {
        return func.call(context, accumulator, value, index, collection);
      };
    }
    return function() {
      return func.apply(context, arguments);
    };
  };

  var builtinIteratee;

  // An internal function to generate callbacks that can be applied to each
  // element in a collection, returning the desired result — either `identity`,
  // an arbitrary callback, a property matcher, or a property accessor.
  var cb = function(value, context, argCount) {
    if (_.iteratee !== builtinIteratee) return _.iteratee(value, context);
    if (value == null) return _.identity;
    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
    if (_.isObject(value) && !_.isArray(value)) return _.matcher(value);
    return _.property(value);
  };

  // External wrapper for our callback generator. Users may customize
  // `_.iteratee` if they want additional predicate/iteratee shorthand styles.
  // This abstraction hides the internal-only argCount argument.
  _.iteratee = builtinIteratee = function(value, context) {
    return cb(value, context, Infinity);
  };

  // Some functions take a variable number of arguments, or a few expected
  // arguments at the beginning and then a variable number of values to operate
  // on. This helper accumulates all remaining arguments past the function’s
  // argument length (or an explicit `startIndex`), into an array that becomes
  // the last argument. Similar to ES6’s "rest parameter".
  var restArguments = function(func, startIndex) {
    startIndex = startIndex == null ? func.length - 1 : +startIndex;
    return function() {
      var length = Math.max(arguments.length - startIndex, 0),
          rest = Array(length),
          index = 0;
      for (; index < length; index++) {
        rest[index] = arguments[index + startIndex];
      }
      switch (startIndex) {
        case 0: return func.call(this, rest);
        case 1: return func.call(this, arguments[0], rest);
        case 2: return func.call(this, arguments[0], arguments[1], rest);
      }
      var args = Array(startIndex + 1);
      for (index = 0; index < startIndex; index++) {
        args[index] = arguments[index];
      }
      args[startIndex] = rest;
      return func.apply(this, args);
    };
  };

  // An internal function for creating a new object that inherits from another.
  var baseCreate = function(prototype) {
    if (!_.isObject(prototype)) return {};
    if (nativeCreate) return nativeCreate(prototype);
    Ctor.prototype = prototype;
    var result = new Ctor;
    Ctor.prototype = null;
    return result;
  };

  var shallowProperty = function(key) {
    return function(obj) {
      return obj == null ? void 0 : obj[key];
    };
  };

  var has = function(obj, path) {
    return obj != null && hasOwnProperty.call(obj, path);
  }

  var deepGet = function(obj, path) {
    var length = path.length;
    for (var i = 0; i < length; i++) {
      if (obj == null) return void 0;
      obj = obj[path[i]];
    }
    return length ? obj : void 0;
  };

  // Helper for collection methods to determine whether a collection
  // should be iterated as an array or as an object.
  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
  var getLength = shallowProperty('length');
  var isArrayLike = function(collection) {
    var length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  };

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles raw objects in addition to array-likes. Treats all
  // sparse array-likes as if they were dense.
  _.each = _.forEach = function(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    var i, length;
    if (isArrayLike(obj)) {
      for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
      }
    } else {
      var keys = _.keys(obj);
      for (i = 0, length = keys.length; i < length; i++) {
        iteratee(obj[keys[i]], keys[i], obj);
      }
    }
    return obj;
  };

  // Return the results of applying the iteratee to each element.
  _.map = _.collect = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length,
        results = Array(length);
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      results[index] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Create a reducing function iterating left or right.
  var createReduce = function(dir) {
    // Wrap code that reassigns argument variables in a separate function than
    // the one that accesses `arguments.length` to avoid a perf hit. (#1991)
    var reducer = function(obj, iteratee, memo, initial) {
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length,
          index = dir > 0 ? 0 : length - 1;
      if (!initial) {
        memo = obj[keys ? keys[index] : index];
        index += dir;
      }
      for (; index >= 0 && index < length; index += dir) {
        var currentKey = keys ? keys[index] : index;
        memo = iteratee(memo, obj[currentKey], currentKey, obj);
      }
      return memo;
    };

    return function(obj, iteratee, memo, context) {
      var initial = arguments.length >= 3;
      return reducer(obj, optimizeCb(iteratee, context, 4), memo, initial);
    };
  };

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`.
  _.reduce = _.foldl = _.inject = createReduce(1);

  // The right-associative version of reduce, also known as `foldr`.
  _.reduceRight = _.foldr = createReduce(-1);

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, predicate, context) {
    var keyFinder = isArrayLike(obj) ? _.findIndex : _.findKey;
    var key = keyFinder(obj, predicate, context);
    if (key !== void 0 && key !== -1) return obj[key];
  };

  // Return all the elements that pass a truth test.
  // Aliased as `select`.
  _.filter = _.select = function(obj, predicate, context) {
    var results = [];
    predicate = cb(predicate, context);
    _.each(obj, function(value, index, list) {
      if (predicate(value, index, list)) results.push(value);
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, predicate, context) {
    return _.filter(obj, _.negate(cb(predicate)), context);
  };

  // Determine whether all of the elements match a truth test.
  // Aliased as `all`.
  _.every = _.all = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (!predicate(obj[currentKey], currentKey, obj)) return false;
    }
    return true;
  };

  // Determine if at least one element in the object matches a truth test.
  // Aliased as `any`.
  _.some = _.any = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (predicate(obj[currentKey], currentKey, obj)) return true;
    }
    return false;
  };

  // Determine if the array or object contains a given item (using `===`).
  // Aliased as `includes` and `include`.
  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
    if (!isArrayLike(obj)) obj = _.values(obj);
    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
    return _.indexOf(obj, item, fromIndex) >= 0;
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = restArguments(function(obj, path, args) {
    var contextPath, func;
    if (_.isFunction(path)) {
      func = path;
    } else if (_.isArray(path)) {
      contextPath = path.slice(0, -1);
      path = path[path.length - 1];
    }
    return _.map(obj, function(context) {
      var method = func;
      if (!method) {
        if (contextPath && contextPath.length) {
          context = deepGet(context, contextPath);
        }
        if (context == null) return void 0;
        method = context[path];
      }
      return method == null ? method : method.apply(context, args);
    });
  });

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, _.property(key));
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs) {
    return _.filter(obj, _.matcher(attrs));
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
    return _.find(obj, _.matcher(attrs));
  };

  // Return the maximum element (or element-based computation).
  _.max = function(obj, iteratee, context) {
    var result = -Infinity, lastComputed = -Infinity,
        value, computed;
    if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value > result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
          result = v;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iteratee, context) {
    var result = Infinity, lastComputed = Infinity,
        value, computed;
    if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value < result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed < lastComputed || computed === Infinity && result === Infinity) {
          result = v;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Shuffle a collection.
  _.shuffle = function(obj) {
    return _.sample(obj, Infinity);
  };

  // Sample **n** random values from a collection using the modern version of the
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
  // If **n** is not specified, returns a single random element.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
    if (n == null || guard) {
      if (!isArrayLike(obj)) obj = _.values(obj);
      return obj[_.random(obj.length - 1)];
    }
    var sample = isArrayLike(obj) ? _.clone(obj) : _.values(obj);
    var length = getLength(sample);
    n = Math.max(Math.min(n, length), 0);
    var last = length - 1;
    for (var index = 0; index < n; index++) {
      var rand = _.random(index, last);
      var temp = sample[index];
      sample[index] = sample[rand];
      sample[rand] = temp;
    }
    return sample.slice(0, n);
  };

  // Sort the object's values by a criterion produced by an iteratee.
  _.sortBy = function(obj, iteratee, context) {
    var index = 0;
    iteratee = cb(iteratee, context);
    return _.pluck(_.map(obj, function(value, key, list) {
      return {
        value: value,
        index: index++,
        criteria: iteratee(value, key, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior, partition) {
    return function(obj, iteratee, context) {
      var result = partition ? [[], []] : {};
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index) {
        var key = iteratee(value, index, obj);
        behavior(result, value, key);
      });
      return result;
    };
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, value, key) {
    if (has(result, key)) result[key].push(value); else result[key] = [value];
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, value, key) {
    result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, value, key) {
    if (has(result, key)) result[key]++; else result[key] = 1;
  });

  var reStrSymbol = /[^\ud800-\udfff]|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff]/g;
  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (_.isString(obj)) {
      // Keep surrogate pair characters together
      return obj.match(reStrSymbol);
    }
    if (isArrayLike(obj)) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
  };

  // Split a collection into two arrays: one whose elements all satisfy the given
  // predicate, and one whose elements all do not satisfy the predicate.
  _.partition = group(function(result, value, pass) {
    result[pass ? 0 : 1].push(value);
  }, true);

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null || array.length < 1) return n == null ? void 0 : [];
    if (n == null || guard) return array[0];
    return _.initial(array, array.length - n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array.
  _.last = function(array, n, guard) {
    if (array == null || array.length < 1) return n == null ? void 0 : [];
    if (n == null || guard) return array[array.length - 1];
    return _.rest(array, Math.max(0, array.length - n));
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, n == null || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, Boolean);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, strict, output) {
    output = output || [];
    var idx = output.length;
    for (var i = 0, length = getLength(input); i < length; i++) {
      var value = input[i];
      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
        // Flatten current level of array or arguments object.
        if (shallow) {
          var j = 0, len = value.length;
          while (j < len) output[idx++] = value[j++];
        } else {
          flatten(value, shallow, strict, output);
          idx = output.length;
        }
      } else if (!strict) {
        output[idx++] = value;
      }
    }
    return output;
  };

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, false);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = restArguments(function(array, otherArrays) {
    return _.difference(array, otherArrays);
  });

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // The faster algorithm will not work with an iteratee if the iteratee
  // is not a one-to-one function, so providing an iteratee will disable
  // the faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
    if (!_.isBoolean(isSorted)) {
      context = iteratee;
      iteratee = isSorted;
      isSorted = false;
    }
    if (iteratee != null) iteratee = cb(iteratee, context);
    var result = [];
    var seen = [];
    for (var i = 0, length = getLength(array); i < length; i++) {
      var value = array[i],
          computed = iteratee ? iteratee(value, i, array) : value;
      if (isSorted && !iteratee) {
        if (!i || seen !== computed) result.push(value);
        seen = computed;
      } else if (iteratee) {
        if (!_.contains(seen, computed)) {
          seen.push(computed);
          result.push(value);
        }
      } else if (!_.contains(result, value)) {
        result.push(value);
      }
    }
    return result;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = restArguments(function(arrays) {
    return _.uniq(flatten(arrays, true, true));
  });

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var result = [];
    var argsLength = arguments.length;
    for (var i = 0, length = getLength(array); i < length; i++) {
      var item = array[i];
      if (_.contains(result, item)) continue;
      var j;
      for (j = 1; j < argsLength; j++) {
        if (!_.contains(arguments[j], item)) break;
      }
      if (j === argsLength) result.push(item);
    }
    return result;
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = restArguments(function(array, rest) {
    rest = flatten(rest, true, true);
    return _.filter(array, function(value){
      return !_.contains(rest, value);
    });
  });

  // Complement of _.zip. Unzip accepts an array of arrays and groups
  // each array's elements on shared indices.
  _.unzip = function(array) {
    var length = array && _.max(array, getLength).length || 0;
    var result = Array(length);

    for (var index = 0; index < length; index++) {
      result[index] = _.pluck(array, index);
    }
    return result;
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = restArguments(_.unzip);

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values. Passing by pairs is the reverse of _.pairs.
  _.object = function(list, values) {
    var result = {};
    for (var i = 0, length = getLength(list); i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // Generator function to create the findIndex and findLastIndex functions.
  var createPredicateIndexFinder = function(dir) {
    return function(array, predicate, context) {
      predicate = cb(predicate, context);
      var length = getLength(array);
      var index = dir > 0 ? 0 : length - 1;
      for (; index >= 0 && index < length; index += dir) {
        if (predicate(array[index], index, array)) return index;
      }
      return -1;
    };
  };

  // Returns the first index on an array-like that passes a predicate test.
  _.findIndex = createPredicateIndexFinder(1);
  _.findLastIndex = createPredicateIndexFinder(-1);

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iteratee, context) {
    iteratee = cb(iteratee, context, 1);
    var value = iteratee(obj);
    var low = 0, high = getLength(array);
    while (low < high) {
      var mid = Math.floor((low + high) / 2);
      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
    }
    return low;
  };

  // Generator function to create the indexOf and lastIndexOf functions.
  var createIndexFinder = function(dir, predicateFind, sortedIndex) {
    return function(array, item, idx) {
      var i = 0, length = getLength(array);
      if (typeof idx == 'number') {
        if (dir > 0) {
          i = idx >= 0 ? idx : Math.max(idx + length, i);
        } else {
          length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
        }
      } else if (sortedIndex && idx && length) {
        idx = sortedIndex(array, item);
        return array[idx] === item ? idx : -1;
      }
      if (item !== item) {
        idx = predicateFind(slice.call(array, i, length), _.isNaN);
        return idx >= 0 ? idx + i : -1;
      }
      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
        if (array[idx] === item) return idx;
      }
      return -1;
    };
  };

  // Return the position of the first occurrence of an item in an array,
  // or -1 if the item is not included in the array.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (stop == null) {
      stop = start || 0;
      start = 0;
    }
    if (!step) {
      step = stop < start ? -1 : 1;
    }

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var range = Array(length);

    for (var idx = 0; idx < length; idx++, start += step) {
      range[idx] = start;
    }

    return range;
  };

  // Chunk a single array into multiple arrays, each containing `count` or fewer
  // items.
  _.chunk = function(array, count) {
    if (count == null || count < 1) return [];
    var result = [];
    var i = 0, length = array.length;
    while (i < length) {
      result.push(slice.call(array, i, i += count));
    }
    return result;
  };

  // Function (ahem) Functions
  // ------------------

  // Determines whether to execute a function as a constructor
  // or a normal function with the provided arguments.
  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
    var self = baseCreate(sourceFunc.prototype);
    var result = sourceFunc.apply(self, args);
    if (_.isObject(result)) return result;
    return self;
  };

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = restArguments(function(func, context, args) {
    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
    var bound = restArguments(function(callArgs) {
      return executeBound(func, bound, context, this, args.concat(callArgs));
    });
    return bound;
  });

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context. _ acts
  // as a placeholder by default, allowing any combination of arguments to be
  // pre-filled. Set `_.partial.placeholder` for a custom placeholder argument.
  _.partial = restArguments(function(func, boundArgs) {
    var placeholder = _.partial.placeholder;
    var bound = function() {
      var position = 0, length = boundArgs.length;
      var args = Array(length);
      for (var i = 0; i < length; i++) {
        args[i] = boundArgs[i] === placeholder ? arguments[position++] : boundArgs[i];
      }
      while (position < arguments.length) args.push(arguments[position++]);
      return executeBound(func, bound, this, this, args);
    };
    return bound;
  });

  _.partial.placeholder = _;

  // Bind a number of an object's methods to that object. Remaining arguments
  // are the method names to be bound. Useful for ensuring that all callbacks
  // defined on an object belong to it.
  _.bindAll = restArguments(function(obj, keys) {
    keys = flatten(keys, false, false);
    var index = keys.length;
    if (index < 1) throw new Error('bindAll must be passed function names');
    while (index--) {
      var key = keys[index];
      obj[key] = _.bind(obj[key], obj);
    }
  });

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memoize = function(key) {
      var cache = memoize.cache;
      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
      if (!has(cache, address)) cache[address] = func.apply(this, arguments);
      return cache[address];
    };
    memoize.cache = {};
    return memoize;
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = restArguments(function(func, wait, args) {
    return setTimeout(function() {
      return func.apply(null, args);
    }, wait);
  });

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = _.partial(_.delay, _, 1);

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
    var timeout, context, args, result;
    var previous = 0;
    if (!options) options = {};

    var later = function() {
      previous = options.leading === false ? 0 : _.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };

    var throttled = function() {
      var now = _.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };

    throttled.cancel = function() {
      clearTimeout(timeout);
      previous = 0;
      timeout = context = args = null;
    };

    return throttled;
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, result;

    var later = function(context, args) {
      timeout = null;
      if (args) result = func.apply(context, args);
    };

    var debounced = restArguments(function(args) {
      if (timeout) clearTimeout(timeout);
      if (immediate) {
        var callNow = !timeout;
        timeout = setTimeout(later, wait);
        if (callNow) result = func.apply(this, args);
      } else {
        timeout = _.delay(later, wait, this, args);
      }

      return result;
    });

    debounced.cancel = function() {
      clearTimeout(timeout);
      timeout = null;
    };

    return debounced;
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return _.partial(wrapper, func);
  };

  // Returns a negated version of the passed-in predicate.
  _.negate = function(predicate) {
    return function() {
      return !predicate.apply(this, arguments);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var args = arguments;
    var start = args.length - 1;
    return function() {
      var i = start;
      var result = args[start].apply(this, arguments);
      while (i--) result = args[i].call(this, result);
      return result;
    };
  };

  // Returns a function that will only be executed on and after the Nth call.
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Returns a function that will only be executed up to (but not including) the Nth call.
  _.before = function(times, func) {
    var memo;
    return function() {
      if (--times > 0) {
        memo = func.apply(this, arguments);
      }
      if (times <= 1) func = null;
      return memo;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = _.partial(_.before, 2);

  _.restArguments = restArguments;

  // Object Functions
  // ----------------

  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
    'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

  var collectNonEnumProps = function(obj, keys) {
    var nonEnumIdx = nonEnumerableProps.length;
    var constructor = obj.constructor;
    var proto = _.isFunction(constructor) && constructor.prototype || ObjProto;

    // Constructor is a special case.
    var prop = 'constructor';
    if (has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

    while (nonEnumIdx--) {
      prop = nonEnumerableProps[nonEnumIdx];
      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
        keys.push(prop);
      }
    }
  };

  // Retrieve the names of an object's own properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`.
  _.keys = function(obj) {
    if (!_.isObject(obj)) return [];
    if (nativeKeys) return nativeKeys(obj);
    var keys = [];
    for (var key in obj) if (has(obj, key)) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve all the property names of an object.
  _.allKeys = function(obj) {
    if (!_.isObject(obj)) return [];
    var keys = [];
    for (var key in obj) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // Returns the results of applying the iteratee to each element of the object.
  // In contrast to _.map it returns an object.
  _.mapObject = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = _.keys(obj),
        length = keys.length,
        results = {};
    for (var index = 0; index < length; index++) {
      var currentKey = keys[index];
      results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Convert an object into a list of `[key, value]` pairs.
  // The opposite of _.object.
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`.
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // An internal function for creating assigner functions.
  var createAssigner = function(keysFunc, defaults) {
    return function(obj) {
      var length = arguments.length;
      if (defaults) obj = Object(obj);
      if (length < 2 || obj == null) return obj;
      for (var index = 1; index < length; index++) {
        var source = arguments[index],
            keys = keysFunc(source),
            l = keys.length;
        for (var i = 0; i < l; i++) {
          var key = keys[i];
          if (!defaults || obj[key] === void 0) obj[key] = source[key];
        }
      }
      return obj;
    };
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = createAssigner(_.allKeys);

  // Assigns a given object with all the own properties in the passed-in object(s).
  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
  _.extendOwn = _.assign = createAssigner(_.keys);

  // Returns the first key on an object that passes a predicate test.
  _.findKey = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = _.keys(obj), key;
    for (var i = 0, length = keys.length; i < length; i++) {
      key = keys[i];
      if (predicate(obj[key], key, obj)) return key;
    }
  };

  // Internal pick helper function to determine if `obj` has key `key`.
  var keyInObj = function(value, key, obj) {
    return key in obj;
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = restArguments(function(obj, keys) {
    var result = {}, iteratee = keys[0];
    if (obj == null) return result;
    if (_.isFunction(iteratee)) {
      if (keys.length > 1) iteratee = optimizeCb(iteratee, keys[1]);
      keys = _.allKeys(obj);
    } else {
      iteratee = keyInObj;
      keys = flatten(keys, false, false);
      obj = Object(obj);
    }
    for (var i = 0, length = keys.length; i < length; i++) {
      var key = keys[i];
      var value = obj[key];
      if (iteratee(value, key, obj)) result[key] = value;
    }
    return result;
  });

  // Return a copy of the object without the blacklisted properties.
  _.omit = restArguments(function(obj, keys) {
    var iteratee = keys[0], context;
    if (_.isFunction(iteratee)) {
      iteratee = _.negate(iteratee);
      if (keys.length > 1) context = keys[1];
    } else {
      keys = _.map(flatten(keys, false, false), String);
      iteratee = function(value, key) {
        return !_.contains(keys, key);
      };
    }
    return _.pick(obj, iteratee, context);
  });

  // Fill in a given object with default properties.
  _.defaults = createAssigner(_.allKeys, true);

  // Creates an object that inherits from the given prototype object.
  // If additional properties are provided then they will be added to the
  // created object.
  _.create = function(prototype, props) {
    var result = baseCreate(prototype);
    if (props) _.extendOwn(result, props);
    return result;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Returns whether an object has a given set of `key:value` pairs.
  _.isMatch = function(object, attrs) {
    var keys = _.keys(attrs), length = keys.length;
    if (object == null) return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      if (attrs[key] !== obj[key] || !(key in obj)) return false;
    }
    return true;
  };


  // Internal recursive comparison function for `isEqual`.
  var eq, deepEq;
  eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a === 1 / b;
    // `null` or `undefined` only equal to itself (strict comparison).
    if (a == null || b == null) return false;
    // `NaN`s are equivalent, but non-reflexive.
    if (a !== a) return b !== b;
    // Exhaust primitive checks
    var type = typeof a;
    if (type !== 'function' && type !== 'object' && typeof b != 'object') return false;
    return deepEq(a, b, aStack, bStack);
  };

  // Internal recursive comparison function for `isEqual`.
  deepEq = function(a, b, aStack, bStack) {
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className !== toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
      case '[object RegExp]':
      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return '' + a === '' + b;
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive.
        // Object(NaN) is equivalent to NaN.
        if (+a !== +a) return +b !== +b;
        // An `egal` comparison is performed for other numeric values.
        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a === +b;
      case '[object Symbol]':
        return SymbolProto.valueOf.call(a) === SymbolProto.valueOf.call(b);
    }

    var areArrays = className === '[object Array]';
    if (!areArrays) {
      if (typeof a != 'object' || typeof b != 'object') return false;

      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                               _.isFunction(bCtor) && bCtor instanceof bCtor)
                          && ('constructor' in a && 'constructor' in b)) {
        return false;
      }
    }
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

    // Initializing stack of traversed objects.
    // It's done here since we only need them for objects and arrays comparison.
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] === a) return bStack[length] === b;
    }

    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);

    // Recursively compare objects and arrays.
    if (areArrays) {
      // Compare array lengths to determine if a deep comparison is necessary.
      length = a.length;
      if (length !== b.length) return false;
      // Deep compare the contents, ignoring non-numeric properties.
      while (length--) {
        if (!eq(a[length], b[length], aStack, bStack)) return false;
      }
    } else {
      // Deep compare objects.
      var keys = _.keys(a), key;
      length = keys.length;
      // Ensure that both objects contain the same number of properties before comparing deep equality.
      if (_.keys(b).length !== length) return false;
      while (length--) {
        // Deep compare each member
        key = keys[length];
        if (!(has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return true;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
    return _.keys(obj).length === 0;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) === '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError, isMap, isWeakMap, isSet, isWeakSet.
  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error', 'Symbol', 'Map', 'WeakMap', 'Set', 'WeakSet'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) === '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE < 9), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return has(obj, 'callee');
    };
  }

  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
  // IE 11 (#1621), Safari 8 (#1929), and PhantomJS (#2236).
  var nodelist = root.document && root.document.childNodes;
  if (typeof /./ != 'function' && typeof Int8Array != 'object' && typeof nodelist != 'function') {
    _.isFunction = function(obj) {
      return typeof obj == 'function' || false;
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return !_.isSymbol(obj) && isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`?
  _.isNaN = function(obj) {
    return _.isNumber(obj) && isNaN(obj);
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, path) {
    if (!_.isArray(path)) {
      return has(obj, path);
    }
    var length = path.length;
    for (var i = 0; i < length; i++) {
      var key = path[i];
      if (obj == null || !hasOwnProperty.call(obj, key)) {
        return false;
      }
      obj = obj[key];
    }
    return !!length;
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iteratees.
  _.identity = function(value) {
    return value;
  };

  // Predicate-generating functions. Often useful outside of Underscore.
  _.constant = function(value) {
    return function() {
      return value;
    };
  };

  _.noop = function(){};

  // Creates a function that, when passed an object, will traverse that object’s
  // properties down the given `path`, specified as an array of keys or indexes.
  _.property = function(path) {
    if (!_.isArray(path)) {
      return shallowProperty(path);
    }
    return function(obj) {
      return deepGet(obj, path);
    };
  };

  // Generates a function for a given object that returns a given property.
  _.propertyOf = function(obj) {
    if (obj == null) {
      return function(){};
    }
    return function(path) {
      return !_.isArray(path) ? obj[path] : deepGet(obj, path);
    };
  };

  // Returns a predicate for checking whether an object has a given set of
  // `key:value` pairs.
  _.matcher = _.matches = function(attrs) {
    attrs = _.extendOwn({}, attrs);
    return function(obj) {
      return _.isMatch(obj, attrs);
    };
  };

  // Run a function **n** times.
  _.times = function(n, iteratee, context) {
    var accum = Array(Math.max(0, n));
    iteratee = optimizeCb(iteratee, context, 1);
    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };

  // A (possibly faster) way to get the current timestamp as an integer.
  _.now = Date.now || function() {
    return new Date().getTime();
  };

  // List of HTML entities for escaping.
  var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
  };
  var unescapeMap = _.invert(escapeMap);

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  var createEscaper = function(map) {
    var escaper = function(match) {
      return map[match];
    };
    // Regexes for identifying a key that needs to be escaped.
    var source = '(?:' + _.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
      string = string == null ? '' : '' + string;
      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
  };
  _.escape = createEscaper(escapeMap);
  _.unescape = createEscaper(unescapeMap);

  // Traverses the children of `obj` along `path`. If a child is a function, it
  // is invoked with its parent as context. Returns the value of the final
  // child, or `fallback` if any child is undefined.
  _.result = function(obj, path, fallback) {
    if (!_.isArray(path)) path = [path];
    var length = path.length;
    if (!length) {
      return _.isFunction(fallback) ? fallback.call(obj) : fallback;
    }
    for (var i = 0; i < length; i++) {
      var prop = obj == null ? void 0 : obj[path[i]];
      if (prop === void 0) {
        prop = fallback;
        i = length; // Ensure we don't continue iterating.
      }
      obj = _.isFunction(prop) ? prop.call(obj) : prop;
    }
    return obj;
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate: /<%([\s\S]+?)%>/g,
    interpolate: /<%=([\s\S]+?)%>/g,
    escape: /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'": "'",
    '\\': '\\',
    '\r': 'r',
    '\n': 'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escapeRegExp = /\\|'|\r|\n|\u2028|\u2029/g;

  var escapeChar = function(match) {
    return '\\' + escapes[match];
  };

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  // NB: `oldSettings` only exists for backwards compatibility.
  _.template = function(text, settings, oldSettings) {
    if (!settings && oldSettings) settings = oldSettings;
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset).replace(escapeRegExp, escapeChar);
      index = offset + match.length;

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      } else if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      } else if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }

      // Adobe VMs need the match returned to produce the correct offset.
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + 'return __p;\n';

    var render;
    try {
      render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled source as a convenience for precompilation.
    var argument = settings.variable || 'obj';
    template.source = 'function(' + argument + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function. Start chaining a wrapped Underscore object.
  _.chain = function(obj) {
    var instance = _(obj);
    instance._chain = true;
    return instance;
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var chainResult = function(instance, obj) {
    return instance._chain ? _(obj).chain() : obj;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    _.each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return chainResult(this, func.apply(_, args));
      };
    });
    return _;
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
      return chainResult(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  _.each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return chainResult(this, method.apply(this._wrapped, arguments));
    };
  });

  // Extracts the result from a wrapped and chained object.
  _.prototype.value = function() {
    return this._wrapped;
  };

  // Provide unwrapping proxy for some methods used in engine operations
  // such as arithmetic and JSON stringification.
  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

  _.prototype.toString = function() {
    return String(this._wrapped);
  };

  // AMD registration happens at the end for compatibility with AMD loaders
  // that may not enforce next-turn semantics on modules. Even though general
  // practice for AMD registration is to be anonymous, underscore registers
  // as a named module because, like jQuery, it is a base library that is
  // popular enough to be bundled in a third party lib, but not be part of
  // an AMD load request. Those cases could generate an error when an
  // anonymous define() is called outside of a loader request.
  if (typeof define == 'function' && define.amd) {
    define('underscore', [], function() {
      return _;
    });
  }
}());

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}]},{},[1])(1)
});

//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJsaWIvaHRtbDJtZC5qcyIsIm5vZGVfbW9kdWxlcy9idGktZG9tLXV0aWxzL2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL3NwZWNpYWwtZW50aXRpZXMvZGF0YS9lbnRpdGllcy5qc29uIiwibm9kZV9tb2R1bGVzL3NwZWNpYWwtZW50aXRpZXMvbGliL2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL3VuZGVyc2NvcmUvdW5kZXJzY29yZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQ25aQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ2pSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOWtFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQzNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKipcbiAqICMgSFRNTDJNRFxuICogSFRNTDJNRCBhdHRlbXB0cyB0byBjb252ZXJ0IEhUTUwgaW50byBNYXJrZG93biBieSByZWR1Y2luZyBhbiBIVE1MIGRvY3VtZW50IGludG8gc2ltcGxlLCBNYXJrZG93bi1jb21wYXRpYmxlIHBhcnRzLiBUaGlzIGxpYnJhcnkgaXMgY29tcGF0aWJsZSB3aXRoIGJvdGggYnJvd3NlcnMgYW5kIE5vZGUuanMuXG4gKlxuICogVG8gdXNlLCBwYXNzIGEgc3RyaW5nIG9mIEhUTUwgdG8gdGhlIGZ1bmN0aW9uLlxuICogXG4gKiBgYGBqYXZhc2NyaXB0XG4gKiB2YXIgbWFya2Rvd24gPSBodG1sMm1kKFwiPGgxPkhlbGxvIFdvcmxkPC9oMT5cIik7XG4gKiBjb25zb2xlLmxvZyhtYXJrZG93bik7IC8vIC0+ICMgSGVsbG8gV29ybGRcbiAqIGBgYFxuICovXG5cbnZhciBfID0gcmVxdWlyZShcInVuZGVyc2NvcmVcIiksXG5cdEVudGl0aWVzID0gcmVxdWlyZShcInNwZWNpYWwtZW50aXRpZXNcIiksXG5cdERPTVV0aWxzID0gcmVxdWlyZShcImJ0aS1kb20tdXRpbHNcIik7XG5cbmZ1bmN0aW9uIGh0bWwybWQoZG9jLCBvcHRpb25zKSB7XG5cdHJldHVybiBodG1sMm1kLnRvTWFya2Rvd24oaHRtbDJtZC5wYXJzZShkb2MsIG9wdGlvbnMpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBodG1sMm1kO1xuXG52YXIgU0NSSVBUX1JFR0VYID0gLzxzY3JpcHRcXGJbXjxdKig/Oig/ITxcXC9zY3JpcHQ+KTxbXjxdKikqPFxcL3NjcmlwdD4vZ2k7XG5cbnZhciBibG9ja19lbGVtZW50cyA9IFsgXCJhcnRpY2xlXCIsIFwiYXNpZGVcIiwgXCJibG9ja3F1b3RlXCIsIFwiYm9keVwiLCBcImJ1dHRvblwiLCBcImNhbnZhc1wiLCBcImNhcHRpb25cIiwgXCJjb2xcIiwgXCJjb2xncm91cFwiLCBcImRkXCIsIFwiZGl2XCIsIFwiZGxcIiwgXCJkdFwiLCBcImVtYmVkXCIsIFwiZmllbGRzZXRcIiwgXCJmaWdjYXB0aW9uXCIsIFwiZmlndXJlXCIsIFwiZm9vdGVyXCIsIFwiZm9ybVwiLCBcImgxXCIsIFwiaDJcIiwgXCJoM1wiLCBcImg0XCIsIFwiaDVcIiwgXCJoNlwiLCBcImhlYWRlclwiLCBcImhncm91cFwiLCBcImhyXCIsIFwibGlcIiwgXCJtYXBcIiwgXCJvYmplY3RcIiwgXCJvbFwiLCBcIm91dHB1dFwiLCBcInBcIiwgXCJwcmVcIiwgXCJwcm9ncmVzc1wiLCBcInNlY3Rpb25cIiwgXCJ0YWJsZVwiLCBcInRib2R5XCIsIFwidGV4dGFyZWFcIiwgXCJ0Zm9vdFwiLCBcInRoXCIsIFwidGhlYWRcIiwgXCJ0clwiLCBcInVsXCIsIFwidmlkZW9cIiBdO1xuXG52YXIgZW1wdHlfdGFncyA9IChbIFwiaHJcIiwgXCJiclwiLCBcImltZ1wiLCBcInZpZGVvXCIsIFwiYXVkaW9cIiBdKS5qb2luKFwiLCBcIik7XG5cbnZhciBtYXJrZG93bl9ibG9ja190YWdzID0gWyBcImJsb2NrcXVvdGVcIiwgXCJoMVwiLCBcImgyXCIsIFwiaDNcIiwgXCJoNFwiLCBcImg1XCIsIFwiaDZcIiwgXCJoclwiLCBcImxpXCIsIFwicHJlXCIsIFwicFwiIF07XG5cbnZhciBtYXJrZG93bl9lbXB0eV9ibG9ja3MgPSBbIFwiaHJcIiBdO1xuXG52YXIgbWFya2Rvd25faW5saW5lX3RhZ3MgPSB7XG5cdFwiYiwgc3Ryb25nXCI6IFwiYm9sZFwiLFxuXHRcImksIGVtXCI6IFwiaXRhbGljXCIsXG5cdFwiY29kZVwiOiBcImNvZGVcIlxufTtcblxudmFyIG1hcmtkb3duX3N5bnRheCA9IHtcblx0aHI6ICAgICAgICAgXCItIC0gLVwiLFxuXHRicjogICAgICAgICBcIiAgXFxuXCIsXG5cdGgxOiAgICAgICAgIFwiIyBcIixcblx0aDI6ICAgICAgICAgXCIjIyBcIixcblx0aDM6ICAgICAgICAgXCIjIyMgXCIsXG5cdGg0OiAgICAgICAgIFwiIyMjIyBcIixcblx0aDU6ICAgICAgICAgXCIjIyMjIyBcIixcblx0aDY6ICAgICAgICAgXCIjIyMjIyMgXCIsXG5cdHVsOiAgICAgICAgIFwiKiBcIixcblx0b2w6ICAgICAgICAgXCIxLiBcIixcblx0YmxvY2txdW90ZTogXCI+IFwiLFxuXHRwcmU6ICAgICAgICBcIiAgXCIsXG5cdHA6ICAgICAgICAgIFwiXCIsXG5cdGJvbGQ6ICAgICAgIFwiKipcIixcblx0aXRhbGljOiAgICAgXCJfXCIsXG5cdGNvZGU6ICAgICAgIFwiYFwiXG59XG5cbi8qKlxuICogIyMgcGFyc2UoKVxuICpcbiAqIFRoaXMgaXMgd2hlcmUgdGhlIG1hZ2ljIGhhcHBlbnMuIFRoaXMgbWV0aG9kIHRha2VzIGEgc3RyaW5nIG9mIEhUTUwgKG9yIGFuIEhUTUwgRG9jdW1lbnQpIGFuZCByZXR1cm5zIGEgTWFya2Rvd24gYWJzdHJhY3Qgc3ludGF4IHRyZWUuXG4gKiBcbiAqICMjIyMgQXJndW1lbnRzXG4gKiBcbiAqIC0gKipodG1sKiogX3N0cmluZyB8IERvY3VtZW50XyAtIEEgc3RyaW5nIG9mIEhUTUwgb3IgYSBEb2N1bWVudCBpbnN0YW5jZSBwcm92aWRlZCBieSB0aGUgRE9NLlxuICogLSAqKm9wdGlvbnMqKiBfb2JqZWN0OyBvcHRpb25hbF8gLSBBbiBvYmplY3Qgb2Ygb3B0aW9ucy5cbiAqICAgLSAqKm9wdGlvbnMud2luZG93KiogX29iamVjdF8gLSBUaGUgd2luZG93IG9iamVjdCB0byB1c2Ugd2hpbGUgcGFyc2luZy4gVGhpcyBpcyB0byBnYWluIGFjY2VzcyB0byBzb21lIGdsb2JhbCBtZXRob2RzIG5lZWRlZCB3aGlsZSBwYXJzaW5nLiBVc3VhbGx5IHRoaXMgaXMgbm90IG5lZWRlZCBiZWNhdXNlIGl0cyB2YWx1ZSBjYW4gYmUgaW5mZXJyZWQuXG4gKi9cbmh0bWwybWQucGFyc2UgPSBmdW5jdGlvbihkb2MsIG9wdGlvbnMpIHtcblx0b3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cblx0aWYgKHR5cGVvZiBkb2MgPT09IFwic3RyaW5nXCIpIHtcblx0XHRkb2MgPSBodG1sMm1kLnRvRE9NKGRvYyk7XG5cdH1cblxuXHR2YXIgd2luID0gb3B0aW9ucy53aW5kb3cgPyBvcHRpb25zLndpbmRvdyA6XG5cdFx0dHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvdyA6XG5cdFx0ZG9jLnBhcmVudFdpbmRvdyB8fCBkb2MuZGVmYXVsdFZpZXc7XG5cblx0aWYgKHdpbiA9PSBudWxsKSB7XG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiTWlzc2luZyB3aW5kb3cgcmVmZXJlbmNlLlwiKTtcblx0fVxuXG5cdGlmICh0eXBlb2YgRE9NVXRpbHMgPT0gXCJmdW5jdGlvblwiKSB7XG5cdFx0dmFyIHV0aWxzID0gRE9NVXRpbHMod2luKTtcblx0fSBlbHNlIHsgXG5cdFx0dmFyIHV0aWxzID0gRE9NVXRpbHM7XG5cdH1cblx0Lypcblx0ICogIyMjIyBTVEVQIDE6IENvbnZlcnQgSFRNTCBpbnRvIGEgc2V0IG9mIGJhc2ljIGJsb2Nrcy5cblx0ICpcblx0ICogTWFya2Rvd24gb3JnYW5pemVzIGEgZG9jdW1lbnQgaW50byBhIHNldCBvZiBibG9ja3MgYW5kIHVubGlrZSBIVE1MXG5cdCAqIGJsb2NrcywgdGhlc2UgY2Fubm90IGNvbnRhaW4gb3RoZXIgYmxvY2tzLCBvbmx5IGlubGluZSBlbGVtZW50cy4gVGhpcyBpc1xuXHQgKiBhY2NvbXBsaXNoZWQgYnkgcmVkdWNpbmcgdGhlIEhUTUwgdG8gaXRzIHNtYWxsZXN0IGJsb2NrIHBhcnRzLCB3aXRoIHRoZVxuXHQgKiBhc3N1bXB0aW9uIHRoYXQgYmxvY2sgZWxlbWVudHMgdXN1YWxseSBkZWZpbmUgc2VwYXJhdGUgYm9kaWVzIG9mXG5cdCAqIGluZm9ybWF0aW9uIHdpdGhpbiBhbiBIVE1MIGRvY3VtZW50LlxuXHQgKlxuXHQgKiBFYWNoIGJsb2NrIGlzIGdpdmVuIGEgdHlwZSBiYXNlZCBvbiB0aGUgTWFya2Rvd24gdHlwZXMuIFRoaXMgdHlwZSBpc1xuXHQgKiBkZXRlcm1pbmVkIGZyb20gdGhlIGNsb3Nlc3QgYW5jZXN0b3Igd2l0aCBvbmUgb2YgdGhlIGZvbGxvd2luZyB0YWdzOlxuXHQgKiBgYmxvY2txdW90ZWAsIGBwcmVgLCBgbGlgLCBgaHJgLCBgaDEtNmAuIEFsbCBvdGhlciBibG9ja3MgYmVjb21lXG5cdCAqIHBhcmFncmFwaHMuXG5cdCAqXG5cdCAqIFRoaXMgb3BlcmF0ZXMgb24gYSBmZXcgYXNzdW1wdGlvbnMsIHdoaWNoIG91dGxpbmUgaXRzIGxpbWl0YXRpb25zOlxuXHQgKiAgIC0gSW5saW5lIGVsZW1lbnRzIGRvIG5vdCBjb250YWluIGJsb2NrIGVsZW1lbnRzLlxuXHQgKiAgIC0gU3RhbmRhcmQgSFRNTCBibG9jayBlbGVtZW50cyBhcmUgdXNlZCB0byBkZWZpbmUgYW5kIHNlcGFyYXRlIGNvbnRlbnQuXG5cdCAqL1xuXG5cdHZhciBibG9ja3MgPSBjb21wYWN0QmxvY2tzKGV4dHJhY3RCbG9ja3MoZG9jLmJvZHkpKTtcblx0XG5cdGZ1bmN0aW9uIGV4dHJhY3RCbG9ja3Mobm9kZSkge1xuXHRcdHZhciBjdXJyZW50QmxvY2ssIGJsb2NrcztcblxuXHRcdGJsb2NrcyA9IFtdO1xuXHRcdGV4dHJhY3Qobm9kZSk7XG5cdFx0cmV0dXJuIGJsb2NrcztcblxuXHRcdGZ1bmN0aW9uIGFkZElubGluZShlbCkge1xuXHRcdFx0aWYgKGN1cnJlbnRCbG9jayAhPSBudWxsKSB7XG5cdFx0XHRcdGN1cnJlbnRCbG9jay5ub2Rlcy5wdXNoKGVsKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGJsb2Nrcy5wdXNoKHsgdHlwZTogXCJwXCIsIG5vZGVzOiBbIGVsIF0gfSk7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBibG9ja3M7XG5cdFx0fVxuXG5cdFx0ZnVuY3Rpb24gZXh0cmFjdChlbCkge1xuXHRcdFx0aWYgKGVsLm5vZGVUeXBlICE9PSAxKSByZXR1cm4gYWRkSW5saW5lKGVsKTtcblxuXHRcdFx0dmFyIHRhZyA9IGVsLnRhZ05hbWUudG9Mb3dlckNhc2UoKTtcblx0XHRcdGlmICghXy5jb250YWlucyhibG9ja19lbGVtZW50cywgdGFnKSkgcmV0dXJuIGFkZElubGluZShlbCk7XG5cblx0XHRcdC8vIHJlbW92ZSB0aGUgY3VycmVudCBibG9jayBpZiBpdCdzIGVtcHR5XG5cdFx0XHRpZiAoY3VycmVudEJsb2NrICE9IG51bGwgJiZcblx0XHRcdFx0IWN1cnJlbnRCbG9jay5ub2Rlcy5sZW5ndGggJiZcblx0XHRcdFx0IV8uY29udGFpbnMobWFya2Rvd25fZW1wdHlfYmxvY2tzLCBjdXJyZW50QmxvY2sudHlwZSkgJiZcblx0XHRcdFx0Xy5sYXN0KGJsb2NrcykgPT09IGN1cnJlbnRCbG9jaykgYmxvY2tzLnBvcCgpO1xuXG5cdFx0XHQvLyBhZGQgYSBuZXcgYmxvY2tcblx0XHRcdGJsb2Nrcy5wdXNoKGN1cnJlbnRCbG9jayA9IHtcblx0XHRcdFx0dHlwZTogXy5jb250YWlucyhtYXJrZG93bl9ibG9ja190YWdzLCB0YWcpID8gdGFnIDogXCJwXCIsXG5cdFx0XHRcdG5vZGVzOiBbXVxuXHRcdFx0fSk7XG5cblx0XHRcdC8vIHByb2Nlc3MgY2hpbGRyZW5cblx0XHRcdF8uZWFjaChlbC5jaGlsZE5vZGVzLCBmdW5jdGlvbihjaGlsZCkge1xuXHRcdFx0XHRleHRyYWN0KGNoaWxkKTtcblx0XHRcdH0pO1xuXG5cdFx0XHQvLyByZXNldCBjdXJyZW50IGJsb2NrXG5cdFx0XHRjdXJyZW50QmxvY2sgPSBudWxsO1xuXHRcdH1cblx0fVxuXG5cdGZ1bmN0aW9uIGNvbXBhY3RCbG9ja3MoYmxvY2tzKSB7XG5cdFx0cmV0dXJuIGJsb2Nrcy5maWx0ZXIoZnVuY3Rpb24oYikge1xuXHRcdFx0dmFyIGVtcHR5QmxvY2sgPSBfLmNvbnRhaW5zKG1hcmtkb3duX2VtcHR5X2Jsb2NrcywgYi50eXBlKTtcblxuXHRcdFx0Ly8gZGVsZXRlIG5vZGVzIGFycmF5IGlmIHRoaXMgaXMgYW4gZW1wdHkgYmxvY2tcblx0XHRcdGlmIChlbXB0eUJsb2NrKSBkZWxldGUgYi5ub2RlcztcblxuXHRcdFx0Ly8gbWFrZSBzdXJlIHRoZSBibG9jayBpc24ndCBlbXB0eVxuXHRcdFx0cmV0dXJuIGVtcHR5QmxvY2sgfHwgXy5zb21lKGIubm9kZXMsIGZ1bmN0aW9uKG4pIHtcblx0XHRcdFx0cmV0dXJuIHV0aWxzLmdldFRleHRDb250ZW50KG4pLnRyaW0oKSAhPSBcIlwiIHx8XG5cdFx0XHRcdFx0KG4ubm9kZVR5cGUgPT09IDEgJiYgKHV0aWxzLm1hdGNoZXNTZWxlY3RvcihuLCBlbXB0eV90YWdzKSB8fCBuLnF1ZXJ5U2VsZWN0b3IoZW1wdHlfdGFncykpKTtcblx0XHRcdH0pO1xuXHRcdH0pO1xuXHR9XG5cblx0Lypcblx0ICogIyMjIyBTVEVQIDI6IENvbnZlcnQgaW5saW5lIEhUTUwgaW50byBpbmxpbmUgTWFya2Rvd24uXG5cdCAqXG5cdCAqIEJhc2ljYWxseSB3ZSBwdXNoIGVhY2ggdGV4dCBub2RlIG9udG8gYSBzdGFjaywgYWNjb3VudGluZyBmb3Igc3BlY2lmaWNcblx0ICogc3R5bGluZyBsaWtlIGl0YWxpY3MgYW5kIGJvbGQuIE90aGVyIGlubGluZSBlbGVtZW50cyBsaWtlIGBicmAgYW5kIGBpbWdgXG5cdCAqIGFyZSBwcmVzZXJ2ZWQsIGJ1dCBldmVyeXRoaW5nIGVsc2UgaXMgdGhyb3duIG91dC5cblx0ICovXG5cblx0YmxvY2tzLmZvckVhY2goZnVuY3Rpb24oYikge1xuXHRcdGlmIChfLmNvbnRhaW5zKG1hcmtkb3duX2VtcHR5X2Jsb2NrcywgYi50eXBlKSkgcmV0dXJuO1xuXHRcdFxuXHRcdGIuY29udGVudCA9IGNsZWFuSW5saW5lcyhiLm5vZGVzLnJlZHVjZShmdW5jdGlvbihtLCBuKSB7XG5cdFx0XHRyZXR1cm4gZXh0cmFjdElubGluZXMobiwgbSk7XG5cdFx0fSwgW10pKTtcblx0fSk7XG5cblx0ZnVuY3Rpb24gZXh0cmFjdElubGluZXMoZWwsIGlubGluZXMpIHtcblx0XHR2YXIgbGFzdElubGluZSwgc3R5bGVzLCBjb250ZW50O1xuXG5cdFx0aWYgKGlubGluZXMgPT0gbnVsbCkgaW5saW5lcyA9IFtdO1xuXG5cdFx0c3dpdGNoIChlbC5ub2RlVHlwZSkge1xuXHRcdFx0Y2FzZSAxOlxuXHRcdFx0XHRzd2l0Y2ggKGVsLnRhZ05hbWUudG9Mb3dlckNhc2UoKSkge1xuXHRcdFx0XHRcdGNhc2UgXCJiclwiOlxuXHRcdFx0XHRcdFx0aW5saW5lcy5wdXNoKHsgdHlwZTogXCJiclwiIH0pO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cblx0XHRcdFx0XHRjYXNlIFwiaW1nXCI6XG5cdFx0XHRcdFx0XHRpbmxpbmVzLnB1c2goe1xuXHRcdFx0XHRcdFx0XHR0eXBlOiBcImltZ1wiLFxuXHRcdFx0XHRcdFx0XHRzcmM6IGVsLmdldEF0dHJpYnV0ZShcInNyY1wiKVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRicmVhaztcblxuXHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRfLmVhY2goZWwuY2hpbGROb2RlcywgZnVuY3Rpb24obikge1xuXHRcdFx0XHRcdFx0XHRleHRyYWN0SW5saW5lcyhuLCBpbmxpbmVzKTtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRicmVhaztcblxuXHRcdFx0Y2FzZSAzOlxuXHRcdFx0XHRsYXN0SW5saW5lID0gXy5sYXN0KGlubGluZXMpO1xuXHRcdFx0XHRjb250ZW50ID0gRW50aXRpZXMubm9ybWFsaXplWE1MKHV0aWxzLmdldFRleHRDb250ZW50KGVsKSwgXCJodG1sXCIpO1xuXHRcdFx0XHRzdHlsZXMgPSBfLmZpbHRlcihtYXJrZG93bl9pbmxpbmVfdGFncywgZnVuY3Rpb24ocywgc2VsKSB7XG5cdFx0XHRcdFx0cmV0dXJuICEhY2xvc2VzdChlbCwgc2VsKTtcblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0aWYgKGxhc3RJbmxpbmUgJiYgbGFzdElubGluZS5jb250ZW50ICE9IG51bGwgJiYgXy5pc0VxdWFsKGxhc3RJbmxpbmUuc3R5bGVzLCBzdHlsZXMpKSB7XG5cdFx0XHRcdFx0bGFzdElubGluZS5jb250ZW50ICs9IGNvbnRlbnQ7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0aW5saW5lcy5wdXNoKHtcblx0XHRcdFx0XHRcdHR5cGU6IFwidGV4dFwiLFxuXHRcdFx0XHRcdFx0Y29udGVudDogY29udGVudCxcblx0XHRcdFx0XHRcdHN0eWxlczogc3R5bGVzXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRicmVhaztcblx0XHR9XG5cblx0XHRyZXR1cm4gaW5saW5lcztcblx0fVxuXG5cdGZ1bmN0aW9uIGNsZWFuSW5saW5lcyhpbmxpbmVzKSB7XG5cdFx0Ly8gY2xlYW4gdXAgd2hpdGVzcGFjZSBhbmQgZHJvcCBlbXB0eSBpbmxpbmVzXG5cdFx0aW5saW5lcyA9IGlubGluZXMucmVkdWNlKGZ1bmN0aW9uKG0sIGlubGluZSwgaSkge1xuXHRcdFx0aWYgKGlubGluZS50eXBlICE9PSBcInRleHRcIikgbS5wdXNoKGlubGluZSk7XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0dmFyIHByZXYgPSBpID4gMCA/IGlubGluZXNbaS0xXSA6IG51bGw7XG5cblx0XHRcdFx0Ly8gcmVkdWNlIG11bHRpcGxlIHNwYWNlcyB0byBvbmVcblx0XHRcdFx0aW5saW5lLmNvbnRlbnQgPSBpbmxpbmUuY29udGVudC5yZXBsYWNlKC9cXHMrL2csIFwiIFwiKTtcblxuXHRcdFx0XHQvLyByZW1vdmUgbGVhZGluZyBzcGFjZSBpZiBwcmV2aW91cyBpbmxpbmUgaGFzIHRyYWlsaW5nIHNwYWNlXG5cdFx0XHRcdGlmIChpbmxpbmUuY29udGVudFswXSA9PT0gXCIgXCIgJiYgcHJldiAmJiAoXG5cdFx0XHRcdFx0cHJldi50eXBlID09PSBcImJyXCIgfHwgKFxuXHRcdFx0XHRcdHByZXYudHlwZSA9PT0gXCJ0ZXh0XCIgJiZcblx0XHRcdFx0XHRwcmV2LmNvbnRlbnRbcHJldi5jb250ZW50Lmxlbmd0aCAtIDFdID09PSBcIiBcIilcblx0XHRcdFx0KSkge1xuXHRcdFx0XHRcdGlubGluZS5jb250ZW50ID0gaW5saW5lLmNvbnRlbnQuc3Vic3RyKDEpO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0Ly8gb25seSBhZGQgaWYgdGhpcyBoYXMgcmVhbCBjb250ZW50XG5cdFx0XHRcdGlmIChpbmxpbmUuY29udGVudCkgbS5wdXNoKGlubGluZSk7XG5cdFx0XHR9XG5cblx0XHRcdHJldHVybiBtO1xuXHRcdH0sIFtdKTtcblxuXHRcdC8vIHRyaW0gbGVhZGluZyB3aGl0ZXNwYWNlXG5cdFx0d2hpbGUgKGlubGluZXMubGVuZ3RoICYmIGlubGluZXNbMF0udHlwZSA9PT0gXCJ0ZXh0XCIpIHtcblx0XHRcdGlubGluZXNbMF0uY29udGVudCA9IGlubGluZXNbMF0uY29udGVudC5yZXBsYWNlKC9eXFxzKy8sIFwiXCIpO1xuXHRcdFx0aWYgKGlubGluZXNbMF0uY29udGVudCkgYnJlYWs7XG5cdFx0XHRpbmxpbmVzLnNoaWZ0KCk7XG5cdFx0fVxuXG5cdFx0Ly8gdHJpbSB0cmFpbGluZyB3aGl0ZXNwYWNlXG5cdFx0dmFyIGxhc3RJbmxpbmU7XG5cdFx0d2hpbGUgKGlubGluZXMubGVuZ3RoICYmIChsYXN0SW5saW5lID0gXy5sYXN0KGlubGluZXMpKS50eXBlID09PSBcInRleHRcIikge1xuXHRcdFx0bGFzdElubGluZS5jb250ZW50ID0gbGFzdElubGluZS5jb250ZW50LnJlcGxhY2UoL1xccyskLywgXCJcIik7XG5cdFx0XHRpZiAobGFzdElubGluZS5jb250ZW50KSBicmVhaztcblx0XHRcdGlubGluZXMucG9wKCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGlubGluZXM7XG5cdH1cblxuXHRmdW5jdGlvbiBjbG9zZXN0KGVsLCBzZWxlY3Rvcikge1xuXHRcdHdoaWxlIChlbCAhPSBudWxsKSB7XG5cdFx0XHRpZiAoZWwubm9kZVR5cGUgPT09IDEgJiYgdXRpbHMubWF0Y2hlc1NlbGVjdG9yKGVsLCBzZWxlY3RvcikpIHJldHVybiBlbDtcblx0XHRcdGVsID0gZWwucGFyZW50Tm9kZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXG5cdC8qXG5cdCAqICMjIyMgU1RFUCAzOiBDbGVhbiB1cFxuXHQgKlxuXHQgKiBUaGUgbGFzdCBzdGVwIGlzIHRvIGNsZWFuIHVwIHRoZSByZXN1bHRpbmcgQVNUIGJlZm9yZSByZXR1cm5pbmcgaXQuXG5cdCAqL1xuXG5cdC8vIGNhbm5vdCBiZSBlbXB0eSB1bmxlc3Mgb3RoZXJ3aXNlIHNwZWNpZmllZFxuXHRibG9ja3MgPSBibG9ja3MuZmlsdGVyKGZ1bmN0aW9uKGIpIHtcblx0XHRyZXR1cm4gXy5jb250YWlucyhtYXJrZG93bl9lbXB0eV9ibG9ja3MsIGIudHlwZSkgfHwgYi5jb250ZW50Lmxlbmd0aDtcblx0fSk7XG5cblx0Ly8gcmVtb3ZlIERPTSBub2RlcyByZWZlcmVuY2UgdG8ga2VlcCBpdCBjbGVhblxuXHRibG9ja3MuZm9yRWFjaChmdW5jdGlvbihiKSB7IGRlbGV0ZSBiLm5vZGVzOyB9KTtcblxuXHRyZXR1cm4gYmxvY2tzO1xufVxuXG4vKipcbiAqICMjIHRvTWFya2Rvd24oKVxuICpcbiAqIFRoaXMgbWV0aG9kcyBjb252ZXJ0cyB0aGUgb3V0cHV0IG9mIGAucGFyc2UoKWAgaW50byBhIHN0cmluZyBvZiBNYXJrZG93bi5cbiAqIFxuICogIyMjIyBBcmd1bWVudHNcbiAqIFxuICogLSAqKnRyZWUqKiBfb2JqZWN0XyAtIEEgTWFya2Rvd24gQVNUIG9iamVjdCByZXR1cm5lZCBmcm9tIGAucGFyc2UoKWAuXG4gKi9cbmh0bWwybWQudG9NYXJrZG93biA9IGZ1bmN0aW9uKHRyZWUpIHtcblx0cmV0dXJuIHRyZWUubWFwKGZ1bmN0aW9uKGJsb2NrKSB7XG5cdFx0dmFyIGFjdGl2ZVN0eWxlcyA9IFtdLFxuXHRcdFx0Y29udGVudCA9IFwiXCI7XG5cblx0XHRpZiAoYmxvY2suY29udGVudCAhPSBudWxsKSB7XG5cdFx0XHRibG9jay5jb250ZW50LmZvckVhY2goZnVuY3Rpb24oaW5saW5lKSB7XG5cdFx0XHRcdHN3aXRjaCAoaW5saW5lLnR5cGUpIHtcblx0XHRcdFx0XHRjYXNlIFwidGV4dFwiOlxuXHRcdFx0XHRcdFx0dXBkYXRlU3R5bGVzKGlubGluZS5zdHlsZXMpO1xuXHRcdFx0XHRcdFx0Y29udGVudCArPSBpbmxpbmUuY29udGVudDtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXG5cdFx0XHRcdFx0Y2FzZSBcImJyXCI6XG5cdFx0XHRcdFx0XHRjb250ZW50ICs9IG1hcmtkb3duX3N5bnRheC5icjtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXG5cdFx0XHRcdFx0Y2FzZSBcImltZ1wiOlxuXHRcdFx0XHRcdFx0Y29udGVudCArPSBcIiFbXShcIiArIGlubGluZS5zcmMgKyBcIilcIjtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcblx0XHRcdHVwZGF0ZVN0eWxlcygpO1xuXHRcdH1cblxuXHRcdHN3aXRjaCAoYmxvY2sudHlwZSkge1xuXHRcdFx0Y2FzZSBcImJsb2NrcXVvdGVcIjpcblx0XHRcdGNhc2UgXCJwcmVcIjpcblx0XHRcdGNhc2UgXCJwXCI6XG5cdFx0XHRjYXNlIFwiaHJcIjpcblx0XHRcdGNhc2UgXCJoMVwiOlxuXHRcdFx0Y2FzZSBcImgyXCI6XG5cdFx0XHRjYXNlIFwiaDNcIjpcblx0XHRcdGNhc2UgXCJoNFwiOlxuXHRcdFx0Y2FzZSBcImg1XCI6XG5cdFx0XHRjYXNlIFwiaDZcIjpcblx0XHRcdFx0cmV0dXJuIChtYXJrZG93bl9zeW50YXhbYmxvY2sudHlwZV0gfHwgXCJcIikgKyBjb250ZW50O1xuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHVwZGF0ZVN0eWxlcyhzdHlsZXMpIHtcblx0XHRcdGlmIChzdHlsZXMgPT0gbnVsbCkgc3R5bGVzID0gW107XG5cblx0XHRcdC8vIGNsb3NlIGFjdGl2ZSBzdHlsZXNcblx0XHRcdHZhciBjbG9zZSA9IF8uZGlmZmVyZW5jZShhY3RpdmVTdHlsZXMsIHN0eWxlcyk7XG5cdFx0XHRhY3RpdmVTdHlsZXMgPSBfLndpdGhvdXQuYXBwbHkoXywgWyBhY3RpdmVTdHlsZXMgXS5jb25jYXQoY2xvc2UpKTtcblx0XHRcdGNsb3NlLnJldmVyc2UoKS5mb3JFYWNoKGZ1bmN0aW9uKHN0eWxlKSB7XG5cdFx0XHRcdGNvbnRlbnQgKz0gbWFya2Rvd25fc3ludGF4W3N0eWxlXSB8fCBcIlwiO1xuXHRcdFx0fSk7XG5cblx0XHRcdC8vIG9wZW4gbmV3IHN0eWxlc1xuXHRcdFx0Xy5kaWZmZXJlbmNlKHN0eWxlcywgYWN0aXZlU3R5bGVzKS5mb3JFYWNoKGZ1bmN0aW9uKHN0eWxlKSB7XG5cdFx0XHRcdGFjdGl2ZVN0eWxlcy5wdXNoKHN0eWxlKTtcblx0XHRcdFx0Y29udGVudCArPSBtYXJrZG93bl9zeW50YXhbc3R5bGVdIHx8IFwiXCI7XG5cdFx0XHR9KTtcblx0XHR9XG5cdH0pLmpvaW4oXCJcXG5cXG5cIik7XHRcbn1cblxuLyoqXG4gKiAjIyB0b0RPTSgpXG4gKlxuICogQSBzbWFsbCB1dGlsaXR5IHRoYXQgdGFrZXMgYSBzdHJpbmcgb2YgSFRNTCBhbmQgcmV0dXJucyBhIG5ldyBIVE1MRG9jdW1lbnQgaW5zdGFuY2UuIEluIE5vZGUuanMsIGBqc2RvbWAgaXMgdXNlZCB0byBzaW11bGF0ZSB0aGUgRE9NLlxuICogXG4gKiAjIyMjIEFyZ3VtZW50c1xuICogXG4gKiAtICoqaHRtbCoqIF9zdHJpbmdfIC0gQSBzdHJpbmcgb2YgSFRNTC5cbiAqL1xuaHRtbDJtZC50b0RPTSA9IGZ1bmN0aW9uKGh0bWwpIHtcblx0dmFyIGRvYztcblxuXHQvLyBjbGVhbiBodG1sIGJlZm9yZSB3ZSBwYXJzZVxuXHRodG1sID0gaHRtbC5yZXBsYWNlKFNDUklQVF9SRUdFWCwgJycpO1xuXHRodG1sID0gRW50aXRpZXMubm9ybWFsaXplWE1MKGh0bWwsICd4aHRtbCcpO1xuXG5cdC8vIGJyb3dzZXJzXG5cdGlmICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiICYmIHdpbmRvdy5kb2N1bWVudCkge1xuXHRcdHZhciBkb2MgPSB3aW5kb3cuZG9jdW1lbnQuaW1wbGVtZW50YXRpb24uY3JlYXRlSFRNTERvY3VtZW50KCk7XG5cdFx0ZG9jLmRvY3VtZW50RWxlbWVudC5pbm5lckhUTUwgPSBodG1sO1xuXHR9XG5cblx0Ly8gbm9kZWpzXG5cdC8vZWxzZSB7XG5cdC8vXHRkb2MgPSByZXF1aXJlKFwianNkb21cIikuanNkb20oaHRtbCk7XG5cdC8vfVxuXG5cdHJldHVybiBkb2M7XG59XG4iLCIvKipcbiAqICMgRE9NIFV0aWxpdGllc1xuICpcbiAqIFRoaXMgaXMgYSBjb2xsZWN0aW9uIG9mIGNvbW1vbiB1dGlsaXR5IG1ldGhvZHMgZm9yIHRoZSBET00uIFdoaWxlIHNpbWlsYXIgaW4gbmF0dXJlIHRvIGxpYnJhcmllcyBsaWtlIGpRdWVyeSwgdGhpcyBsaWJyYXJ5IGFpbXMgdG8gcHJvdmlkZSBtZXRob2RzIGZvciB1bmlxdWUgYW5kIG9kZCBmZWF0dXJlcy5cbiAqL1xuXG52YXIgTm9kZSA9IGdsb2JhbC5Ob2RlO1xudmFyIEVsZW1lbnQgPSBnbG9iYWwuRWxlbWVudDtcblxuLyoqXG4gKiAjIyBpc05vZGUoKVxuICpcbiAqIERldGVybWluZXMgaWYgYSB2YWx1ZSBpcyBhIERPTSBub2RlLlxuICpcbiAqICMjIyMgQXJndW1lbnRzXG4gKlxuICogLSAqKnZhbHVlKiogX21peGVkXyAtIEEgdmFsdWUgdG8gdGVzdCBhcyBhIERPTSBub2RlLlxuICovXG52YXIgaXNOb2RlID1cbmV4cG9ydHMuaXNOb2RlID0gZnVuY3Rpb24obm9kZSkge1xuXHRyZXR1cm4gbm9kZSAhPSBudWxsICYmIHR5cGVvZiBub2RlLm5vZGVUeXBlID09PSBcIm51bWJlclwiO1xufVxuXG4vKipcbiAqICMjIG1hdGNoZXNTZWxlY3RvcigpXG4gKlxuICogQSBjcm9zcyBicm93c2VyIGNvbXBhdGlibGUgc29sdXRpb24gdG8gdGVzdGluZyBhIERPTSBlbGVtZW50IGFnYWluc3QgYSBDU1Mgc2VsZWN0b3IuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIEEgRE9NIG5vZGUgdG8gdGVzdC5cbiAqIC0gKipzZWxlY3RvcioqIF9zdHJpbmdfIC0gQSBDU1Mgc2VsZWN0b3IuXG4gKi9cbnZhciBtYXRjaGVzU2VsZWN0b3IgPSB0eXBlb2YgRWxlbWVudCAhPT0gXCJ1bmRlZmluZWRcIiA/XG5cdEVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXMgfHxcblx0RWxlbWVudC5wcm90b3R5cGUud2Via2l0TWF0Y2hlc1NlbGVjdG9yIHx8XG5cdEVsZW1lbnQucHJvdG90eXBlLm1vek1hdGNoZXNTZWxlY3RvciB8fFxuXHRFbGVtZW50LnByb3RvdHlwZS5tc01hdGNoZXNTZWxlY3RvciA6XG5cdGZ1bmN0aW9uKCkgeyByZXR1cm4gZmFsc2U7IH07XG5cbmV4cG9ydHMubWF0Y2hlc1NlbGVjdG9yID0gZnVuY3Rpb24obm9kZSwgc2VsZWN0b3IpIHtcblx0cmV0dXJuIG1hdGNoZXNTZWxlY3Rvci5jYWxsKG5vZGUsIHNlbGVjdG9yKVxufVxuXG4vKipcbiAqICMjIG1hdGNoZXMoKVxuICpcbiAqIFNpbWlsYXIgdG8gYG1hdGNoZXNTZWxlY3RvcigpYCwgdGhpcyBtZXRob2Qgd2lsbCB0ZXN0IGEgRE9NIG5vZGUgYWdhaW5zdCBDU1Mgc2VsZWN0b3JzLCBvdGhlciBET00gbm9kZXMgYW5kIGZ1bmN0aW9ucy5cbiAqXG4gKiAjIyMjIEFyZ3VtZW50c1xuICpcbiAqIC0gKipub2RlKiogX05vZGVfIC0gQSBET00gbm9kZSB0byB0ZXN0LlxuICogLSAqKnNlbGVjdG9yKiogX3N0cmluZyB8IGZ1bmN0aW9uIHwgTm9kZSB8IEFycmF5XyAtIEEgQ1NTIHNlbGVjdG9yLCBhIGZ1bmN0aW9uIChjYWxsZWQgd2l0aCBvbmUgYXJndW1lbnQsIHRoZSBub2RlKSBvciBhIERPTSBub2RlLiBQYXNzIGFuIGFycmF5IG9mIHNlbGVjdG9ycyB0byBtYXRjaCBhbnkgb2YgdGhlbS5cbiAqL1xudmFyIG1hdGNoZXMgPVxuZXhwb3J0cy5tYXRjaGVzID0gZnVuY3Rpb24obm9kZSwgc2VsZWN0b3IpIHtcblx0aWYgKEFycmF5LmlzQXJyYXkoc2VsZWN0b3IpKSByZXR1cm4gc2VsZWN0b3Iuc29tZShmdW5jdGlvbihzKSB7XG5cdFx0cmV0dXJuIG1hdGNoZXMobm9kZSwgcyk7XG5cdH0pO1xuXG5cdGlmIChpc05vZGUoc2VsZWN0b3IpKSB7XG5cdFx0cmV0dXJuIG5vZGUgPT09IHNlbGVjdG9yO1xuXHR9XG5cdFxuXHRpZiAodHlwZW9mIHNlbGVjdG9yID09PSBcImZ1bmN0aW9uXCIpIHtcblx0XHRyZXR1cm4gISFzZWxlY3Rvcihub2RlKTtcblx0fVxuXHRcblx0aWYgKG5vZGUubm9kZVR5cGUgPT09IE5vZGUuRUxFTUVOVF9OT0RFKSB7XG5cdFx0cmV0dXJuIG1hdGNoZXNTZWxlY3Rvci5jYWxsKG5vZGUsIHNlbGVjdG9yKTtcblx0fVxuXG5cdHJldHVybiBmYWxzZTtcbn07XG5cbi8qKlxuICogIyMgY2xvc2VzdCgpXG4gKlxuICogU3RhdGluZyBhdCBgZWxlbWAsIHRoaXMgbWV0aG9kIHRyYXZlcnNlcyB1cCB0aGUgcGFyZW50IG5vZGVzIGFuZCByZXR1cm5zIHRoZSBmaXJzdCBvbmUgdGhhdCBtYXRjaGVzIGBzZWxlY3RvcmAuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIEEgRE9NIG5vZGUgdG8gdGVzdC5cbiAqIC0gKipzZWxlY3RvcioqIF9zdHJpbmcgfCBmdW5jdGlvbiB8IE5vZGVfIC0gQSBDU1Mgc2VsZWN0b3IsIGEgZnVuY3Rpb24gKGNhbGxlZCB3aXRoIG9uZSBhcmd1bWVudCwgdGhlIG5vZGUpIG9yIGEgRE9NIG5vZGUuXG4gKi9cbmV4cG9ydHMuY2xvc2VzdCA9IGZ1bmN0aW9uKGVsZW0sIHNlbGVjdG9yKSB7XG5cdHdoaWxlIChlbGVtICE9IG51bGwpIHtcblx0XHRpZiAoZWxlbS5ub2RlVHlwZSA9PT0gMSAmJiBtYXRjaGVzKGVsZW0sIHNlbGVjdG9yKSkgcmV0dXJuIGVsZW07XG5cdFx0ZWxlbSA9IGVsZW0ucGFyZW50Tm9kZTtcblx0fVxuXG5cdHJldHVybiBudWxsO1xufTtcblxuLyoqXG4gKiAjIyByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKVxuICpcbiAqIEEgY3Jvc3MtYnJvd3NlciByZXF1ZXN0QW5pbWF0aW9uRnJhbWUuIEZhbGwgYmFjayBvbiBgc2V0VGltZW91dCgpYCBpZiByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgaXNuJ3QgZGVmaW5lZC5cbiAqXG4gKiAjIyMjIEFyZ3VtZW50c1xuICpcbiAqIC0gKipmbioqIF9mdW5jdGlvbl8gLSBBIGZ1bmNpdG9uIHRvIGNhbGwgb24gdGhlIG5leHQgYW5pbWF0aW9uIGZyYW1lLlxuICovXG5leHBvcnRzLnJlcXVlc3RBbmltYXRpb25GcmFtZSA9XG5cdHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUgfHxcblx0d2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuXHR3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG5cdHdpbmRvdy5vUmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG5cdGZ1bmN0aW9uIChmKSB7IHNldFRpbWVvdXQoZiwgMTYpOyB9O1xuXG4vKipcbiAqICMjIGdldEZpcnN0TGVhZk5vZGUoKVxuICpcbiAqIFJldHVybnMgdGhlIGZpcnN0IGRlc2NlbmRhbnQgbm9kZSB3aXRob3V0IGNoaWxkcmVuIG9yIGBudWxsYCBpZiBkb2Vzbid0IGV4aXN0LlxuICpcbiAqICMjIyMgQXJndW1lbnRzXG4gKlxuICogLSAqKm5vZGUqKiBfTm9kZV8gLSBBIERPTSBub2RlIHRvIGZpbmQgdGhlIGZpcnN0IGxlYWYgb2YuXG4gKi9cbnZhciBnZXRGaXJzdExlYWZOb2RlID1cbmV4cG9ydHMuZ2V0Rmlyc3RMZWFmTm9kZSA9IGZ1bmN0aW9uKG5vZGUpIHtcblx0d2hpbGUgKG5vZGUuaGFzQ2hpbGROb2RlcygpKSBub2RlID0gbm9kZS5maXJzdENoaWxkO1xuXHRyZXR1cm4gbm9kZTtcbn1cblxuLyoqXG4gKiAjIyBnZXRMYXN0TGVhZk5vZGUoKVxuICpcbiAqIFJldHVybnMgdGhlIGxhc3QgZGVzY2VuZGFudCBub2RlIHdpdGhvdXQgY2hpbGRyZW4gb3IgYG51bGxgIGlmIGRvZXNuJ3QgZXhpc3QuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIEEgRE9NIG5vZGUgdG8gZmluZCB0aGUgbGFzdCBsZWFmIG9mLlxuICovXG52YXIgZ2V0TGFzdExlYWZOb2RlID1cbmV4cG9ydHMuZ2V0TGFzdExlYWZOb2RlID0gZnVuY3Rpb24obm9kZSkge1xuXHR3aGlsZSAobm9kZS5oYXNDaGlsZE5vZGVzKCkpIG5vZGUgPSBub2RlLmxhc3RDaGlsZDtcblx0cmV0dXJuIG5vZGU7XG59XG5cbi8qKlxuICogIyMgZ2V0TmV4dEV4dGVuZGVkU2libGluZygpXG4gKlxuICogUmV0dXJucyB0aGUgbmV4dCBzaWJsaW5nIG9mIHRoaXMgbm9kZSwgYSBkaXJlY3QgYW5jZXN0b3Igbm9kZSdzIG5leHQgc2libGluZywgb3IgYG51bGxgLlxuICpcbiAqICMjIyMgQXJndW1lbnRzXG4gKlxuICogLSAqKm5vZGUqKiBfTm9kZV8gLSBBIG5vZGUgdG8gZ2V0IHRoZSBuZXh0IGV4dGVuZGVkIHNpYmxpbmcgb2YuXG4gKi9cbnZhciBnZXROZXh0RXh0ZW5kZWRTaWJsaW5nID1cbmV4cG9ydHMuZ2V0TmV4dEV4dGVuZGVkU2libGluZyA9IGZ1bmN0aW9uKG5vZGUpIHtcblx0d2hpbGUgKG5vZGUgIT0gbnVsbCkge1xuXHRcdGlmIChub2RlLm5leHRTaWJsaW5nICE9IG51bGwpIHJldHVybiBub2RlLm5leHRTaWJsaW5nO1xuXHRcdG5vZGUgPSBub2RlLnBhcmVudE5vZGU7XG5cdH1cblxuXHRyZXR1cm4gbnVsbDtcbn1cblxuLyoqXG4gKiAjIyBnZXRQcmV2aW91c0V4dGVuZGVkU2libGluZygpXG4gKlxuICogUmV0dXJucyB0aGUgcHJldmlvdXMgc2libGluZyBvZiB0aGlzIG5vZGUsIGEgZGlyZWN0IGFuY2VzdG9yIG5vZGUncyBwcmV2aW91cyBzaWJsaW5nLCBvciBgbnVsbGAuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIEEgbm9kZSB0byBnZXQgdGhlIHByZXZpb3VzIGV4dGVuZGVkIHNpYmxpbmcgb2YuXG4gKi9cbnZhciBnZXRQcmV2aW91c0V4dGVuZGVkU2libGluZyA9XG5leHBvcnRzLmdldFByZXZpb3VzRXh0ZW5kZWRTaWJsaW5nID0gZnVuY3Rpb24obm9kZSkge1xuXHR3aGlsZSAobm9kZSAhPSBudWxsKSB7XG5cdFx0aWYgKG5vZGUucHJldmlvdXNTaWJsaW5nICE9IG51bGwpIHJldHVybiBub2RlLnByZXZpb3VzU2libGluZztcblx0XHRub2RlID0gbm9kZS5wYXJlbnROb2RlO1xuXHR9XG5cblx0cmV0dXJuIG51bGw7XG59XG5cbi8qKlxuICogIyMgZ2V0TmV4dE5vZGUoKVxuICpcbiAqIEdldHMgdGhlIG5leHQgbm9kZSBpbiB0aGUgRE9NIHRyZWUuIFRoaXMgaXMgZWl0aGVyIHRoZSBmaXJzdCBjaGlsZCBub2RlLCB0aGUgbmV4dCBzaWJsaW5nIG5vZGUsIGEgZGlyZWN0IGFuY2VzdG9yIG5vZGUncyBuZXh0IHNpYmxpbmcsIG9yIGBudWxsYC5cbiAqXG4gKiAjIyMjIEFyZ3VtZW50c1xuICpcbiAqIC0gKipub2RlKiogX05vZGVfIC0gQSBub2RlIHRvIGdldCB0aGUgbmV4dCBub2RlIG9mLlxuICovXG52YXIgZ2V0TmV4dE5vZGUgPVxuZXhwb3J0cy5nZXROZXh0Tm9kZSA9IGZ1bmN0aW9uKG5vZGUpIHtcblx0cmV0dXJuIG5vZGUuaGFzQ2hpbGROb2RlcygpID8gbm9kZS5maXJzdENoaWxkIDogZ2V0TmV4dEV4dGVuZGVkU2libGluZyhub2RlKTtcbn1cblxuLyoqXG4gKiAjIyBnZXRQcmV2aW91c05vZGUoKVxuICpcbiAqIEdldHMgdGhlIHByZXZpb3VzIG5vZGUgaW4gdGhlIERPTSB0cmVlLiBUaGlzIHdpbGwgcmV0dXJuIHRoZSBwcmV2aW91cyBleHRlbmRlZCBzaWJsaW5nJ3MgbGFzdCwgZGVlcGVzdCBsZWFmIG5vZGUgb3IgYG51bGxgIGlmIGRvZXNuJ3QgZXhpc3QuIFRoaXMgcmV0dXJucyB0aGUgZXhhY3Qgb3Bwb3NpdGUgcmVzdWx0IG9mIGBnZXROZXh0Tm9kZWAgKGllIGBnZXROZXh0Tm9kZShnZXRQcmV2aW91c05vZGUobm9kZSkpID09PSBub2RlYCkuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIEEgbm9kZSB0byBnZXQgdGhlIHByZXZpb3VzIG5vZGUgb2YuXG4gKi9cbnZhciBnZXRQcmV2aW91c05vZGUgPVxuZXhwb3J0cy5nZXRQcmV2aW91c05vZGUgPSBmdW5jdGlvbihub2RlKSB7XG5cdHJldHVybiBub2RlLnByZXZpb3VzU2libGluZyA9PSBudWxsID8gbm9kZS5wYXJlbnROb2RlIDogZ2V0TGFzdExlYWZOb2RlKG5vZGUucHJldmlvdXNTaWJsaW5nKTtcbn1cblxuLyoqXG4gKiAjIyBnZXRUZXh0Q29udGVudCgpXG4gKlxuICogR2V0cyB0aGUgdGV4dCBjb250ZW50IG9mIGEgbm9kZSBhbmQgaXRzIGRlc2NlbmRhbnRzLiBUaGlzIGlzIHRoZSB0ZXh0IGNvbnRlbnQgdGhhdCBpcyB2aXNpYmxlIHRvIGEgdXNlciB2aWV3aW5nIHRoZSBIVE1MIGZyb20gYnJvd3Nlci4gSGlkZGVuIG5vZGVzLCBzdWNoIGFzIGNvbW1lbnRzLCBhcmUgbm90IGluY2x1ZGVkIGluIHRoZSBvdXRwdXQuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIEEgbm9kZSB0byBnZXQgdGhlIHRleHQgY29udGVudCBvZi5cbiAqL1xudmFyIGdldFRleHRDb250ZW50ID1cbmV4cG9ydHMuZ2V0VGV4dENvbnRlbnQgPSBmdW5jdGlvbihub2RlKSB7XG5cdGlmIChBcnJheS5pc0FycmF5KG5vZGUpKSByZXR1cm4gbm9kZS5tYXAoZ2V0VGV4dENvbnRlbnQpLmpvaW4oXCJcIik7XG5cblx0c3dpdGNoKG5vZGUubm9kZVR5cGUpIHtcblx0XHRjYXNlIE5vZGUuRE9DVU1FTlRfTk9ERTpcblx0XHRjYXNlIE5vZGUuRE9DVU1FTlRfRlJBR01FTlRfTk9ERTpcblx0XHRcdHJldHVybiBnZXRUZXh0Q29udGVudChBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlLmNoaWxkTm9kZXMsIDApKTtcblxuXHRcdGNhc2UgTm9kZS5FTEVNRU5UX05PREU6XG5cdFx0XHRpZiAodHlwZW9mIG5vZGUuaW5uZXJUZXh0ID09PSBcInN0cmluZ1wiKSByZXR1cm4gbm9kZS5pbm5lclRleHQ7XHRcdC8vIHdlYmtpdFxuXHRcdFx0aWYgKHR5cGVvZiBub2RlLnRleHRDb250ZW50ID09PSBcInN0cmluZ1wiKSByZXR1cm4gbm9kZS50ZXh0Q29udGVudDtcdC8vIGZpcmVmb3hcblx0XHRcdHJldHVybiBnZXRUZXh0Q29udGVudChBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlLmNoaWxkTm9kZXMsIDApKTsvLyBvdGhlclxuXHRcdFxuXHRcdGNhc2UgTm9kZS5URVhUX05PREU6XG5cdFx0XHRyZXR1cm4gbm9kZS5ub2RlVmFsdWUgfHwgXCJcIjtcblxuXHRcdGRlZmF1bHQ6XG5cdFx0XHRyZXR1cm4gXCJcIjtcblx0fVxufVxuXG4vKipcbiAqICMjIGdldFJvb3ROb2RlKClcbiAqXG4gKiBSZXR1cm5zIHRoZSByb290IG5vZGUgb2YgYSBET00gdHJlZS5cbiAqXG4gKiAjIyMjIEFyZ3VtZW50c1xuICpcbiAqIC0gKipub2RlKiogX05vZGVfIC0gQSBub2RlIGluIHRoZSBET00gdHJlZSB5b3UgbmVlZCB0aGUgcm9vdCBvZi5cbiAqL1xudmFyIGdldFJvb3ROb2RlID1cbmV4cG9ydHMuZ2V0Um9vdE5vZGUgPSBmdW5jdGlvbihub2RlKSB7XG5cdHdoaWxlIChub2RlLnBhcmVudE5vZGUgIT0gbnVsbCkge1xuXHRcdG5vZGUgPSBub2RlLnBhcmVudE5vZGVcblx0fVxuXHRcblx0cmV0dXJuIG5vZGU7XG59XG5cbi8qKlxuICogIyMgY29udGFpbnMoKVxuICpcbiAqIERldGVybWluZXMgaWYgYSBub2RlIGlzIGEgZGlyZWN0IGFuY2VzdG9yIG9mIGFub3RoZXIgbm9kZS4gVGhpcyBpcyB0aGUgc2FtZSBzeW50YXggYXMgalF1ZXJ5J3MgYCQuY29udGFpbnMoKWAuXG4gKlxuICogIyMjIyBBcmd1bWVudHNcbiAqXG4gKiAtICoqcGFyZW50KiogX05vZGVfIC0gVGhlIGFuY2VzdG9yIG5vZGUuXG4gKiAtICoqbm9kZSoqIF9Ob2RlXyAtIFRoZSBub2RlIHdoaWNoIG1heSBvciBtYXkgbm90IGJlIGEgZGVzY2VuZGFudCBvZiB0aGUgcGFyZW50LlxuICovXG52YXIgY29udGFpbnMgPVxuZXhwb3J0cy5jb250YWlucyA9IGZ1bmN0aW9uKHBhcmVudCwgbm9kZSkge1xuXHR3aGlsZSAobm9kZSAhPSBudWxsKSB7XG5cdFx0aWYgKG1hdGNoZXMobm9kZSwgcGFyZW50KSkgcmV0dXJuIHRydWU7XG5cdFx0bm9kZSA9IG5vZGUucGFyZW50Tm9kZTtcblx0fVxuXHRcblx0cmV0dXJuIGZhbHNlO1xufSIsIm1vZHVsZS5leHBvcnRzPXtcblx0XCJBYWN1dGVcIjogWyAxOTMgXSxcblx0XCJhYWN1dGVcIjogWyAyMjUgXSxcblx0XCJBYnJldmVcIjogWyAyNTggXSxcblx0XCJhYnJldmVcIjogWyAyNTkgXSxcblx0XCJhY1wiOiBbIDg3NjYgXSxcblx0XCJhY2RcIjogWyA4NzY3IF0sXG5cdFwiYWNFXCI6IFsgODc2NiwgODE5IF0sXG5cdFwiQWNpcmNcIjogWyAxOTQgXSxcblx0XCJhY2lyY1wiOiBbIDIyNiBdLFxuXHRcImFjdXRlXCI6IFsgMTgwIF0sXG5cdFwiQWN5XCI6IFsgMTA0MCBdLFxuXHRcImFjeVwiOiBbIDEwNzIgXSxcblx0XCJBRWxpZ1wiOiBbIDE5OCBdLFxuXHRcImFlbGlnXCI6IFsgMjMwIF0sXG5cdFwiYWZcIjogWyA4Mjg5IF0sXG5cdFwiQWZyXCI6IFsgMTIwMDY4IF0sXG5cdFwiYWZyXCI6IFsgMTIwMDk0IF0sXG5cdFwiQWdyYXZlXCI6IFsgMTkyIF0sXG5cdFwiYWdyYXZlXCI6IFsgMjI0IF0sXG5cdFwiYWxlZnN5bVwiOiBbIDg1MDEgXSxcblx0XCJhbGVwaFwiOiBbIDg1MDEgXSxcblx0XCJBbHBoYVwiOiBbIDkxMyBdLFxuXHRcImFscGhhXCI6IFsgOTQ1IF0sXG5cdFwiQW1hY3JcIjogWyAyNTYgXSxcblx0XCJhbWFjclwiOiBbIDI1NyBdLFxuXHRcImFtYWxnXCI6IFsgMTA4MTUgXSxcblx0XCJBTVBcIjogWyAzOCBdLFxuXHRcImFtcFwiOiBbIDM4IF0sXG5cdFwiQW5kXCI6IFsgMTA4MzUgXSxcblx0XCJhbmRcIjogWyA4NzQzIF0sXG5cdFwiYW5kYW5kXCI6IFsgMTA4MzcgXSxcblx0XCJhbmRkXCI6IFsgMTA4NDQgXSxcblx0XCJhbmRzbG9wZVwiOiBbIDEwODQwIF0sXG5cdFwiYW5kdlwiOiBbIDEwODQyIF0sXG5cdFwiYW5nXCI6IFsgODczNiBdLFxuXHRcImFuZ2VcIjogWyAxMDY2MCBdLFxuXHRcImFuZ2xlXCI6IFsgODczNiBdLFxuXHRcImFuZ21zZFwiOiBbIDg3MzcgXSxcblx0XCJhbmdtc2RhYVwiOiBbIDEwNjY0IF0sXG5cdFwiYW5nbXNkYWJcIjogWyAxMDY2NSBdLFxuXHRcImFuZ21zZGFjXCI6IFsgMTA2NjYgXSxcblx0XCJhbmdtc2RhZFwiOiBbIDEwNjY3IF0sXG5cdFwiYW5nbXNkYWVcIjogWyAxMDY2OCBdLFxuXHRcImFuZ21zZGFmXCI6IFsgMTA2NjkgXSxcblx0XCJhbmdtc2RhZ1wiOiBbIDEwNjcwIF0sXG5cdFwiYW5nbXNkYWhcIjogWyAxMDY3MSBdLFxuXHRcImFuZ3J0XCI6IFsgODczNSBdLFxuXHRcImFuZ3J0dmJcIjogWyA4ODk0IF0sXG5cdFwiYW5ncnR2YmRcIjogWyAxMDY1MyBdLFxuXHRcImFuZ3NwaFwiOiBbIDg3MzggXSxcblx0XCJhbmdzdFwiOiBbIDE5NyBdLFxuXHRcImFuZ3phcnJcIjogWyA5MDg0IF0sXG5cdFwiQW9nb25cIjogWyAyNjAgXSxcblx0XCJhb2dvblwiOiBbIDI2MSBdLFxuXHRcIkFvcGZcIjogWyAxMjAxMjAgXSxcblx0XCJhb3BmXCI6IFsgMTIwMTQ2IF0sXG5cdFwiYXBcIjogWyA4Nzc2IF0sXG5cdFwiYXBhY2lyXCI6IFsgMTA4NjMgXSxcblx0XCJhcEVcIjogWyAxMDg2NCBdLFxuXHRcImFwZVwiOiBbIDg3NzggXSxcblx0XCJhcGlkXCI6IFsgODc3OSBdLFxuXHRcImFwb3NcIjogWyAzOSBdLFxuXHRcIkFwcGx5RnVuY3Rpb25cIjogWyA4Mjg5IF0sXG5cdFwiYXBwcm94XCI6IFsgODc3NiBdLFxuXHRcImFwcHJveGVxXCI6IFsgODc3OCBdLFxuXHRcIkFyaW5nXCI6IFsgMTk3IF0sXG5cdFwiYXJpbmdcIjogWyAyMjkgXSxcblx0XCJBc2NyXCI6IFsgMTE5OTY0IF0sXG5cdFwiYXNjclwiOiBbIDExOTk5MCBdLFxuXHRcIkFzc2lnblwiOiBbIDg3ODggXSxcblx0XCJhc3RcIjogWyA0MiBdLFxuXHRcImFzeW1wXCI6IFsgODc3NiBdLFxuXHRcImFzeW1wZXFcIjogWyA4NzgxIF0sXG5cdFwiQXRpbGRlXCI6IFsgMTk1IF0sXG5cdFwiYXRpbGRlXCI6IFsgMjI3IF0sXG5cdFwiQXVtbFwiOiBbIDE5NiBdLFxuXHRcImF1bWxcIjogWyAyMjggXSxcblx0XCJhd2NvbmludFwiOiBbIDg3NTUgXSxcblx0XCJhd2ludFwiOiBbIDEwNzY5IF0sXG5cdFwiYmFja2NvbmdcIjogWyA4NzgwIF0sXG5cdFwiYmFja2Vwc2lsb25cIjogWyAxMDE0IF0sXG5cdFwiYmFja3ByaW1lXCI6IFsgODI0NSBdLFxuXHRcImJhY2tzaW1cIjogWyA4NzY1IF0sXG5cdFwiYmFja3NpbWVxXCI6IFsgODkwOSBdLFxuXHRcIkJhY2tzbGFzaFwiOiBbIDg3MjYgXSxcblx0XCJCYXJ2XCI6IFsgMTA5ODMgXSxcblx0XCJiYXJ2ZWVcIjogWyA4ODkzIF0sXG5cdFwiQmFyd2VkXCI6IFsgODk2NiBdLFxuXHRcImJhcndlZFwiOiBbIDg5NjUgXSxcblx0XCJiYXJ3ZWRnZVwiOiBbIDg5NjUgXSxcblx0XCJiYnJrXCI6IFsgOTE0MSBdLFxuXHRcImJicmt0YnJrXCI6IFsgOTE0MiBdLFxuXHRcImJjb25nXCI6IFsgODc4MCBdLFxuXHRcIkJjeVwiOiBbIDEwNDEgXSxcblx0XCJiY3lcIjogWyAxMDczIF0sXG5cdFwiYmRxdW9cIjogWyA4MjIyIF0sXG5cdFwiYmVjYXVzXCI6IFsgODc1NyBdLFxuXHRcIkJlY2F1c2VcIjogWyA4NzU3IF0sXG5cdFwiYmVjYXVzZVwiOiBbIDg3NTcgXSxcblx0XCJiZW1wdHl2XCI6IFsgMTA2NzIgXSxcblx0XCJiZXBzaVwiOiBbIDEwMTQgXSxcblx0XCJiZXJub3VcIjogWyA4NDkyIF0sXG5cdFwiQmVybm91bGxpc1wiOiBbIDg0OTIgXSxcblx0XCJCZXRhXCI6IFsgOTE0IF0sXG5cdFwiYmV0YVwiOiBbIDk0NiBdLFxuXHRcImJldGhcIjogWyA4NTAyIF0sXG5cdFwiYmV0d2VlblwiOiBbIDg4MTIgXSxcblx0XCJCZnJcIjogWyAxMjAwNjkgXSxcblx0XCJiZnJcIjogWyAxMjAwOTUgXSxcblx0XCJiaWdjYXBcIjogWyA4ODk4IF0sXG5cdFwiYmlnY2lyY1wiOiBbIDk3MTEgXSxcblx0XCJiaWdjdXBcIjogWyA4ODk5IF0sXG5cdFwiYmlnb2RvdFwiOiBbIDEwNzUyIF0sXG5cdFwiYmlnb3BsdXNcIjogWyAxMDc1MyBdLFxuXHRcImJpZ290aW1lc1wiOiBbIDEwNzU0IF0sXG5cdFwiYmlnc3FjdXBcIjogWyAxMDc1OCBdLFxuXHRcImJpZ3N0YXJcIjogWyA5NzMzIF0sXG5cdFwiYmlndHJpYW5nbGVkb3duXCI6IFsgOTY2MSBdLFxuXHRcImJpZ3RyaWFuZ2xldXBcIjogWyA5NjUxIF0sXG5cdFwiYmlndXBsdXNcIjogWyAxMDc1NiBdLFxuXHRcImJpZ3ZlZVwiOiBbIDg4OTcgXSxcblx0XCJiaWd3ZWRnZVwiOiBbIDg4OTYgXSxcblx0XCJia2Fyb3dcIjogWyAxMDUwOSBdLFxuXHRcImJsYWNrbG96ZW5nZVwiOiBbIDEwNzMxIF0sXG5cdFwiYmxhY2tzcXVhcmVcIjogWyA5NjQyIF0sXG5cdFwiYmxhY2t0cmlhbmdsZVwiOiBbIDk2NTIgXSxcblx0XCJibGFja3RyaWFuZ2xlZG93blwiOiBbIDk2NjIgXSxcblx0XCJibGFja3RyaWFuZ2xlbGVmdFwiOiBbIDk2NjYgXSxcblx0XCJibGFja3RyaWFuZ2xlcmlnaHRcIjogWyA5NjU2IF0sXG5cdFwiYmxhbmtcIjogWyA5MjUxIF0sXG5cdFwiYmxrMTJcIjogWyA5NjE4IF0sXG5cdFwiYmxrMTRcIjogWyA5NjE3IF0sXG5cdFwiYmxrMzRcIjogWyA5NjE5IF0sXG5cdFwiYmxvY2tcIjogWyA5NjA4IF0sXG5cdFwiYm5lXCI6IFsgNjEsIDg0MjEgXSxcblx0XCJibmVxdWl2XCI6IFsgODgwMSwgODQyMSBdLFxuXHRcImJOb3RcIjogWyAxMDk4OSBdLFxuXHRcImJub3RcIjogWyA4OTc2IF0sXG5cdFwiQm9wZlwiOiBbIDEyMDEyMSBdLFxuXHRcImJvcGZcIjogWyAxMjAxNDcgXSxcblx0XCJib3RcIjogWyA4ODY5IF0sXG5cdFwiYm90dG9tXCI6IFsgODg2OSBdLFxuXHRcImJvd3RpZVwiOiBbIDg5MDQgXSxcblx0XCJib3hib3hcIjogWyAxMDY5NyBdLFxuXHRcImJveERMXCI6IFsgOTU1OSBdLFxuXHRcImJveERsXCI6IFsgOTU1OCBdLFxuXHRcImJveGRMXCI6IFsgOTU1NyBdLFxuXHRcImJveGRsXCI6IFsgOTQ4OCBdLFxuXHRcImJveERSXCI6IFsgOTU1NiBdLFxuXHRcImJveERyXCI6IFsgOTU1NSBdLFxuXHRcImJveGRSXCI6IFsgOTU1NCBdLFxuXHRcImJveGRyXCI6IFsgOTQ4NCBdLFxuXHRcImJveEhcIjogWyA5NTUyIF0sXG5cdFwiYm94aFwiOiBbIDk0NzIgXSxcblx0XCJib3hIRFwiOiBbIDk1NzQgXSxcblx0XCJib3hIZFwiOiBbIDk1NzIgXSxcblx0XCJib3hoRFwiOiBbIDk1NzMgXSxcblx0XCJib3hoZFwiOiBbIDk1MTYgXSxcblx0XCJib3hIVVwiOiBbIDk1NzcgXSxcblx0XCJib3hIdVwiOiBbIDk1NzUgXSxcblx0XCJib3hoVVwiOiBbIDk1NzYgXSxcblx0XCJib3hodVwiOiBbIDk1MjQgXSxcblx0XCJib3htaW51c1wiOiBbIDg4NjMgXSxcblx0XCJib3hwbHVzXCI6IFsgODg2MiBdLFxuXHRcImJveHRpbWVzXCI6IFsgODg2NCBdLFxuXHRcImJveFVMXCI6IFsgOTU2NSBdLFxuXHRcImJveFVsXCI6IFsgOTU2NCBdLFxuXHRcImJveHVMXCI6IFsgOTU2MyBdLFxuXHRcImJveHVsXCI6IFsgOTQ5NiBdLFxuXHRcImJveFVSXCI6IFsgOTU2MiBdLFxuXHRcImJveFVyXCI6IFsgOTU2MSBdLFxuXHRcImJveHVSXCI6IFsgOTU2MCBdLFxuXHRcImJveHVyXCI6IFsgOTQ5MiBdLFxuXHRcImJveFZcIjogWyA5NTUzIF0sXG5cdFwiYm94dlwiOiBbIDk0NzQgXSxcblx0XCJib3hWSFwiOiBbIDk1ODAgXSxcblx0XCJib3hWaFwiOiBbIDk1NzkgXSxcblx0XCJib3h2SFwiOiBbIDk1NzggXSxcblx0XCJib3h2aFwiOiBbIDk1MzIgXSxcblx0XCJib3hWTFwiOiBbIDk1NzEgXSxcblx0XCJib3hWbFwiOiBbIDk1NzAgXSxcblx0XCJib3h2TFwiOiBbIDk1NjkgXSxcblx0XCJib3h2bFwiOiBbIDk1MDggXSxcblx0XCJib3hWUlwiOiBbIDk1NjggXSxcblx0XCJib3hWclwiOiBbIDk1NjcgXSxcblx0XCJib3h2UlwiOiBbIDk1NjYgXSxcblx0XCJib3h2clwiOiBbIDk1MDAgXSxcblx0XCJicHJpbWVcIjogWyA4MjQ1IF0sXG5cdFwiQnJldmVcIjogWyA3MjggXSxcblx0XCJicmV2ZVwiOiBbIDcyOCBdLFxuXHRcImJydmJhclwiOiBbIDE2NiBdLFxuXHRcIkJzY3JcIjogWyA4NDkyIF0sXG5cdFwiYnNjclwiOiBbIDExOTk5MSBdLFxuXHRcImJzZW1pXCI6IFsgODI3MSBdLFxuXHRcImJzaW1cIjogWyA4NzY1IF0sXG5cdFwiYnNpbWVcIjogWyA4OTA5IF0sXG5cdFwiYnNvbFwiOiBbIDkyIF0sXG5cdFwiYnNvbGJcIjogWyAxMDY5MyBdLFxuXHRcImJzb2xoc3ViXCI6IFsgMTAxODQgXSxcblx0XCJidWxsXCI6IFsgODIyNiBdLFxuXHRcImJ1bGxldFwiOiBbIDgyMjYgXSxcblx0XCJidW1wXCI6IFsgODc4MiBdLFxuXHRcImJ1bXBFXCI6IFsgMTA5MjYgXSxcblx0XCJidW1wZVwiOiBbIDg3ODMgXSxcblx0XCJCdW1wZXFcIjogWyA4NzgyIF0sXG5cdFwiYnVtcGVxXCI6IFsgODc4MyBdLFxuXHRcIkNhY3V0ZVwiOiBbIDI2MiBdLFxuXHRcImNhY3V0ZVwiOiBbIDI2MyBdLFxuXHRcIkNhcFwiOiBbIDg5MTQgXSxcblx0XCJjYXBcIjogWyA4NzQ1IF0sXG5cdFwiY2FwYW5kXCI6IFsgMTA4MjAgXSxcblx0XCJjYXBicmN1cFwiOiBbIDEwODI1IF0sXG5cdFwiY2FwY2FwXCI6IFsgMTA4MjcgXSxcblx0XCJjYXBjdXBcIjogWyAxMDgyMyBdLFxuXHRcImNhcGRvdFwiOiBbIDEwODE2IF0sXG5cdFwiQ2FwaXRhbERpZmZlcmVudGlhbERcIjogWyA4NTE3IF0sXG5cdFwiY2Fwc1wiOiBbIDg3NDUsIDY1MDI0IF0sXG5cdFwiY2FyZXRcIjogWyA4MjU3IF0sXG5cdFwiY2Fyb25cIjogWyA3MTEgXSxcblx0XCJDYXlsZXlzXCI6IFsgODQ5MyBdLFxuXHRcImNjYXBzXCI6IFsgMTA4MjkgXSxcblx0XCJDY2Fyb25cIjogWyAyNjggXSxcblx0XCJjY2Fyb25cIjogWyAyNjkgXSxcblx0XCJDY2VkaWxcIjogWyAxOTkgXSxcblx0XCJjY2VkaWxcIjogWyAyMzEgXSxcblx0XCJDY2lyY1wiOiBbIDI2NCBdLFxuXHRcImNjaXJjXCI6IFsgMjY1IF0sXG5cdFwiQ2NvbmludFwiOiBbIDg3NTIgXSxcblx0XCJjY3Vwc1wiOiBbIDEwODI4IF0sXG5cdFwiY2N1cHNzbVwiOiBbIDEwODMyIF0sXG5cdFwiQ2RvdFwiOiBbIDI2NiBdLFxuXHRcImNkb3RcIjogWyAyNjcgXSxcblx0XCJjZWRpbFwiOiBbIDE4NCBdLFxuXHRcIkNlZGlsbGFcIjogWyAxODQgXSxcblx0XCJjZW1wdHl2XCI6IFsgMTA2NzQgXSxcblx0XCJjZW50XCI6IFsgMTYyIF0sXG5cdFwiQ2VudGVyRG90XCI6IFsgMTgzIF0sXG5cdFwiY2VudGVyZG90XCI6IFsgMTgzIF0sXG5cdFwiQ2ZyXCI6IFsgODQ5MyBdLFxuXHRcImNmclwiOiBbIDEyMDA5NiBdLFxuXHRcIkNIY3lcIjogWyAxMDYzIF0sXG5cdFwiY2hjeVwiOiBbIDEwOTUgXSxcblx0XCJjaGVja1wiOiBbIDEwMDAzIF0sXG5cdFwiY2hlY2ttYXJrXCI6IFsgMTAwMDMgXSxcblx0XCJDaGlcIjogWyA5MzUgXSxcblx0XCJjaGlcIjogWyA5NjcgXSxcblx0XCJjaXJcIjogWyA5Njc1IF0sXG5cdFwiY2lyY1wiOiBbIDcxMCBdLFxuXHRcImNpcmNlcVwiOiBbIDg3OTEgXSxcblx0XCJjaXJjbGVhcnJvd2xlZnRcIjogWyA4NjM0IF0sXG5cdFwiY2lyY2xlYXJyb3dyaWdodFwiOiBbIDg2MzUgXSxcblx0XCJjaXJjbGVkYXN0XCI6IFsgODg1OSBdLFxuXHRcImNpcmNsZWRjaXJjXCI6IFsgODg1OCBdLFxuXHRcImNpcmNsZWRkYXNoXCI6IFsgODg2MSBdLFxuXHRcIkNpcmNsZURvdFwiOiBbIDg4NTcgXSxcblx0XCJjaXJjbGVkUlwiOiBbIDE3NCBdLFxuXHRcImNpcmNsZWRTXCI6IFsgOTQxNiBdLFxuXHRcIkNpcmNsZU1pbnVzXCI6IFsgODg1NCBdLFxuXHRcIkNpcmNsZVBsdXNcIjogWyA4ODUzIF0sXG5cdFwiQ2lyY2xlVGltZXNcIjogWyA4ODU1IF0sXG5cdFwiY2lyRVwiOiBbIDEwNjkxIF0sXG5cdFwiY2lyZVwiOiBbIDg3OTEgXSxcblx0XCJjaXJmbmludFwiOiBbIDEwNzY4IF0sXG5cdFwiY2lybWlkXCI6IFsgMTA5OTEgXSxcblx0XCJjaXJzY2lyXCI6IFsgMTA2OTAgXSxcblx0XCJDbG9ja3dpc2VDb250b3VySW50ZWdyYWxcIjogWyA4NzU0IF0sXG5cdFwiQ2xvc2VDdXJseURvdWJsZVF1b3RlXCI6IFsgODIyMSBdLFxuXHRcIkNsb3NlQ3VybHlRdW90ZVwiOiBbIDgyMTcgXSxcblx0XCJjbHVic1wiOiBbIDk4MjcgXSxcblx0XCJjbHVic3VpdFwiOiBbIDk4MjcgXSxcblx0XCJDb2xvblwiOiBbIDg3NTkgXSxcblx0XCJjb2xvblwiOiBbIDU4IF0sXG5cdFwiQ29sb25lXCI6IFsgMTA4NjggXSxcblx0XCJjb2xvbmVcIjogWyA4Nzg4IF0sXG5cdFwiY29sb25lcVwiOiBbIDg3ODggXSxcblx0XCJjb21tYVwiOiBbIDQ0IF0sXG5cdFwiY29tbWF0XCI6IFsgNjQgXSxcblx0XCJjb21wXCI6IFsgODcwNSBdLFxuXHRcImNvbXBmblwiOiBbIDg3MjggXSxcblx0XCJjb21wbGVtZW50XCI6IFsgODcwNSBdLFxuXHRcImNvbXBsZXhlc1wiOiBbIDg0NTAgXSxcblx0XCJjb25nXCI6IFsgODc3MyBdLFxuXHRcImNvbmdkb3RcIjogWyAxMDg2MSBdLFxuXHRcIkNvbmdydWVudFwiOiBbIDg4MDEgXSxcblx0XCJDb25pbnRcIjogWyA4NzUxIF0sXG5cdFwiY29uaW50XCI6IFsgODc1MCBdLFxuXHRcIkNvbnRvdXJJbnRlZ3JhbFwiOiBbIDg3NTAgXSxcblx0XCJDb3BmXCI6IFsgODQ1MCBdLFxuXHRcImNvcGZcIjogWyAxMjAxNDggXSxcblx0XCJjb3Byb2RcIjogWyA4NzIwIF0sXG5cdFwiQ29wcm9kdWN0XCI6IFsgODcyMCBdLFxuXHRcIkNPUFlcIjogWyAxNjkgXSxcblx0XCJjb3B5XCI6IFsgMTY5IF0sXG5cdFwiY29weXNyXCI6IFsgODQ3MSBdLFxuXHRcIkNvdW50ZXJDbG9ja3dpc2VDb250b3VySW50ZWdyYWxcIjogWyA4NzU1IF0sXG5cdFwiY3JhcnJcIjogWyA4NjI5IF0sXG5cdFwiQ3Jvc3NcIjogWyAxMDc5OSBdLFxuXHRcImNyb3NzXCI6IFsgMTAwMDcgXSxcblx0XCJDc2NyXCI6IFsgMTE5OTY2IF0sXG5cdFwiY3NjclwiOiBbIDExOTk5MiBdLFxuXHRcImNzdWJcIjogWyAxMDk1OSBdLFxuXHRcImNzdWJlXCI6IFsgMTA5NjEgXSxcblx0XCJjc3VwXCI6IFsgMTA5NjAgXSxcblx0XCJjc3VwZVwiOiBbIDEwOTYyIF0sXG5cdFwiY3Rkb3RcIjogWyA4OTQzIF0sXG5cdFwiY3VkYXJybFwiOiBbIDEwNTUyIF0sXG5cdFwiY3VkYXJyclwiOiBbIDEwNTQ5IF0sXG5cdFwiY3VlcHJcIjogWyA4OTI2IF0sXG5cdFwiY3Vlc2NcIjogWyA4OTI3IF0sXG5cdFwiY3VsYXJyXCI6IFsgODYzMCBdLFxuXHRcImN1bGFycnBcIjogWyAxMDU1NyBdLFxuXHRcIkN1cFwiOiBbIDg5MTUgXSxcblx0XCJjdXBcIjogWyA4NzQ2IF0sXG5cdFwiY3VwYnJjYXBcIjogWyAxMDgyNCBdLFxuXHRcIkN1cENhcFwiOiBbIDg3ODEgXSxcblx0XCJjdXBjYXBcIjogWyAxMDgyMiBdLFxuXHRcImN1cGN1cFwiOiBbIDEwODI2IF0sXG5cdFwiY3VwZG90XCI6IFsgODg0NSBdLFxuXHRcImN1cG9yXCI6IFsgMTA4MjEgXSxcblx0XCJjdXBzXCI6IFsgODc0NiwgNjUwMjQgXSxcblx0XCJjdXJhcnJcIjogWyA4NjMxIF0sXG5cdFwiY3VyYXJybVwiOiBbIDEwNTU2IF0sXG5cdFwiY3VybHllcXByZWNcIjogWyA4OTI2IF0sXG5cdFwiY3VybHllcXN1Y2NcIjogWyA4OTI3IF0sXG5cdFwiY3VybHl2ZWVcIjogWyA4OTEwIF0sXG5cdFwiY3VybHl3ZWRnZVwiOiBbIDg5MTEgXSxcblx0XCJjdXJyZW5cIjogWyAxNjQgXSxcblx0XCJjdXJ2ZWFycm93bGVmdFwiOiBbIDg2MzAgXSxcblx0XCJjdXJ2ZWFycm93cmlnaHRcIjogWyA4NjMxIF0sXG5cdFwiY3V2ZWVcIjogWyA4OTEwIF0sXG5cdFwiY3V3ZWRcIjogWyA4OTExIF0sXG5cdFwiY3djb25pbnRcIjogWyA4NzU0IF0sXG5cdFwiY3dpbnRcIjogWyA4NzUzIF0sXG5cdFwiY3lsY3R5XCI6IFsgOTAwNSBdLFxuXHRcIkRhZ2dlclwiOiBbIDgyMjUgXSxcblx0XCJkYWdnZXJcIjogWyA4MjI0IF0sXG5cdFwiZGFsZXRoXCI6IFsgODUwNCBdLFxuXHRcIkRhcnJcIjogWyA4NjA5IF0sXG5cdFwiZEFyclwiOiBbIDg2NTkgXSxcblx0XCJkYXJyXCI6IFsgODU5NSBdLFxuXHRcImRhc2hcIjogWyA4MjA4IF0sXG5cdFwiRGFzaHZcIjogWyAxMDk4MCBdLFxuXHRcImRhc2h2XCI6IFsgODg2NyBdLFxuXHRcImRia2Fyb3dcIjogWyAxMDUxMSBdLFxuXHRcImRibGFjXCI6IFsgNzMzIF0sXG5cdFwiRGNhcm9uXCI6IFsgMjcwIF0sXG5cdFwiZGNhcm9uXCI6IFsgMjcxIF0sXG5cdFwiRGN5XCI6IFsgMTA0NCBdLFxuXHRcImRjeVwiOiBbIDEwNzYgXSxcblx0XCJERFwiOiBbIDg1MTcgXSxcblx0XCJkZFwiOiBbIDg1MTggXSxcblx0XCJkZGFnZ2VyXCI6IFsgODIyNSBdLFxuXHRcImRkYXJyXCI6IFsgODY1MCBdLFxuXHRcIkREb3RyYWhkXCI6IFsgMTA1MTMgXSxcblx0XCJkZG90c2VxXCI6IFsgMTA4NzEgXSxcblx0XCJkZWdcIjogWyAxNzYgXSxcblx0XCJEZWxcIjogWyA4NzExIF0sXG5cdFwiRGVsdGFcIjogWyA5MTYgXSxcblx0XCJkZWx0YVwiOiBbIDk0OCBdLFxuXHRcImRlbXB0eXZcIjogWyAxMDY3MyBdLFxuXHRcImRmaXNodFwiOiBbIDEwNjIzIF0sXG5cdFwiRGZyXCI6IFsgMTIwMDcxIF0sXG5cdFwiZGZyXCI6IFsgMTIwMDk3IF0sXG5cdFwiZEhhclwiOiBbIDEwNTk3IF0sXG5cdFwiZGhhcmxcIjogWyA4NjQzIF0sXG5cdFwiZGhhcnJcIjogWyA4NjQyIF0sXG5cdFwiRGlhY3JpdGljYWxBY3V0ZVwiOiBbIDE4MCBdLFxuXHRcIkRpYWNyaXRpY2FsRG90XCI6IFsgNzI5IF0sXG5cdFwiRGlhY3JpdGljYWxEb3VibGVBY3V0ZVwiOiBbIDczMyBdLFxuXHRcIkRpYWNyaXRpY2FsR3JhdmVcIjogWyA5NiBdLFxuXHRcIkRpYWNyaXRpY2FsVGlsZGVcIjogWyA3MzIgXSxcblx0XCJkaWFtXCI6IFsgODkwMCBdLFxuXHRcIkRpYW1vbmRcIjogWyA4OTAwIF0sXG5cdFwiZGlhbW9uZFwiOiBbIDg5MDAgXSxcblx0XCJkaWFtb25kc3VpdFwiOiBbIDk4MzAgXSxcblx0XCJkaWFtc1wiOiBbIDk4MzAgXSxcblx0XCJkaWVcIjogWyAxNjggXSxcblx0XCJEaWZmZXJlbnRpYWxEXCI6IFsgODUxOCBdLFxuXHRcImRpZ2FtbWFcIjogWyA5ODkgXSxcblx0XCJkaXNpblwiOiBbIDg5NDYgXSxcblx0XCJkaXZcIjogWyAyNDcgXSxcblx0XCJkaXZpZGVcIjogWyAyNDcgXSxcblx0XCJkaXZpZGVvbnRpbWVzXCI6IFsgODkwMyBdLFxuXHRcImRpdm9ueFwiOiBbIDg5MDMgXSxcblx0XCJESmN5XCI6IFsgMTAyNiBdLFxuXHRcImRqY3lcIjogWyAxMTA2IF0sXG5cdFwiZGxjb3JuXCI6IFsgODk5MCBdLFxuXHRcImRsY3JvcFwiOiBbIDg5NzMgXSxcblx0XCJkb2xsYXJcIjogWyAzNiBdLFxuXHRcIkRvcGZcIjogWyAxMjAxMjMgXSxcblx0XCJkb3BmXCI6IFsgMTIwMTQ5IF0sXG5cdFwiRG90XCI6IFsgMTY4IF0sXG5cdFwiZG90XCI6IFsgNzI5IF0sXG5cdFwiRG90RG90XCI6IFsgODQxMiBdLFxuXHRcImRvdGVxXCI6IFsgODc4NCBdLFxuXHRcImRvdGVxZG90XCI6IFsgODc4NSBdLFxuXHRcIkRvdEVxdWFsXCI6IFsgODc4NCBdLFxuXHRcImRvdG1pbnVzXCI6IFsgODc2MCBdLFxuXHRcImRvdHBsdXNcIjogWyA4NzI0IF0sXG5cdFwiZG90c3F1YXJlXCI6IFsgODg2NSBdLFxuXHRcImRvdWJsZWJhcndlZGdlXCI6IFsgODk2NiBdLFxuXHRcIkRvdWJsZUNvbnRvdXJJbnRlZ3JhbFwiOiBbIDg3NTEgXSxcblx0XCJEb3VibGVEb3RcIjogWyAxNjggXSxcblx0XCJEb3VibGVEb3duQXJyb3dcIjogWyA4NjU5IF0sXG5cdFwiRG91YmxlTGVmdEFycm93XCI6IFsgODY1NiBdLFxuXHRcIkRvdWJsZUxlZnRSaWdodEFycm93XCI6IFsgODY2MCBdLFxuXHRcIkRvdWJsZUxlZnRUZWVcIjogWyAxMDk4MCBdLFxuXHRcIkRvdWJsZUxvbmdMZWZ0QXJyb3dcIjogWyAxMDIzMiBdLFxuXHRcIkRvdWJsZUxvbmdMZWZ0UmlnaHRBcnJvd1wiOiBbIDEwMjM0IF0sXG5cdFwiRG91YmxlTG9uZ1JpZ2h0QXJyb3dcIjogWyAxMDIzMyBdLFxuXHRcIkRvdWJsZVJpZ2h0QXJyb3dcIjogWyA4NjU4IF0sXG5cdFwiRG91YmxlUmlnaHRUZWVcIjogWyA4ODcyIF0sXG5cdFwiRG91YmxlVXBBcnJvd1wiOiBbIDg2NTcgXSxcblx0XCJEb3VibGVVcERvd25BcnJvd1wiOiBbIDg2NjEgXSxcblx0XCJEb3VibGVWZXJ0aWNhbEJhclwiOiBbIDg3NDEgXSxcblx0XCJEb3duQXJyb3dcIjogWyA4NTk1IF0sXG5cdFwiRG93bmFycm93XCI6IFsgODY1OSBdLFxuXHRcImRvd25hcnJvd1wiOiBbIDg1OTUgXSxcblx0XCJEb3duQXJyb3dCYXJcIjogWyAxMDUxNSBdLFxuXHRcIkRvd25BcnJvd1VwQXJyb3dcIjogWyA4NjkzIF0sXG5cdFwiRG93bkJyZXZlXCI6IFsgNzg1IF0sXG5cdFwiZG93bmRvd25hcnJvd3NcIjogWyA4NjUwIF0sXG5cdFwiZG93bmhhcnBvb25sZWZ0XCI6IFsgODY0MyBdLFxuXHRcImRvd25oYXJwb29ucmlnaHRcIjogWyA4NjQyIF0sXG5cdFwiRG93bkxlZnRSaWdodFZlY3RvclwiOiBbIDEwNTc2IF0sXG5cdFwiRG93bkxlZnRUZWVWZWN0b3JcIjogWyAxMDU5MCBdLFxuXHRcIkRvd25MZWZ0VmVjdG9yXCI6IFsgODYzNyBdLFxuXHRcIkRvd25MZWZ0VmVjdG9yQmFyXCI6IFsgMTA1ODIgXSxcblx0XCJEb3duUmlnaHRUZWVWZWN0b3JcIjogWyAxMDU5MSBdLFxuXHRcIkRvd25SaWdodFZlY3RvclwiOiBbIDg2NDEgXSxcblx0XCJEb3duUmlnaHRWZWN0b3JCYXJcIjogWyAxMDU4MyBdLFxuXHRcIkRvd25UZWVcIjogWyA4ODY4IF0sXG5cdFwiRG93blRlZUFycm93XCI6IFsgODYxNSBdLFxuXHRcImRyYmthcm93XCI6IFsgMTA1MTIgXSxcblx0XCJkcmNvcm5cIjogWyA4OTkxIF0sXG5cdFwiZHJjcm9wXCI6IFsgODk3MiBdLFxuXHRcIkRzY3JcIjogWyAxMTk5NjcgXSxcblx0XCJkc2NyXCI6IFsgMTE5OTkzIF0sXG5cdFwiRFNjeVwiOiBbIDEwMjkgXSxcblx0XCJkc2N5XCI6IFsgMTEwOSBdLFxuXHRcImRzb2xcIjogWyAxMDc0MiBdLFxuXHRcIkRzdHJva1wiOiBbIDI3MiBdLFxuXHRcImRzdHJva1wiOiBbIDI3MyBdLFxuXHRcImR0ZG90XCI6IFsgODk0NSBdLFxuXHRcImR0cmlcIjogWyA5NjYzIF0sXG5cdFwiZHRyaWZcIjogWyA5NjYyIF0sXG5cdFwiZHVhcnJcIjogWyA4NjkzIF0sXG5cdFwiZHVoYXJcIjogWyAxMDYwNyBdLFxuXHRcImR3YW5nbGVcIjogWyAxMDY2MiBdLFxuXHRcIkRaY3lcIjogWyAxMDM5IF0sXG5cdFwiZHpjeVwiOiBbIDExMTkgXSxcblx0XCJkemlncmFyclwiOiBbIDEwMjM5IF0sXG5cdFwiRWFjdXRlXCI6IFsgMjAxIF0sXG5cdFwiZWFjdXRlXCI6IFsgMjMzIF0sXG5cdFwiZWFzdGVyXCI6IFsgMTA4NjIgXSxcblx0XCJFY2Fyb25cIjogWyAyODIgXSxcblx0XCJlY2Fyb25cIjogWyAyODMgXSxcblx0XCJlY2lyXCI6IFsgODc5MCBdLFxuXHRcIkVjaXJjXCI6IFsgMjAyIF0sXG5cdFwiZWNpcmNcIjogWyAyMzQgXSxcblx0XCJlY29sb25cIjogWyA4Nzg5IF0sXG5cdFwiRWN5XCI6IFsgMTA2OSBdLFxuXHRcImVjeVwiOiBbIDExMDEgXSxcblx0XCJlRERvdFwiOiBbIDEwODcxIF0sXG5cdFwiRWRvdFwiOiBbIDI3OCBdLFxuXHRcImVEb3RcIjogWyA4Nzg1IF0sXG5cdFwiZWRvdFwiOiBbIDI3OSBdLFxuXHRcImVlXCI6IFsgODUxOSBdLFxuXHRcImVmRG90XCI6IFsgODc4NiBdLFxuXHRcIkVmclwiOiBbIDEyMDA3MiBdLFxuXHRcImVmclwiOiBbIDEyMDA5OCBdLFxuXHRcImVnXCI6IFsgMTA5MDYgXSxcblx0XCJFZ3JhdmVcIjogWyAyMDAgXSxcblx0XCJlZ3JhdmVcIjogWyAyMzIgXSxcblx0XCJlZ3NcIjogWyAxMDkwMiBdLFxuXHRcImVnc2RvdFwiOiBbIDEwOTA0IF0sXG5cdFwiZWxcIjogWyAxMDkwNSBdLFxuXHRcIkVsZW1lbnRcIjogWyA4NzEyIF0sXG5cdFwiZWxpbnRlcnNcIjogWyA5MTkxIF0sXG5cdFwiZWxsXCI6IFsgODQ2NyBdLFxuXHRcImVsc1wiOiBbIDEwOTAxIF0sXG5cdFwiZWxzZG90XCI6IFsgMTA5MDMgXSxcblx0XCJFbWFjclwiOiBbIDI3NCBdLFxuXHRcImVtYWNyXCI6IFsgMjc1IF0sXG5cdFwiZW1wdHlcIjogWyA4NzA5IF0sXG5cdFwiZW1wdHlzZXRcIjogWyA4NzA5IF0sXG5cdFwiRW1wdHlTbWFsbFNxdWFyZVwiOiBbIDk3MjMgXSxcblx0XCJlbXB0eXZcIjogWyA4NzA5IF0sXG5cdFwiRW1wdHlWZXJ5U21hbGxTcXVhcmVcIjogWyA5NjQzIF0sXG5cdFwiZW1zcFwiOiBbIDgxOTUgXSxcblx0XCJlbXNwMTNcIjogWyA4MTk2IF0sXG5cdFwiZW1zcDE0XCI6IFsgODE5NyBdLFxuXHRcIkVOR1wiOiBbIDMzMCBdLFxuXHRcImVuZ1wiOiBbIDMzMSBdLFxuXHRcImVuc3BcIjogWyA4MTk0IF0sXG5cdFwiRW9nb25cIjogWyAyODAgXSxcblx0XCJlb2dvblwiOiBbIDI4MSBdLFxuXHRcIkVvcGZcIjogWyAxMjAxMjQgXSxcblx0XCJlb3BmXCI6IFsgMTIwMTUwIF0sXG5cdFwiZXBhclwiOiBbIDg5MTcgXSxcblx0XCJlcGFyc2xcIjogWyAxMDcyMyBdLFxuXHRcImVwbHVzXCI6IFsgMTA4NjUgXSxcblx0XCJlcHNpXCI6IFsgOTQ5IF0sXG5cdFwiRXBzaWxvblwiOiBbIDkxNyBdLFxuXHRcImVwc2lsb25cIjogWyA5NDkgXSxcblx0XCJlcHNpdlwiOiBbIDEwMTMgXSxcblx0XCJlcWNpcmNcIjogWyA4NzkwIF0sXG5cdFwiZXFjb2xvblwiOiBbIDg3ODkgXSxcblx0XCJlcXNpbVwiOiBbIDg3NzAgXSxcblx0XCJlcXNsYW50Z3RyXCI6IFsgMTA5MDIgXSxcblx0XCJlcXNsYW50bGVzc1wiOiBbIDEwOTAxIF0sXG5cdFwiRXF1YWxcIjogWyAxMDg2OSBdLFxuXHRcImVxdWFsc1wiOiBbIDYxIF0sXG5cdFwiRXF1YWxUaWxkZVwiOiBbIDg3NzAgXSxcblx0XCJlcXVlc3RcIjogWyA4Nzk5IF0sXG5cdFwiRXF1aWxpYnJpdW1cIjogWyA4NjUyIF0sXG5cdFwiZXF1aXZcIjogWyA4ODAxIF0sXG5cdFwiZXF1aXZERFwiOiBbIDEwODcyIF0sXG5cdFwiZXF2cGFyc2xcIjogWyAxMDcyNSBdLFxuXHRcImVyYXJyXCI6IFsgMTA2MDkgXSxcblx0XCJlckRvdFwiOiBbIDg3ODcgXSxcblx0XCJFc2NyXCI6IFsgODQ5NiBdLFxuXHRcImVzY3JcIjogWyA4NDk1IF0sXG5cdFwiZXNkb3RcIjogWyA4Nzg0IF0sXG5cdFwiRXNpbVwiOiBbIDEwODY3IF0sXG5cdFwiZXNpbVwiOiBbIDg3NzAgXSxcblx0XCJFdGFcIjogWyA5MTkgXSxcblx0XCJldGFcIjogWyA5NTEgXSxcblx0XCJFVEhcIjogWyAyMDggXSxcblx0XCJldGhcIjogWyAyNDAgXSxcblx0XCJFdW1sXCI6IFsgMjAzIF0sXG5cdFwiZXVtbFwiOiBbIDIzNSBdLFxuXHRcImV1cm9cIjogWyA4MzY0IF0sXG5cdFwiZXhjbFwiOiBbIDMzIF0sXG5cdFwiZXhpc3RcIjogWyA4NzA3IF0sXG5cdFwiRXhpc3RzXCI6IFsgODcwNyBdLFxuXHRcImV4cGVjdGF0aW9uXCI6IFsgODQ5NiBdLFxuXHRcIkV4cG9uZW50aWFsRVwiOiBbIDg1MTkgXSxcblx0XCJleHBvbmVudGlhbGVcIjogWyA4NTE5IF0sXG5cdFwiZmFsbGluZ2RvdHNlcVwiOiBbIDg3ODYgXSxcblx0XCJGY3lcIjogWyAxMDYwIF0sXG5cdFwiZmN5XCI6IFsgMTA5MiBdLFxuXHRcImZlbWFsZVwiOiBbIDk3OTIgXSxcblx0XCJmZmlsaWdcIjogWyA2NDI1OSBdLFxuXHRcImZmbGlnXCI6IFsgNjQyNTYgXSxcblx0XCJmZmxsaWdcIjogWyA2NDI2MCBdLFxuXHRcIkZmclwiOiBbIDEyMDA3MyBdLFxuXHRcImZmclwiOiBbIDEyMDA5OSBdLFxuXHRcImZpbGlnXCI6IFsgNjQyNTcgXSxcblx0XCJGaWxsZWRTbWFsbFNxdWFyZVwiOiBbIDk3MjQgXSxcblx0XCJGaWxsZWRWZXJ5U21hbGxTcXVhcmVcIjogWyA5NjQyIF0sXG5cdFwiZmpsaWdcIjogWyAxMDIsIDEwNiBdLFxuXHRcImZsYXRcIjogWyA5ODM3IF0sXG5cdFwiZmxsaWdcIjogWyA2NDI1OCBdLFxuXHRcImZsdG5zXCI6IFsgOTY0OSBdLFxuXHRcImZub2ZcIjogWyA0MDIgXSxcblx0XCJGb3BmXCI6IFsgMTIwMTI1IF0sXG5cdFwiZm9wZlwiOiBbIDEyMDE1MSBdLFxuXHRcIkZvckFsbFwiOiBbIDg3MDQgXSxcblx0XCJmb3JhbGxcIjogWyA4NzA0IF0sXG5cdFwiZm9ya1wiOiBbIDg5MTYgXSxcblx0XCJmb3JrdlwiOiBbIDEwOTY5IF0sXG5cdFwiRm91cmllcnRyZlwiOiBbIDg0OTcgXSxcblx0XCJmcGFydGludFwiOiBbIDEwNzY1IF0sXG5cdFwiZnJhYzEyXCI6IFsgMTg5IF0sXG5cdFwiZnJhYzEzXCI6IFsgODUzMSBdLFxuXHRcImZyYWMxNFwiOiBbIDE4OCBdLFxuXHRcImZyYWMxNVwiOiBbIDg1MzMgXSxcblx0XCJmcmFjMTZcIjogWyA4NTM3IF0sXG5cdFwiZnJhYzE4XCI6IFsgODUzOSBdLFxuXHRcImZyYWMyM1wiOiBbIDg1MzIgXSxcblx0XCJmcmFjMjVcIjogWyA4NTM0IF0sXG5cdFwiZnJhYzM0XCI6IFsgMTkwIF0sXG5cdFwiZnJhYzM1XCI6IFsgODUzNSBdLFxuXHRcImZyYWMzOFwiOiBbIDg1NDAgXSxcblx0XCJmcmFjNDVcIjogWyA4NTM2IF0sXG5cdFwiZnJhYzU2XCI6IFsgODUzOCBdLFxuXHRcImZyYWM1OFwiOiBbIDg1NDEgXSxcblx0XCJmcmFjNzhcIjogWyA4NTQyIF0sXG5cdFwiZnJhc2xcIjogWyA4MjYwIF0sXG5cdFwiZnJvd25cIjogWyA4OTk0IF0sXG5cdFwiRnNjclwiOiBbIDg0OTcgXSxcblx0XCJmc2NyXCI6IFsgMTE5OTk1IF0sXG5cdFwiZ2FjdXRlXCI6IFsgNTAxIF0sXG5cdFwiR2FtbWFcIjogWyA5MTUgXSxcblx0XCJnYW1tYVwiOiBbIDk0NyBdLFxuXHRcIkdhbW1hZFwiOiBbIDk4OCBdLFxuXHRcImdhbW1hZFwiOiBbIDk4OSBdLFxuXHRcImdhcFwiOiBbIDEwODg2IF0sXG5cdFwiR2JyZXZlXCI6IFsgMjg2IF0sXG5cdFwiZ2JyZXZlXCI6IFsgMjg3IF0sXG5cdFwiR2NlZGlsXCI6IFsgMjkwIF0sXG5cdFwiR2NpcmNcIjogWyAyODQgXSxcblx0XCJnY2lyY1wiOiBbIDI4NSBdLFxuXHRcIkdjeVwiOiBbIDEwNDMgXSxcblx0XCJnY3lcIjogWyAxMDc1IF0sXG5cdFwiR2RvdFwiOiBbIDI4OCBdLFxuXHRcImdkb3RcIjogWyAyODkgXSxcblx0XCJnRVwiOiBbIDg4MDcgXSxcblx0XCJnZVwiOiBbIDg4MDUgXSxcblx0XCJnRWxcIjogWyAxMDg5MiBdLFxuXHRcImdlbFwiOiBbIDg5MjMgXSxcblx0XCJnZXFcIjogWyA4ODA1IF0sXG5cdFwiZ2VxcVwiOiBbIDg4MDcgXSxcblx0XCJnZXFzbGFudFwiOiBbIDEwODc4IF0sXG5cdFwiZ2VzXCI6IFsgMTA4NzggXSxcblx0XCJnZXNjY1wiOiBbIDEwOTIxIF0sXG5cdFwiZ2VzZG90XCI6IFsgMTA4ODAgXSxcblx0XCJnZXNkb3RvXCI6IFsgMTA4ODIgXSxcblx0XCJnZXNkb3RvbFwiOiBbIDEwODg0IF0sXG5cdFwiZ2VzbFwiOiBbIDg5MjMsIDY1MDI0IF0sXG5cdFwiZ2VzbGVzXCI6IFsgMTA5MDAgXSxcblx0XCJHZnJcIjogWyAxMjAwNzQgXSxcblx0XCJnZnJcIjogWyAxMjAxMDAgXSxcblx0XCJHZ1wiOiBbIDg5MjEgXSxcblx0XCJnZ1wiOiBbIDg4MTEgXSxcblx0XCJnZ2dcIjogWyA4OTIxIF0sXG5cdFwiZ2ltZWxcIjogWyA4NTAzIF0sXG5cdFwiR0pjeVwiOiBbIDEwMjcgXSxcblx0XCJnamN5XCI6IFsgMTEwNyBdLFxuXHRcImdsXCI6IFsgODgyMyBdLFxuXHRcImdsYVwiOiBbIDEwOTE3IF0sXG5cdFwiZ2xFXCI6IFsgMTA4OTggXSxcblx0XCJnbGpcIjogWyAxMDkxNiBdLFxuXHRcImduYXBcIjogWyAxMDg5MCBdLFxuXHRcImduYXBwcm94XCI6IFsgMTA4OTAgXSxcblx0XCJnbkVcIjogWyA4ODA5IF0sXG5cdFwiZ25lXCI6IFsgMTA4ODggXSxcblx0XCJnbmVxXCI6IFsgMTA4ODggXSxcblx0XCJnbmVxcVwiOiBbIDg4MDkgXSxcblx0XCJnbnNpbVwiOiBbIDg5MzUgXSxcblx0XCJHb3BmXCI6IFsgMTIwMTI2IF0sXG5cdFwiZ29wZlwiOiBbIDEyMDE1MiBdLFxuXHRcImdyYXZlXCI6IFsgOTYgXSxcblx0XCJHcmVhdGVyRXF1YWxcIjogWyA4ODA1IF0sXG5cdFwiR3JlYXRlckVxdWFsTGVzc1wiOiBbIDg5MjMgXSxcblx0XCJHcmVhdGVyRnVsbEVxdWFsXCI6IFsgODgwNyBdLFxuXHRcIkdyZWF0ZXJHcmVhdGVyXCI6IFsgMTA5MTQgXSxcblx0XCJHcmVhdGVyTGVzc1wiOiBbIDg4MjMgXSxcblx0XCJHcmVhdGVyU2xhbnRFcXVhbFwiOiBbIDEwODc4IF0sXG5cdFwiR3JlYXRlclRpbGRlXCI6IFsgODgxOSBdLFxuXHRcIkdzY3JcIjogWyAxMTk5NzAgXSxcblx0XCJnc2NyXCI6IFsgODQ1OCBdLFxuXHRcImdzaW1cIjogWyA4ODE5IF0sXG5cdFwiZ3NpbWVcIjogWyAxMDg5NCBdLFxuXHRcImdzaW1sXCI6IFsgMTA4OTYgXSxcblx0XCJHVFwiOiBbIDYyIF0sXG5cdFwiR3RcIjogWyA4ODExIF0sXG5cdFwiZ3RcIjogWyA2MiBdLFxuXHRcImd0Y2NcIjogWyAxMDkxOSBdLFxuXHRcImd0Y2lyXCI6IFsgMTA4NzQgXSxcblx0XCJndGRvdFwiOiBbIDg5MTkgXSxcblx0XCJndGxQYXJcIjogWyAxMDY0NSBdLFxuXHRcImd0cXVlc3RcIjogWyAxMDg3NiBdLFxuXHRcImd0cmFwcHJveFwiOiBbIDEwODg2IF0sXG5cdFwiZ3RyYXJyXCI6IFsgMTA2MTYgXSxcblx0XCJndHJkb3RcIjogWyA4OTE5IF0sXG5cdFwiZ3RyZXFsZXNzXCI6IFsgODkyMyBdLFxuXHRcImd0cmVxcWxlc3NcIjogWyAxMDg5MiBdLFxuXHRcImd0cmxlc3NcIjogWyA4ODIzIF0sXG5cdFwiZ3Ryc2ltXCI6IFsgODgxOSBdLFxuXHRcImd2ZXJ0bmVxcVwiOiBbIDg4MDksIDY1MDI0IF0sXG5cdFwiZ3ZuRVwiOiBbIDg4MDksIDY1MDI0IF0sXG5cdFwiSGFjZWtcIjogWyA3MTEgXSxcblx0XCJoYWlyc3BcIjogWyA4MjAyIF0sXG5cdFwiaGFsZlwiOiBbIDE4OSBdLFxuXHRcImhhbWlsdFwiOiBbIDg0NTkgXSxcblx0XCJIQVJEY3lcIjogWyAxMDY2IF0sXG5cdFwiaGFyZGN5XCI6IFsgMTA5OCBdLFxuXHRcImhBcnJcIjogWyA4NjYwIF0sXG5cdFwiaGFyclwiOiBbIDg1OTYgXSxcblx0XCJoYXJyY2lyXCI6IFsgMTA1NjggXSxcblx0XCJoYXJyd1wiOiBbIDg2MjEgXSxcblx0XCJIYXRcIjogWyA5NCBdLFxuXHRcImhiYXJcIjogWyA4NDYzIF0sXG5cdFwiSGNpcmNcIjogWyAyOTIgXSxcblx0XCJoY2lyY1wiOiBbIDI5MyBdLFxuXHRcImhlYXJ0c1wiOiBbIDk4MjkgXSxcblx0XCJoZWFydHN1aXRcIjogWyA5ODI5IF0sXG5cdFwiaGVsbGlwXCI6IFsgODIzMCBdLFxuXHRcImhlcmNvblwiOiBbIDg4ODkgXSxcblx0XCJIZnJcIjogWyA4NDYwIF0sXG5cdFwiaGZyXCI6IFsgMTIwMTAxIF0sXG5cdFwiSGlsYmVydFNwYWNlXCI6IFsgODQ1OSBdLFxuXHRcImhrc2Vhcm93XCI6IFsgMTA1MzMgXSxcblx0XCJoa3N3YXJvd1wiOiBbIDEwNTM0IF0sXG5cdFwiaG9hcnJcIjogWyA4NzAzIF0sXG5cdFwiaG9tdGh0XCI6IFsgODc2MyBdLFxuXHRcImhvb2tsZWZ0YXJyb3dcIjogWyA4NjE3IF0sXG5cdFwiaG9va3JpZ2h0YXJyb3dcIjogWyA4NjE4IF0sXG5cdFwiSG9wZlwiOiBbIDg0NjEgXSxcblx0XCJob3BmXCI6IFsgMTIwMTUzIF0sXG5cdFwiaG9yYmFyXCI6IFsgODIxMyBdLFxuXHRcIkhvcml6b250YWxMaW5lXCI6IFsgOTQ3MiBdLFxuXHRcIkhzY3JcIjogWyA4NDU5IF0sXG5cdFwiaHNjclwiOiBbIDExOTk5NyBdLFxuXHRcImhzbGFzaFwiOiBbIDg0NjMgXSxcblx0XCJIc3Ryb2tcIjogWyAyOTQgXSxcblx0XCJoc3Ryb2tcIjogWyAyOTUgXSxcblx0XCJIdW1wRG93bkh1bXBcIjogWyA4NzgyIF0sXG5cdFwiSHVtcEVxdWFsXCI6IFsgODc4MyBdLFxuXHRcImh5YnVsbFwiOiBbIDgyNTkgXSxcblx0XCJoeXBoZW5cIjogWyA4MjA4IF0sXG5cdFwiSWFjdXRlXCI6IFsgMjA1IF0sXG5cdFwiaWFjdXRlXCI6IFsgMjM3IF0sXG5cdFwiaWNcIjogWyA4MjkxIF0sXG5cdFwiSWNpcmNcIjogWyAyMDYgXSxcblx0XCJpY2lyY1wiOiBbIDIzOCBdLFxuXHRcIkljeVwiOiBbIDEwNDggXSxcblx0XCJpY3lcIjogWyAxMDgwIF0sXG5cdFwiSWRvdFwiOiBbIDMwNCBdLFxuXHRcIklFY3lcIjogWyAxMDQ1IF0sXG5cdFwiaWVjeVwiOiBbIDEwNzcgXSxcblx0XCJpZXhjbFwiOiBbIDE2MSBdLFxuXHRcImlmZlwiOiBbIDg2NjAgXSxcblx0XCJJZnJcIjogWyA4NDY1IF0sXG5cdFwiaWZyXCI6IFsgMTIwMTAyIF0sXG5cdFwiSWdyYXZlXCI6IFsgMjA0IF0sXG5cdFwiaWdyYXZlXCI6IFsgMjM2IF0sXG5cdFwiaWlcIjogWyA4NTIwIF0sXG5cdFwiaWlpaW50XCI6IFsgMTA3NjQgXSxcblx0XCJpaWludFwiOiBbIDg3NDkgXSxcblx0XCJpaW5maW5cIjogWyAxMDcxNiBdLFxuXHRcImlpb3RhXCI6IFsgODQ4OSBdLFxuXHRcIklKbGlnXCI6IFsgMzA2IF0sXG5cdFwiaWpsaWdcIjogWyAzMDcgXSxcblx0XCJJbVwiOiBbIDg0NjUgXSxcblx0XCJJbWFjclwiOiBbIDI5OCBdLFxuXHRcImltYWNyXCI6IFsgMjk5IF0sXG5cdFwiaW1hZ2VcIjogWyA4NDY1IF0sXG5cdFwiSW1hZ2luYXJ5SVwiOiBbIDg1MjAgXSxcblx0XCJpbWFnbGluZVwiOiBbIDg0NjQgXSxcblx0XCJpbWFncGFydFwiOiBbIDg0NjUgXSxcblx0XCJpbWF0aFwiOiBbIDMwNSBdLFxuXHRcImltb2ZcIjogWyA4ODg3IF0sXG5cdFwiaW1wZWRcIjogWyA0MzcgXSxcblx0XCJJbXBsaWVzXCI6IFsgODY1OCBdLFxuXHRcImluXCI6IFsgODcxMiBdLFxuXHRcImluY2FyZVwiOiBbIDg0NTMgXSxcblx0XCJpbmZpblwiOiBbIDg3MzQgXSxcblx0XCJpbmZpbnRpZVwiOiBbIDEwNzE3IF0sXG5cdFwiaW5vZG90XCI6IFsgMzA1IF0sXG5cdFwiSW50XCI6IFsgODc0OCBdLFxuXHRcImludFwiOiBbIDg3NDcgXSxcblx0XCJpbnRjYWxcIjogWyA4ODkwIF0sXG5cdFwiaW50ZWdlcnNcIjogWyA4NDg0IF0sXG5cdFwiSW50ZWdyYWxcIjogWyA4NzQ3IF0sXG5cdFwiaW50ZXJjYWxcIjogWyA4ODkwIF0sXG5cdFwiSW50ZXJzZWN0aW9uXCI6IFsgODg5OCBdLFxuXHRcImludGxhcmhrXCI6IFsgMTA3NzUgXSxcblx0XCJpbnRwcm9kXCI6IFsgMTA4MTIgXSxcblx0XCJJbnZpc2libGVDb21tYVwiOiBbIDgyOTEgXSxcblx0XCJJbnZpc2libGVUaW1lc1wiOiBbIDgyOTAgXSxcblx0XCJJT2N5XCI6IFsgMTAyNSBdLFxuXHRcImlvY3lcIjogWyAxMTA1IF0sXG5cdFwiSW9nb25cIjogWyAzMDIgXSxcblx0XCJpb2dvblwiOiBbIDMwMyBdLFxuXHRcIklvcGZcIjogWyAxMjAxMjggXSxcblx0XCJpb3BmXCI6IFsgMTIwMTU0IF0sXG5cdFwiSW90YVwiOiBbIDkyMSBdLFxuXHRcImlvdGFcIjogWyA5NTMgXSxcblx0XCJpcHJvZFwiOiBbIDEwODEyIF0sXG5cdFwiaXF1ZXN0XCI6IFsgMTkxIF0sXG5cdFwiSXNjclwiOiBbIDg0NjQgXSxcblx0XCJpc2NyXCI6IFsgMTE5OTk4IF0sXG5cdFwiaXNpblwiOiBbIDg3MTIgXSxcblx0XCJpc2luZG90XCI6IFsgODk0OSBdLFxuXHRcImlzaW5FXCI6IFsgODk1MyBdLFxuXHRcImlzaW5zXCI6IFsgODk0OCBdLFxuXHRcImlzaW5zdlwiOiBbIDg5NDcgXSxcblx0XCJpc2ludlwiOiBbIDg3MTIgXSxcblx0XCJpdFwiOiBbIDgyOTAgXSxcblx0XCJJdGlsZGVcIjogWyAyOTYgXSxcblx0XCJpdGlsZGVcIjogWyAyOTcgXSxcblx0XCJJdWtjeVwiOiBbIDEwMzAgXSxcblx0XCJpdWtjeVwiOiBbIDExMTAgXSxcblx0XCJJdW1sXCI6IFsgMjA3IF0sXG5cdFwiaXVtbFwiOiBbIDIzOSBdLFxuXHRcIkpjaXJjXCI6IFsgMzA4IF0sXG5cdFwiamNpcmNcIjogWyAzMDkgXSxcblx0XCJKY3lcIjogWyAxMDQ5IF0sXG5cdFwiamN5XCI6IFsgMTA4MSBdLFxuXHRcIkpmclwiOiBbIDEyMDA3NyBdLFxuXHRcImpmclwiOiBbIDEyMDEwMyBdLFxuXHRcImptYXRoXCI6IFsgNTY3IF0sXG5cdFwiSm9wZlwiOiBbIDEyMDEyOSBdLFxuXHRcImpvcGZcIjogWyAxMjAxNTUgXSxcblx0XCJKc2NyXCI6IFsgMTE5OTczIF0sXG5cdFwianNjclwiOiBbIDExOTk5OSBdLFxuXHRcIkpzZXJjeVwiOiBbIDEwMzIgXSxcblx0XCJqc2VyY3lcIjogWyAxMTEyIF0sXG5cdFwiSnVrY3lcIjogWyAxMDI4IF0sXG5cdFwianVrY3lcIjogWyAxMTA4IF0sXG5cdFwiS2FwcGFcIjogWyA5MjIgXSxcblx0XCJrYXBwYVwiOiBbIDk1NCBdLFxuXHRcImthcHBhdlwiOiBbIDEwMDggXSxcblx0XCJLY2VkaWxcIjogWyAzMTAgXSxcblx0XCJrY2VkaWxcIjogWyAzMTEgXSxcblx0XCJLY3lcIjogWyAxMDUwIF0sXG5cdFwia2N5XCI6IFsgMTA4MiBdLFxuXHRcIktmclwiOiBbIDEyMDA3OCBdLFxuXHRcImtmclwiOiBbIDEyMDEwNCBdLFxuXHRcImtncmVlblwiOiBbIDMxMiBdLFxuXHRcIktIY3lcIjogWyAxMDYxIF0sXG5cdFwia2hjeVwiOiBbIDEwOTMgXSxcblx0XCJLSmN5XCI6IFsgMTAzNiBdLFxuXHRcImtqY3lcIjogWyAxMTE2IF0sXG5cdFwiS29wZlwiOiBbIDEyMDEzMCBdLFxuXHRcImtvcGZcIjogWyAxMjAxNTYgXSxcblx0XCJLc2NyXCI6IFsgMTE5OTc0IF0sXG5cdFwia3NjclwiOiBbIDEyMDAwMCBdLFxuXHRcImxBYXJyXCI6IFsgODY2NiBdLFxuXHRcIkxhY3V0ZVwiOiBbIDMxMyBdLFxuXHRcImxhY3V0ZVwiOiBbIDMxNCBdLFxuXHRcImxhZW1wdHl2XCI6IFsgMTA2NzYgXSxcblx0XCJsYWdyYW5cIjogWyA4NDY2IF0sXG5cdFwiTGFtYmRhXCI6IFsgOTIzIF0sXG5cdFwibGFtYmRhXCI6IFsgOTU1IF0sXG5cdFwiTGFuZ1wiOiBbIDEwMjE4IF0sXG5cdFwibGFuZ1wiOiBbIDEwMjE2IF0sXG5cdFwibGFuZ2RcIjogWyAxMDY0MSBdLFxuXHRcImxhbmdsZVwiOiBbIDEwMjE2IF0sXG5cdFwibGFwXCI6IFsgMTA4ODUgXSxcblx0XCJMYXBsYWNldHJmXCI6IFsgODQ2NiBdLFxuXHRcImxhcXVvXCI6IFsgMTcxIF0sXG5cdFwiTGFyclwiOiBbIDg2MDYgXSxcblx0XCJsQXJyXCI6IFsgODY1NiBdLFxuXHRcImxhcnJcIjogWyA4NTkyIF0sXG5cdFwibGFycmJcIjogWyA4Njc2IF0sXG5cdFwibGFycmJmc1wiOiBbIDEwNTI3IF0sXG5cdFwibGFycmZzXCI6IFsgMTA1MjUgXSxcblx0XCJsYXJyaGtcIjogWyA4NjE3IF0sXG5cdFwibGFycmxwXCI6IFsgODYxOSBdLFxuXHRcImxhcnJwbFwiOiBbIDEwNTUzIF0sXG5cdFwibGFycnNpbVwiOiBbIDEwNjExIF0sXG5cdFwibGFycnRsXCI6IFsgODYxMCBdLFxuXHRcImxhdFwiOiBbIDEwOTIzIF0sXG5cdFwibEF0YWlsXCI6IFsgMTA1MjMgXSxcblx0XCJsYXRhaWxcIjogWyAxMDUyMSBdLFxuXHRcImxhdGVcIjogWyAxMDkyNSBdLFxuXHRcImxhdGVzXCI6IFsgMTA5MjUsIDY1MDI0IF0sXG5cdFwibEJhcnJcIjogWyAxMDUxMCBdLFxuXHRcImxiYXJyXCI6IFsgMTA1MDggXSxcblx0XCJsYmJya1wiOiBbIDEwMDk4IF0sXG5cdFwibGJyYWNlXCI6IFsgMTIzIF0sXG5cdFwibGJyYWNrXCI6IFsgOTEgXSxcblx0XCJsYnJrZVwiOiBbIDEwNjM1IF0sXG5cdFwibGJya3NsZFwiOiBbIDEwNjM5IF0sXG5cdFwibGJya3NsdVwiOiBbIDEwNjM3IF0sXG5cdFwiTGNhcm9uXCI6IFsgMzE3IF0sXG5cdFwibGNhcm9uXCI6IFsgMzE4IF0sXG5cdFwiTGNlZGlsXCI6IFsgMzE1IF0sXG5cdFwibGNlZGlsXCI6IFsgMzE2IF0sXG5cdFwibGNlaWxcIjogWyA4OTY4IF0sXG5cdFwibGN1YlwiOiBbIDEyMyBdLFxuXHRcIkxjeVwiOiBbIDEwNTEgXSxcblx0XCJsY3lcIjogWyAxMDgzIF0sXG5cdFwibGRjYVwiOiBbIDEwNTUwIF0sXG5cdFwibGRxdW9cIjogWyA4MjIwIF0sXG5cdFwibGRxdW9yXCI6IFsgODIyMiBdLFxuXHRcImxkcmRoYXJcIjogWyAxMDU5OSBdLFxuXHRcImxkcnVzaGFyXCI6IFsgMTA1NzEgXSxcblx0XCJsZHNoXCI6IFsgODYyNiBdLFxuXHRcImxFXCI6IFsgODgwNiBdLFxuXHRcImxlXCI6IFsgODgwNCBdLFxuXHRcIkxlZnRBbmdsZUJyYWNrZXRcIjogWyAxMDIxNiBdLFxuXHRcIkxlZnRBcnJvd1wiOiBbIDg1OTIgXSxcblx0XCJMZWZ0YXJyb3dcIjogWyA4NjU2IF0sXG5cdFwibGVmdGFycm93XCI6IFsgODU5MiBdLFxuXHRcIkxlZnRBcnJvd0JhclwiOiBbIDg2NzYgXSxcblx0XCJMZWZ0QXJyb3dSaWdodEFycm93XCI6IFsgODY0NiBdLFxuXHRcImxlZnRhcnJvd3RhaWxcIjogWyA4NjEwIF0sXG5cdFwiTGVmdENlaWxpbmdcIjogWyA4OTY4IF0sXG5cdFwiTGVmdERvdWJsZUJyYWNrZXRcIjogWyAxMDIxNCBdLFxuXHRcIkxlZnREb3duVGVlVmVjdG9yXCI6IFsgMTA1OTMgXSxcblx0XCJMZWZ0RG93blZlY3RvclwiOiBbIDg2NDMgXSxcblx0XCJMZWZ0RG93blZlY3RvckJhclwiOiBbIDEwNTg1IF0sXG5cdFwiTGVmdEZsb29yXCI6IFsgODk3MCBdLFxuXHRcImxlZnRoYXJwb29uZG93blwiOiBbIDg2MzcgXSxcblx0XCJsZWZ0aGFycG9vbnVwXCI6IFsgODYzNiBdLFxuXHRcImxlZnRsZWZ0YXJyb3dzXCI6IFsgODY0NyBdLFxuXHRcIkxlZnRSaWdodEFycm93XCI6IFsgODU5NiBdLFxuXHRcIkxlZnRyaWdodGFycm93XCI6IFsgODY2MCBdLFxuXHRcImxlZnRyaWdodGFycm93XCI6IFsgODU5NiBdLFxuXHRcImxlZnRyaWdodGFycm93c1wiOiBbIDg2NDYgXSxcblx0XCJsZWZ0cmlnaHRoYXJwb29uc1wiOiBbIDg2NTEgXSxcblx0XCJsZWZ0cmlnaHRzcXVpZ2Fycm93XCI6IFsgODYyMSBdLFxuXHRcIkxlZnRSaWdodFZlY3RvclwiOiBbIDEwNTc0IF0sXG5cdFwiTGVmdFRlZVwiOiBbIDg4NjcgXSxcblx0XCJMZWZ0VGVlQXJyb3dcIjogWyA4NjEyIF0sXG5cdFwiTGVmdFRlZVZlY3RvclwiOiBbIDEwNTg2IF0sXG5cdFwibGVmdHRocmVldGltZXNcIjogWyA4OTA3IF0sXG5cdFwiTGVmdFRyaWFuZ2xlXCI6IFsgODg4MiBdLFxuXHRcIkxlZnRUcmlhbmdsZUJhclwiOiBbIDEwNzAzIF0sXG5cdFwiTGVmdFRyaWFuZ2xlRXF1YWxcIjogWyA4ODg0IF0sXG5cdFwiTGVmdFVwRG93blZlY3RvclwiOiBbIDEwNTc3IF0sXG5cdFwiTGVmdFVwVGVlVmVjdG9yXCI6IFsgMTA1OTIgXSxcblx0XCJMZWZ0VXBWZWN0b3JcIjogWyA4NjM5IF0sXG5cdFwiTGVmdFVwVmVjdG9yQmFyXCI6IFsgMTA1ODQgXSxcblx0XCJMZWZ0VmVjdG9yXCI6IFsgODYzNiBdLFxuXHRcIkxlZnRWZWN0b3JCYXJcIjogWyAxMDU3OCBdLFxuXHRcImxFZ1wiOiBbIDEwODkxIF0sXG5cdFwibGVnXCI6IFsgODkyMiBdLFxuXHRcImxlcVwiOiBbIDg4MDQgXSxcblx0XCJsZXFxXCI6IFsgODgwNiBdLFxuXHRcImxlcXNsYW50XCI6IFsgMTA4NzcgXSxcblx0XCJsZXNcIjogWyAxMDg3NyBdLFxuXHRcImxlc2NjXCI6IFsgMTA5MjAgXSxcblx0XCJsZXNkb3RcIjogWyAxMDg3OSBdLFxuXHRcImxlc2RvdG9cIjogWyAxMDg4MSBdLFxuXHRcImxlc2RvdG9yXCI6IFsgMTA4ODMgXSxcblx0XCJsZXNnXCI6IFsgODkyMiwgNjUwMjQgXSxcblx0XCJsZXNnZXNcIjogWyAxMDg5OSBdLFxuXHRcImxlc3NhcHByb3hcIjogWyAxMDg4NSBdLFxuXHRcImxlc3Nkb3RcIjogWyA4OTE4IF0sXG5cdFwibGVzc2VxZ3RyXCI6IFsgODkyMiBdLFxuXHRcImxlc3NlcXFndHJcIjogWyAxMDg5MSBdLFxuXHRcIkxlc3NFcXVhbEdyZWF0ZXJcIjogWyA4OTIyIF0sXG5cdFwiTGVzc0Z1bGxFcXVhbFwiOiBbIDg4MDYgXSxcblx0XCJMZXNzR3JlYXRlclwiOiBbIDg4MjIgXSxcblx0XCJsZXNzZ3RyXCI6IFsgODgyMiBdLFxuXHRcIkxlc3NMZXNzXCI6IFsgMTA5MTMgXSxcblx0XCJsZXNzc2ltXCI6IFsgODgxOCBdLFxuXHRcIkxlc3NTbGFudEVxdWFsXCI6IFsgMTA4NzcgXSxcblx0XCJMZXNzVGlsZGVcIjogWyA4ODE4IF0sXG5cdFwibGZpc2h0XCI6IFsgMTA2MjAgXSxcblx0XCJsZmxvb3JcIjogWyA4OTcwIF0sXG5cdFwiTGZyXCI6IFsgMTIwMDc5IF0sXG5cdFwibGZyXCI6IFsgMTIwMTA1IF0sXG5cdFwibGdcIjogWyA4ODIyIF0sXG5cdFwibGdFXCI6IFsgMTA4OTcgXSxcblx0XCJsSGFyXCI6IFsgMTA1OTQgXSxcblx0XCJsaGFyZFwiOiBbIDg2MzcgXSxcblx0XCJsaGFydVwiOiBbIDg2MzYgXSxcblx0XCJsaGFydWxcIjogWyAxMDYwMiBdLFxuXHRcImxoYmxrXCI6IFsgOTYwNCBdLFxuXHRcIkxKY3lcIjogWyAxMDMzIF0sXG5cdFwibGpjeVwiOiBbIDExMTMgXSxcblx0XCJMbFwiOiBbIDg5MjAgXSxcblx0XCJsbFwiOiBbIDg4MTAgXSxcblx0XCJsbGFyclwiOiBbIDg2NDcgXSxcblx0XCJsbGNvcm5lclwiOiBbIDg5OTAgXSxcblx0XCJMbGVmdGFycm93XCI6IFsgODY2NiBdLFxuXHRcImxsaGFyZFwiOiBbIDEwNjAzIF0sXG5cdFwibGx0cmlcIjogWyA5NzIyIF0sXG5cdFwiTG1pZG90XCI6IFsgMzE5IF0sXG5cdFwibG1pZG90XCI6IFsgMzIwIF0sXG5cdFwibG1vdXN0XCI6IFsgOTEzNiBdLFxuXHRcImxtb3VzdGFjaGVcIjogWyA5MTM2IF0sXG5cdFwibG5hcFwiOiBbIDEwODg5IF0sXG5cdFwibG5hcHByb3hcIjogWyAxMDg4OSBdLFxuXHRcImxuRVwiOiBbIDg4MDggXSxcblx0XCJsbmVcIjogWyAxMDg4NyBdLFxuXHRcImxuZXFcIjogWyAxMDg4NyBdLFxuXHRcImxuZXFxXCI6IFsgODgwOCBdLFxuXHRcImxuc2ltXCI6IFsgODkzNCBdLFxuXHRcImxvYW5nXCI6IFsgMTAyMjAgXSxcblx0XCJsb2FyclwiOiBbIDg3MDEgXSxcblx0XCJsb2Jya1wiOiBbIDEwMjE0IF0sXG5cdFwiTG9uZ0xlZnRBcnJvd1wiOiBbIDEwMjI5IF0sXG5cdFwiTG9uZ2xlZnRhcnJvd1wiOiBbIDEwMjMyIF0sXG5cdFwibG9uZ2xlZnRhcnJvd1wiOiBbIDEwMjI5IF0sXG5cdFwiTG9uZ0xlZnRSaWdodEFycm93XCI6IFsgMTAyMzEgXSxcblx0XCJMb25nbGVmdHJpZ2h0YXJyb3dcIjogWyAxMDIzNCBdLFxuXHRcImxvbmdsZWZ0cmlnaHRhcnJvd1wiOiBbIDEwMjMxIF0sXG5cdFwibG9uZ21hcHN0b1wiOiBbIDEwMjM2IF0sXG5cdFwiTG9uZ1JpZ2h0QXJyb3dcIjogWyAxMDIzMCBdLFxuXHRcIkxvbmdyaWdodGFycm93XCI6IFsgMTAyMzMgXSxcblx0XCJsb25ncmlnaHRhcnJvd1wiOiBbIDEwMjMwIF0sXG5cdFwibG9vcGFycm93bGVmdFwiOiBbIDg2MTkgXSxcblx0XCJsb29wYXJyb3dyaWdodFwiOiBbIDg2MjAgXSxcblx0XCJsb3BhclwiOiBbIDEwNjI5IF0sXG5cdFwiTG9wZlwiOiBbIDEyMDEzMSBdLFxuXHRcImxvcGZcIjogWyAxMjAxNTcgXSxcblx0XCJsb3BsdXNcIjogWyAxMDc5NyBdLFxuXHRcImxvdGltZXNcIjogWyAxMDgwNCBdLFxuXHRcImxvd2FzdFwiOiBbIDg3MjcgXSxcblx0XCJsb3diYXJcIjogWyA5NSBdLFxuXHRcIkxvd2VyTGVmdEFycm93XCI6IFsgODYwMSBdLFxuXHRcIkxvd2VyUmlnaHRBcnJvd1wiOiBbIDg2MDAgXSxcblx0XCJsb3pcIjogWyA5Njc0IF0sXG5cdFwibG96ZW5nZVwiOiBbIDk2NzQgXSxcblx0XCJsb3pmXCI6IFsgMTA3MzEgXSxcblx0XCJscGFyXCI6IFsgNDAgXSxcblx0XCJscGFybHRcIjogWyAxMDY0MyBdLFxuXHRcImxyYXJyXCI6IFsgODY0NiBdLFxuXHRcImxyY29ybmVyXCI6IFsgODk5MSBdLFxuXHRcImxyaGFyXCI6IFsgODY1MSBdLFxuXHRcImxyaGFyZFwiOiBbIDEwNjA1IF0sXG5cdFwibHJtXCI6IFsgODIwNiBdLFxuXHRcImxydHJpXCI6IFsgODg5NSBdLFxuXHRcImxzYXF1b1wiOiBbIDgyNDkgXSxcblx0XCJMc2NyXCI6IFsgODQ2NiBdLFxuXHRcImxzY3JcIjogWyAxMjAwMDEgXSxcblx0XCJMc2hcIjogWyA4NjI0IF0sXG5cdFwibHNoXCI6IFsgODYyNCBdLFxuXHRcImxzaW1cIjogWyA4ODE4IF0sXG5cdFwibHNpbWVcIjogWyAxMDg5MyBdLFxuXHRcImxzaW1nXCI6IFsgMTA4OTUgXSxcblx0XCJsc3FiXCI6IFsgOTEgXSxcblx0XCJsc3F1b1wiOiBbIDgyMTYgXSxcblx0XCJsc3F1b3JcIjogWyA4MjE4IF0sXG5cdFwiTHN0cm9rXCI6IFsgMzIxIF0sXG5cdFwibHN0cm9rXCI6IFsgMzIyIF0sXG5cdFwiTFRcIjogWyA2MCBdLFxuXHRcIkx0XCI6IFsgODgxMCBdLFxuXHRcImx0XCI6IFsgNjAgXSxcblx0XCJsdGNjXCI6IFsgMTA5MTggXSxcblx0XCJsdGNpclwiOiBbIDEwODczIF0sXG5cdFwibHRkb3RcIjogWyA4OTE4IF0sXG5cdFwibHRocmVlXCI6IFsgODkwNyBdLFxuXHRcImx0aW1lc1wiOiBbIDg5MDUgXSxcblx0XCJsdGxhcnJcIjogWyAxMDYxNCBdLFxuXHRcImx0cXVlc3RcIjogWyAxMDg3NSBdLFxuXHRcImx0cmlcIjogWyA5NjY3IF0sXG5cdFwibHRyaWVcIjogWyA4ODg0IF0sXG5cdFwibHRyaWZcIjogWyA5NjY2IF0sXG5cdFwibHRyUGFyXCI6IFsgMTA2NDYgXSxcblx0XCJsdXJkc2hhclwiOiBbIDEwNTcwIF0sXG5cdFwibHVydWhhclwiOiBbIDEwNTk4IF0sXG5cdFwibHZlcnRuZXFxXCI6IFsgODgwOCwgNjUwMjQgXSxcblx0XCJsdm5FXCI6IFsgODgwOCwgNjUwMjQgXSxcblx0XCJtYWNyXCI6IFsgMTc1IF0sXG5cdFwibWFsZVwiOiBbIDk3OTQgXSxcblx0XCJtYWx0XCI6IFsgMTAwMTYgXSxcblx0XCJtYWx0ZXNlXCI6IFsgMTAwMTYgXSxcblx0XCJNYXBcIjogWyAxMDUwMSBdLFxuXHRcIm1hcFwiOiBbIDg2MTQgXSxcblx0XCJtYXBzdG9cIjogWyA4NjE0IF0sXG5cdFwibWFwc3RvZG93blwiOiBbIDg2MTUgXSxcblx0XCJtYXBzdG9sZWZ0XCI6IFsgODYxMiBdLFxuXHRcIm1hcHN0b3VwXCI6IFsgODYxMyBdLFxuXHRcIm1hcmtlclwiOiBbIDk2NDYgXSxcblx0XCJtY29tbWFcIjogWyAxMDc5MyBdLFxuXHRcIk1jeVwiOiBbIDEwNTIgXSxcblx0XCJtY3lcIjogWyAxMDg0IF0sXG5cdFwibWRhc2hcIjogWyA4MjEyIF0sXG5cdFwibUREb3RcIjogWyA4NzYyIF0sXG5cdFwibWVhc3VyZWRhbmdsZVwiOiBbIDg3MzcgXSxcblx0XCJNZWRpdW1TcGFjZVwiOiBbIDgyODcgXSxcblx0XCJNZWxsaW50cmZcIjogWyA4NDk5IF0sXG5cdFwiTWZyXCI6IFsgMTIwMDgwIF0sXG5cdFwibWZyXCI6IFsgMTIwMTA2IF0sXG5cdFwibWhvXCI6IFsgODQ4NyBdLFxuXHRcIm1pY3JvXCI6IFsgMTgxIF0sXG5cdFwibWlkXCI6IFsgODczOSBdLFxuXHRcIm1pZGFzdFwiOiBbIDQyIF0sXG5cdFwibWlkY2lyXCI6IFsgMTA5OTIgXSxcblx0XCJtaWRkb3RcIjogWyAxODMgXSxcblx0XCJtaW51c1wiOiBbIDg3MjIgXSxcblx0XCJtaW51c2JcIjogWyA4ODYzIF0sXG5cdFwibWludXNkXCI6IFsgODc2MCBdLFxuXHRcIm1pbnVzZHVcIjogWyAxMDc5NCBdLFxuXHRcIk1pbnVzUGx1c1wiOiBbIDg3MjMgXSxcblx0XCJtbGNwXCI6IFsgMTA5NzEgXSxcblx0XCJtbGRyXCI6IFsgODIzMCBdLFxuXHRcIm1ucGx1c1wiOiBbIDg3MjMgXSxcblx0XCJtb2RlbHNcIjogWyA4ODcxIF0sXG5cdFwiTW9wZlwiOiBbIDEyMDEzMiBdLFxuXHRcIm1vcGZcIjogWyAxMjAxNTggXSxcblx0XCJtcFwiOiBbIDg3MjMgXSxcblx0XCJNc2NyXCI6IFsgODQ5OSBdLFxuXHRcIm1zY3JcIjogWyAxMjAwMDIgXSxcblx0XCJtc3Rwb3NcIjogWyA4NzY2IF0sXG5cdFwiTXVcIjogWyA5MjQgXSxcblx0XCJtdVwiOiBbIDk1NiBdLFxuXHRcIm11bHRpbWFwXCI6IFsgODg4OCBdLFxuXHRcIm11bWFwXCI6IFsgODg4OCBdLFxuXHRcIm5hYmxhXCI6IFsgODcxMSBdLFxuXHRcIk5hY3V0ZVwiOiBbIDMyMyBdLFxuXHRcIm5hY3V0ZVwiOiBbIDMyNCBdLFxuXHRcIm5hbmdcIjogWyA4NzM2LCA4NDAyIF0sXG5cdFwibmFwXCI6IFsgODc3NyBdLFxuXHRcIm5hcEVcIjogWyAxMDg2NCwgODI0IF0sXG5cdFwibmFwaWRcIjogWyA4Nzc5LCA4MjQgXSxcblx0XCJuYXBvc1wiOiBbIDMyOSBdLFxuXHRcIm5hcHByb3hcIjogWyA4Nzc3IF0sXG5cdFwibmF0dXJcIjogWyA5ODM4IF0sXG5cdFwibmF0dXJhbFwiOiBbIDk4MzggXSxcblx0XCJuYXR1cmFsc1wiOiBbIDg0NjkgXSxcblx0XCJuYnNwXCI6IFsgMTYwIF0sXG5cdFwibmJ1bXBcIjogWyA4NzgyLCA4MjQgXSxcblx0XCJuYnVtcGVcIjogWyA4NzgzLCA4MjQgXSxcblx0XCJuY2FwXCI6IFsgMTA4MTkgXSxcblx0XCJOY2Fyb25cIjogWyAzMjcgXSxcblx0XCJuY2Fyb25cIjogWyAzMjggXSxcblx0XCJOY2VkaWxcIjogWyAzMjUgXSxcblx0XCJuY2VkaWxcIjogWyAzMjYgXSxcblx0XCJuY29uZ1wiOiBbIDg3NzUgXSxcblx0XCJuY29uZ2RvdFwiOiBbIDEwODYxLCA4MjQgXSxcblx0XCJuY3VwXCI6IFsgMTA4MTggXSxcblx0XCJOY3lcIjogWyAxMDUzIF0sXG5cdFwibmN5XCI6IFsgMTA4NSBdLFxuXHRcIm5kYXNoXCI6IFsgODIxMSBdLFxuXHRcIm5lXCI6IFsgODgwMCBdLFxuXHRcIm5lYXJoa1wiOiBbIDEwNTMyIF0sXG5cdFwibmVBcnJcIjogWyA4NjYzIF0sXG5cdFwibmVhcnJcIjogWyA4NTk5IF0sXG5cdFwibmVhcnJvd1wiOiBbIDg1OTkgXSxcblx0XCJuZWRvdFwiOiBbIDg3ODQsIDgyNCBdLFxuXHRcIk5lZ2F0aXZlTWVkaXVtU3BhY2VcIjogWyA4MjAzIF0sXG5cdFwiTmVnYXRpdmVUaGlja1NwYWNlXCI6IFsgODIwMyBdLFxuXHRcIk5lZ2F0aXZlVGhpblNwYWNlXCI6IFsgODIwMyBdLFxuXHRcIk5lZ2F0aXZlVmVyeVRoaW5TcGFjZVwiOiBbIDgyMDMgXSxcblx0XCJuZXF1aXZcIjogWyA4ODAyIF0sXG5cdFwibmVzZWFyXCI6IFsgMTA1MzYgXSxcblx0XCJuZXNpbVwiOiBbIDg3NzAsIDgyNCBdLFxuXHRcIk5lc3RlZEdyZWF0ZXJHcmVhdGVyXCI6IFsgODgxMSBdLFxuXHRcIk5lc3RlZExlc3NMZXNzXCI6IFsgODgxMCBdLFxuXHRcIk5ld0xpbmVcIjogWyAxMCBdLFxuXHRcIm5leGlzdFwiOiBbIDg3MDggXSxcblx0XCJuZXhpc3RzXCI6IFsgODcwOCBdLFxuXHRcIk5mclwiOiBbIDEyMDA4MSBdLFxuXHRcIm5mclwiOiBbIDEyMDEwNyBdLFxuXHRcIm5nRVwiOiBbIDg4MDcsIDgyNCBdLFxuXHRcIm5nZVwiOiBbIDg4MTcgXSxcblx0XCJuZ2VxXCI6IFsgODgxNyBdLFxuXHRcIm5nZXFxXCI6IFsgODgwNywgODI0IF0sXG5cdFwibmdlcXNsYW50XCI6IFsgMTA4NzgsIDgyNCBdLFxuXHRcIm5nZXNcIjogWyAxMDg3OCwgODI0IF0sXG5cdFwibkdnXCI6IFsgODkyMSwgODI0IF0sXG5cdFwibmdzaW1cIjogWyA4ODIxIF0sXG5cdFwibkd0XCI6IFsgODgxMSwgODQwMiBdLFxuXHRcIm5ndFwiOiBbIDg4MTUgXSxcblx0XCJuZ3RyXCI6IFsgODgxNSBdLFxuXHRcIm5HdHZcIjogWyA4ODExLCA4MjQgXSxcblx0XCJuaEFyclwiOiBbIDg2NTQgXSxcblx0XCJuaGFyclwiOiBbIDg2MjIgXSxcblx0XCJuaHBhclwiOiBbIDEwOTk0IF0sXG5cdFwibmlcIjogWyA4NzE1IF0sXG5cdFwibmlzXCI6IFsgODk1NiBdLFxuXHRcIm5pc2RcIjogWyA4OTU0IF0sXG5cdFwibml2XCI6IFsgODcxNSBdLFxuXHRcIk5KY3lcIjogWyAxMDM0IF0sXG5cdFwibmpjeVwiOiBbIDExMTQgXSxcblx0XCJubEFyclwiOiBbIDg2NTMgXSxcblx0XCJubGFyclwiOiBbIDg2MDIgXSxcblx0XCJubGRyXCI6IFsgODIyOSBdLFxuXHRcIm5sRVwiOiBbIDg4MDYsIDgyNCBdLFxuXHRcIm5sZVwiOiBbIDg4MTYgXSxcblx0XCJuTGVmdGFycm93XCI6IFsgODY1MyBdLFxuXHRcIm5sZWZ0YXJyb3dcIjogWyA4NjAyIF0sXG5cdFwibkxlZnRyaWdodGFycm93XCI6IFsgODY1NCBdLFxuXHRcIm5sZWZ0cmlnaHRhcnJvd1wiOiBbIDg2MjIgXSxcblx0XCJubGVxXCI6IFsgODgxNiBdLFxuXHRcIm5sZXFxXCI6IFsgODgwNiwgODI0IF0sXG5cdFwibmxlcXNsYW50XCI6IFsgMTA4NzcsIDgyNCBdLFxuXHRcIm5sZXNcIjogWyAxMDg3NywgODI0IF0sXG5cdFwibmxlc3NcIjogWyA4ODE0IF0sXG5cdFwibkxsXCI6IFsgODkyMCwgODI0IF0sXG5cdFwibmxzaW1cIjogWyA4ODIwIF0sXG5cdFwibkx0XCI6IFsgODgxMCwgODQwMiBdLFxuXHRcIm5sdFwiOiBbIDg4MTQgXSxcblx0XCJubHRyaVwiOiBbIDg5MzggXSxcblx0XCJubHRyaWVcIjogWyA4OTQwIF0sXG5cdFwibkx0dlwiOiBbIDg4MTAsIDgyNCBdLFxuXHRcIm5taWRcIjogWyA4NzQwIF0sXG5cdFwiTm9CcmVha1wiOiBbIDgyODggXSxcblx0XCJOb25CcmVha2luZ1NwYWNlXCI6IFsgMTYwIF0sXG5cdFwiTm9wZlwiOiBbIDg0NjkgXSxcblx0XCJub3BmXCI6IFsgMTIwMTU5IF0sXG5cdFwiTm90XCI6IFsgMTA5ODggXSxcblx0XCJub3RcIjogWyAxNzIgXSxcblx0XCJOb3RDb25ncnVlbnRcIjogWyA4ODAyIF0sXG5cdFwiTm90Q3VwQ2FwXCI6IFsgODgxMyBdLFxuXHRcIk5vdERvdWJsZVZlcnRpY2FsQmFyXCI6IFsgODc0MiBdLFxuXHRcIk5vdEVsZW1lbnRcIjogWyA4NzEzIF0sXG5cdFwiTm90RXF1YWxcIjogWyA4ODAwIF0sXG5cdFwiTm90RXF1YWxUaWxkZVwiOiBbIDg3NzAsIDgyNCBdLFxuXHRcIk5vdEV4aXN0c1wiOiBbIDg3MDggXSxcblx0XCJOb3RHcmVhdGVyXCI6IFsgODgxNSBdLFxuXHRcIk5vdEdyZWF0ZXJFcXVhbFwiOiBbIDg4MTcgXSxcblx0XCJOb3RHcmVhdGVyRnVsbEVxdWFsXCI6IFsgODgwNywgODI0IF0sXG5cdFwiTm90R3JlYXRlckdyZWF0ZXJcIjogWyA4ODExLCA4MjQgXSxcblx0XCJOb3RHcmVhdGVyTGVzc1wiOiBbIDg4MjUgXSxcblx0XCJOb3RHcmVhdGVyU2xhbnRFcXVhbFwiOiBbIDEwODc4LCA4MjQgXSxcblx0XCJOb3RHcmVhdGVyVGlsZGVcIjogWyA4ODIxIF0sXG5cdFwiTm90SHVtcERvd25IdW1wXCI6IFsgODc4MiwgODI0IF0sXG5cdFwiTm90SHVtcEVxdWFsXCI6IFsgODc4MywgODI0IF0sXG5cdFwibm90aW5cIjogWyA4NzEzIF0sXG5cdFwibm90aW5kb3RcIjogWyA4OTQ5LCA4MjQgXSxcblx0XCJub3RpbkVcIjogWyA4OTUzLCA4MjQgXSxcblx0XCJub3RpbnZhXCI6IFsgODcxMyBdLFxuXHRcIm5vdGludmJcIjogWyA4OTUxIF0sXG5cdFwibm90aW52Y1wiOiBbIDg5NTAgXSxcblx0XCJOb3RMZWZ0VHJpYW5nbGVcIjogWyA4OTM4IF0sXG5cdFwiTm90TGVmdFRyaWFuZ2xlQmFyXCI6IFsgMTA3MDMsIDgyNCBdLFxuXHRcIk5vdExlZnRUcmlhbmdsZUVxdWFsXCI6IFsgODk0MCBdLFxuXHRcIk5vdExlc3NcIjogWyA4ODE0IF0sXG5cdFwiTm90TGVzc0VxdWFsXCI6IFsgODgxNiBdLFxuXHRcIk5vdExlc3NHcmVhdGVyXCI6IFsgODgyNCBdLFxuXHRcIk5vdExlc3NMZXNzXCI6IFsgODgxMCwgODI0IF0sXG5cdFwiTm90TGVzc1NsYW50RXF1YWxcIjogWyAxMDg3NywgODI0IF0sXG5cdFwiTm90TGVzc1RpbGRlXCI6IFsgODgyMCBdLFxuXHRcIk5vdE5lc3RlZEdyZWF0ZXJHcmVhdGVyXCI6IFsgMTA5MTQsIDgyNCBdLFxuXHRcIk5vdE5lc3RlZExlc3NMZXNzXCI6IFsgMTA5MTMsIDgyNCBdLFxuXHRcIm5vdG5pXCI6IFsgODcxNiBdLFxuXHRcIm5vdG5pdmFcIjogWyA4NzE2IF0sXG5cdFwibm90bml2YlwiOiBbIDg5NTggXSxcblx0XCJub3RuaXZjXCI6IFsgODk1NyBdLFxuXHRcIk5vdFByZWNlZGVzXCI6IFsgODgzMiBdLFxuXHRcIk5vdFByZWNlZGVzRXF1YWxcIjogWyAxMDkyNywgODI0IF0sXG5cdFwiTm90UHJlY2VkZXNTbGFudEVxdWFsXCI6IFsgODkyOCBdLFxuXHRcIk5vdFJldmVyc2VFbGVtZW50XCI6IFsgODcxNiBdLFxuXHRcIk5vdFJpZ2h0VHJpYW5nbGVcIjogWyA4OTM5IF0sXG5cdFwiTm90UmlnaHRUcmlhbmdsZUJhclwiOiBbIDEwNzA0LCA4MjQgXSxcblx0XCJOb3RSaWdodFRyaWFuZ2xlRXF1YWxcIjogWyA4OTQxIF0sXG5cdFwiTm90U3F1YXJlU3Vic2V0XCI6IFsgODg0NywgODI0IF0sXG5cdFwiTm90U3F1YXJlU3Vic2V0RXF1YWxcIjogWyA4OTMwIF0sXG5cdFwiTm90U3F1YXJlU3VwZXJzZXRcIjogWyA4ODQ4LCA4MjQgXSxcblx0XCJOb3RTcXVhcmVTdXBlcnNldEVxdWFsXCI6IFsgODkzMSBdLFxuXHRcIk5vdFN1YnNldFwiOiBbIDg4MzQsIDg0MDIgXSxcblx0XCJOb3RTdWJzZXRFcXVhbFwiOiBbIDg4NDAgXSxcblx0XCJOb3RTdWNjZWVkc1wiOiBbIDg4MzMgXSxcblx0XCJOb3RTdWNjZWVkc0VxdWFsXCI6IFsgMTA5MjgsIDgyNCBdLFxuXHRcIk5vdFN1Y2NlZWRzU2xhbnRFcXVhbFwiOiBbIDg5MjkgXSxcblx0XCJOb3RTdWNjZWVkc1RpbGRlXCI6IFsgODgzMSwgODI0IF0sXG5cdFwiTm90U3VwZXJzZXRcIjogWyA4ODM1LCA4NDAyIF0sXG5cdFwiTm90U3VwZXJzZXRFcXVhbFwiOiBbIDg4NDEgXSxcblx0XCJOb3RUaWxkZVwiOiBbIDg3NjkgXSxcblx0XCJOb3RUaWxkZUVxdWFsXCI6IFsgODc3MiBdLFxuXHRcIk5vdFRpbGRlRnVsbEVxdWFsXCI6IFsgODc3NSBdLFxuXHRcIk5vdFRpbGRlVGlsZGVcIjogWyA4Nzc3IF0sXG5cdFwiTm90VmVydGljYWxCYXJcIjogWyA4NzQwIF0sXG5cdFwibnBhclwiOiBbIDg3NDIgXSxcblx0XCJucGFyYWxsZWxcIjogWyA4NzQyIF0sXG5cdFwibnBhcnNsXCI6IFsgMTEwMDUsIDg0MjEgXSxcblx0XCJucGFydFwiOiBbIDg3MDYsIDgyNCBdLFxuXHRcIm5wb2xpbnRcIjogWyAxMDc3MiBdLFxuXHRcIm5wclwiOiBbIDg4MzIgXSxcblx0XCJucHJjdWVcIjogWyA4OTI4IF0sXG5cdFwibnByZVwiOiBbIDEwOTI3LCA4MjQgXSxcblx0XCJucHJlY1wiOiBbIDg4MzIgXSxcblx0XCJucHJlY2VxXCI6IFsgMTA5MjcsIDgyNCBdLFxuXHRcIm5yQXJyXCI6IFsgODY1NSBdLFxuXHRcIm5yYXJyXCI6IFsgODYwMyBdLFxuXHRcIm5yYXJyY1wiOiBbIDEwNTQ3LCA4MjQgXSxcblx0XCJucmFycndcIjogWyA4NjA1LCA4MjQgXSxcblx0XCJuUmlnaHRhcnJvd1wiOiBbIDg2NTUgXSxcblx0XCJucmlnaHRhcnJvd1wiOiBbIDg2MDMgXSxcblx0XCJucnRyaVwiOiBbIDg5MzkgXSxcblx0XCJucnRyaWVcIjogWyA4OTQxIF0sXG5cdFwibnNjXCI6IFsgODgzMyBdLFxuXHRcIm5zY2N1ZVwiOiBbIDg5MjkgXSxcblx0XCJuc2NlXCI6IFsgMTA5MjgsIDgyNCBdLFxuXHRcIk5zY3JcIjogWyAxMTk5NzcgXSxcblx0XCJuc2NyXCI6IFsgMTIwMDAzIF0sXG5cdFwibnNob3J0bWlkXCI6IFsgODc0MCBdLFxuXHRcIm5zaG9ydHBhcmFsbGVsXCI6IFsgODc0MiBdLFxuXHRcIm5zaW1cIjogWyA4NzY5IF0sXG5cdFwibnNpbWVcIjogWyA4NzcyIF0sXG5cdFwibnNpbWVxXCI6IFsgODc3MiBdLFxuXHRcIm5zbWlkXCI6IFsgODc0MCBdLFxuXHRcIm5zcGFyXCI6IFsgODc0MiBdLFxuXHRcIm5zcXN1YmVcIjogWyA4OTMwIF0sXG5cdFwibnNxc3VwZVwiOiBbIDg5MzEgXSxcblx0XCJuc3ViXCI6IFsgODgzNiBdLFxuXHRcIm5zdWJFXCI6IFsgMTA5NDksIDgyNCBdLFxuXHRcIm5zdWJlXCI6IFsgODg0MCBdLFxuXHRcIm5zdWJzZXRcIjogWyA4ODM0LCA4NDAyIF0sXG5cdFwibnN1YnNldGVxXCI6IFsgODg0MCBdLFxuXHRcIm5zdWJzZXRlcXFcIjogWyAxMDk0OSwgODI0IF0sXG5cdFwibnN1Y2NcIjogWyA4ODMzIF0sXG5cdFwibnN1Y2NlcVwiOiBbIDEwOTI4LCA4MjQgXSxcblx0XCJuc3VwXCI6IFsgODgzNyBdLFxuXHRcIm5zdXBFXCI6IFsgMTA5NTAsIDgyNCBdLFxuXHRcIm5zdXBlXCI6IFsgODg0MSBdLFxuXHRcIm5zdXBzZXRcIjogWyA4ODM1LCA4NDAyIF0sXG5cdFwibnN1cHNldGVxXCI6IFsgODg0MSBdLFxuXHRcIm5zdXBzZXRlcXFcIjogWyAxMDk1MCwgODI0IF0sXG5cdFwibnRnbFwiOiBbIDg4MjUgXSxcblx0XCJOdGlsZGVcIjogWyAyMDkgXSxcblx0XCJudGlsZGVcIjogWyAyNDEgXSxcblx0XCJudGxnXCI6IFsgODgyNCBdLFxuXHRcIm50cmlhbmdsZWxlZnRcIjogWyA4OTM4IF0sXG5cdFwibnRyaWFuZ2xlbGVmdGVxXCI6IFsgODk0MCBdLFxuXHRcIm50cmlhbmdsZXJpZ2h0XCI6IFsgODkzOSBdLFxuXHRcIm50cmlhbmdsZXJpZ2h0ZXFcIjogWyA4OTQxIF0sXG5cdFwiTnVcIjogWyA5MjUgXSxcblx0XCJudVwiOiBbIDk1NyBdLFxuXHRcIm51bVwiOiBbIDM1IF0sXG5cdFwibnVtZXJvXCI6IFsgODQ3MCBdLFxuXHRcIm51bXNwXCI6IFsgODE5OSBdLFxuXHRcIm52YXBcIjogWyA4NzgxLCA4NDAyIF0sXG5cdFwiblZEYXNoXCI6IFsgODg3OSBdLFxuXHRcIm5WZGFzaFwiOiBbIDg4NzggXSxcblx0XCJudkRhc2hcIjogWyA4ODc3IF0sXG5cdFwibnZkYXNoXCI6IFsgODg3NiBdLFxuXHRcIm52Z2VcIjogWyA4ODA1LCA4NDAyIF0sXG5cdFwibnZndFwiOiBbIDYyLCA4NDAyIF0sXG5cdFwibnZIYXJyXCI6IFsgMTA1MDAgXSxcblx0XCJudmluZmluXCI6IFsgMTA3MTggXSxcblx0XCJudmxBcnJcIjogWyAxMDQ5OCBdLFxuXHRcIm52bGVcIjogWyA4ODA0LCA4NDAyIF0sXG5cdFwibnZsdFwiOiBbIDYwLCA4NDAyIF0sXG5cdFwibnZsdHJpZVwiOiBbIDg4ODQsIDg0MDIgXSxcblx0XCJudnJBcnJcIjogWyAxMDQ5OSBdLFxuXHRcIm52cnRyaWVcIjogWyA4ODg1LCA4NDAyIF0sXG5cdFwibnZzaW1cIjogWyA4NzY0LCA4NDAyIF0sXG5cdFwibndhcmhrXCI6IFsgMTA1MzEgXSxcblx0XCJud0FyclwiOiBbIDg2NjIgXSxcblx0XCJud2FyclwiOiBbIDg1OTggXSxcblx0XCJud2Fycm93XCI6IFsgODU5OCBdLFxuXHRcIm53bmVhclwiOiBbIDEwNTM1IF0sXG5cdFwiT2FjdXRlXCI6IFsgMjExIF0sXG5cdFwib2FjdXRlXCI6IFsgMjQzIF0sXG5cdFwib2FzdFwiOiBbIDg4NTkgXSxcblx0XCJvY2lyXCI6IFsgODg1OCBdLFxuXHRcIk9jaXJjXCI6IFsgMjEyIF0sXG5cdFwib2NpcmNcIjogWyAyNDQgXSxcblx0XCJPY3lcIjogWyAxMDU0IF0sXG5cdFwib2N5XCI6IFsgMTA4NiBdLFxuXHRcIm9kYXNoXCI6IFsgODg2MSBdLFxuXHRcIk9kYmxhY1wiOiBbIDMzNiBdLFxuXHRcIm9kYmxhY1wiOiBbIDMzNyBdLFxuXHRcIm9kaXZcIjogWyAxMDgwOCBdLFxuXHRcIm9kb3RcIjogWyA4ODU3IF0sXG5cdFwib2Rzb2xkXCI6IFsgMTA2ODQgXSxcblx0XCJPRWxpZ1wiOiBbIDMzOCBdLFxuXHRcIm9lbGlnXCI6IFsgMzM5IF0sXG5cdFwib2ZjaXJcIjogWyAxMDY4NyBdLFxuXHRcIk9mclwiOiBbIDEyMDA4MiBdLFxuXHRcIm9mclwiOiBbIDEyMDEwOCBdLFxuXHRcIm9nb25cIjogWyA3MzEgXSxcblx0XCJPZ3JhdmVcIjogWyAyMTAgXSxcblx0XCJvZ3JhdmVcIjogWyAyNDIgXSxcblx0XCJvZ3RcIjogWyAxMDY4OSBdLFxuXHRcIm9oYmFyXCI6IFsgMTA2NzcgXSxcblx0XCJvaG1cIjogWyA5MzcgXSxcblx0XCJvaW50XCI6IFsgODc1MCBdLFxuXHRcIm9sYXJyXCI6IFsgODYzNCBdLFxuXHRcIm9sY2lyXCI6IFsgMTA2ODYgXSxcblx0XCJvbGNyb3NzXCI6IFsgMTA2ODMgXSxcblx0XCJvbGluZVwiOiBbIDgyNTQgXSxcblx0XCJvbHRcIjogWyAxMDY4OCBdLFxuXHRcIk9tYWNyXCI6IFsgMzMyIF0sXG5cdFwib21hY3JcIjogWyAzMzMgXSxcblx0XCJPbWVnYVwiOiBbIDkzNyBdLFxuXHRcIm9tZWdhXCI6IFsgOTY5IF0sXG5cdFwiT21pY3JvblwiOiBbIDkyNyBdLFxuXHRcIm9taWNyb25cIjogWyA5NTkgXSxcblx0XCJvbWlkXCI6IFsgMTA2NzggXSxcblx0XCJvbWludXNcIjogWyA4ODU0IF0sXG5cdFwiT29wZlwiOiBbIDEyMDEzNCBdLFxuXHRcIm9vcGZcIjogWyAxMjAxNjAgXSxcblx0XCJvcGFyXCI6IFsgMTA2NzkgXSxcblx0XCJPcGVuQ3VybHlEb3VibGVRdW90ZVwiOiBbIDgyMjAgXSxcblx0XCJPcGVuQ3VybHlRdW90ZVwiOiBbIDgyMTYgXSxcblx0XCJvcGVycFwiOiBbIDEwNjgxIF0sXG5cdFwib3BsdXNcIjogWyA4ODUzIF0sXG5cdFwiT3JcIjogWyAxMDgzNiBdLFxuXHRcIm9yXCI6IFsgODc0NCBdLFxuXHRcIm9yYXJyXCI6IFsgODYzNSBdLFxuXHRcIm9yZFwiOiBbIDEwODQ1IF0sXG5cdFwib3JkZXJcIjogWyA4NTAwIF0sXG5cdFwib3JkZXJvZlwiOiBbIDg1MDAgXSxcblx0XCJvcmRmXCI6IFsgMTcwIF0sXG5cdFwib3JkbVwiOiBbIDE4NiBdLFxuXHRcIm9yaWdvZlwiOiBbIDg4ODYgXSxcblx0XCJvcm9yXCI6IFsgMTA4MzggXSxcblx0XCJvcnNsb3BlXCI6IFsgMTA4MzkgXSxcblx0XCJvcnZcIjogWyAxMDg0MyBdLFxuXHRcIm9TXCI6IFsgOTQxNiBdLFxuXHRcIk9zY3JcIjogWyAxMTk5NzggXSxcblx0XCJvc2NyXCI6IFsgODUwMCBdLFxuXHRcIk9zbGFzaFwiOiBbIDIxNiBdLFxuXHRcIm9zbGFzaFwiOiBbIDI0OCBdLFxuXHRcIm9zb2xcIjogWyA4ODU2IF0sXG5cdFwiT3RpbGRlXCI6IFsgMjEzIF0sXG5cdFwib3RpbGRlXCI6IFsgMjQ1IF0sXG5cdFwiT3RpbWVzXCI6IFsgMTA4MDcgXSxcblx0XCJvdGltZXNcIjogWyA4ODU1IF0sXG5cdFwib3RpbWVzYXNcIjogWyAxMDgwNiBdLFxuXHRcIk91bWxcIjogWyAyMTQgXSxcblx0XCJvdW1sXCI6IFsgMjQ2IF0sXG5cdFwib3ZiYXJcIjogWyA5MDIxIF0sXG5cdFwiT3ZlckJhclwiOiBbIDgyNTQgXSxcblx0XCJPdmVyQnJhY2VcIjogWyA5MTgyIF0sXG5cdFwiT3ZlckJyYWNrZXRcIjogWyA5MTQwIF0sXG5cdFwiT3ZlclBhcmVudGhlc2lzXCI6IFsgOTE4MCBdLFxuXHRcInBhclwiOiBbIDg3NDEgXSxcblx0XCJwYXJhXCI6IFsgMTgyIF0sXG5cdFwicGFyYWxsZWxcIjogWyA4NzQxIF0sXG5cdFwicGFyc2ltXCI6IFsgMTA5OTUgXSxcblx0XCJwYXJzbFwiOiBbIDExMDA1IF0sXG5cdFwicGFydFwiOiBbIDg3MDYgXSxcblx0XCJQYXJ0aWFsRFwiOiBbIDg3MDYgXSxcblx0XCJQY3lcIjogWyAxMDU1IF0sXG5cdFwicGN5XCI6IFsgMTA4NyBdLFxuXHRcInBlcmNudFwiOiBbIDM3IF0sXG5cdFwicGVyaW9kXCI6IFsgNDYgXSxcblx0XCJwZXJtaWxcIjogWyA4MjQwIF0sXG5cdFwicGVycFwiOiBbIDg4NjkgXSxcblx0XCJwZXJ0ZW5rXCI6IFsgODI0MSBdLFxuXHRcIlBmclwiOiBbIDEyMDA4MyBdLFxuXHRcInBmclwiOiBbIDEyMDEwOSBdLFxuXHRcIlBoaVwiOiBbIDkzNCBdLFxuXHRcInBoaVwiOiBbIDk2NiBdLFxuXHRcInBoaXZcIjogWyA5ODEgXSxcblx0XCJwaG1tYXRcIjogWyA4NDk5IF0sXG5cdFwicGhvbmVcIjogWyA5NzQyIF0sXG5cdFwiUGlcIjogWyA5MjggXSxcblx0XCJwaVwiOiBbIDk2MCBdLFxuXHRcInBpdGNoZm9ya1wiOiBbIDg5MTYgXSxcblx0XCJwaXZcIjogWyA5ODIgXSxcblx0XCJwbGFuY2tcIjogWyA4NDYzIF0sXG5cdFwicGxhbmNraFwiOiBbIDg0NjIgXSxcblx0XCJwbGFua3ZcIjogWyA4NDYzIF0sXG5cdFwicGx1c1wiOiBbIDQzIF0sXG5cdFwicGx1c2FjaXJcIjogWyAxMDc4NyBdLFxuXHRcInBsdXNiXCI6IFsgODg2MiBdLFxuXHRcInBsdXNjaXJcIjogWyAxMDc4NiBdLFxuXHRcInBsdXNkb1wiOiBbIDg3MjQgXSxcblx0XCJwbHVzZHVcIjogWyAxMDc4OSBdLFxuXHRcInBsdXNlXCI6IFsgMTA4NjYgXSxcblx0XCJQbHVzTWludXNcIjogWyAxNzcgXSxcblx0XCJwbHVzbW5cIjogWyAxNzcgXSxcblx0XCJwbHVzc2ltXCI6IFsgMTA3OTAgXSxcblx0XCJwbHVzdHdvXCI6IFsgMTA3OTEgXSxcblx0XCJwbVwiOiBbIDE3NyBdLFxuXHRcIlBvaW5jYXJlcGxhbmVcIjogWyA4NDYwIF0sXG5cdFwicG9pbnRpbnRcIjogWyAxMDc3MyBdLFxuXHRcIlBvcGZcIjogWyA4NDczIF0sXG5cdFwicG9wZlwiOiBbIDEyMDE2MSBdLFxuXHRcInBvdW5kXCI6IFsgMTYzIF0sXG5cdFwiUHJcIjogWyAxMDkzOSBdLFxuXHRcInByXCI6IFsgODgyNiBdLFxuXHRcInByYXBcIjogWyAxMDkzNSBdLFxuXHRcInByY3VlXCI6IFsgODgyOCBdLFxuXHRcInByRVwiOiBbIDEwOTMxIF0sXG5cdFwicHJlXCI6IFsgMTA5MjcgXSxcblx0XCJwcmVjXCI6IFsgODgyNiBdLFxuXHRcInByZWNhcHByb3hcIjogWyAxMDkzNSBdLFxuXHRcInByZWNjdXJseWVxXCI6IFsgODgyOCBdLFxuXHRcIlByZWNlZGVzXCI6IFsgODgyNiBdLFxuXHRcIlByZWNlZGVzRXF1YWxcIjogWyAxMDkyNyBdLFxuXHRcIlByZWNlZGVzU2xhbnRFcXVhbFwiOiBbIDg4MjggXSxcblx0XCJQcmVjZWRlc1RpbGRlXCI6IFsgODgzMCBdLFxuXHRcInByZWNlcVwiOiBbIDEwOTI3IF0sXG5cdFwicHJlY25hcHByb3hcIjogWyAxMDkzNyBdLFxuXHRcInByZWNuZXFxXCI6IFsgMTA5MzMgXSxcblx0XCJwcmVjbnNpbVwiOiBbIDg5MzYgXSxcblx0XCJwcmVjc2ltXCI6IFsgODgzMCBdLFxuXHRcIlByaW1lXCI6IFsgODI0MyBdLFxuXHRcInByaW1lXCI6IFsgODI0MiBdLFxuXHRcInByaW1lc1wiOiBbIDg0NzMgXSxcblx0XCJwcm5hcFwiOiBbIDEwOTM3IF0sXG5cdFwicHJuRVwiOiBbIDEwOTMzIF0sXG5cdFwicHJuc2ltXCI6IFsgODkzNiBdLFxuXHRcInByb2RcIjogWyA4NzE5IF0sXG5cdFwiUHJvZHVjdFwiOiBbIDg3MTkgXSxcblx0XCJwcm9mYWxhclwiOiBbIDkwMDYgXSxcblx0XCJwcm9mbGluZVwiOiBbIDg5NzggXSxcblx0XCJwcm9mc3VyZlwiOiBbIDg5NzkgXSxcblx0XCJwcm9wXCI6IFsgODczMyBdLFxuXHRcIlByb3BvcnRpb25cIjogWyA4NzU5IF0sXG5cdFwiUHJvcG9ydGlvbmFsXCI6IFsgODczMyBdLFxuXHRcInByb3B0b1wiOiBbIDg3MzMgXSxcblx0XCJwcnNpbVwiOiBbIDg4MzAgXSxcblx0XCJwcnVyZWxcIjogWyA4ODgwIF0sXG5cdFwiUHNjclwiOiBbIDExOTk3OSBdLFxuXHRcInBzY3JcIjogWyAxMjAwMDUgXSxcblx0XCJQc2lcIjogWyA5MzYgXSxcblx0XCJwc2lcIjogWyA5NjggXSxcblx0XCJwdW5jc3BcIjogWyA4MjAwIF0sXG5cdFwiUWZyXCI6IFsgMTIwMDg0IF0sXG5cdFwicWZyXCI6IFsgMTIwMTEwIF0sXG5cdFwicWludFwiOiBbIDEwNzY0IF0sXG5cdFwiUW9wZlwiOiBbIDg0NzQgXSxcblx0XCJxb3BmXCI6IFsgMTIwMTYyIF0sXG5cdFwicXByaW1lXCI6IFsgODI3OSBdLFxuXHRcIlFzY3JcIjogWyAxMTk5ODAgXSxcblx0XCJxc2NyXCI6IFsgMTIwMDA2IF0sXG5cdFwicXVhdGVybmlvbnNcIjogWyA4NDYxIF0sXG5cdFwicXVhdGludFwiOiBbIDEwNzc0IF0sXG5cdFwicXVlc3RcIjogWyA2MyBdLFxuXHRcInF1ZXN0ZXFcIjogWyA4Nzk5IF0sXG5cdFwiUVVPVFwiOiBbIDM0IF0sXG5cdFwicXVvdFwiOiBbIDM0IF0sXG5cdFwickFhcnJcIjogWyA4NjY3IF0sXG5cdFwicmFjZVwiOiBbIDg3NjUsIDgxNyBdLFxuXHRcIlJhY3V0ZVwiOiBbIDM0MCBdLFxuXHRcInJhY3V0ZVwiOiBbIDM0MSBdLFxuXHRcInJhZGljXCI6IFsgODczMCBdLFxuXHRcInJhZW1wdHl2XCI6IFsgMTA2NzUgXSxcblx0XCJSYW5nXCI6IFsgMTAyMTkgXSxcblx0XCJyYW5nXCI6IFsgMTAyMTcgXSxcblx0XCJyYW5nZFwiOiBbIDEwNjQyIF0sXG5cdFwicmFuZ2VcIjogWyAxMDY2MSBdLFxuXHRcInJhbmdsZVwiOiBbIDEwMjE3IF0sXG5cdFwicmFxdW9cIjogWyAxODcgXSxcblx0XCJSYXJyXCI6IFsgODYwOCBdLFxuXHRcInJBcnJcIjogWyA4NjU4IF0sXG5cdFwicmFyclwiOiBbIDg1OTQgXSxcblx0XCJyYXJyYXBcIjogWyAxMDYxMyBdLFxuXHRcInJhcnJiXCI6IFsgODY3NyBdLFxuXHRcInJhcnJiZnNcIjogWyAxMDUyOCBdLFxuXHRcInJhcnJjXCI6IFsgMTA1NDcgXSxcblx0XCJyYXJyZnNcIjogWyAxMDUyNiBdLFxuXHRcInJhcnJoa1wiOiBbIDg2MTggXSxcblx0XCJyYXJybHBcIjogWyA4NjIwIF0sXG5cdFwicmFycnBsXCI6IFsgMTA1NjUgXSxcblx0XCJyYXJyc2ltXCI6IFsgMTA2MTIgXSxcblx0XCJSYXJydGxcIjogWyAxMDUxOCBdLFxuXHRcInJhcnJ0bFwiOiBbIDg2MTEgXSxcblx0XCJyYXJyd1wiOiBbIDg2MDUgXSxcblx0XCJyQXRhaWxcIjogWyAxMDUyNCBdLFxuXHRcInJhdGFpbFwiOiBbIDEwNTIyIF0sXG5cdFwicmF0aW9cIjogWyA4NzU4IF0sXG5cdFwicmF0aW9uYWxzXCI6IFsgODQ3NCBdLFxuXHRcIlJCYXJyXCI6IFsgMTA1MTIgXSxcblx0XCJyQmFyclwiOiBbIDEwNTExIF0sXG5cdFwicmJhcnJcIjogWyAxMDUwOSBdLFxuXHRcInJiYnJrXCI6IFsgMTAwOTkgXSxcblx0XCJyYnJhY2VcIjogWyAxMjUgXSxcblx0XCJyYnJhY2tcIjogWyA5MyBdLFxuXHRcInJicmtlXCI6IFsgMTA2MzYgXSxcblx0XCJyYnJrc2xkXCI6IFsgMTA2MzggXSxcblx0XCJyYnJrc2x1XCI6IFsgMTA2NDAgXSxcblx0XCJSY2Fyb25cIjogWyAzNDQgXSxcblx0XCJyY2Fyb25cIjogWyAzNDUgXSxcblx0XCJSY2VkaWxcIjogWyAzNDIgXSxcblx0XCJyY2VkaWxcIjogWyAzNDMgXSxcblx0XCJyY2VpbFwiOiBbIDg5NjkgXSxcblx0XCJyY3ViXCI6IFsgMTI1IF0sXG5cdFwiUmN5XCI6IFsgMTA1NiBdLFxuXHRcInJjeVwiOiBbIDEwODggXSxcblx0XCJyZGNhXCI6IFsgMTA1NTEgXSxcblx0XCJyZGxkaGFyXCI6IFsgMTA2MDEgXSxcblx0XCJyZHF1b1wiOiBbIDgyMjEgXSxcblx0XCJyZHF1b3JcIjogWyA4MjIxIF0sXG5cdFwicmRzaFwiOiBbIDg2MjcgXSxcblx0XCJSZVwiOiBbIDg0NzYgXSxcblx0XCJyZWFsXCI6IFsgODQ3NiBdLFxuXHRcInJlYWxpbmVcIjogWyA4NDc1IF0sXG5cdFwicmVhbHBhcnRcIjogWyA4NDc2IF0sXG5cdFwicmVhbHNcIjogWyA4NDc3IF0sXG5cdFwicmVjdFwiOiBbIDk2NDUgXSxcblx0XCJSRUdcIjogWyAxNzQgXSxcblx0XCJyZWdcIjogWyAxNzQgXSxcblx0XCJSZXZlcnNlRWxlbWVudFwiOiBbIDg3MTUgXSxcblx0XCJSZXZlcnNlRXF1aWxpYnJpdW1cIjogWyA4NjUxIF0sXG5cdFwiUmV2ZXJzZVVwRXF1aWxpYnJpdW1cIjogWyAxMDYwNyBdLFxuXHRcInJmaXNodFwiOiBbIDEwNjIxIF0sXG5cdFwicmZsb29yXCI6IFsgODk3MSBdLFxuXHRcIlJmclwiOiBbIDg0NzYgXSxcblx0XCJyZnJcIjogWyAxMjAxMTEgXSxcblx0XCJySGFyXCI6IFsgMTA1OTYgXSxcblx0XCJyaGFyZFwiOiBbIDg2NDEgXSxcblx0XCJyaGFydVwiOiBbIDg2NDAgXSxcblx0XCJyaGFydWxcIjogWyAxMDYwNCBdLFxuXHRcIlJob1wiOiBbIDkyOSBdLFxuXHRcInJob1wiOiBbIDk2MSBdLFxuXHRcInJob3ZcIjogWyAxMDA5IF0sXG5cdFwiUmlnaHRBbmdsZUJyYWNrZXRcIjogWyAxMDIxNyBdLFxuXHRcIlJpZ2h0QXJyb3dcIjogWyA4NTk0IF0sXG5cdFwiUmlnaHRhcnJvd1wiOiBbIDg2NTggXSxcblx0XCJyaWdodGFycm93XCI6IFsgODU5NCBdLFxuXHRcIlJpZ2h0QXJyb3dCYXJcIjogWyA4Njc3IF0sXG5cdFwiUmlnaHRBcnJvd0xlZnRBcnJvd1wiOiBbIDg2NDQgXSxcblx0XCJyaWdodGFycm93dGFpbFwiOiBbIDg2MTEgXSxcblx0XCJSaWdodENlaWxpbmdcIjogWyA4OTY5IF0sXG5cdFwiUmlnaHREb3VibGVCcmFja2V0XCI6IFsgMTAyMTUgXSxcblx0XCJSaWdodERvd25UZWVWZWN0b3JcIjogWyAxMDU4OSBdLFxuXHRcIlJpZ2h0RG93blZlY3RvclwiOiBbIDg2NDIgXSxcblx0XCJSaWdodERvd25WZWN0b3JCYXJcIjogWyAxMDU4MSBdLFxuXHRcIlJpZ2h0Rmxvb3JcIjogWyA4OTcxIF0sXG5cdFwicmlnaHRoYXJwb29uZG93blwiOiBbIDg2NDEgXSxcblx0XCJyaWdodGhhcnBvb251cFwiOiBbIDg2NDAgXSxcblx0XCJyaWdodGxlZnRhcnJvd3NcIjogWyA4NjQ0IF0sXG5cdFwicmlnaHRsZWZ0aGFycG9vbnNcIjogWyA4NjUyIF0sXG5cdFwicmlnaHRyaWdodGFycm93c1wiOiBbIDg2NDkgXSxcblx0XCJyaWdodHNxdWlnYXJyb3dcIjogWyA4NjA1IF0sXG5cdFwiUmlnaHRUZWVcIjogWyA4ODY2IF0sXG5cdFwiUmlnaHRUZWVBcnJvd1wiOiBbIDg2MTQgXSxcblx0XCJSaWdodFRlZVZlY3RvclwiOiBbIDEwNTg3IF0sXG5cdFwicmlnaHR0aHJlZXRpbWVzXCI6IFsgODkwOCBdLFxuXHRcIlJpZ2h0VHJpYW5nbGVcIjogWyA4ODgzIF0sXG5cdFwiUmlnaHRUcmlhbmdsZUJhclwiOiBbIDEwNzA0IF0sXG5cdFwiUmlnaHRUcmlhbmdsZUVxdWFsXCI6IFsgODg4NSBdLFxuXHRcIlJpZ2h0VXBEb3duVmVjdG9yXCI6IFsgMTA1NzUgXSxcblx0XCJSaWdodFVwVGVlVmVjdG9yXCI6IFsgMTA1ODggXSxcblx0XCJSaWdodFVwVmVjdG9yXCI6IFsgODYzOCBdLFxuXHRcIlJpZ2h0VXBWZWN0b3JCYXJcIjogWyAxMDU4MCBdLFxuXHRcIlJpZ2h0VmVjdG9yXCI6IFsgODY0MCBdLFxuXHRcIlJpZ2h0VmVjdG9yQmFyXCI6IFsgMTA1NzkgXSxcblx0XCJyaW5nXCI6IFsgNzMwIF0sXG5cdFwicmlzaW5nZG90c2VxXCI6IFsgODc4NyBdLFxuXHRcInJsYXJyXCI6IFsgODY0NCBdLFxuXHRcInJsaGFyXCI6IFsgODY1MiBdLFxuXHRcInJsbVwiOiBbIDgyMDcgXSxcblx0XCJybW91c3RcIjogWyA5MTM3IF0sXG5cdFwicm1vdXN0YWNoZVwiOiBbIDkxMzcgXSxcblx0XCJybm1pZFwiOiBbIDEwOTkwIF0sXG5cdFwicm9hbmdcIjogWyAxMDIyMSBdLFxuXHRcInJvYXJyXCI6IFsgODcwMiBdLFxuXHRcInJvYnJrXCI6IFsgMTAyMTUgXSxcblx0XCJyb3BhclwiOiBbIDEwNjMwIF0sXG5cdFwiUm9wZlwiOiBbIDg0NzcgXSxcblx0XCJyb3BmXCI6IFsgMTIwMTYzIF0sXG5cdFwicm9wbHVzXCI6IFsgMTA3OTggXSxcblx0XCJyb3RpbWVzXCI6IFsgMTA4MDUgXSxcblx0XCJSb3VuZEltcGxpZXNcIjogWyAxMDYwOCBdLFxuXHRcInJwYXJcIjogWyA0MSBdLFxuXHRcInJwYXJndFwiOiBbIDEwNjQ0IF0sXG5cdFwicnBwb2xpbnRcIjogWyAxMDc3MCBdLFxuXHRcInJyYXJyXCI6IFsgODY0OSBdLFxuXHRcIlJyaWdodGFycm93XCI6IFsgODY2NyBdLFxuXHRcInJzYXF1b1wiOiBbIDgyNTAgXSxcblx0XCJSc2NyXCI6IFsgODQ3NSBdLFxuXHRcInJzY3JcIjogWyAxMjAwMDcgXSxcblx0XCJSc2hcIjogWyA4NjI1IF0sXG5cdFwicnNoXCI6IFsgODYyNSBdLFxuXHRcInJzcWJcIjogWyA5MyBdLFxuXHRcInJzcXVvXCI6IFsgODIxNyBdLFxuXHRcInJzcXVvclwiOiBbIDgyMTcgXSxcblx0XCJydGhyZWVcIjogWyA4OTA4IF0sXG5cdFwicnRpbWVzXCI6IFsgODkwNiBdLFxuXHRcInJ0cmlcIjogWyA5NjU3IF0sXG5cdFwicnRyaWVcIjogWyA4ODg1IF0sXG5cdFwicnRyaWZcIjogWyA5NjU2IF0sXG5cdFwicnRyaWx0cmlcIjogWyAxMDcwMiBdLFxuXHRcIlJ1bGVEZWxheWVkXCI6IFsgMTA3NDAgXSxcblx0XCJydWx1aGFyXCI6IFsgMTA2MDAgXSxcblx0XCJyeFwiOiBbIDg0NzggXSxcblx0XCJTYWN1dGVcIjogWyAzNDYgXSxcblx0XCJzYWN1dGVcIjogWyAzNDcgXSxcblx0XCJzYnF1b1wiOiBbIDgyMTggXSxcblx0XCJTY1wiOiBbIDEwOTQwIF0sXG5cdFwic2NcIjogWyA4ODI3IF0sXG5cdFwic2NhcFwiOiBbIDEwOTM2IF0sXG5cdFwiU2Nhcm9uXCI6IFsgMzUyIF0sXG5cdFwic2Nhcm9uXCI6IFsgMzUzIF0sXG5cdFwic2NjdWVcIjogWyA4ODI5IF0sXG5cdFwic2NFXCI6IFsgMTA5MzIgXSxcblx0XCJzY2VcIjogWyAxMDkyOCBdLFxuXHRcIlNjZWRpbFwiOiBbIDM1MCBdLFxuXHRcInNjZWRpbFwiOiBbIDM1MSBdLFxuXHRcIlNjaXJjXCI6IFsgMzQ4IF0sXG5cdFwic2NpcmNcIjogWyAzNDkgXSxcblx0XCJzY25hcFwiOiBbIDEwOTM4IF0sXG5cdFwic2NuRVwiOiBbIDEwOTM0IF0sXG5cdFwic2Nuc2ltXCI6IFsgODkzNyBdLFxuXHRcInNjcG9saW50XCI6IFsgMTA3NzEgXSxcblx0XCJzY3NpbVwiOiBbIDg4MzEgXSxcblx0XCJTY3lcIjogWyAxMDU3IF0sXG5cdFwic2N5XCI6IFsgMTA4OSBdLFxuXHRcInNkb3RcIjogWyA4OTAxIF0sXG5cdFwic2RvdGJcIjogWyA4ODY1IF0sXG5cdFwic2RvdGVcIjogWyAxMDg1NCBdLFxuXHRcInNlYXJoa1wiOiBbIDEwNTMzIF0sXG5cdFwic2VBcnJcIjogWyA4NjY0IF0sXG5cdFwic2VhcnJcIjogWyA4NjAwIF0sXG5cdFwic2VhcnJvd1wiOiBbIDg2MDAgXSxcblx0XCJzZWN0XCI6IFsgMTY3IF0sXG5cdFwic2VtaVwiOiBbIDU5IF0sXG5cdFwic2Vzd2FyXCI6IFsgMTA1MzcgXSxcblx0XCJzZXRtaW51c1wiOiBbIDg3MjYgXSxcblx0XCJzZXRtblwiOiBbIDg3MjYgXSxcblx0XCJzZXh0XCI6IFsgMTAwMzggXSxcblx0XCJTZnJcIjogWyAxMjAwODYgXSxcblx0XCJzZnJcIjogWyAxMjAxMTIgXSxcblx0XCJzZnJvd25cIjogWyA4OTk0IF0sXG5cdFwic2hhcnBcIjogWyA5ODM5IF0sXG5cdFwiU0hDSGN5XCI6IFsgMTA2NSBdLFxuXHRcInNoY2hjeVwiOiBbIDEwOTcgXSxcblx0XCJTSGN5XCI6IFsgMTA2NCBdLFxuXHRcInNoY3lcIjogWyAxMDk2IF0sXG5cdFwiU2hvcnREb3duQXJyb3dcIjogWyA4NTk1IF0sXG5cdFwiU2hvcnRMZWZ0QXJyb3dcIjogWyA4NTkyIF0sXG5cdFwic2hvcnRtaWRcIjogWyA4NzM5IF0sXG5cdFwic2hvcnRwYXJhbGxlbFwiOiBbIDg3NDEgXSxcblx0XCJTaG9ydFJpZ2h0QXJyb3dcIjogWyA4NTk0IF0sXG5cdFwiU2hvcnRVcEFycm93XCI6IFsgODU5MyBdLFxuXHRcInNoeVwiOiBbIDE3MyBdLFxuXHRcIlNpZ21hXCI6IFsgOTMxIF0sXG5cdFwic2lnbWFcIjogWyA5NjMgXSxcblx0XCJzaWdtYWZcIjogWyA5NjIgXSxcblx0XCJzaWdtYXZcIjogWyA5NjIgXSxcblx0XCJzaW1cIjogWyA4NzY0IF0sXG5cdFwic2ltZG90XCI6IFsgMTA4NTggXSxcblx0XCJzaW1lXCI6IFsgODc3MSBdLFxuXHRcInNpbWVxXCI6IFsgODc3MSBdLFxuXHRcInNpbWdcIjogWyAxMDkxMCBdLFxuXHRcInNpbWdFXCI6IFsgMTA5MTIgXSxcblx0XCJzaW1sXCI6IFsgMTA5MDkgXSxcblx0XCJzaW1sRVwiOiBbIDEwOTExIF0sXG5cdFwic2ltbmVcIjogWyA4Nzc0IF0sXG5cdFwic2ltcGx1c1wiOiBbIDEwNzg4IF0sXG5cdFwic2ltcmFyclwiOiBbIDEwNjEwIF0sXG5cdFwic2xhcnJcIjogWyA4NTkyIF0sXG5cdFwiU21hbGxDaXJjbGVcIjogWyA4NzI4IF0sXG5cdFwic21hbGxzZXRtaW51c1wiOiBbIDg3MjYgXSxcblx0XCJzbWFzaHBcIjogWyAxMDgwMyBdLFxuXHRcInNtZXBhcnNsXCI6IFsgMTA3MjQgXSxcblx0XCJzbWlkXCI6IFsgODczOSBdLFxuXHRcInNtaWxlXCI6IFsgODk5NSBdLFxuXHRcInNtdFwiOiBbIDEwOTIyIF0sXG5cdFwic210ZVwiOiBbIDEwOTI0IF0sXG5cdFwic210ZXNcIjogWyAxMDkyNCwgNjUwMjQgXSxcblx0XCJTT0ZUY3lcIjogWyAxMDY4IF0sXG5cdFwic29mdGN5XCI6IFsgMTEwMCBdLFxuXHRcInNvbFwiOiBbIDQ3IF0sXG5cdFwic29sYlwiOiBbIDEwNjkyIF0sXG5cdFwic29sYmFyXCI6IFsgOTAyMyBdLFxuXHRcIlNvcGZcIjogWyAxMjAxMzggXSxcblx0XCJzb3BmXCI6IFsgMTIwMTY0IF0sXG5cdFwic3BhZGVzXCI6IFsgOTgyNCBdLFxuXHRcInNwYWRlc3VpdFwiOiBbIDk4MjQgXSxcblx0XCJzcGFyXCI6IFsgODc0MSBdLFxuXHRcInNxY2FwXCI6IFsgODg1MSBdLFxuXHRcInNxY2Fwc1wiOiBbIDg4NTEsIDY1MDI0IF0sXG5cdFwic3FjdXBcIjogWyA4ODUyIF0sXG5cdFwic3FjdXBzXCI6IFsgODg1MiwgNjUwMjQgXSxcblx0XCJTcXJ0XCI6IFsgODczMCBdLFxuXHRcInNxc3ViXCI6IFsgODg0NyBdLFxuXHRcInNxc3ViZVwiOiBbIDg4NDkgXSxcblx0XCJzcXN1YnNldFwiOiBbIDg4NDcgXSxcblx0XCJzcXN1YnNldGVxXCI6IFsgODg0OSBdLFxuXHRcInNxc3VwXCI6IFsgODg0OCBdLFxuXHRcInNxc3VwZVwiOiBbIDg4NTAgXSxcblx0XCJzcXN1cHNldFwiOiBbIDg4NDggXSxcblx0XCJzcXN1cHNldGVxXCI6IFsgODg1MCBdLFxuXHRcInNxdVwiOiBbIDk2MzMgXSxcblx0XCJTcXVhcmVcIjogWyA5NjMzIF0sXG5cdFwic3F1YXJlXCI6IFsgOTYzMyBdLFxuXHRcIlNxdWFyZUludGVyc2VjdGlvblwiOiBbIDg4NTEgXSxcblx0XCJTcXVhcmVTdWJzZXRcIjogWyA4ODQ3IF0sXG5cdFwiU3F1YXJlU3Vic2V0RXF1YWxcIjogWyA4ODQ5IF0sXG5cdFwiU3F1YXJlU3VwZXJzZXRcIjogWyA4ODQ4IF0sXG5cdFwiU3F1YXJlU3VwZXJzZXRFcXVhbFwiOiBbIDg4NTAgXSxcblx0XCJTcXVhcmVVbmlvblwiOiBbIDg4NTIgXSxcblx0XCJzcXVhcmZcIjogWyA5NjQyIF0sXG5cdFwic3F1ZlwiOiBbIDk2NDIgXSxcblx0XCJzcmFyclwiOiBbIDg1OTQgXSxcblx0XCJTc2NyXCI6IFsgMTE5OTgyIF0sXG5cdFwic3NjclwiOiBbIDEyMDAwOCBdLFxuXHRcInNzZXRtblwiOiBbIDg3MjYgXSxcblx0XCJzc21pbGVcIjogWyA4OTk1IF0sXG5cdFwic3N0YXJmXCI6IFsgODkwMiBdLFxuXHRcIlN0YXJcIjogWyA4OTAyIF0sXG5cdFwic3RhclwiOiBbIDk3MzQgXSxcblx0XCJzdGFyZlwiOiBbIDk3MzMgXSxcblx0XCJzdHJhaWdodGVwc2lsb25cIjogWyAxMDEzIF0sXG5cdFwic3RyYWlnaHRwaGlcIjogWyA5ODEgXSxcblx0XCJzdHJuc1wiOiBbIDE3NSBdLFxuXHRcIlN1YlwiOiBbIDg5MTIgXSxcblx0XCJzdWJcIjogWyA4ODM0IF0sXG5cdFwic3ViZG90XCI6IFsgMTA5NDEgXSxcblx0XCJzdWJFXCI6IFsgMTA5NDkgXSxcblx0XCJzdWJlXCI6IFsgODgzOCBdLFxuXHRcInN1YmVkb3RcIjogWyAxMDk0NyBdLFxuXHRcInN1Ym11bHRcIjogWyAxMDk0NSBdLFxuXHRcInN1Ym5FXCI6IFsgMTA5NTUgXSxcblx0XCJzdWJuZVwiOiBbIDg4NDIgXSxcblx0XCJzdWJwbHVzXCI6IFsgMTA5NDMgXSxcblx0XCJzdWJyYXJyXCI6IFsgMTA2MTcgXSxcblx0XCJTdWJzZXRcIjogWyA4OTEyIF0sXG5cdFwic3Vic2V0XCI6IFsgODgzNCBdLFxuXHRcInN1YnNldGVxXCI6IFsgODgzOCBdLFxuXHRcInN1YnNldGVxcVwiOiBbIDEwOTQ5IF0sXG5cdFwiU3Vic2V0RXF1YWxcIjogWyA4ODM4IF0sXG5cdFwic3Vic2V0bmVxXCI6IFsgODg0MiBdLFxuXHRcInN1YnNldG5lcXFcIjogWyAxMDk1NSBdLFxuXHRcInN1YnNpbVwiOiBbIDEwOTUxIF0sXG5cdFwic3Vic3ViXCI6IFsgMTA5NjUgXSxcblx0XCJzdWJzdXBcIjogWyAxMDk2MyBdLFxuXHRcInN1Y2NcIjogWyA4ODI3IF0sXG5cdFwic3VjY2FwcHJveFwiOiBbIDEwOTM2IF0sXG5cdFwic3VjY2N1cmx5ZXFcIjogWyA4ODI5IF0sXG5cdFwiU3VjY2VlZHNcIjogWyA4ODI3IF0sXG5cdFwiU3VjY2VlZHNFcXVhbFwiOiBbIDEwOTI4IF0sXG5cdFwiU3VjY2VlZHNTbGFudEVxdWFsXCI6IFsgODgyOSBdLFxuXHRcIlN1Y2NlZWRzVGlsZGVcIjogWyA4ODMxIF0sXG5cdFwic3VjY2VxXCI6IFsgMTA5MjggXSxcblx0XCJzdWNjbmFwcHJveFwiOiBbIDEwOTM4IF0sXG5cdFwic3VjY25lcXFcIjogWyAxMDkzNCBdLFxuXHRcInN1Y2Nuc2ltXCI6IFsgODkzNyBdLFxuXHRcInN1Y2NzaW1cIjogWyA4ODMxIF0sXG5cdFwiU3VjaFRoYXRcIjogWyA4NzE1IF0sXG5cdFwiU3VtXCI6IFsgODcyMSBdLFxuXHRcInN1bVwiOiBbIDg3MjEgXSxcblx0XCJzdW5nXCI6IFsgOTgzNCBdLFxuXHRcIlN1cFwiOiBbIDg5MTMgXSxcblx0XCJzdXBcIjogWyA4ODM1IF0sXG5cdFwic3VwMVwiOiBbIDE4NSBdLFxuXHRcInN1cDJcIjogWyAxNzggXSxcblx0XCJzdXAzXCI6IFsgMTc5IF0sXG5cdFwic3VwZG90XCI6IFsgMTA5NDIgXSxcblx0XCJzdXBkc3ViXCI6IFsgMTA5NjggXSxcblx0XCJzdXBFXCI6IFsgMTA5NTAgXSxcblx0XCJzdXBlXCI6IFsgODgzOSBdLFxuXHRcInN1cGVkb3RcIjogWyAxMDk0OCBdLFxuXHRcIlN1cGVyc2V0XCI6IFsgODgzNSBdLFxuXHRcIlN1cGVyc2V0RXF1YWxcIjogWyA4ODM5IF0sXG5cdFwic3VwaHNvbFwiOiBbIDEwMTg1IF0sXG5cdFwic3VwaHN1YlwiOiBbIDEwOTY3IF0sXG5cdFwic3VwbGFyclwiOiBbIDEwNjE5IF0sXG5cdFwic3VwbXVsdFwiOiBbIDEwOTQ2IF0sXG5cdFwic3VwbkVcIjogWyAxMDk1NiBdLFxuXHRcInN1cG5lXCI6IFsgODg0MyBdLFxuXHRcInN1cHBsdXNcIjogWyAxMDk0NCBdLFxuXHRcIlN1cHNldFwiOiBbIDg5MTMgXSxcblx0XCJzdXBzZXRcIjogWyA4ODM1IF0sXG5cdFwic3Vwc2V0ZXFcIjogWyA4ODM5IF0sXG5cdFwic3Vwc2V0ZXFxXCI6IFsgMTA5NTAgXSxcblx0XCJzdXBzZXRuZXFcIjogWyA4ODQzIF0sXG5cdFwic3Vwc2V0bmVxcVwiOiBbIDEwOTU2IF0sXG5cdFwic3Vwc2ltXCI6IFsgMTA5NTIgXSxcblx0XCJzdXBzdWJcIjogWyAxMDk2NCBdLFxuXHRcInN1cHN1cFwiOiBbIDEwOTY2IF0sXG5cdFwic3dhcmhrXCI6IFsgMTA1MzQgXSxcblx0XCJzd0FyclwiOiBbIDg2NjUgXSxcblx0XCJzd2FyclwiOiBbIDg2MDEgXSxcblx0XCJzd2Fycm93XCI6IFsgODYwMSBdLFxuXHRcInN3bndhclwiOiBbIDEwNTM4IF0sXG5cdFwic3psaWdcIjogWyAyMjMgXSxcblx0XCJUYWJcIjogWyA5IF0sXG5cdFwidGFyZ2V0XCI6IFsgODk4MiBdLFxuXHRcIlRhdVwiOiBbIDkzMiBdLFxuXHRcInRhdVwiOiBbIDk2NCBdLFxuXHRcInRicmtcIjogWyA5MTQwIF0sXG5cdFwiVGNhcm9uXCI6IFsgMzU2IF0sXG5cdFwidGNhcm9uXCI6IFsgMzU3IF0sXG5cdFwiVGNlZGlsXCI6IFsgMzU0IF0sXG5cdFwidGNlZGlsXCI6IFsgMzU1IF0sXG5cdFwiVGN5XCI6IFsgMTA1OCBdLFxuXHRcInRjeVwiOiBbIDEwOTAgXSxcblx0XCJ0ZG90XCI6IFsgODQxMSBdLFxuXHRcInRlbHJlY1wiOiBbIDg5ODEgXSxcblx0XCJUZnJcIjogWyAxMjAwODcgXSxcblx0XCJ0ZnJcIjogWyAxMjAxMTMgXSxcblx0XCJ0aGVyZTRcIjogWyA4NzU2IF0sXG5cdFwiVGhlcmVmb3JlXCI6IFsgODc1NiBdLFxuXHRcInRoZXJlZm9yZVwiOiBbIDg3NTYgXSxcblx0XCJUaGV0YVwiOiBbIDkyMCBdLFxuXHRcInRoZXRhXCI6IFsgOTUyIF0sXG5cdFwidGhldGFzeW1cIjogWyA5NzcgXSxcblx0XCJ0aGV0YXZcIjogWyA5NzcgXSxcblx0XCJ0aGlja2FwcHJveFwiOiBbIDg3NzYgXSxcblx0XCJ0aGlja3NpbVwiOiBbIDg3NjQgXSxcblx0XCJUaGlja1NwYWNlXCI6IFsgODI4NywgODIwMiBdLFxuXHRcInRoaW5zcFwiOiBbIDgyMDEgXSxcblx0XCJUaGluU3BhY2VcIjogWyA4MjAxIF0sXG5cdFwidGhrYXBcIjogWyA4Nzc2IF0sXG5cdFwidGhrc2ltXCI6IFsgODc2NCBdLFxuXHRcIlRIT1JOXCI6IFsgMjIyIF0sXG5cdFwidGhvcm5cIjogWyAyNTQgXSxcblx0XCJUaWxkZVwiOiBbIDg3NjQgXSxcblx0XCJ0aWxkZVwiOiBbIDczMiBdLFxuXHRcIlRpbGRlRXF1YWxcIjogWyA4NzcxIF0sXG5cdFwiVGlsZGVGdWxsRXF1YWxcIjogWyA4NzczIF0sXG5cdFwiVGlsZGVUaWxkZVwiOiBbIDg3NzYgXSxcblx0XCJ0aW1lc1wiOiBbIDIxNSBdLFxuXHRcInRpbWVzYlwiOiBbIDg4NjQgXSxcblx0XCJ0aW1lc2JhclwiOiBbIDEwODAxIF0sXG5cdFwidGltZXNkXCI6IFsgMTA4MDAgXSxcblx0XCJ0aW50XCI6IFsgODc0OSBdLFxuXHRcInRvZWFcIjogWyAxMDUzNiBdLFxuXHRcInRvcFwiOiBbIDg4NjggXSxcblx0XCJ0b3Bib3RcIjogWyA5MDE0IF0sXG5cdFwidG9wY2lyXCI6IFsgMTA5OTMgXSxcblx0XCJUb3BmXCI6IFsgMTIwMTM5IF0sXG5cdFwidG9wZlwiOiBbIDEyMDE2NSBdLFxuXHRcInRvcGZvcmtcIjogWyAxMDk3MCBdLFxuXHRcInRvc2FcIjogWyAxMDUzNyBdLFxuXHRcInRwcmltZVwiOiBbIDgyNDQgXSxcblx0XCJUUkFERVwiOiBbIDg0ODIgXSxcblx0XCJ0cmFkZVwiOiBbIDg0ODIgXSxcblx0XCJ0cmlhbmdsZVwiOiBbIDk2NTMgXSxcblx0XCJ0cmlhbmdsZWRvd25cIjogWyA5NjYzIF0sXG5cdFwidHJpYW5nbGVsZWZ0XCI6IFsgOTY2NyBdLFxuXHRcInRyaWFuZ2xlbGVmdGVxXCI6IFsgODg4NCBdLFxuXHRcInRyaWFuZ2xlcVwiOiBbIDg3OTYgXSxcblx0XCJ0cmlhbmdsZXJpZ2h0XCI6IFsgOTY1NyBdLFxuXHRcInRyaWFuZ2xlcmlnaHRlcVwiOiBbIDg4ODUgXSxcblx0XCJ0cmlkb3RcIjogWyA5NzA4IF0sXG5cdFwidHJpZVwiOiBbIDg3OTYgXSxcblx0XCJ0cmltaW51c1wiOiBbIDEwODEwIF0sXG5cdFwiVHJpcGxlRG90XCI6IFsgODQxMSBdLFxuXHRcInRyaXBsdXNcIjogWyAxMDgwOSBdLFxuXHRcInRyaXNiXCI6IFsgMTA3MDEgXSxcblx0XCJ0cml0aW1lXCI6IFsgMTA4MTEgXSxcblx0XCJ0cnBleml1bVwiOiBbIDkxODYgXSxcblx0XCJUc2NyXCI6IFsgMTE5OTgzIF0sXG5cdFwidHNjclwiOiBbIDEyMDAwOSBdLFxuXHRcIlRTY3lcIjogWyAxMDYyIF0sXG5cdFwidHNjeVwiOiBbIDEwOTQgXSxcblx0XCJUU0hjeVwiOiBbIDEwMzUgXSxcblx0XCJ0c2hjeVwiOiBbIDExMTUgXSxcblx0XCJUc3Ryb2tcIjogWyAzNTggXSxcblx0XCJ0c3Ryb2tcIjogWyAzNTkgXSxcblx0XCJ0d2l4dFwiOiBbIDg4MTIgXSxcblx0XCJ0d29oZWFkbGVmdGFycm93XCI6IFsgODYwNiBdLFxuXHRcInR3b2hlYWRyaWdodGFycm93XCI6IFsgODYwOCBdLFxuXHRcIlVhY3V0ZVwiOiBbIDIxOCBdLFxuXHRcInVhY3V0ZVwiOiBbIDI1MCBdLFxuXHRcIlVhcnJcIjogWyA4NjA3IF0sXG5cdFwidUFyclwiOiBbIDg2NTcgXSxcblx0XCJ1YXJyXCI6IFsgODU5MyBdLFxuXHRcIlVhcnJvY2lyXCI6IFsgMTA1NjkgXSxcblx0XCJVYnJjeVwiOiBbIDEwMzggXSxcblx0XCJ1YnJjeVwiOiBbIDExMTggXSxcblx0XCJVYnJldmVcIjogWyAzNjQgXSxcblx0XCJ1YnJldmVcIjogWyAzNjUgXSxcblx0XCJVY2lyY1wiOiBbIDIxOSBdLFxuXHRcInVjaXJjXCI6IFsgMjUxIF0sXG5cdFwiVWN5XCI6IFsgMTA1OSBdLFxuXHRcInVjeVwiOiBbIDEwOTEgXSxcblx0XCJ1ZGFyclwiOiBbIDg2NDUgXSxcblx0XCJVZGJsYWNcIjogWyAzNjggXSxcblx0XCJ1ZGJsYWNcIjogWyAzNjkgXSxcblx0XCJ1ZGhhclwiOiBbIDEwNjA2IF0sXG5cdFwidWZpc2h0XCI6IFsgMTA2MjIgXSxcblx0XCJVZnJcIjogWyAxMjAwODggXSxcblx0XCJ1ZnJcIjogWyAxMjAxMTQgXSxcblx0XCJVZ3JhdmVcIjogWyAyMTcgXSxcblx0XCJ1Z3JhdmVcIjogWyAyNDkgXSxcblx0XCJ1SGFyXCI6IFsgMTA1OTUgXSxcblx0XCJ1aGFybFwiOiBbIDg2MzkgXSxcblx0XCJ1aGFyclwiOiBbIDg2MzggXSxcblx0XCJ1aGJsa1wiOiBbIDk2MDAgXSxcblx0XCJ1bGNvcm5cIjogWyA4OTg4IF0sXG5cdFwidWxjb3JuZXJcIjogWyA4OTg4IF0sXG5cdFwidWxjcm9wXCI6IFsgODk3NSBdLFxuXHRcInVsdHJpXCI6IFsgOTcyMCBdLFxuXHRcIlVtYWNyXCI6IFsgMzYyIF0sXG5cdFwidW1hY3JcIjogWyAzNjMgXSxcblx0XCJ1bWxcIjogWyAxNjggXSxcblx0XCJVbmRlckJhclwiOiBbIDk1IF0sXG5cdFwiVW5kZXJCcmFjZVwiOiBbIDkxODMgXSxcblx0XCJVbmRlckJyYWNrZXRcIjogWyA5MTQxIF0sXG5cdFwiVW5kZXJQYXJlbnRoZXNpc1wiOiBbIDkxODEgXSxcblx0XCJVbmlvblwiOiBbIDg4OTkgXSxcblx0XCJVbmlvblBsdXNcIjogWyA4ODQ2IF0sXG5cdFwiVW9nb25cIjogWyAzNzAgXSxcblx0XCJ1b2dvblwiOiBbIDM3MSBdLFxuXHRcIlVvcGZcIjogWyAxMjAxNDAgXSxcblx0XCJ1b3BmXCI6IFsgMTIwMTY2IF0sXG5cdFwiVXBBcnJvd1wiOiBbIDg1OTMgXSxcblx0XCJVcGFycm93XCI6IFsgODY1NyBdLFxuXHRcInVwYXJyb3dcIjogWyA4NTkzIF0sXG5cdFwiVXBBcnJvd0JhclwiOiBbIDEwNTE0IF0sXG5cdFwiVXBBcnJvd0Rvd25BcnJvd1wiOiBbIDg2NDUgXSxcblx0XCJVcERvd25BcnJvd1wiOiBbIDg1OTcgXSxcblx0XCJVcGRvd25hcnJvd1wiOiBbIDg2NjEgXSxcblx0XCJ1cGRvd25hcnJvd1wiOiBbIDg1OTcgXSxcblx0XCJVcEVxdWlsaWJyaXVtXCI6IFsgMTA2MDYgXSxcblx0XCJ1cGhhcnBvb25sZWZ0XCI6IFsgODYzOSBdLFxuXHRcInVwaGFycG9vbnJpZ2h0XCI6IFsgODYzOCBdLFxuXHRcInVwbHVzXCI6IFsgODg0NiBdLFxuXHRcIlVwcGVyTGVmdEFycm93XCI6IFsgODU5OCBdLFxuXHRcIlVwcGVyUmlnaHRBcnJvd1wiOiBbIDg1OTkgXSxcblx0XCJVcHNpXCI6IFsgOTc4IF0sXG5cdFwidXBzaVwiOiBbIDk2NSBdLFxuXHRcInVwc2loXCI6IFsgOTc4IF0sXG5cdFwiVXBzaWxvblwiOiBbIDkzMyBdLFxuXHRcInVwc2lsb25cIjogWyA5NjUgXSxcblx0XCJVcFRlZVwiOiBbIDg4NjkgXSxcblx0XCJVcFRlZUFycm93XCI6IFsgODYxMyBdLFxuXHRcInVwdXBhcnJvd3NcIjogWyA4NjQ4IF0sXG5cdFwidXJjb3JuXCI6IFsgODk4OSBdLFxuXHRcInVyY29ybmVyXCI6IFsgODk4OSBdLFxuXHRcInVyY3JvcFwiOiBbIDg5NzQgXSxcblx0XCJVcmluZ1wiOiBbIDM2NiBdLFxuXHRcInVyaW5nXCI6IFsgMzY3IF0sXG5cdFwidXJ0cmlcIjogWyA5NzIxIF0sXG5cdFwiVXNjclwiOiBbIDExOTk4NCBdLFxuXHRcInVzY3JcIjogWyAxMjAwMTAgXSxcblx0XCJ1dGRvdFwiOiBbIDg5NDQgXSxcblx0XCJVdGlsZGVcIjogWyAzNjAgXSxcblx0XCJ1dGlsZGVcIjogWyAzNjEgXSxcblx0XCJ1dHJpXCI6IFsgOTY1MyBdLFxuXHRcInV0cmlmXCI6IFsgOTY1MiBdLFxuXHRcInV1YXJyXCI6IFsgODY0OCBdLFxuXHRcIlV1bWxcIjogWyAyMjAgXSxcblx0XCJ1dW1sXCI6IFsgMjUyIF0sXG5cdFwidXdhbmdsZVwiOiBbIDEwNjYzIF0sXG5cdFwidmFuZ3J0XCI6IFsgMTA2NTIgXSxcblx0XCJ2YXJlcHNpbG9uXCI6IFsgMTAxMyBdLFxuXHRcInZhcmthcHBhXCI6IFsgMTAwOCBdLFxuXHRcInZhcm5vdGhpbmdcIjogWyA4NzA5IF0sXG5cdFwidmFycGhpXCI6IFsgOTgxIF0sXG5cdFwidmFycGlcIjogWyA5ODIgXSxcblx0XCJ2YXJwcm9wdG9cIjogWyA4NzMzIF0sXG5cdFwidkFyclwiOiBbIDg2NjEgXSxcblx0XCJ2YXJyXCI6IFsgODU5NyBdLFxuXHRcInZhcnJob1wiOiBbIDEwMDkgXSxcblx0XCJ2YXJzaWdtYVwiOiBbIDk2MiBdLFxuXHRcInZhcnN1YnNldG5lcVwiOiBbIDg4NDIsIDY1MDI0IF0sXG5cdFwidmFyc3Vic2V0bmVxcVwiOiBbIDEwOTU1LCA2NTAyNCBdLFxuXHRcInZhcnN1cHNldG5lcVwiOiBbIDg4NDMsIDY1MDI0IF0sXG5cdFwidmFyc3Vwc2V0bmVxcVwiOiBbIDEwOTU2LCA2NTAyNCBdLFxuXHRcInZhcnRoZXRhXCI6IFsgOTc3IF0sXG5cdFwidmFydHJpYW5nbGVsZWZ0XCI6IFsgODg4MiBdLFxuXHRcInZhcnRyaWFuZ2xlcmlnaHRcIjogWyA4ODgzIF0sXG5cdFwiVmJhclwiOiBbIDEwOTg3IF0sXG5cdFwidkJhclwiOiBbIDEwOTg0IF0sXG5cdFwidkJhcnZcIjogWyAxMDk4NSBdLFxuXHRcIlZjeVwiOiBbIDEwNDIgXSxcblx0XCJ2Y3lcIjogWyAxMDc0IF0sXG5cdFwiVkRhc2hcIjogWyA4ODc1IF0sXG5cdFwiVmRhc2hcIjogWyA4ODczIF0sXG5cdFwidkRhc2hcIjogWyA4ODcyIF0sXG5cdFwidmRhc2hcIjogWyA4ODY2IF0sXG5cdFwiVmRhc2hsXCI6IFsgMTA5ODIgXSxcblx0XCJWZWVcIjogWyA4ODk3IF0sXG5cdFwidmVlXCI6IFsgODc0NCBdLFxuXHRcInZlZWJhclwiOiBbIDg4OTEgXSxcblx0XCJ2ZWVlcVwiOiBbIDg3OTQgXSxcblx0XCJ2ZWxsaXBcIjogWyA4OTQyIF0sXG5cdFwiVmVyYmFyXCI6IFsgODIxNCBdLFxuXHRcInZlcmJhclwiOiBbIDEyNCBdLFxuXHRcIlZlcnRcIjogWyA4MjE0IF0sXG5cdFwidmVydFwiOiBbIDEyNCBdLFxuXHRcIlZlcnRpY2FsQmFyXCI6IFsgODczOSBdLFxuXHRcIlZlcnRpY2FsTGluZVwiOiBbIDEyNCBdLFxuXHRcIlZlcnRpY2FsU2VwYXJhdG9yXCI6IFsgMTAwNzIgXSxcblx0XCJWZXJ0aWNhbFRpbGRlXCI6IFsgODc2OCBdLFxuXHRcIlZlcnlUaGluU3BhY2VcIjogWyA4MjAyIF0sXG5cdFwiVmZyXCI6IFsgMTIwMDg5IF0sXG5cdFwidmZyXCI6IFsgMTIwMTE1IF0sXG5cdFwidmx0cmlcIjogWyA4ODgyIF0sXG5cdFwidm5zdWJcIjogWyA4ODM0LCA4NDAyIF0sXG5cdFwidm5zdXBcIjogWyA4ODM1LCA4NDAyIF0sXG5cdFwiVm9wZlwiOiBbIDEyMDE0MSBdLFxuXHRcInZvcGZcIjogWyAxMjAxNjcgXSxcblx0XCJ2cHJvcFwiOiBbIDg3MzMgXSxcblx0XCJ2cnRyaVwiOiBbIDg4ODMgXSxcblx0XCJWc2NyXCI6IFsgMTE5OTg1IF0sXG5cdFwidnNjclwiOiBbIDEyMDAxMSBdLFxuXHRcInZzdWJuRVwiOiBbIDEwOTU1LCA2NTAyNCBdLFxuXHRcInZzdWJuZVwiOiBbIDg4NDIsIDY1MDI0IF0sXG5cdFwidnN1cG5FXCI6IFsgMTA5NTYsIDY1MDI0IF0sXG5cdFwidnN1cG5lXCI6IFsgODg0MywgNjUwMjQgXSxcblx0XCJWdmRhc2hcIjogWyA4ODc0IF0sXG5cdFwidnppZ3phZ1wiOiBbIDEwNjUwIF0sXG5cdFwiV2NpcmNcIjogWyAzNzIgXSxcblx0XCJ3Y2lyY1wiOiBbIDM3MyBdLFxuXHRcIndlZGJhclwiOiBbIDEwODQ3IF0sXG5cdFwiV2VkZ2VcIjogWyA4ODk2IF0sXG5cdFwid2VkZ2VcIjogWyA4NzQzIF0sXG5cdFwid2VkZ2VxXCI6IFsgODc5MyBdLFxuXHRcIndlaWVycFwiOiBbIDg0NzIgXSxcblx0XCJXZnJcIjogWyAxMjAwOTAgXSxcblx0XCJ3ZnJcIjogWyAxMjAxMTYgXSxcblx0XCJXb3BmXCI6IFsgMTIwMTQyIF0sXG5cdFwid29wZlwiOiBbIDEyMDE2OCBdLFxuXHRcIndwXCI6IFsgODQ3MiBdLFxuXHRcIndyXCI6IFsgODc2OCBdLFxuXHRcIndyZWF0aFwiOiBbIDg3NjggXSxcblx0XCJXc2NyXCI6IFsgMTE5OTg2IF0sXG5cdFwid3NjclwiOiBbIDEyMDAxMiBdLFxuXHRcInhjYXBcIjogWyA4ODk4IF0sXG5cdFwieGNpcmNcIjogWyA5NzExIF0sXG5cdFwieGN1cFwiOiBbIDg4OTkgXSxcblx0XCJ4ZHRyaVwiOiBbIDk2NjEgXSxcblx0XCJYZnJcIjogWyAxMjAwOTEgXSxcblx0XCJ4ZnJcIjogWyAxMjAxMTcgXSxcblx0XCJ4aEFyclwiOiBbIDEwMjM0IF0sXG5cdFwieGhhcnJcIjogWyAxMDIzMSBdLFxuXHRcIlhpXCI6IFsgOTI2IF0sXG5cdFwieGlcIjogWyA5NTggXSxcblx0XCJ4bEFyclwiOiBbIDEwMjMyIF0sXG5cdFwieGxhcnJcIjogWyAxMDIyOSBdLFxuXHRcInhtYXBcIjogWyAxMDIzNiBdLFxuXHRcInhuaXNcIjogWyA4OTU1IF0sXG5cdFwieG9kb3RcIjogWyAxMDc1MiBdLFxuXHRcIlhvcGZcIjogWyAxMjAxNDMgXSxcblx0XCJ4b3BmXCI6IFsgMTIwMTY5IF0sXG5cdFwieG9wbHVzXCI6IFsgMTA3NTMgXSxcblx0XCJ4b3RpbWVcIjogWyAxMDc1NCBdLFxuXHRcInhyQXJyXCI6IFsgMTAyMzMgXSxcblx0XCJ4cmFyclwiOiBbIDEwMjMwIF0sXG5cdFwiWHNjclwiOiBbIDExOTk4NyBdLFxuXHRcInhzY3JcIjogWyAxMjAwMTMgXSxcblx0XCJ4c3FjdXBcIjogWyAxMDc1OCBdLFxuXHRcInh1cGx1c1wiOiBbIDEwNzU2IF0sXG5cdFwieHV0cmlcIjogWyA5NjUxIF0sXG5cdFwieHZlZVwiOiBbIDg4OTcgXSxcblx0XCJ4d2VkZ2VcIjogWyA4ODk2IF0sXG5cdFwiWWFjdXRlXCI6IFsgMjIxIF0sXG5cdFwieWFjdXRlXCI6IFsgMjUzIF0sXG5cdFwiWUFjeVwiOiBbIDEwNzEgXSxcblx0XCJ5YWN5XCI6IFsgMTEwMyBdLFxuXHRcIlljaXJjXCI6IFsgMzc0IF0sXG5cdFwieWNpcmNcIjogWyAzNzUgXSxcblx0XCJZY3lcIjogWyAxMDY3IF0sXG5cdFwieWN5XCI6IFsgMTA5OSBdLFxuXHRcInllblwiOiBbIDE2NSBdLFxuXHRcIllmclwiOiBbIDEyMDA5MiBdLFxuXHRcInlmclwiOiBbIDEyMDExOCBdLFxuXHRcIllJY3lcIjogWyAxMDMxIF0sXG5cdFwieWljeVwiOiBbIDExMTEgXSxcblx0XCJZb3BmXCI6IFsgMTIwMTQ0IF0sXG5cdFwieW9wZlwiOiBbIDEyMDE3MCBdLFxuXHRcIllzY3JcIjogWyAxMTk5ODggXSxcblx0XCJ5c2NyXCI6IFsgMTIwMDE0IF0sXG5cdFwiWVVjeVwiOiBbIDEwNzAgXSxcblx0XCJ5dWN5XCI6IFsgMTEwMiBdLFxuXHRcIll1bWxcIjogWyAzNzYgXSxcblx0XCJ5dW1sXCI6IFsgMjU1IF0sXG5cdFwiWmFjdXRlXCI6IFsgMzc3IF0sXG5cdFwiemFjdXRlXCI6IFsgMzc4IF0sXG5cdFwiWmNhcm9uXCI6IFsgMzgxIF0sXG5cdFwiemNhcm9uXCI6IFsgMzgyIF0sXG5cdFwiWmN5XCI6IFsgMTA0NyBdLFxuXHRcInpjeVwiOiBbIDEwNzkgXSxcblx0XCJaZG90XCI6IFsgMzc5IF0sXG5cdFwiemRvdFwiOiBbIDM4MCBdLFxuXHRcInplZXRyZlwiOiBbIDg0ODggXSxcblx0XCJaZXJvV2lkdGhTcGFjZVwiOiBbIDgyMDMgXSxcblx0XCJaZXRhXCI6IFsgOTE4IF0sXG5cdFwiemV0YVwiOiBbIDk1MCBdLFxuXHRcIlpmclwiOiBbIDg0ODggXSxcblx0XCJ6ZnJcIjogWyAxMjAxMTkgXSxcblx0XCJaSGN5XCI6IFsgMTA0NiBdLFxuXHRcInpoY3lcIjogWyAxMDc4IF0sXG5cdFwiemlncmFyclwiOiBbIDg2NjkgXSxcblx0XCJab3BmXCI6IFsgODQ4NCBdLFxuXHRcInpvcGZcIjogWyAxMjAxNzEgXSxcblx0XCJac2NyXCI6IFsgMTE5OTg5IF0sXG5cdFwienNjclwiOiBbIDEyMDAxNSBdLFxuXHRcInp3alwiOiBbIDgyMDUgXSxcblx0XCJ6d25qXCI6IFsgODIwNCBdXG59IiwiLy8gdGhlIHJhdyBtYXBwaW5nIG9mIHNwZWNpYWwgbmFtZXMgdG8gdW5pY29kZSB2YWx1ZXNcbnZhciBieV9uYW1lID1cbmV4cG9ydHMuYnlfbmFtZSA9IHJlcXVpcmUoXCIuLi9kYXRhL2VudGl0aWVzLmpzb25cIik7XG5cbi8vIGluZGV4IHRoZSByZXZlcnNlIG1hcHBpbmdzIGltbWVkaWF0ZWx5XG52YXIgYnlfY29kZSA9IGV4cG9ydHMuYnlfY29kZSA9IHt9O1xuT2JqZWN0LmtleXMoYnlfbmFtZSkuZm9yRWFjaChmdW5jdGlvbihuKSB7XG5cdGJ5X25hbWVbbl0uZm9yRWFjaChmdW5jdGlvbihjKSB7XG5cdFx0aWYgKGJ5X2NvZGVbY10gPT0gbnVsbCkgYnlfY29kZVtjXSA9IFtdO1xuXHRcdGJ5X2NvZGVbY10ucHVzaChuKTtcblx0fSk7XG59KTtcblxuLy8gYSBmZXcgcmVndWxhciBleHByZXNzaW9ucyBmb3IgcGFyc2luZyBlbnRpdGllc1xudmFyIGhleF92YWx1ZV9yZWdleCA9IC9eMD94KFthLWYwLTldKykkL2ksXG5cdGVudGl0eV9yZWdleCA9IC8mKD86I3hbYS1mMC05XSt8I1swLTldK3xbYS16MC05XSspOz8vaWcsXG5cdGRlY19lbnRpdHlfcmVnZXggPSAvXiYjKFswLTldKyk7PyQvaSxcblx0aGV4X2VudGl0eV9yZWdleCA9IC9eJiN4KFthLWYwLTldKyk7PyQvaSxcblx0c3BlY19lbnRpdHlfcmVnZXggPSAvXiYoW2EtejAtOV0rKTs/JC9pO1xuXG4vLyBjb252ZXJ0cyBhbGwgZW50aXRpZXMgZm91bmQgaW4gc3RyaW5nIHRvIHNwZWNpZmljIGZvcm1hdFxudmFyIG5vcm1hbGl6ZUVudGl0aWVzID1cbmV4cG9ydHMubm9ybWFsaXplRW50aXRpZXMgPSBmdW5jdGlvbihzdHIsIGZvcm1hdCkge1xuXHRyZXR1cm4gc3RyLnJlcGxhY2UoZW50aXR5X3JlZ2V4LCBmdW5jdGlvbihlbnRpdHkpIHtcblx0XHRyZXR1cm4gY29udmVydChlbnRpdHksIGZvcm1hdCkgfHwgZW50aXR5O1xuXHR9KTtcbn1cblxuLy8gY29udmVydHMgYWxsIGVudGl0aWVzIGFuZCBoaWdoZXIgb3JkZXIgdXRmLTggY2hhcnMgdG8gZm9ybWF0XG5leHBvcnRzLm5vcm1hbGl6ZVhNTCA9IGZ1bmN0aW9uKHN0ciwgZm9ybWF0KSB7XG5cdHZhciBpLCBjb2RlLCByZXM7XG5cdFxuXHQvLyBjb252ZXJ0IGVudGl0aWVzIGZpcnN0XG5cdHN0ciA9IG5vcm1hbGl6ZUVudGl0aWVzKHN0ciwgZm9ybWF0KTtcblxuXHQvLyBmaW5kIGFsbCBjaGFyYWN0ZXJzIGFib3ZlIEFTQ0lJIGFuZCByZXBsYWNlLCBiYWNrd2FyZHNcblx0Zm9yIChpID0gc3RyLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG5cdFx0Y29kZSA9IHN0ci5jaGFyQ29kZUF0KGkpO1xuXHRcdGlmIChjb2RlID4gMTI3KSB7XG5cdFx0XHRyZXMgPSBjb252ZXJ0KHN0cltpXSwgZm9ybWF0KTtcblx0XHRcdHN0ciA9IHN0ci5zdWJzdHIoMCwgaSkgKyAocmVzICE9IG51bGwgPyByZXMgOiBzdHJbaV0pICsgc3RyLnN1YnN0cihpICsgMSk7XG5cdFx0fVxuXHR9XG5cblx0cmV0dXJuIHN0cjtcbn1cblxuLy8gY29udmVydHMgc2luZ2xlIHZhbHVlIGludG8gdXRmLTggY2hhcmFjdGVyIG9yIGVudGl0eSBlcXVpdmFsZW50XG52YXIgY29udmVydCA9XG5leHBvcnRzLmNvbnZlcnQgPSBmdW5jdGlvbihzLCBmb3JtYXQpIHtcblx0dmFyIGNvZGUsIG5hbWU7XG5cdGlmIChmb3JtYXQgPT0gbnVsbCkgZm9ybWF0ID0gXCJodG1sXCI7XG5cblx0Y29kZSA9IHRvQ2hhckNvZGUocyk7XG5cdGlmIChjb2RlID09PSBmYWxzZSkgcmV0dXJuIG51bGw7XG5cblx0c3dpdGNoKGZvcm1hdCkge1xuXHRcdC8vIG9ubHkgZGVjaW1hbCBlbnRpdGllc1xuXHRcdGNhc2UgXCJ4bWxcIjpcblx0XHRjYXNlIFwieGh0bWxcIjpcblx0XHRjYXNlIFwibnVtXCI6XG5cdFx0Y2FzZSBcIm51bWVyaWNcIjpcblx0XHRcdHJldHVybiB0b0VudGl0eShjb2RlKTtcblxuXHRcdC8vIG9ubHkgaGV4IGVudGl0aWVzXG5cdFx0Y2FzZSBcImhleFwiOlxuXHRcdFx0cmV0dXJuIHRvRW50aXR5KGNvZGUsIHRydWUpO1xuXG5cdFx0Ly8gZmlyc3Qgc3BlY2lhbCwgdGhlbiBkZWNpbWFsXG5cdFx0Y2FzZSBcImh0bWxcIjpcblx0XHRcdHJldHVybiB0b0VudGl0eSgobmFtZSA9IGN0b24oY29kZSkpICE9IG51bGwgPyBuYW1lIDogY29kZSk7XG5cblx0XHQvLyBvbmx5IHNwZWNpYWwgZW50aXRpZXNcblx0XHRjYXNlIFwibmFtZVwiOlxuXHRcdGNhc2UgXCJzcGVjaWFsXCI6XG5cdFx0XHRyZXR1cm4gKG5hbWUgPSBjdG9uKGNvZGUpKSAhPSBudWxsID8gdG9FbnRpdHkobmFtZSkgOiBudWxsO1xuXG5cdFx0Ly8gdXRmLTggY2hhcmFjdGVyXG5cdFx0Y2FzZSBcImNoYXJcIjpcblx0XHRjYXNlIFwiY2hhcmFjdGVyXCI6XG5cdFx0Y2FzZSBcInV0Zi04XCI6XG5cdFx0Y2FzZSBcIlVURi04XCI6XG5cdFx0XHRyZXR1cm4gU3RyaW5nLmZyb21DaGFyQ29kZShjb2RlKTtcblxuXHRcdC8vIHJlZ3VsYXIgbnVtYmVyXG5cdFx0Y2FzZSBcImNvZGVcIjpcblx0XHRcdHJldHVybiBjb2RlO1xuXHR9XG5cblx0cmV0dXJuIG51bGw7XG59XG5cbi8vIGNvZGUgdG8gbmFtZVxudmFyIGN0b24gPVxuZXhwb3J0cy5jb2RlVG9OYW1lID0gZnVuY3Rpb24oYykge1xuXHR2YXIgbiA9IGJ5X2NvZGVbYy50b1N0cmluZygpXTtcblx0cmV0dXJuIG4gIT0gbnVsbCA/IG5bMF0gOiBudWxsO1xufVxuXG4vLyBuYW1lIHRvIGNvZGVcbnZhciBudG9jID1cbmV4cG9ydHMubmFtZVRvQ29kZSA9IGZ1bmN0aW9uKG4pIHtcblx0dmFyIGMgPSBieV9uYW1lW25dXG5cdHJldHVybiBjICE9IG51bGwgPyBjWzBdIDogbnVsbDtcbn1cblxuLy8gcGFyc2UgYW4gYXJyYXkgb2YgaW5wdXRzIGFuZCByZXR1cm5zIHRoZSBlcXVpdmFsZW50XG4vLyB1bmljb2RlIGRlY2ltYWwgb3IgZmFsc2UgaWYgaW52YWxpZFxudmFyIHRvQ2hhckNvZGUgPVxuZXhwb3J0cy50b0NoYXJDb2RlID0gZnVuY3Rpb24ocykge1xuXHR2YXIgbSwgY29kZTtcblxuXHRpZiAodHlwZW9mIHMgPT09IFwic3RyaW5nXCIpIHtcblx0XHRpZiAocyA9PT0gXCJcIikgcmV0dXJuIGZhbHNlO1xuXG5cdFx0Ly8gcmVndWxhciBjaGFyXG5cdFx0aWYgKHMubGVuZ3RoID09PSAxKSByZXR1cm4gcy5jaGFyQ29kZUF0KDApO1xuXG5cdFx0Ly8gc3BlY2lhbCBlbnRpdHlcblx0XHRpZiAobSA9IHNwZWNfZW50aXR5X3JlZ2V4LmV4ZWMocykpIHtcblx0XHRcdHJldHVybiBudG9jKG1bMV0pIHx8IGZhbHNlO1xuXHRcdH1cblxuXHRcdC8vIGRlY2ltYWwgZW50aXR5XG5cdFx0aWYgKG0gPSBkZWNfZW50aXR5X3JlZ2V4LmV4ZWMocykpIHtcblx0XHRcdHJldHVybiBwYXJzZUludChtWzFdLCAxMCk7XG5cdFx0fVxuXG5cdFx0Ly8gaGV4IGVudGl0eVxuXHRcdGlmIChtID0gaGV4X2VudGl0eV9yZWdleC5leGVjKHMpKSB7XG5cdFx0XHRyZXR1cm4gcGFyc2VJbnQobVsxXSwgMTYpO1xuXHRcdH1cblxuXHRcdC8vIGhleCB2YWx1ZVxuXHRcdGlmIChtID0gaGV4X3ZhbHVlX3JlZ2V4LmV4ZWMocykpIHtcblx0XHRcdHJldHVybiBwYXJzZUludChtWzFdLCAxNik7XG5cdFx0fVxuXG5cdFx0Ly8gb3RoZXJ3aXNlIGxvb2sgaXQgdXAgYXMgYSBzcGVjaWFsIGVudGl0eSBuYW1lXG5cdFx0cmV0dXJuIG50b2MocykgfHwgZmFsc2U7XG5cdH1cblxuXHRpZiAodHlwZW9mIHMgPT09IFwibnVtYmVyXCIpIHJldHVybiBzO1xuXG5cdHJldHVybiBmYWxzZTtcbn1cblxuLy8gY29udmVydHMgc3RyaW5nIG9yIG51bWJlciB0byB2YWxpZCBlbnRpdHlcbnZhciB0b0VudGl0eSA9XG5leHBvcnRzLnRvRW50aXR5ID0gZnVuY3Rpb24obiwgaGV4KSB7XG5cdHJldHVybiBcIiZcIiArIChcblx0XHR0eXBlb2YgbiA9PT0gXCJudW1iZXJcIiA/XG5cdFx0XCIjXCIgKyAoaGV4ID8gXCJ4XCIgOiBcIlwiKSArXG5cdFx0bi50b1N0cmluZyhoZXggPyAxNiA6IDEwKS50b1VwcGVyQ2FzZSgpIDogblxuXHQpICsgXCI7XCI7XG59IiwiLy8gICAgIFVuZGVyc2NvcmUuanMgMS45LjFcbi8vICAgICBodHRwOi8vdW5kZXJzY29yZWpzLm9yZ1xuLy8gICAgIChjKSAyMDA5LTIwMTggSmVyZW15IEFzaGtlbmFzLCBEb2N1bWVudENsb3VkIGFuZCBJbnZlc3RpZ2F0aXZlIFJlcG9ydGVycyAmIEVkaXRvcnNcbi8vICAgICBVbmRlcnNjb3JlIG1heSBiZSBmcmVlbHkgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlLlxuXG4oZnVuY3Rpb24oKSB7XG5cbiAgLy8gQmFzZWxpbmUgc2V0dXBcbiAgLy8gLS0tLS0tLS0tLS0tLS1cblxuICAvLyBFc3RhYmxpc2ggdGhlIHJvb3Qgb2JqZWN0LCBgd2luZG93YCAoYHNlbGZgKSBpbiB0aGUgYnJvd3NlciwgYGdsb2JhbGBcbiAgLy8gb24gdGhlIHNlcnZlciwgb3IgYHRoaXNgIGluIHNvbWUgdmlydHVhbCBtYWNoaW5lcy4gV2UgdXNlIGBzZWxmYFxuICAvLyBpbnN0ZWFkIG9mIGB3aW5kb3dgIGZvciBgV2ViV29ya2VyYCBzdXBwb3J0LlxuICB2YXIgcm9vdCA9IHR5cGVvZiBzZWxmID09ICdvYmplY3QnICYmIHNlbGYuc2VsZiA9PT0gc2VsZiAmJiBzZWxmIHx8XG4gICAgICAgICAgICB0eXBlb2YgZ2xvYmFsID09ICdvYmplY3QnICYmIGdsb2JhbC5nbG9iYWwgPT09IGdsb2JhbCAmJiBnbG9iYWwgfHxcbiAgICAgICAgICAgIHRoaXMgfHxcbiAgICAgICAgICAgIHt9O1xuXG4gIC8vIFNhdmUgdGhlIHByZXZpb3VzIHZhbHVlIG9mIHRoZSBgX2AgdmFyaWFibGUuXG4gIHZhciBwcmV2aW91c1VuZGVyc2NvcmUgPSByb290Ll87XG5cbiAgLy8gU2F2ZSBieXRlcyBpbiB0aGUgbWluaWZpZWQgKGJ1dCBub3QgZ3ppcHBlZCkgdmVyc2lvbjpcbiAgdmFyIEFycmF5UHJvdG8gPSBBcnJheS5wcm90b3R5cGUsIE9ialByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcbiAgdmFyIFN5bWJvbFByb3RvID0gdHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgPyBTeW1ib2wucHJvdG90eXBlIDogbnVsbDtcblxuICAvLyBDcmVhdGUgcXVpY2sgcmVmZXJlbmNlIHZhcmlhYmxlcyBmb3Igc3BlZWQgYWNjZXNzIHRvIGNvcmUgcHJvdG90eXBlcy5cbiAgdmFyIHB1c2ggPSBBcnJheVByb3RvLnB1c2gsXG4gICAgICBzbGljZSA9IEFycmF5UHJvdG8uc2xpY2UsXG4gICAgICB0b1N0cmluZyA9IE9ialByb3RvLnRvU3RyaW5nLFxuICAgICAgaGFzT3duUHJvcGVydHkgPSBPYmpQcm90by5oYXNPd25Qcm9wZXJ0eTtcblxuICAvLyBBbGwgKipFQ01BU2NyaXB0IDUqKiBuYXRpdmUgZnVuY3Rpb24gaW1wbGVtZW50YXRpb25zIHRoYXQgd2UgaG9wZSB0byB1c2VcbiAgLy8gYXJlIGRlY2xhcmVkIGhlcmUuXG4gIHZhciBuYXRpdmVJc0FycmF5ID0gQXJyYXkuaXNBcnJheSxcbiAgICAgIG5hdGl2ZUtleXMgPSBPYmplY3Qua2V5cyxcbiAgICAgIG5hdGl2ZUNyZWF0ZSA9IE9iamVjdC5jcmVhdGU7XG5cbiAgLy8gTmFrZWQgZnVuY3Rpb24gcmVmZXJlbmNlIGZvciBzdXJyb2dhdGUtcHJvdG90eXBlLXN3YXBwaW5nLlxuICB2YXIgQ3RvciA9IGZ1bmN0aW9uKCl7fTtcblxuICAvLyBDcmVhdGUgYSBzYWZlIHJlZmVyZW5jZSB0byB0aGUgVW5kZXJzY29yZSBvYmplY3QgZm9yIHVzZSBiZWxvdy5cbiAgdmFyIF8gPSBmdW5jdGlvbihvYmopIHtcbiAgICBpZiAob2JqIGluc3RhbmNlb2YgXykgcmV0dXJuIG9iajtcbiAgICBpZiAoISh0aGlzIGluc3RhbmNlb2YgXykpIHJldHVybiBuZXcgXyhvYmopO1xuICAgIHRoaXMuX3dyYXBwZWQgPSBvYmo7XG4gIH07XG5cbiAgLy8gRXhwb3J0IHRoZSBVbmRlcnNjb3JlIG9iamVjdCBmb3IgKipOb2RlLmpzKiosIHdpdGhcbiAgLy8gYmFja3dhcmRzLWNvbXBhdGliaWxpdHkgZm9yIHRoZWlyIG9sZCBtb2R1bGUgQVBJLiBJZiB3ZSdyZSBpblxuICAvLyB0aGUgYnJvd3NlciwgYWRkIGBfYCBhcyBhIGdsb2JhbCBvYmplY3QuXG4gIC8vIChgbm9kZVR5cGVgIGlzIGNoZWNrZWQgdG8gZW5zdXJlIHRoYXQgYG1vZHVsZWBcbiAgLy8gYW5kIGBleHBvcnRzYCBhcmUgbm90IEhUTUwgZWxlbWVudHMuKVxuICBpZiAodHlwZW9mIGV4cG9ydHMgIT0gJ3VuZGVmaW5lZCcgJiYgIWV4cG9ydHMubm9kZVR5cGUpIHtcbiAgICBpZiAodHlwZW9mIG1vZHVsZSAhPSAndW5kZWZpbmVkJyAmJiAhbW9kdWxlLm5vZGVUeXBlICYmIG1vZHVsZS5leHBvcnRzKSB7XG4gICAgICBleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSBfO1xuICAgIH1cbiAgICBleHBvcnRzLl8gPSBfO1xuICB9IGVsc2Uge1xuICAgIHJvb3QuXyA9IF87XG4gIH1cblxuICAvLyBDdXJyZW50IHZlcnNpb24uXG4gIF8uVkVSU0lPTiA9ICcxLjkuMSc7XG5cbiAgLy8gSW50ZXJuYWwgZnVuY3Rpb24gdGhhdCByZXR1cm5zIGFuIGVmZmljaWVudCAoZm9yIGN1cnJlbnQgZW5naW5lcykgdmVyc2lvblxuICAvLyBvZiB0aGUgcGFzc2VkLWluIGNhbGxiYWNrLCB0byBiZSByZXBlYXRlZGx5IGFwcGxpZWQgaW4gb3RoZXIgVW5kZXJzY29yZVxuICAvLyBmdW5jdGlvbnMuXG4gIHZhciBvcHRpbWl6ZUNiID0gZnVuY3Rpb24oZnVuYywgY29udGV4dCwgYXJnQ291bnQpIHtcbiAgICBpZiAoY29udGV4dCA9PT0gdm9pZCAwKSByZXR1cm4gZnVuYztcbiAgICBzd2l0Y2ggKGFyZ0NvdW50ID09IG51bGwgPyAzIDogYXJnQ291bnQpIHtcbiAgICAgIGNhc2UgMTogcmV0dXJuIGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgIHJldHVybiBmdW5jLmNhbGwoY29udGV4dCwgdmFsdWUpO1xuICAgICAgfTtcbiAgICAgIC8vIFRoZSAyLWFyZ3VtZW50IGNhc2UgaXMgb21pdHRlZCBiZWNhdXNlIHdl4oCZcmUgbm90IHVzaW5nIGl0LlxuICAgICAgY2FzZSAzOiByZXR1cm4gZnVuY3Rpb24odmFsdWUsIGluZGV4LCBjb2xsZWN0aW9uKSB7XG4gICAgICAgIHJldHVybiBmdW5jLmNhbGwoY29udGV4dCwgdmFsdWUsIGluZGV4LCBjb2xsZWN0aW9uKTtcbiAgICAgIH07XG4gICAgICBjYXNlIDQ6IHJldHVybiBmdW5jdGlvbihhY2N1bXVsYXRvciwgdmFsdWUsIGluZGV4LCBjb2xsZWN0aW9uKSB7XG4gICAgICAgIHJldHVybiBmdW5jLmNhbGwoY29udGV4dCwgYWNjdW11bGF0b3IsIHZhbHVlLCBpbmRleCwgY29sbGVjdGlvbik7XG4gICAgICB9O1xuICAgIH1cbiAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gZnVuYy5hcHBseShjb250ZXh0LCBhcmd1bWVudHMpO1xuICAgIH07XG4gIH07XG5cbiAgdmFyIGJ1aWx0aW5JdGVyYXRlZTtcblxuICAvLyBBbiBpbnRlcm5hbCBmdW5jdGlvbiB0byBnZW5lcmF0ZSBjYWxsYmFja3MgdGhhdCBjYW4gYmUgYXBwbGllZCB0byBlYWNoXG4gIC8vIGVsZW1lbnQgaW4gYSBjb2xsZWN0aW9uLCByZXR1cm5pbmcgdGhlIGRlc2lyZWQgcmVzdWx0IOKAlCBlaXRoZXIgYGlkZW50aXR5YCxcbiAgLy8gYW4gYXJiaXRyYXJ5IGNhbGxiYWNrLCBhIHByb3BlcnR5IG1hdGNoZXIsIG9yIGEgcHJvcGVydHkgYWNjZXNzb3IuXG4gIHZhciBjYiA9IGZ1bmN0aW9uKHZhbHVlLCBjb250ZXh0LCBhcmdDb3VudCkge1xuICAgIGlmIChfLml0ZXJhdGVlICE9PSBidWlsdGluSXRlcmF0ZWUpIHJldHVybiBfLml0ZXJhdGVlKHZhbHVlLCBjb250ZXh0KTtcbiAgICBpZiAodmFsdWUgPT0gbnVsbCkgcmV0dXJuIF8uaWRlbnRpdHk7XG4gICAgaWYgKF8uaXNGdW5jdGlvbih2YWx1ZSkpIHJldHVybiBvcHRpbWl6ZUNiKHZhbHVlLCBjb250ZXh0LCBhcmdDb3VudCk7XG4gICAgaWYgKF8uaXNPYmplY3QodmFsdWUpICYmICFfLmlzQXJyYXkodmFsdWUpKSByZXR1cm4gXy5tYXRjaGVyKHZhbHVlKTtcbiAgICByZXR1cm4gXy5wcm9wZXJ0eSh2YWx1ZSk7XG4gIH07XG5cbiAgLy8gRXh0ZXJuYWwgd3JhcHBlciBmb3Igb3VyIGNhbGxiYWNrIGdlbmVyYXRvci4gVXNlcnMgbWF5IGN1c3RvbWl6ZVxuICAvLyBgXy5pdGVyYXRlZWAgaWYgdGhleSB3YW50IGFkZGl0aW9uYWwgcHJlZGljYXRlL2l0ZXJhdGVlIHNob3J0aGFuZCBzdHlsZXMuXG4gIC8vIFRoaXMgYWJzdHJhY3Rpb24gaGlkZXMgdGhlIGludGVybmFsLW9ubHkgYXJnQ291bnQgYXJndW1lbnQuXG4gIF8uaXRlcmF0ZWUgPSBidWlsdGluSXRlcmF0ZWUgPSBmdW5jdGlvbih2YWx1ZSwgY29udGV4dCkge1xuICAgIHJldHVybiBjYih2YWx1ZSwgY29udGV4dCwgSW5maW5pdHkpO1xuICB9O1xuXG4gIC8vIFNvbWUgZnVuY3Rpb25zIHRha2UgYSB2YXJpYWJsZSBudW1iZXIgb2YgYXJndW1lbnRzLCBvciBhIGZldyBleHBlY3RlZFxuICAvLyBhcmd1bWVudHMgYXQgdGhlIGJlZ2lubmluZyBhbmQgdGhlbiBhIHZhcmlhYmxlIG51bWJlciBvZiB2YWx1ZXMgdG8gb3BlcmF0ZVxuICAvLyBvbi4gVGhpcyBoZWxwZXIgYWNjdW11bGF0ZXMgYWxsIHJlbWFpbmluZyBhcmd1bWVudHMgcGFzdCB0aGUgZnVuY3Rpb27igJlzXG4gIC8vIGFyZ3VtZW50IGxlbmd0aCAob3IgYW4gZXhwbGljaXQgYHN0YXJ0SW5kZXhgKSwgaW50byBhbiBhcnJheSB0aGF0IGJlY29tZXNcbiAgLy8gdGhlIGxhc3QgYXJndW1lbnQuIFNpbWlsYXIgdG8gRVM24oCZcyBcInJlc3QgcGFyYW1ldGVyXCIuXG4gIHZhciByZXN0QXJndW1lbnRzID0gZnVuY3Rpb24oZnVuYywgc3RhcnRJbmRleCkge1xuICAgIHN0YXJ0SW5kZXggPSBzdGFydEluZGV4ID09IG51bGwgPyBmdW5jLmxlbmd0aCAtIDEgOiArc3RhcnRJbmRleDtcbiAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgbGVuZ3RoID0gTWF0aC5tYXgoYXJndW1lbnRzLmxlbmd0aCAtIHN0YXJ0SW5kZXgsIDApLFxuICAgICAgICAgIHJlc3QgPSBBcnJheShsZW5ndGgpLFxuICAgICAgICAgIGluZGV4ID0gMDtcbiAgICAgIGZvciAoOyBpbmRleCA8IGxlbmd0aDsgaW5kZXgrKykge1xuICAgICAgICByZXN0W2luZGV4XSA9IGFyZ3VtZW50c1tpbmRleCArIHN0YXJ0SW5kZXhdO1xuICAgICAgfVxuICAgICAgc3dpdGNoIChzdGFydEluZGV4KSB7XG4gICAgICAgIGNhc2UgMDogcmV0dXJuIGZ1bmMuY2FsbCh0aGlzLCByZXN0KTtcbiAgICAgICAgY2FzZSAxOiByZXR1cm4gZnVuYy5jYWxsKHRoaXMsIGFyZ3VtZW50c1swXSwgcmVzdCk7XG4gICAgICAgIGNhc2UgMjogcmV0dXJuIGZ1bmMuY2FsbCh0aGlzLCBhcmd1bWVudHNbMF0sIGFyZ3VtZW50c1sxXSwgcmVzdCk7XG4gICAgICB9XG4gICAgICB2YXIgYXJncyA9IEFycmF5KHN0YXJ0SW5kZXggKyAxKTtcbiAgICAgIGZvciAoaW5kZXggPSAwOyBpbmRleCA8IHN0YXJ0SW5kZXg7IGluZGV4KyspIHtcbiAgICAgICAgYXJnc1tpbmRleF0gPSBhcmd1bWVudHNbaW5kZXhdO1xuICAgICAgfVxuICAgICAgYXJnc1tzdGFydEluZGV4XSA9IHJlc3Q7XG4gICAgICByZXR1cm4gZnVuYy5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICB9O1xuICB9O1xuXG4gIC8vIEFuIGludGVybmFsIGZ1bmN0aW9uIGZvciBjcmVhdGluZyBhIG5ldyBvYmplY3QgdGhhdCBpbmhlcml0cyBmcm9tIGFub3RoZXIuXG4gIHZhciBiYXNlQ3JlYXRlID0gZnVuY3Rpb24ocHJvdG90eXBlKSB7XG4gICAgaWYgKCFfLmlzT2JqZWN0KHByb3RvdHlwZSkpIHJldHVybiB7fTtcbiAgICBpZiAobmF0aXZlQ3JlYXRlKSByZXR1cm4gbmF0aXZlQ3JlYXRlKHByb3RvdHlwZSk7XG4gICAgQ3Rvci5wcm90b3R5cGUgPSBwcm90b3R5cGU7XG4gICAgdmFyIHJlc3VsdCA9IG5ldyBDdG9yO1xuICAgIEN0b3IucHJvdG90eXBlID0gbnVsbDtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xuXG4gIHZhciBzaGFsbG93UHJvcGVydHkgPSBmdW5jdGlvbihrZXkpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24ob2JqKSB7XG4gICAgICByZXR1cm4gb2JqID09IG51bGwgPyB2b2lkIDAgOiBvYmpba2V5XTtcbiAgICB9O1xuICB9O1xuXG4gIHZhciBoYXMgPSBmdW5jdGlvbihvYmosIHBhdGgpIHtcbiAgICByZXR1cm4gb2JqICE9IG51bGwgJiYgaGFzT3duUHJvcGVydHkuY2FsbChvYmosIHBhdGgpO1xuICB9XG5cbiAgdmFyIGRlZXBHZXQgPSBmdW5jdGlvbihvYmosIHBhdGgpIHtcbiAgICB2YXIgbGVuZ3RoID0gcGF0aC5sZW5ndGg7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgaWYgKG9iaiA9PSBudWxsKSByZXR1cm4gdm9pZCAwO1xuICAgICAgb2JqID0gb2JqW3BhdGhbaV1dO1xuICAgIH1cbiAgICByZXR1cm4gbGVuZ3RoID8gb2JqIDogdm9pZCAwO1xuICB9O1xuXG4gIC8vIEhlbHBlciBmb3IgY29sbGVjdGlvbiBtZXRob2RzIHRvIGRldGVybWluZSB3aGV0aGVyIGEgY29sbGVjdGlvblxuICAvLyBzaG91bGQgYmUgaXRlcmF0ZWQgYXMgYW4gYXJyYXkgb3IgYXMgYW4gb2JqZWN0LlxuICAvLyBSZWxhdGVkOiBodHRwOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy10b2xlbmd0aFxuICAvLyBBdm9pZHMgYSB2ZXJ5IG5hc3R5IGlPUyA4IEpJVCBidWcgb24gQVJNLTY0LiAjMjA5NFxuICB2YXIgTUFYX0FSUkFZX0lOREVYID0gTWF0aC5wb3coMiwgNTMpIC0gMTtcbiAgdmFyIGdldExlbmd0aCA9IHNoYWxsb3dQcm9wZXJ0eSgnbGVuZ3RoJyk7XG4gIHZhciBpc0FycmF5TGlrZSA9IGZ1bmN0aW9uKGNvbGxlY3Rpb24pIHtcbiAgICB2YXIgbGVuZ3RoID0gZ2V0TGVuZ3RoKGNvbGxlY3Rpb24pO1xuICAgIHJldHVybiB0eXBlb2YgbGVuZ3RoID09ICdudW1iZXInICYmIGxlbmd0aCA+PSAwICYmIGxlbmd0aCA8PSBNQVhfQVJSQVlfSU5ERVg7XG4gIH07XG5cbiAgLy8gQ29sbGVjdGlvbiBGdW5jdGlvbnNcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS1cblxuICAvLyBUaGUgY29ybmVyc3RvbmUsIGFuIGBlYWNoYCBpbXBsZW1lbnRhdGlvbiwgYWthIGBmb3JFYWNoYC5cbiAgLy8gSGFuZGxlcyByYXcgb2JqZWN0cyBpbiBhZGRpdGlvbiB0byBhcnJheS1saWtlcy4gVHJlYXRzIGFsbFxuICAvLyBzcGFyc2UgYXJyYXktbGlrZXMgYXMgaWYgdGhleSB3ZXJlIGRlbnNlLlxuICBfLmVhY2ggPSBfLmZvckVhY2ggPSBmdW5jdGlvbihvYmosIGl0ZXJhdGVlLCBjb250ZXh0KSB7XG4gICAgaXRlcmF0ZWUgPSBvcHRpbWl6ZUNiKGl0ZXJhdGVlLCBjb250ZXh0KTtcbiAgICB2YXIgaSwgbGVuZ3RoO1xuICAgIGlmIChpc0FycmF5TGlrZShvYmopKSB7XG4gICAgICBmb3IgKGkgPSAwLCBsZW5ndGggPSBvYmoubGVuZ3RoOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaXRlcmF0ZWUob2JqW2ldLCBpLCBvYmopO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIga2V5cyA9IF8ua2V5cyhvYmopO1xuICAgICAgZm9yIChpID0gMCwgbGVuZ3RoID0ga2V5cy5sZW5ndGg7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICBpdGVyYXRlZShvYmpba2V5c1tpXV0sIGtleXNbaV0sIG9iaik7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvYmo7XG4gIH07XG5cbiAgLy8gUmV0dXJuIHRoZSByZXN1bHRzIG9mIGFwcGx5aW5nIHRoZSBpdGVyYXRlZSB0byBlYWNoIGVsZW1lbnQuXG4gIF8ubWFwID0gXy5jb2xsZWN0ID0gZnVuY3Rpb24ob2JqLCBpdGVyYXRlZSwgY29udGV4dCkge1xuICAgIGl0ZXJhdGVlID0gY2IoaXRlcmF0ZWUsIGNvbnRleHQpO1xuICAgIHZhciBrZXlzID0gIWlzQXJyYXlMaWtlKG9iaikgJiYgXy5rZXlzKG9iaiksXG4gICAgICAgIGxlbmd0aCA9IChrZXlzIHx8IG9iaikubGVuZ3RoLFxuICAgICAgICByZXN1bHRzID0gQXJyYXkobGVuZ3RoKTtcbiAgICBmb3IgKHZhciBpbmRleCA9IDA7IGluZGV4IDwgbGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICB2YXIgY3VycmVudEtleSA9IGtleXMgPyBrZXlzW2luZGV4XSA6IGluZGV4O1xuICAgICAgcmVzdWx0c1tpbmRleF0gPSBpdGVyYXRlZShvYmpbY3VycmVudEtleV0sIGN1cnJlbnRLZXksIG9iaik7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHRzO1xuICB9O1xuXG4gIC8vIENyZWF0ZSBhIHJlZHVjaW5nIGZ1bmN0aW9uIGl0ZXJhdGluZyBsZWZ0IG9yIHJpZ2h0LlxuICB2YXIgY3JlYXRlUmVkdWNlID0gZnVuY3Rpb24oZGlyKSB7XG4gICAgLy8gV3JhcCBjb2RlIHRoYXQgcmVhc3NpZ25zIGFyZ3VtZW50IHZhcmlhYmxlcyBpbiBhIHNlcGFyYXRlIGZ1bmN0aW9uIHRoYW5cbiAgICAvLyB0aGUgb25lIHRoYXQgYWNjZXNzZXMgYGFyZ3VtZW50cy5sZW5ndGhgIHRvIGF2b2lkIGEgcGVyZiBoaXQuICgjMTk5MSlcbiAgICB2YXIgcmVkdWNlciA9IGZ1bmN0aW9uKG9iaiwgaXRlcmF0ZWUsIG1lbW8sIGluaXRpYWwpIHtcbiAgICAgIHZhciBrZXlzID0gIWlzQXJyYXlMaWtlKG9iaikgJiYgXy5rZXlzKG9iaiksXG4gICAgICAgICAgbGVuZ3RoID0gKGtleXMgfHwgb2JqKS5sZW5ndGgsXG4gICAgICAgICAgaW5kZXggPSBkaXIgPiAwID8gMCA6IGxlbmd0aCAtIDE7XG4gICAgICBpZiAoIWluaXRpYWwpIHtcbiAgICAgICAgbWVtbyA9IG9ialtrZXlzID8ga2V5c1tpbmRleF0gOiBpbmRleF07XG4gICAgICAgIGluZGV4ICs9IGRpcjtcbiAgICAgIH1cbiAgICAgIGZvciAoOyBpbmRleCA+PSAwICYmIGluZGV4IDwgbGVuZ3RoOyBpbmRleCArPSBkaXIpIHtcbiAgICAgICAgdmFyIGN1cnJlbnRLZXkgPSBrZXlzID8ga2V5c1tpbmRleF0gOiBpbmRleDtcbiAgICAgICAgbWVtbyA9IGl0ZXJhdGVlKG1lbW8sIG9ialtjdXJyZW50S2V5XSwgY3VycmVudEtleSwgb2JqKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBtZW1vO1xuICAgIH07XG5cbiAgICByZXR1cm4gZnVuY3Rpb24ob2JqLCBpdGVyYXRlZSwgbWVtbywgY29udGV4dCkge1xuICAgICAgdmFyIGluaXRpYWwgPSBhcmd1bWVudHMubGVuZ3RoID49IDM7XG4gICAgICByZXR1cm4gcmVkdWNlcihvYmosIG9wdGltaXplQ2IoaXRlcmF0ZWUsIGNvbnRleHQsIDQpLCBtZW1vLCBpbml0aWFsKTtcbiAgICB9O1xuICB9O1xuXG4gIC8vICoqUmVkdWNlKiogYnVpbGRzIHVwIGEgc2luZ2xlIHJlc3VsdCBmcm9tIGEgbGlzdCBvZiB2YWx1ZXMsIGFrYSBgaW5qZWN0YCxcbiAgLy8gb3IgYGZvbGRsYC5cbiAgXy5yZWR1Y2UgPSBfLmZvbGRsID0gXy5pbmplY3QgPSBjcmVhdGVSZWR1Y2UoMSk7XG5cbiAgLy8gVGhlIHJpZ2h0LWFzc29jaWF0aXZlIHZlcnNpb24gb2YgcmVkdWNlLCBhbHNvIGtub3duIGFzIGBmb2xkcmAuXG4gIF8ucmVkdWNlUmlnaHQgPSBfLmZvbGRyID0gY3JlYXRlUmVkdWNlKC0xKTtcblxuICAvLyBSZXR1cm4gdGhlIGZpcnN0IHZhbHVlIHdoaWNoIHBhc3NlcyBhIHRydXRoIHRlc3QuIEFsaWFzZWQgYXMgYGRldGVjdGAuXG4gIF8uZmluZCA9IF8uZGV0ZWN0ID0gZnVuY3Rpb24ob2JqLCBwcmVkaWNhdGUsIGNvbnRleHQpIHtcbiAgICB2YXIga2V5RmluZGVyID0gaXNBcnJheUxpa2Uob2JqKSA/IF8uZmluZEluZGV4IDogXy5maW5kS2V5O1xuICAgIHZhciBrZXkgPSBrZXlGaW5kZXIob2JqLCBwcmVkaWNhdGUsIGNvbnRleHQpO1xuICAgIGlmIChrZXkgIT09IHZvaWQgMCAmJiBrZXkgIT09IC0xKSByZXR1cm4gb2JqW2tleV07XG4gIH07XG5cbiAgLy8gUmV0dXJuIGFsbCB0aGUgZWxlbWVudHMgdGhhdCBwYXNzIGEgdHJ1dGggdGVzdC5cbiAgLy8gQWxpYXNlZCBhcyBgc2VsZWN0YC5cbiAgXy5maWx0ZXIgPSBfLnNlbGVjdCA9IGZ1bmN0aW9uKG9iaiwgcHJlZGljYXRlLCBjb250ZXh0KSB7XG4gICAgdmFyIHJlc3VsdHMgPSBbXTtcbiAgICBwcmVkaWNhdGUgPSBjYihwcmVkaWNhdGUsIGNvbnRleHQpO1xuICAgIF8uZWFjaChvYmosIGZ1bmN0aW9uKHZhbHVlLCBpbmRleCwgbGlzdCkge1xuICAgICAgaWYgKHByZWRpY2F0ZSh2YWx1ZSwgaW5kZXgsIGxpc3QpKSByZXN1bHRzLnB1c2godmFsdWUpO1xuICAgIH0pO1xuICAgIHJldHVybiByZXN1bHRzO1xuICB9O1xuXG4gIC8vIFJldHVybiBhbGwgdGhlIGVsZW1lbnRzIGZvciB3aGljaCBhIHRydXRoIHRlc3QgZmFpbHMuXG4gIF8ucmVqZWN0ID0gZnVuY3Rpb24ob2JqLCBwcmVkaWNhdGUsIGNvbnRleHQpIHtcbiAgICByZXR1cm4gXy5maWx0ZXIob2JqLCBfLm5lZ2F0ZShjYihwcmVkaWNhdGUpKSwgY29udGV4dCk7XG4gIH07XG5cbiAgLy8gRGV0ZXJtaW5lIHdoZXRoZXIgYWxsIG9mIHRoZSBlbGVtZW50cyBtYXRjaCBhIHRydXRoIHRlc3QuXG4gIC8vIEFsaWFzZWQgYXMgYGFsbGAuXG4gIF8uZXZlcnkgPSBfLmFsbCA9IGZ1bmN0aW9uKG9iaiwgcHJlZGljYXRlLCBjb250ZXh0KSB7XG4gICAgcHJlZGljYXRlID0gY2IocHJlZGljYXRlLCBjb250ZXh0KTtcbiAgICB2YXIga2V5cyA9ICFpc0FycmF5TGlrZShvYmopICYmIF8ua2V5cyhvYmopLFxuICAgICAgICBsZW5ndGggPSAoa2V5cyB8fCBvYmopLmxlbmd0aDtcbiAgICBmb3IgKHZhciBpbmRleCA9IDA7IGluZGV4IDwgbGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICB2YXIgY3VycmVudEtleSA9IGtleXMgPyBrZXlzW2luZGV4XSA6IGluZGV4O1xuICAgICAgaWYgKCFwcmVkaWNhdGUob2JqW2N1cnJlbnRLZXldLCBjdXJyZW50S2V5LCBvYmopKSByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9O1xuXG4gIC8vIERldGVybWluZSBpZiBhdCBsZWFzdCBvbmUgZWxlbWVudCBpbiB0aGUgb2JqZWN0IG1hdGNoZXMgYSB0cnV0aCB0ZXN0LlxuICAvLyBBbGlhc2VkIGFzIGBhbnlgLlxuICBfLnNvbWUgPSBfLmFueSA9IGZ1bmN0aW9uKG9iaiwgcHJlZGljYXRlLCBjb250ZXh0KSB7XG4gICAgcHJlZGljYXRlID0gY2IocHJlZGljYXRlLCBjb250ZXh0KTtcbiAgICB2YXIga2V5cyA9ICFpc0FycmF5TGlrZShvYmopICYmIF8ua2V5cyhvYmopLFxuICAgICAgICBsZW5ndGggPSAoa2V5cyB8fCBvYmopLmxlbmd0aDtcbiAgICBmb3IgKHZhciBpbmRleCA9IDA7IGluZGV4IDwgbGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICB2YXIgY3VycmVudEtleSA9IGtleXMgPyBrZXlzW2luZGV4XSA6IGluZGV4O1xuICAgICAgaWYgKHByZWRpY2F0ZShvYmpbY3VycmVudEtleV0sIGN1cnJlbnRLZXksIG9iaikpIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH07XG5cbiAgLy8gRGV0ZXJtaW5lIGlmIHRoZSBhcnJheSBvciBvYmplY3QgY29udGFpbnMgYSBnaXZlbiBpdGVtICh1c2luZyBgPT09YCkuXG4gIC8vIEFsaWFzZWQgYXMgYGluY2x1ZGVzYCBhbmQgYGluY2x1ZGVgLlxuICBfLmNvbnRhaW5zID0gXy5pbmNsdWRlcyA9IF8uaW5jbHVkZSA9IGZ1bmN0aW9uKG9iaiwgaXRlbSwgZnJvbUluZGV4LCBndWFyZCkge1xuICAgIGlmICghaXNBcnJheUxpa2Uob2JqKSkgb2JqID0gXy52YWx1ZXMob2JqKTtcbiAgICBpZiAodHlwZW9mIGZyb21JbmRleCAhPSAnbnVtYmVyJyB8fCBndWFyZCkgZnJvbUluZGV4ID0gMDtcbiAgICByZXR1cm4gXy5pbmRleE9mKG9iaiwgaXRlbSwgZnJvbUluZGV4KSA+PSAwO1xuICB9O1xuXG4gIC8vIEludm9rZSBhIG1ldGhvZCAod2l0aCBhcmd1bWVudHMpIG9uIGV2ZXJ5IGl0ZW0gaW4gYSBjb2xsZWN0aW9uLlxuICBfLmludm9rZSA9IHJlc3RBcmd1bWVudHMoZnVuY3Rpb24ob2JqLCBwYXRoLCBhcmdzKSB7XG4gICAgdmFyIGNvbnRleHRQYXRoLCBmdW5jO1xuICAgIGlmIChfLmlzRnVuY3Rpb24ocGF0aCkpIHtcbiAgICAgIGZ1bmMgPSBwYXRoO1xuICAgIH0gZWxzZSBpZiAoXy5pc0FycmF5KHBhdGgpKSB7XG4gICAgICBjb250ZXh0UGF0aCA9IHBhdGguc2xpY2UoMCwgLTEpO1xuICAgICAgcGF0aCA9IHBhdGhbcGF0aC5sZW5ndGggLSAxXTtcbiAgICB9XG4gICAgcmV0dXJuIF8ubWFwKG9iaiwgZnVuY3Rpb24oY29udGV4dCkge1xuICAgICAgdmFyIG1ldGhvZCA9IGZ1bmM7XG4gICAgICBpZiAoIW1ldGhvZCkge1xuICAgICAgICBpZiAoY29udGV4dFBhdGggJiYgY29udGV4dFBhdGgubGVuZ3RoKSB7XG4gICAgICAgICAgY29udGV4dCA9IGRlZXBHZXQoY29udGV4dCwgY29udGV4dFBhdGgpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChjb250ZXh0ID09IG51bGwpIHJldHVybiB2b2lkIDA7XG4gICAgICAgIG1ldGhvZCA9IGNvbnRleHRbcGF0aF07XG4gICAgICB9XG4gICAgICByZXR1cm4gbWV0aG9kID09IG51bGwgPyBtZXRob2QgOiBtZXRob2QuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgfSk7XG4gIH0pO1xuXG4gIC8vIENvbnZlbmllbmNlIHZlcnNpb24gb2YgYSBjb21tb24gdXNlIGNhc2Ugb2YgYG1hcGA6IGZldGNoaW5nIGEgcHJvcGVydHkuXG4gIF8ucGx1Y2sgPSBmdW5jdGlvbihvYmosIGtleSkge1xuICAgIHJldHVybiBfLm1hcChvYmosIF8ucHJvcGVydHkoa2V5KSk7XG4gIH07XG5cbiAgLy8gQ29udmVuaWVuY2UgdmVyc2lvbiBvZiBhIGNvbW1vbiB1c2UgY2FzZSBvZiBgZmlsdGVyYDogc2VsZWN0aW5nIG9ubHkgb2JqZWN0c1xuICAvLyBjb250YWluaW5nIHNwZWNpZmljIGBrZXk6dmFsdWVgIHBhaXJzLlxuICBfLndoZXJlID0gZnVuY3Rpb24ob2JqLCBhdHRycykge1xuICAgIHJldHVybiBfLmZpbHRlcihvYmosIF8ubWF0Y2hlcihhdHRycykpO1xuICB9O1xuXG4gIC8vIENvbnZlbmllbmNlIHZlcnNpb24gb2YgYSBjb21tb24gdXNlIGNhc2Ugb2YgYGZpbmRgOiBnZXR0aW5nIHRoZSBmaXJzdCBvYmplY3RcbiAgLy8gY29udGFpbmluZyBzcGVjaWZpYyBga2V5OnZhbHVlYCBwYWlycy5cbiAgXy5maW5kV2hlcmUgPSBmdW5jdGlvbihvYmosIGF0dHJzKSB7XG4gICAgcmV0dXJuIF8uZmluZChvYmosIF8ubWF0Y2hlcihhdHRycykpO1xuICB9O1xuXG4gIC8vIFJldHVybiB0aGUgbWF4aW11bSBlbGVtZW50IChvciBlbGVtZW50LWJhc2VkIGNvbXB1dGF0aW9uKS5cbiAgXy5tYXggPSBmdW5jdGlvbihvYmosIGl0ZXJhdGVlLCBjb250ZXh0KSB7XG4gICAgdmFyIHJlc3VsdCA9IC1JbmZpbml0eSwgbGFzdENvbXB1dGVkID0gLUluZmluaXR5LFxuICAgICAgICB2YWx1ZSwgY29tcHV0ZWQ7XG4gICAgaWYgKGl0ZXJhdGVlID09IG51bGwgfHwgdHlwZW9mIGl0ZXJhdGVlID09ICdudW1iZXInICYmIHR5cGVvZiBvYmpbMF0gIT0gJ29iamVjdCcgJiYgb2JqICE9IG51bGwpIHtcbiAgICAgIG9iaiA9IGlzQXJyYXlMaWtlKG9iaikgPyBvYmogOiBfLnZhbHVlcyhvYmopO1xuICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbmd0aCA9IG9iai5sZW5ndGg7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICB2YWx1ZSA9IG9ialtpXTtcbiAgICAgICAgaWYgKHZhbHVlICE9IG51bGwgJiYgdmFsdWUgPiByZXN1bHQpIHtcbiAgICAgICAgICByZXN1bHQgPSB2YWx1ZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpdGVyYXRlZSA9IGNiKGl0ZXJhdGVlLCBjb250ZXh0KTtcbiAgICAgIF8uZWFjaChvYmosIGZ1bmN0aW9uKHYsIGluZGV4LCBsaXN0KSB7XG4gICAgICAgIGNvbXB1dGVkID0gaXRlcmF0ZWUodiwgaW5kZXgsIGxpc3QpO1xuICAgICAgICBpZiAoY29tcHV0ZWQgPiBsYXN0Q29tcHV0ZWQgfHwgY29tcHV0ZWQgPT09IC1JbmZpbml0eSAmJiByZXN1bHQgPT09IC1JbmZpbml0eSkge1xuICAgICAgICAgIHJlc3VsdCA9IHY7XG4gICAgICAgICAgbGFzdENvbXB1dGVkID0gY29tcHV0ZWQ7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xuXG4gIC8vIFJldHVybiB0aGUgbWluaW11bSBlbGVtZW50IChvciBlbGVtZW50LWJhc2VkIGNvbXB1dGF0aW9uKS5cbiAgXy5taW4gPSBmdW5jdGlvbihvYmosIGl0ZXJhdGVlLCBjb250ZXh0KSB7XG4gICAgdmFyIHJlc3VsdCA9IEluZmluaXR5LCBsYXN0Q29tcHV0ZWQgPSBJbmZpbml0eSxcbiAgICAgICAgdmFsdWUsIGNvbXB1dGVkO1xuICAgIGlmIChpdGVyYXRlZSA9PSBudWxsIHx8IHR5cGVvZiBpdGVyYXRlZSA9PSAnbnVtYmVyJyAmJiB0eXBlb2Ygb2JqWzBdICE9ICdvYmplY3QnICYmIG9iaiAhPSBudWxsKSB7XG4gICAgICBvYmogPSBpc0FycmF5TGlrZShvYmopID8gb2JqIDogXy52YWx1ZXMob2JqKTtcbiAgICAgIGZvciAodmFyIGkgPSAwLCBsZW5ndGggPSBvYmoubGVuZ3RoOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFsdWUgPSBvYmpbaV07XG4gICAgICAgIGlmICh2YWx1ZSAhPSBudWxsICYmIHZhbHVlIDwgcmVzdWx0KSB7XG4gICAgICAgICAgcmVzdWx0ID0gdmFsdWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaXRlcmF0ZWUgPSBjYihpdGVyYXRlZSwgY29udGV4dCk7XG4gICAgICBfLmVhY2gob2JqLCBmdW5jdGlvbih2LCBpbmRleCwgbGlzdCkge1xuICAgICAgICBjb21wdXRlZCA9IGl0ZXJhdGVlKHYsIGluZGV4LCBsaXN0KTtcbiAgICAgICAgaWYgKGNvbXB1dGVkIDwgbGFzdENvbXB1dGVkIHx8IGNvbXB1dGVkID09PSBJbmZpbml0eSAmJiByZXN1bHQgPT09IEluZmluaXR5KSB7XG4gICAgICAgICAgcmVzdWx0ID0gdjtcbiAgICAgICAgICBsYXN0Q29tcHV0ZWQgPSBjb21wdXRlZDtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH07XG5cbiAgLy8gU2h1ZmZsZSBhIGNvbGxlY3Rpb24uXG4gIF8uc2h1ZmZsZSA9IGZ1bmN0aW9uKG9iaikge1xuICAgIHJldHVybiBfLnNhbXBsZShvYmosIEluZmluaXR5KTtcbiAgfTtcblxuICAvLyBTYW1wbGUgKipuKiogcmFuZG9tIHZhbHVlcyBmcm9tIGEgY29sbGVjdGlvbiB1c2luZyB0aGUgbW9kZXJuIHZlcnNpb24gb2YgdGhlXG4gIC8vIFtGaXNoZXItWWF0ZXMgc2h1ZmZsZV0oaHR0cDovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9GaXNoZXLigJNZYXRlc19zaHVmZmxlKS5cbiAgLy8gSWYgKipuKiogaXMgbm90IHNwZWNpZmllZCwgcmV0dXJucyBhIHNpbmdsZSByYW5kb20gZWxlbWVudC5cbiAgLy8gVGhlIGludGVybmFsIGBndWFyZGAgYXJndW1lbnQgYWxsb3dzIGl0IHRvIHdvcmsgd2l0aCBgbWFwYC5cbiAgXy5zYW1wbGUgPSBmdW5jdGlvbihvYmosIG4sIGd1YXJkKSB7XG4gICAgaWYgKG4gPT0gbnVsbCB8fCBndWFyZCkge1xuICAgICAgaWYgKCFpc0FycmF5TGlrZShvYmopKSBvYmogPSBfLnZhbHVlcyhvYmopO1xuICAgICAgcmV0dXJuIG9ialtfLnJhbmRvbShvYmoubGVuZ3RoIC0gMSldO1xuICAgIH1cbiAgICB2YXIgc2FtcGxlID0gaXNBcnJheUxpa2Uob2JqKSA/IF8uY2xvbmUob2JqKSA6IF8udmFsdWVzKG9iaik7XG4gICAgdmFyIGxlbmd0aCA9IGdldExlbmd0aChzYW1wbGUpO1xuICAgIG4gPSBNYXRoLm1heChNYXRoLm1pbihuLCBsZW5ndGgpLCAwKTtcbiAgICB2YXIgbGFzdCA9IGxlbmd0aCAtIDE7XG4gICAgZm9yICh2YXIgaW5kZXggPSAwOyBpbmRleCA8IG47IGluZGV4KyspIHtcbiAgICAgIHZhciByYW5kID0gXy5yYW5kb20oaW5kZXgsIGxhc3QpO1xuICAgICAgdmFyIHRlbXAgPSBzYW1wbGVbaW5kZXhdO1xuICAgICAgc2FtcGxlW2luZGV4XSA9IHNhbXBsZVtyYW5kXTtcbiAgICAgIHNhbXBsZVtyYW5kXSA9IHRlbXA7XG4gICAgfVxuICAgIHJldHVybiBzYW1wbGUuc2xpY2UoMCwgbik7XG4gIH07XG5cbiAgLy8gU29ydCB0aGUgb2JqZWN0J3MgdmFsdWVzIGJ5IGEgY3JpdGVyaW9uIHByb2R1Y2VkIGJ5IGFuIGl0ZXJhdGVlLlxuICBfLnNvcnRCeSA9IGZ1bmN0aW9uKG9iaiwgaXRlcmF0ZWUsIGNvbnRleHQpIHtcbiAgICB2YXIgaW5kZXggPSAwO1xuICAgIGl0ZXJhdGVlID0gY2IoaXRlcmF0ZWUsIGNvbnRleHQpO1xuICAgIHJldHVybiBfLnBsdWNrKF8ubWFwKG9iaiwgZnVuY3Rpb24odmFsdWUsIGtleSwgbGlzdCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgICBpbmRleDogaW5kZXgrKyxcbiAgICAgICAgY3JpdGVyaWE6IGl0ZXJhdGVlKHZhbHVlLCBrZXksIGxpc3QpXG4gICAgICB9O1xuICAgIH0pLnNvcnQoZnVuY3Rpb24obGVmdCwgcmlnaHQpIHtcbiAgICAgIHZhciBhID0gbGVmdC5jcml0ZXJpYTtcbiAgICAgIHZhciBiID0gcmlnaHQuY3JpdGVyaWE7XG4gICAgICBpZiAoYSAhPT0gYikge1xuICAgICAgICBpZiAoYSA+IGIgfHwgYSA9PT0gdm9pZCAwKSByZXR1cm4gMTtcbiAgICAgICAgaWYgKGEgPCBiIHx8IGIgPT09IHZvaWQgMCkgcmV0dXJuIC0xO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGxlZnQuaW5kZXggLSByaWdodC5pbmRleDtcbiAgICB9KSwgJ3ZhbHVlJyk7XG4gIH07XG5cbiAgLy8gQW4gaW50ZXJuYWwgZnVuY3Rpb24gdXNlZCBmb3IgYWdncmVnYXRlIFwiZ3JvdXAgYnlcIiBvcGVyYXRpb25zLlxuICB2YXIgZ3JvdXAgPSBmdW5jdGlvbihiZWhhdmlvciwgcGFydGl0aW9uKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKG9iaiwgaXRlcmF0ZWUsIGNvbnRleHQpIHtcbiAgICAgIHZhciByZXN1bHQgPSBwYXJ0aXRpb24gPyBbW10sIFtdXSA6IHt9O1xuICAgICAgaXRlcmF0ZWUgPSBjYihpdGVyYXRlZSwgY29udGV4dCk7XG4gICAgICBfLmVhY2gob2JqLCBmdW5jdGlvbih2YWx1ZSwgaW5kZXgpIHtcbiAgICAgICAgdmFyIGtleSA9IGl0ZXJhdGVlKHZhbHVlLCBpbmRleCwgb2JqKTtcbiAgICAgICAgYmVoYXZpb3IocmVzdWx0LCB2YWx1ZSwga2V5KTtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9O1xuICB9O1xuXG4gIC8vIEdyb3VwcyB0aGUgb2JqZWN0J3MgdmFsdWVzIGJ5IGEgY3JpdGVyaW9uLiBQYXNzIGVpdGhlciBhIHN0cmluZyBhdHRyaWJ1dGVcbiAgLy8gdG8gZ3JvdXAgYnksIG9yIGEgZnVuY3Rpb24gdGhhdCByZXR1cm5zIHRoZSBjcml0ZXJpb24uXG4gIF8uZ3JvdXBCeSA9IGdyb3VwKGZ1bmN0aW9uKHJlc3VsdCwgdmFsdWUsIGtleSkge1xuICAgIGlmIChoYXMocmVzdWx0LCBrZXkpKSByZXN1bHRba2V5XS5wdXNoKHZhbHVlKTsgZWxzZSByZXN1bHRba2V5XSA9IFt2YWx1ZV07XG4gIH0pO1xuXG4gIC8vIEluZGV4ZXMgdGhlIG9iamVjdCdzIHZhbHVlcyBieSBhIGNyaXRlcmlvbiwgc2ltaWxhciB0byBgZ3JvdXBCeWAsIGJ1dCBmb3JcbiAgLy8gd2hlbiB5b3Uga25vdyB0aGF0IHlvdXIgaW5kZXggdmFsdWVzIHdpbGwgYmUgdW5pcXVlLlxuICBfLmluZGV4QnkgPSBncm91cChmdW5jdGlvbihyZXN1bHQsIHZhbHVlLCBrZXkpIHtcbiAgICByZXN1bHRba2V5XSA9IHZhbHVlO1xuICB9KTtcblxuICAvLyBDb3VudHMgaW5zdGFuY2VzIG9mIGFuIG9iamVjdCB0aGF0IGdyb3VwIGJ5IGEgY2VydGFpbiBjcml0ZXJpb24uIFBhc3NcbiAgLy8gZWl0aGVyIGEgc3RyaW5nIGF0dHJpYnV0ZSB0byBjb3VudCBieSwgb3IgYSBmdW5jdGlvbiB0aGF0IHJldHVybnMgdGhlXG4gIC8vIGNyaXRlcmlvbi5cbiAgXy5jb3VudEJ5ID0gZ3JvdXAoZnVuY3Rpb24ocmVzdWx0LCB2YWx1ZSwga2V5KSB7XG4gICAgaWYgKGhhcyhyZXN1bHQsIGtleSkpIHJlc3VsdFtrZXldKys7IGVsc2UgcmVzdWx0W2tleV0gPSAxO1xuICB9KTtcblxuICB2YXIgcmVTdHJTeW1ib2wgPSAvW15cXHVkODAwLVxcdWRmZmZdfFtcXHVkODAwLVxcdWRiZmZdW1xcdWRjMDAtXFx1ZGZmZl18W1xcdWQ4MDAtXFx1ZGZmZl0vZztcbiAgLy8gU2FmZWx5IGNyZWF0ZSBhIHJlYWwsIGxpdmUgYXJyYXkgZnJvbSBhbnl0aGluZyBpdGVyYWJsZS5cbiAgXy50b0FycmF5ID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgaWYgKCFvYmopIHJldHVybiBbXTtcbiAgICBpZiAoXy5pc0FycmF5KG9iaikpIHJldHVybiBzbGljZS5jYWxsKG9iaik7XG4gICAgaWYgKF8uaXNTdHJpbmcob2JqKSkge1xuICAgICAgLy8gS2VlcCBzdXJyb2dhdGUgcGFpciBjaGFyYWN0ZXJzIHRvZ2V0aGVyXG4gICAgICByZXR1cm4gb2JqLm1hdGNoKHJlU3RyU3ltYm9sKTtcbiAgICB9XG4gICAgaWYgKGlzQXJyYXlMaWtlKG9iaikpIHJldHVybiBfLm1hcChvYmosIF8uaWRlbnRpdHkpO1xuICAgIHJldHVybiBfLnZhbHVlcyhvYmopO1xuICB9O1xuXG4gIC8vIFJldHVybiB0aGUgbnVtYmVyIG9mIGVsZW1lbnRzIGluIGFuIG9iamVjdC5cbiAgXy5zaXplID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgaWYgKG9iaiA9PSBudWxsKSByZXR1cm4gMDtcbiAgICByZXR1cm4gaXNBcnJheUxpa2Uob2JqKSA/IG9iai5sZW5ndGggOiBfLmtleXMob2JqKS5sZW5ndGg7XG4gIH07XG5cbiAgLy8gU3BsaXQgYSBjb2xsZWN0aW9uIGludG8gdHdvIGFycmF5czogb25lIHdob3NlIGVsZW1lbnRzIGFsbCBzYXRpc2Z5IHRoZSBnaXZlblxuICAvLyBwcmVkaWNhdGUsIGFuZCBvbmUgd2hvc2UgZWxlbWVudHMgYWxsIGRvIG5vdCBzYXRpc2Z5IHRoZSBwcmVkaWNhdGUuXG4gIF8ucGFydGl0aW9uID0gZ3JvdXAoZnVuY3Rpb24ocmVzdWx0LCB2YWx1ZSwgcGFzcykge1xuICAgIHJlc3VsdFtwYXNzID8gMCA6IDFdLnB1c2godmFsdWUpO1xuICB9LCB0cnVlKTtcblxuICAvLyBBcnJheSBGdW5jdGlvbnNcbiAgLy8gLS0tLS0tLS0tLS0tLS0tXG5cbiAgLy8gR2V0IHRoZSBmaXJzdCBlbGVtZW50IG9mIGFuIGFycmF5LiBQYXNzaW5nICoqbioqIHdpbGwgcmV0dXJuIHRoZSBmaXJzdCBOXG4gIC8vIHZhbHVlcyBpbiB0aGUgYXJyYXkuIEFsaWFzZWQgYXMgYGhlYWRgIGFuZCBgdGFrZWAuIFRoZSAqKmd1YXJkKiogY2hlY2tcbiAgLy8gYWxsb3dzIGl0IHRvIHdvcmsgd2l0aCBgXy5tYXBgLlxuICBfLmZpcnN0ID0gXy5oZWFkID0gXy50YWtlID0gZnVuY3Rpb24oYXJyYXksIG4sIGd1YXJkKSB7XG4gICAgaWYgKGFycmF5ID09IG51bGwgfHwgYXJyYXkubGVuZ3RoIDwgMSkgcmV0dXJuIG4gPT0gbnVsbCA/IHZvaWQgMCA6IFtdO1xuICAgIGlmIChuID09IG51bGwgfHwgZ3VhcmQpIHJldHVybiBhcnJheVswXTtcbiAgICByZXR1cm4gXy5pbml0aWFsKGFycmF5LCBhcnJheS5sZW5ndGggLSBuKTtcbiAgfTtcblxuICAvLyBSZXR1cm5zIGV2ZXJ5dGhpbmcgYnV0IHRoZSBsYXN0IGVudHJ5IG9mIHRoZSBhcnJheS4gRXNwZWNpYWxseSB1c2VmdWwgb25cbiAgLy8gdGhlIGFyZ3VtZW50cyBvYmplY3QuIFBhc3NpbmcgKipuKiogd2lsbCByZXR1cm4gYWxsIHRoZSB2YWx1ZXMgaW5cbiAgLy8gdGhlIGFycmF5LCBleGNsdWRpbmcgdGhlIGxhc3QgTi5cbiAgXy5pbml0aWFsID0gZnVuY3Rpb24oYXJyYXksIG4sIGd1YXJkKSB7XG4gICAgcmV0dXJuIHNsaWNlLmNhbGwoYXJyYXksIDAsIE1hdGgubWF4KDAsIGFycmF5Lmxlbmd0aCAtIChuID09IG51bGwgfHwgZ3VhcmQgPyAxIDogbikpKTtcbiAgfTtcblxuICAvLyBHZXQgdGhlIGxhc3QgZWxlbWVudCBvZiBhbiBhcnJheS4gUGFzc2luZyAqKm4qKiB3aWxsIHJldHVybiB0aGUgbGFzdCBOXG4gIC8vIHZhbHVlcyBpbiB0aGUgYXJyYXkuXG4gIF8ubGFzdCA9IGZ1bmN0aW9uKGFycmF5LCBuLCBndWFyZCkge1xuICAgIGlmIChhcnJheSA9PSBudWxsIHx8IGFycmF5Lmxlbmd0aCA8IDEpIHJldHVybiBuID09IG51bGwgPyB2b2lkIDAgOiBbXTtcbiAgICBpZiAobiA9PSBudWxsIHx8IGd1YXJkKSByZXR1cm4gYXJyYXlbYXJyYXkubGVuZ3RoIC0gMV07XG4gICAgcmV0dXJuIF8ucmVzdChhcnJheSwgTWF0aC5tYXgoMCwgYXJyYXkubGVuZ3RoIC0gbikpO1xuICB9O1xuXG4gIC8vIFJldHVybnMgZXZlcnl0aGluZyBidXQgdGhlIGZpcnN0IGVudHJ5IG9mIHRoZSBhcnJheS4gQWxpYXNlZCBhcyBgdGFpbGAgYW5kIGBkcm9wYC5cbiAgLy8gRXNwZWNpYWxseSB1c2VmdWwgb24gdGhlIGFyZ3VtZW50cyBvYmplY3QuIFBhc3NpbmcgYW4gKipuKiogd2lsbCByZXR1cm5cbiAgLy8gdGhlIHJlc3QgTiB2YWx1ZXMgaW4gdGhlIGFycmF5LlxuICBfLnJlc3QgPSBfLnRhaWwgPSBfLmRyb3AgPSBmdW5jdGlvbihhcnJheSwgbiwgZ3VhcmQpIHtcbiAgICByZXR1cm4gc2xpY2UuY2FsbChhcnJheSwgbiA9PSBudWxsIHx8IGd1YXJkID8gMSA6IG4pO1xuICB9O1xuXG4gIC8vIFRyaW0gb3V0IGFsbCBmYWxzeSB2YWx1ZXMgZnJvbSBhbiBhcnJheS5cbiAgXy5jb21wYWN0ID0gZnVuY3Rpb24oYXJyYXkpIHtcbiAgICByZXR1cm4gXy5maWx0ZXIoYXJyYXksIEJvb2xlYW4pO1xuICB9O1xuXG4gIC8vIEludGVybmFsIGltcGxlbWVudGF0aW9uIG9mIGEgcmVjdXJzaXZlIGBmbGF0dGVuYCBmdW5jdGlvbi5cbiAgdmFyIGZsYXR0ZW4gPSBmdW5jdGlvbihpbnB1dCwgc2hhbGxvdywgc3RyaWN0LCBvdXRwdXQpIHtcbiAgICBvdXRwdXQgPSBvdXRwdXQgfHwgW107XG4gICAgdmFyIGlkeCA9IG91dHB1dC5sZW5ndGg7XG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbmd0aCA9IGdldExlbmd0aChpbnB1dCk7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHZhbHVlID0gaW5wdXRbaV07XG4gICAgICBpZiAoaXNBcnJheUxpa2UodmFsdWUpICYmIChfLmlzQXJyYXkodmFsdWUpIHx8IF8uaXNBcmd1bWVudHModmFsdWUpKSkge1xuICAgICAgICAvLyBGbGF0dGVuIGN1cnJlbnQgbGV2ZWwgb2YgYXJyYXkgb3IgYXJndW1lbnRzIG9iamVjdC5cbiAgICAgICAgaWYgKHNoYWxsb3cpIHtcbiAgICAgICAgICB2YXIgaiA9IDAsIGxlbiA9IHZhbHVlLmxlbmd0aDtcbiAgICAgICAgICB3aGlsZSAoaiA8IGxlbikgb3V0cHV0W2lkeCsrXSA9IHZhbHVlW2orK107XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZmxhdHRlbih2YWx1ZSwgc2hhbGxvdywgc3RyaWN0LCBvdXRwdXQpO1xuICAgICAgICAgIGlkeCA9IG91dHB1dC5sZW5ndGg7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoIXN0cmljdCkge1xuICAgICAgICBvdXRwdXRbaWR4KytdID0gdmFsdWU7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvdXRwdXQ7XG4gIH07XG5cbiAgLy8gRmxhdHRlbiBvdXQgYW4gYXJyYXksIGVpdGhlciByZWN1cnNpdmVseSAoYnkgZGVmYXVsdCksIG9yIGp1c3Qgb25lIGxldmVsLlxuICBfLmZsYXR0ZW4gPSBmdW5jdGlvbihhcnJheSwgc2hhbGxvdykge1xuICAgIHJldHVybiBmbGF0dGVuKGFycmF5LCBzaGFsbG93LCBmYWxzZSk7XG4gIH07XG5cbiAgLy8gUmV0dXJuIGEgdmVyc2lvbiBvZiB0aGUgYXJyYXkgdGhhdCBkb2VzIG5vdCBjb250YWluIHRoZSBzcGVjaWZpZWQgdmFsdWUocykuXG4gIF8ud2l0aG91dCA9IHJlc3RBcmd1bWVudHMoZnVuY3Rpb24oYXJyYXksIG90aGVyQXJyYXlzKSB7XG4gICAgcmV0dXJuIF8uZGlmZmVyZW5jZShhcnJheSwgb3RoZXJBcnJheXMpO1xuICB9KTtcblxuICAvLyBQcm9kdWNlIGEgZHVwbGljYXRlLWZyZWUgdmVyc2lvbiBvZiB0aGUgYXJyYXkuIElmIHRoZSBhcnJheSBoYXMgYWxyZWFkeVxuICAvLyBiZWVuIHNvcnRlZCwgeW91IGhhdmUgdGhlIG9wdGlvbiBvZiB1c2luZyBhIGZhc3RlciBhbGdvcml0aG0uXG4gIC8vIFRoZSBmYXN0ZXIgYWxnb3JpdGhtIHdpbGwgbm90IHdvcmsgd2l0aCBhbiBpdGVyYXRlZSBpZiB0aGUgaXRlcmF0ZWVcbiAgLy8gaXMgbm90IGEgb25lLXRvLW9uZSBmdW5jdGlvbiwgc28gcHJvdmlkaW5nIGFuIGl0ZXJhdGVlIHdpbGwgZGlzYWJsZVxuICAvLyB0aGUgZmFzdGVyIGFsZ29yaXRobS5cbiAgLy8gQWxpYXNlZCBhcyBgdW5pcXVlYC5cbiAgXy51bmlxID0gXy51bmlxdWUgPSBmdW5jdGlvbihhcnJheSwgaXNTb3J0ZWQsIGl0ZXJhdGVlLCBjb250ZXh0KSB7XG4gICAgaWYgKCFfLmlzQm9vbGVhbihpc1NvcnRlZCkpIHtcbiAgICAgIGNvbnRleHQgPSBpdGVyYXRlZTtcbiAgICAgIGl0ZXJhdGVlID0gaXNTb3J0ZWQ7XG4gICAgICBpc1NvcnRlZCA9IGZhbHNlO1xuICAgIH1cbiAgICBpZiAoaXRlcmF0ZWUgIT0gbnVsbCkgaXRlcmF0ZWUgPSBjYihpdGVyYXRlZSwgY29udGV4dCk7XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgIHZhciBzZWVuID0gW107XG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbmd0aCA9IGdldExlbmd0aChhcnJheSk7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHZhbHVlID0gYXJyYXlbaV0sXG4gICAgICAgICAgY29tcHV0ZWQgPSBpdGVyYXRlZSA/IGl0ZXJhdGVlKHZhbHVlLCBpLCBhcnJheSkgOiB2YWx1ZTtcbiAgICAgIGlmIChpc1NvcnRlZCAmJiAhaXRlcmF0ZWUpIHtcbiAgICAgICAgaWYgKCFpIHx8IHNlZW4gIT09IGNvbXB1dGVkKSByZXN1bHQucHVzaCh2YWx1ZSk7XG4gICAgICAgIHNlZW4gPSBjb21wdXRlZDtcbiAgICAgIH0gZWxzZSBpZiAoaXRlcmF0ZWUpIHtcbiAgICAgICAgaWYgKCFfLmNvbnRhaW5zKHNlZW4sIGNvbXB1dGVkKSkge1xuICAgICAgICAgIHNlZW4ucHVzaChjb21wdXRlZCk7XG4gICAgICAgICAgcmVzdWx0LnB1c2godmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKCFfLmNvbnRhaW5zKHJlc3VsdCwgdmFsdWUpKSB7XG4gICAgICAgIHJlc3VsdC5wdXNoKHZhbHVlKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcblxuICAvLyBQcm9kdWNlIGFuIGFycmF5IHRoYXQgY29udGFpbnMgdGhlIHVuaW9uOiBlYWNoIGRpc3RpbmN0IGVsZW1lbnQgZnJvbSBhbGwgb2ZcbiAgLy8gdGhlIHBhc3NlZC1pbiBhcnJheXMuXG4gIF8udW5pb24gPSByZXN0QXJndW1lbnRzKGZ1bmN0aW9uKGFycmF5cykge1xuICAgIHJldHVybiBfLnVuaXEoZmxhdHRlbihhcnJheXMsIHRydWUsIHRydWUpKTtcbiAgfSk7XG5cbiAgLy8gUHJvZHVjZSBhbiBhcnJheSB0aGF0IGNvbnRhaW5zIGV2ZXJ5IGl0ZW0gc2hhcmVkIGJldHdlZW4gYWxsIHRoZVxuICAvLyBwYXNzZWQtaW4gYXJyYXlzLlxuICBfLmludGVyc2VjdGlvbiA9IGZ1bmN0aW9uKGFycmF5KSB7XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgIHZhciBhcmdzTGVuZ3RoID0gYXJndW1lbnRzLmxlbmd0aDtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuZ3RoID0gZ2V0TGVuZ3RoKGFycmF5KTsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IGFycmF5W2ldO1xuICAgICAgaWYgKF8uY29udGFpbnMocmVzdWx0LCBpdGVtKSkgY29udGludWU7XG4gICAgICB2YXIgajtcbiAgICAgIGZvciAoaiA9IDE7IGogPCBhcmdzTGVuZ3RoOyBqKyspIHtcbiAgICAgICAgaWYgKCFfLmNvbnRhaW5zKGFyZ3VtZW50c1tqXSwgaXRlbSkpIGJyZWFrO1xuICAgICAgfVxuICAgICAgaWYgKGogPT09IGFyZ3NMZW5ndGgpIHJlc3VsdC5wdXNoKGl0ZW0pO1xuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xuXG4gIC8vIFRha2UgdGhlIGRpZmZlcmVuY2UgYmV0d2VlbiBvbmUgYXJyYXkgYW5kIGEgbnVtYmVyIG9mIG90aGVyIGFycmF5cy5cbiAgLy8gT25seSB0aGUgZWxlbWVudHMgcHJlc2VudCBpbiBqdXN0IHRoZSBmaXJzdCBhcnJheSB3aWxsIHJlbWFpbi5cbiAgXy5kaWZmZXJlbmNlID0gcmVzdEFyZ3VtZW50cyhmdW5jdGlvbihhcnJheSwgcmVzdCkge1xuICAgIHJlc3QgPSBmbGF0dGVuKHJlc3QsIHRydWUsIHRydWUpO1xuICAgIHJldHVybiBfLmZpbHRlcihhcnJheSwgZnVuY3Rpb24odmFsdWUpe1xuICAgICAgcmV0dXJuICFfLmNvbnRhaW5zKHJlc3QsIHZhbHVlKTtcbiAgICB9KTtcbiAgfSk7XG5cbiAgLy8gQ29tcGxlbWVudCBvZiBfLnppcC4gVW56aXAgYWNjZXB0cyBhbiBhcnJheSBvZiBhcnJheXMgYW5kIGdyb3Vwc1xuICAvLyBlYWNoIGFycmF5J3MgZWxlbWVudHMgb24gc2hhcmVkIGluZGljZXMuXG4gIF8udW56aXAgPSBmdW5jdGlvbihhcnJheSkge1xuICAgIHZhciBsZW5ndGggPSBhcnJheSAmJiBfLm1heChhcnJheSwgZ2V0TGVuZ3RoKS5sZW5ndGggfHwgMDtcbiAgICB2YXIgcmVzdWx0ID0gQXJyYXkobGVuZ3RoKTtcblxuICAgIGZvciAodmFyIGluZGV4ID0gMDsgaW5kZXggPCBsZW5ndGg7IGluZGV4KyspIHtcbiAgICAgIHJlc3VsdFtpbmRleF0gPSBfLnBsdWNrKGFycmF5LCBpbmRleCk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH07XG5cbiAgLy8gWmlwIHRvZ2V0aGVyIG11bHRpcGxlIGxpc3RzIGludG8gYSBzaW5nbGUgYXJyYXkgLS0gZWxlbWVudHMgdGhhdCBzaGFyZVxuICAvLyBhbiBpbmRleCBnbyB0b2dldGhlci5cbiAgXy56aXAgPSByZXN0QXJndW1lbnRzKF8udW56aXApO1xuXG4gIC8vIENvbnZlcnRzIGxpc3RzIGludG8gb2JqZWN0cy4gUGFzcyBlaXRoZXIgYSBzaW5nbGUgYXJyYXkgb2YgYFtrZXksIHZhbHVlXWBcbiAgLy8gcGFpcnMsIG9yIHR3byBwYXJhbGxlbCBhcnJheXMgb2YgdGhlIHNhbWUgbGVuZ3RoIC0tIG9uZSBvZiBrZXlzLCBhbmQgb25lIG9mXG4gIC8vIHRoZSBjb3JyZXNwb25kaW5nIHZhbHVlcy4gUGFzc2luZyBieSBwYWlycyBpcyB0aGUgcmV2ZXJzZSBvZiBfLnBhaXJzLlxuICBfLm9iamVjdCA9IGZ1bmN0aW9uKGxpc3QsIHZhbHVlcykge1xuICAgIHZhciByZXN1bHQgPSB7fTtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuZ3RoID0gZ2V0TGVuZ3RoKGxpc3QpOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmICh2YWx1ZXMpIHtcbiAgICAgICAgcmVzdWx0W2xpc3RbaV1dID0gdmFsdWVzW2ldO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmVzdWx0W2xpc3RbaV1bMF1dID0gbGlzdFtpXVsxXTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcblxuICAvLyBHZW5lcmF0b3IgZnVuY3Rpb24gdG8gY3JlYXRlIHRoZSBmaW5kSW5kZXggYW5kIGZpbmRMYXN0SW5kZXggZnVuY3Rpb25zLlxuICB2YXIgY3JlYXRlUHJlZGljYXRlSW5kZXhGaW5kZXIgPSBmdW5jdGlvbihkaXIpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24oYXJyYXksIHByZWRpY2F0ZSwgY29udGV4dCkge1xuICAgICAgcHJlZGljYXRlID0gY2IocHJlZGljYXRlLCBjb250ZXh0KTtcbiAgICAgIHZhciBsZW5ndGggPSBnZXRMZW5ndGgoYXJyYXkpO1xuICAgICAgdmFyIGluZGV4ID0gZGlyID4gMCA/IDAgOiBsZW5ndGggLSAxO1xuICAgICAgZm9yICg7IGluZGV4ID49IDAgJiYgaW5kZXggPCBsZW5ndGg7IGluZGV4ICs9IGRpcikge1xuICAgICAgICBpZiAocHJlZGljYXRlKGFycmF5W2luZGV4XSwgaW5kZXgsIGFycmF5KSkgcmV0dXJuIGluZGV4O1xuICAgICAgfVxuICAgICAgcmV0dXJuIC0xO1xuICAgIH07XG4gIH07XG5cbiAgLy8gUmV0dXJucyB0aGUgZmlyc3QgaW5kZXggb24gYW4gYXJyYXktbGlrZSB0aGF0IHBhc3NlcyBhIHByZWRpY2F0ZSB0ZXN0LlxuICBfLmZpbmRJbmRleCA9IGNyZWF0ZVByZWRpY2F0ZUluZGV4RmluZGVyKDEpO1xuICBfLmZpbmRMYXN0SW5kZXggPSBjcmVhdGVQcmVkaWNhdGVJbmRleEZpbmRlcigtMSk7XG5cbiAgLy8gVXNlIGEgY29tcGFyYXRvciBmdW5jdGlvbiB0byBmaWd1cmUgb3V0IHRoZSBzbWFsbGVzdCBpbmRleCBhdCB3aGljaFxuICAvLyBhbiBvYmplY3Qgc2hvdWxkIGJlIGluc2VydGVkIHNvIGFzIHRvIG1haW50YWluIG9yZGVyLiBVc2VzIGJpbmFyeSBzZWFyY2guXG4gIF8uc29ydGVkSW5kZXggPSBmdW5jdGlvbihhcnJheSwgb2JqLCBpdGVyYXRlZSwgY29udGV4dCkge1xuICAgIGl0ZXJhdGVlID0gY2IoaXRlcmF0ZWUsIGNvbnRleHQsIDEpO1xuICAgIHZhciB2YWx1ZSA9IGl0ZXJhdGVlKG9iaik7XG4gICAgdmFyIGxvdyA9IDAsIGhpZ2ggPSBnZXRMZW5ndGgoYXJyYXkpO1xuICAgIHdoaWxlIChsb3cgPCBoaWdoKSB7XG4gICAgICB2YXIgbWlkID0gTWF0aC5mbG9vcigobG93ICsgaGlnaCkgLyAyKTtcbiAgICAgIGlmIChpdGVyYXRlZShhcnJheVttaWRdKSA8IHZhbHVlKSBsb3cgPSBtaWQgKyAxOyBlbHNlIGhpZ2ggPSBtaWQ7XG4gICAgfVxuICAgIHJldHVybiBsb3c7XG4gIH07XG5cbiAgLy8gR2VuZXJhdG9yIGZ1bmN0aW9uIHRvIGNyZWF0ZSB0aGUgaW5kZXhPZiBhbmQgbGFzdEluZGV4T2YgZnVuY3Rpb25zLlxuICB2YXIgY3JlYXRlSW5kZXhGaW5kZXIgPSBmdW5jdGlvbihkaXIsIHByZWRpY2F0ZUZpbmQsIHNvcnRlZEluZGV4KSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKGFycmF5LCBpdGVtLCBpZHgpIHtcbiAgICAgIHZhciBpID0gMCwgbGVuZ3RoID0gZ2V0TGVuZ3RoKGFycmF5KTtcbiAgICAgIGlmICh0eXBlb2YgaWR4ID09ICdudW1iZXInKSB7XG4gICAgICAgIGlmIChkaXIgPiAwKSB7XG4gICAgICAgICAgaSA9IGlkeCA+PSAwID8gaWR4IDogTWF0aC5tYXgoaWR4ICsgbGVuZ3RoLCBpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsZW5ndGggPSBpZHggPj0gMCA/IE1hdGgubWluKGlkeCArIDEsIGxlbmd0aCkgOiBpZHggKyBsZW5ndGggKyAxO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHNvcnRlZEluZGV4ICYmIGlkeCAmJiBsZW5ndGgpIHtcbiAgICAgICAgaWR4ID0gc29ydGVkSW5kZXgoYXJyYXksIGl0ZW0pO1xuICAgICAgICByZXR1cm4gYXJyYXlbaWR4XSA9PT0gaXRlbSA/IGlkeCA6IC0xO1xuICAgICAgfVxuICAgICAgaWYgKGl0ZW0gIT09IGl0ZW0pIHtcbiAgICAgICAgaWR4ID0gcHJlZGljYXRlRmluZChzbGljZS5jYWxsKGFycmF5LCBpLCBsZW5ndGgpLCBfLmlzTmFOKTtcbiAgICAgICAgcmV0dXJuIGlkeCA+PSAwID8gaWR4ICsgaSA6IC0xO1xuICAgICAgfVxuICAgICAgZm9yIChpZHggPSBkaXIgPiAwID8gaSA6IGxlbmd0aCAtIDE7IGlkeCA+PSAwICYmIGlkeCA8IGxlbmd0aDsgaWR4ICs9IGRpcikge1xuICAgICAgICBpZiAoYXJyYXlbaWR4XSA9PT0gaXRlbSkgcmV0dXJuIGlkeDtcbiAgICAgIH1cbiAgICAgIHJldHVybiAtMTtcbiAgICB9O1xuICB9O1xuXG4gIC8vIFJldHVybiB0aGUgcG9zaXRpb24gb2YgdGhlIGZpcnN0IG9jY3VycmVuY2Ugb2YgYW4gaXRlbSBpbiBhbiBhcnJheSxcbiAgLy8gb3IgLTEgaWYgdGhlIGl0ZW0gaXMgbm90IGluY2x1ZGVkIGluIHRoZSBhcnJheS5cbiAgLy8gSWYgdGhlIGFycmF5IGlzIGxhcmdlIGFuZCBhbHJlYWR5IGluIHNvcnQgb3JkZXIsIHBhc3MgYHRydWVgXG4gIC8vIGZvciAqKmlzU29ydGVkKiogdG8gdXNlIGJpbmFyeSBzZWFyY2guXG4gIF8uaW5kZXhPZiA9IGNyZWF0ZUluZGV4RmluZGVyKDEsIF8uZmluZEluZGV4LCBfLnNvcnRlZEluZGV4KTtcbiAgXy5sYXN0SW5kZXhPZiA9IGNyZWF0ZUluZGV4RmluZGVyKC0xLCBfLmZpbmRMYXN0SW5kZXgpO1xuXG4gIC8vIEdlbmVyYXRlIGFuIGludGVnZXIgQXJyYXkgY29udGFpbmluZyBhbiBhcml0aG1ldGljIHByb2dyZXNzaW9uLiBBIHBvcnQgb2ZcbiAgLy8gdGhlIG5hdGl2ZSBQeXRob24gYHJhbmdlKClgIGZ1bmN0aW9uLiBTZWVcbiAgLy8gW3RoZSBQeXRob24gZG9jdW1lbnRhdGlvbl0oaHR0cDovL2RvY3MucHl0aG9uLm9yZy9saWJyYXJ5L2Z1bmN0aW9ucy5odG1sI3JhbmdlKS5cbiAgXy5yYW5nZSA9IGZ1bmN0aW9uKHN0YXJ0LCBzdG9wLCBzdGVwKSB7XG4gICAgaWYgKHN0b3AgPT0gbnVsbCkge1xuICAgICAgc3RvcCA9IHN0YXJ0IHx8IDA7XG4gICAgICBzdGFydCA9IDA7XG4gICAgfVxuICAgIGlmICghc3RlcCkge1xuICAgICAgc3RlcCA9IHN0b3AgPCBzdGFydCA/IC0xIDogMTtcbiAgICB9XG5cbiAgICB2YXIgbGVuZ3RoID0gTWF0aC5tYXgoTWF0aC5jZWlsKChzdG9wIC0gc3RhcnQpIC8gc3RlcCksIDApO1xuICAgIHZhciByYW5nZSA9IEFycmF5KGxlbmd0aCk7XG5cbiAgICBmb3IgKHZhciBpZHggPSAwOyBpZHggPCBsZW5ndGg7IGlkeCsrLCBzdGFydCArPSBzdGVwKSB7XG4gICAgICByYW5nZVtpZHhdID0gc3RhcnQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJhbmdlO1xuICB9O1xuXG4gIC8vIENodW5rIGEgc2luZ2xlIGFycmF5IGludG8gbXVsdGlwbGUgYXJyYXlzLCBlYWNoIGNvbnRhaW5pbmcgYGNvdW50YCBvciBmZXdlclxuICAvLyBpdGVtcy5cbiAgXy5jaHVuayA9IGZ1bmN0aW9uKGFycmF5LCBjb3VudCkge1xuICAgIGlmIChjb3VudCA9PSBudWxsIHx8IGNvdW50IDwgMSkgcmV0dXJuIFtdO1xuICAgIHZhciByZXN1bHQgPSBbXTtcbiAgICB2YXIgaSA9IDAsIGxlbmd0aCA9IGFycmF5Lmxlbmd0aDtcbiAgICB3aGlsZSAoaSA8IGxlbmd0aCkge1xuICAgICAgcmVzdWx0LnB1c2goc2xpY2UuY2FsbChhcnJheSwgaSwgaSArPSBjb3VudCkpO1xuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xuXG4gIC8vIEZ1bmN0aW9uIChhaGVtKSBGdW5jdGlvbnNcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tXG5cbiAgLy8gRGV0ZXJtaW5lcyB3aGV0aGVyIHRvIGV4ZWN1dGUgYSBmdW5jdGlvbiBhcyBhIGNvbnN0cnVjdG9yXG4gIC8vIG9yIGEgbm9ybWFsIGZ1bmN0aW9uIHdpdGggdGhlIHByb3ZpZGVkIGFyZ3VtZW50cy5cbiAgdmFyIGV4ZWN1dGVCb3VuZCA9IGZ1bmN0aW9uKHNvdXJjZUZ1bmMsIGJvdW5kRnVuYywgY29udGV4dCwgY2FsbGluZ0NvbnRleHQsIGFyZ3MpIHtcbiAgICBpZiAoIShjYWxsaW5nQ29udGV4dCBpbnN0YW5jZW9mIGJvdW5kRnVuYykpIHJldHVybiBzb3VyY2VGdW5jLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xuICAgIHZhciBzZWxmID0gYmFzZUNyZWF0ZShzb3VyY2VGdW5jLnByb3RvdHlwZSk7XG4gICAgdmFyIHJlc3VsdCA9IHNvdXJjZUZ1bmMuYXBwbHkoc2VsZiwgYXJncyk7XG4gICAgaWYgKF8uaXNPYmplY3QocmVzdWx0KSkgcmV0dXJuIHJlc3VsdDtcbiAgICByZXR1cm4gc2VsZjtcbiAgfTtcblxuICAvLyBDcmVhdGUgYSBmdW5jdGlvbiBib3VuZCB0byBhIGdpdmVuIG9iamVjdCAoYXNzaWduaW5nIGB0aGlzYCwgYW5kIGFyZ3VtZW50cyxcbiAgLy8gb3B0aW9uYWxseSkuIERlbGVnYXRlcyB0byAqKkVDTUFTY3JpcHQgNSoqJ3MgbmF0aXZlIGBGdW5jdGlvbi5iaW5kYCBpZlxuICAvLyBhdmFpbGFibGUuXG4gIF8uYmluZCA9IHJlc3RBcmd1bWVudHMoZnVuY3Rpb24oZnVuYywgY29udGV4dCwgYXJncykge1xuICAgIGlmICghXy5pc0Z1bmN0aW9uKGZ1bmMpKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdCaW5kIG11c3QgYmUgY2FsbGVkIG9uIGEgZnVuY3Rpb24nKTtcbiAgICB2YXIgYm91bmQgPSByZXN0QXJndW1lbnRzKGZ1bmN0aW9uKGNhbGxBcmdzKSB7XG4gICAgICByZXR1cm4gZXhlY3V0ZUJvdW5kKGZ1bmMsIGJvdW5kLCBjb250ZXh0LCB0aGlzLCBhcmdzLmNvbmNhdChjYWxsQXJncykpO1xuICAgIH0pO1xuICAgIHJldHVybiBib3VuZDtcbiAgfSk7XG5cbiAgLy8gUGFydGlhbGx5IGFwcGx5IGEgZnVuY3Rpb24gYnkgY3JlYXRpbmcgYSB2ZXJzaW9uIHRoYXQgaGFzIGhhZCBzb21lIG9mIGl0c1xuICAvLyBhcmd1bWVudHMgcHJlLWZpbGxlZCwgd2l0aG91dCBjaGFuZ2luZyBpdHMgZHluYW1pYyBgdGhpc2AgY29udGV4dC4gXyBhY3RzXG4gIC8vIGFzIGEgcGxhY2Vob2xkZXIgYnkgZGVmYXVsdCwgYWxsb3dpbmcgYW55IGNvbWJpbmF0aW9uIG9mIGFyZ3VtZW50cyB0byBiZVxuICAvLyBwcmUtZmlsbGVkLiBTZXQgYF8ucGFydGlhbC5wbGFjZWhvbGRlcmAgZm9yIGEgY3VzdG9tIHBsYWNlaG9sZGVyIGFyZ3VtZW50LlxuICBfLnBhcnRpYWwgPSByZXN0QXJndW1lbnRzKGZ1bmN0aW9uKGZ1bmMsIGJvdW5kQXJncykge1xuICAgIHZhciBwbGFjZWhvbGRlciA9IF8ucGFydGlhbC5wbGFjZWhvbGRlcjtcbiAgICB2YXIgYm91bmQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBwb3NpdGlvbiA9IDAsIGxlbmd0aCA9IGJvdW5kQXJncy5sZW5ndGg7XG4gICAgICB2YXIgYXJncyA9IEFycmF5KGxlbmd0aCk7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGFyZ3NbaV0gPSBib3VuZEFyZ3NbaV0gPT09IHBsYWNlaG9sZGVyID8gYXJndW1lbnRzW3Bvc2l0aW9uKytdIDogYm91bmRBcmdzW2ldO1xuICAgICAgfVxuICAgICAgd2hpbGUgKHBvc2l0aW9uIDwgYXJndW1lbnRzLmxlbmd0aCkgYXJncy5wdXNoKGFyZ3VtZW50c1twb3NpdGlvbisrXSk7XG4gICAgICByZXR1cm4gZXhlY3V0ZUJvdW5kKGZ1bmMsIGJvdW5kLCB0aGlzLCB0aGlzLCBhcmdzKTtcbiAgICB9O1xuICAgIHJldHVybiBib3VuZDtcbiAgfSk7XG5cbiAgXy5wYXJ0aWFsLnBsYWNlaG9sZGVyID0gXztcblxuICAvLyBCaW5kIGEgbnVtYmVyIG9mIGFuIG9iamVjdCdzIG1ldGhvZHMgdG8gdGhhdCBvYmplY3QuIFJlbWFpbmluZyBhcmd1bWVudHNcbiAgLy8gYXJlIHRoZSBtZXRob2QgbmFtZXMgdG8gYmUgYm91bmQuIFVzZWZ1bCBmb3IgZW5zdXJpbmcgdGhhdCBhbGwgY2FsbGJhY2tzXG4gIC8vIGRlZmluZWQgb24gYW4gb2JqZWN0IGJlbG9uZyB0byBpdC5cbiAgXy5iaW5kQWxsID0gcmVzdEFyZ3VtZW50cyhmdW5jdGlvbihvYmosIGtleXMpIHtcbiAgICBrZXlzID0gZmxhdHRlbihrZXlzLCBmYWxzZSwgZmFsc2UpO1xuICAgIHZhciBpbmRleCA9IGtleXMubGVuZ3RoO1xuICAgIGlmIChpbmRleCA8IDEpIHRocm93IG5ldyBFcnJvcignYmluZEFsbCBtdXN0IGJlIHBhc3NlZCBmdW5jdGlvbiBuYW1lcycpO1xuICAgIHdoaWxlIChpbmRleC0tKSB7XG4gICAgICB2YXIga2V5ID0ga2V5c1tpbmRleF07XG4gICAgICBvYmpba2V5XSA9IF8uYmluZChvYmpba2V5XSwgb2JqKTtcbiAgICB9XG4gIH0pO1xuXG4gIC8vIE1lbW9pemUgYW4gZXhwZW5zaXZlIGZ1bmN0aW9uIGJ5IHN0b3JpbmcgaXRzIHJlc3VsdHMuXG4gIF8ubWVtb2l6ZSA9IGZ1bmN0aW9uKGZ1bmMsIGhhc2hlcikge1xuICAgIHZhciBtZW1vaXplID0gZnVuY3Rpb24oa2V5KSB7XG4gICAgICB2YXIgY2FjaGUgPSBtZW1vaXplLmNhY2hlO1xuICAgICAgdmFyIGFkZHJlc3MgPSAnJyArIChoYXNoZXIgPyBoYXNoZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKSA6IGtleSk7XG4gICAgICBpZiAoIWhhcyhjYWNoZSwgYWRkcmVzcykpIGNhY2hlW2FkZHJlc3NdID0gZnVuYy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgcmV0dXJuIGNhY2hlW2FkZHJlc3NdO1xuICAgIH07XG4gICAgbWVtb2l6ZS5jYWNoZSA9IHt9O1xuICAgIHJldHVybiBtZW1vaXplO1xuICB9O1xuXG4gIC8vIERlbGF5cyBhIGZ1bmN0aW9uIGZvciB0aGUgZ2l2ZW4gbnVtYmVyIG9mIG1pbGxpc2Vjb25kcywgYW5kIHRoZW4gY2FsbHNcbiAgLy8gaXQgd2l0aCB0aGUgYXJndW1lbnRzIHN1cHBsaWVkLlxuICBfLmRlbGF5ID0gcmVzdEFyZ3VtZW50cyhmdW5jdGlvbihmdW5jLCB3YWl0LCBhcmdzKSB7XG4gICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gZnVuYy5hcHBseShudWxsLCBhcmdzKTtcbiAgICB9LCB3YWl0KTtcbiAgfSk7XG5cbiAgLy8gRGVmZXJzIGEgZnVuY3Rpb24sIHNjaGVkdWxpbmcgaXQgdG8gcnVuIGFmdGVyIHRoZSBjdXJyZW50IGNhbGwgc3RhY2sgaGFzXG4gIC8vIGNsZWFyZWQuXG4gIF8uZGVmZXIgPSBfLnBhcnRpYWwoXy5kZWxheSwgXywgMSk7XG5cbiAgLy8gUmV0dXJucyBhIGZ1bmN0aW9uLCB0aGF0LCB3aGVuIGludm9rZWQsIHdpbGwgb25seSBiZSB0cmlnZ2VyZWQgYXQgbW9zdCBvbmNlXG4gIC8vIGR1cmluZyBhIGdpdmVuIHdpbmRvdyBvZiB0aW1lLiBOb3JtYWxseSwgdGhlIHRocm90dGxlZCBmdW5jdGlvbiB3aWxsIHJ1blxuICAvLyBhcyBtdWNoIGFzIGl0IGNhbiwgd2l0aG91dCBldmVyIGdvaW5nIG1vcmUgdGhhbiBvbmNlIHBlciBgd2FpdGAgZHVyYXRpb247XG4gIC8vIGJ1dCBpZiB5b3UnZCBsaWtlIHRvIGRpc2FibGUgdGhlIGV4ZWN1dGlvbiBvbiB0aGUgbGVhZGluZyBlZGdlLCBwYXNzXG4gIC8vIGB7bGVhZGluZzogZmFsc2V9YC4gVG8gZGlzYWJsZSBleGVjdXRpb24gb24gdGhlIHRyYWlsaW5nIGVkZ2UsIGRpdHRvLlxuICBfLnRocm90dGxlID0gZnVuY3Rpb24oZnVuYywgd2FpdCwgb3B0aW9ucykge1xuICAgIHZhciB0aW1lb3V0LCBjb250ZXh0LCBhcmdzLCByZXN1bHQ7XG4gICAgdmFyIHByZXZpb3VzID0gMDtcbiAgICBpZiAoIW9wdGlvbnMpIG9wdGlvbnMgPSB7fTtcblxuICAgIHZhciBsYXRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgcHJldmlvdXMgPSBvcHRpb25zLmxlYWRpbmcgPT09IGZhbHNlID8gMCA6IF8ubm93KCk7XG4gICAgICB0aW1lb3V0ID0gbnVsbDtcbiAgICAgIHJlc3VsdCA9IGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgICBpZiAoIXRpbWVvdXQpIGNvbnRleHQgPSBhcmdzID0gbnVsbDtcbiAgICB9O1xuXG4gICAgdmFyIHRocm90dGxlZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIG5vdyA9IF8ubm93KCk7XG4gICAgICBpZiAoIXByZXZpb3VzICYmIG9wdGlvbnMubGVhZGluZyA9PT0gZmFsc2UpIHByZXZpb3VzID0gbm93O1xuICAgICAgdmFyIHJlbWFpbmluZyA9IHdhaXQgLSAobm93IC0gcHJldmlvdXMpO1xuICAgICAgY29udGV4dCA9IHRoaXM7XG4gICAgICBhcmdzID0gYXJndW1lbnRzO1xuICAgICAgaWYgKHJlbWFpbmluZyA8PSAwIHx8IHJlbWFpbmluZyA+IHdhaXQpIHtcbiAgICAgICAgaWYgKHRpbWVvdXQpIHtcbiAgICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICAgICAgdGltZW91dCA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgcHJldmlvdXMgPSBub3c7XG4gICAgICAgIHJlc3VsdCA9IGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgICAgIGlmICghdGltZW91dCkgY29udGV4dCA9IGFyZ3MgPSBudWxsO1xuICAgICAgfSBlbHNlIGlmICghdGltZW91dCAmJiBvcHRpb25zLnRyYWlsaW5nICE9PSBmYWxzZSkge1xuICAgICAgICB0aW1lb3V0ID0gc2V0VGltZW91dChsYXRlciwgcmVtYWluaW5nKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfTtcblxuICAgIHRocm90dGxlZC5jYW5jZWwgPSBmdW5jdGlvbigpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aW1lb3V0KTtcbiAgICAgIHByZXZpb3VzID0gMDtcbiAgICAgIHRpbWVvdXQgPSBjb250ZXh0ID0gYXJncyA9IG51bGw7XG4gICAgfTtcblxuICAgIHJldHVybiB0aHJvdHRsZWQ7XG4gIH07XG5cbiAgLy8gUmV0dXJucyBhIGZ1bmN0aW9uLCB0aGF0LCBhcyBsb25nIGFzIGl0IGNvbnRpbnVlcyB0byBiZSBpbnZva2VkLCB3aWxsIG5vdFxuICAvLyBiZSB0cmlnZ2VyZWQuIFRoZSBmdW5jdGlvbiB3aWxsIGJlIGNhbGxlZCBhZnRlciBpdCBzdG9wcyBiZWluZyBjYWxsZWQgZm9yXG4gIC8vIE4gbWlsbGlzZWNvbmRzLiBJZiBgaW1tZWRpYXRlYCBpcyBwYXNzZWQsIHRyaWdnZXIgdGhlIGZ1bmN0aW9uIG9uIHRoZVxuICAvLyBsZWFkaW5nIGVkZ2UsIGluc3RlYWQgb2YgdGhlIHRyYWlsaW5nLlxuICBfLmRlYm91bmNlID0gZnVuY3Rpb24oZnVuYywgd2FpdCwgaW1tZWRpYXRlKSB7XG4gICAgdmFyIHRpbWVvdXQsIHJlc3VsdDtcblxuICAgIHZhciBsYXRlciA9IGZ1bmN0aW9uKGNvbnRleHQsIGFyZ3MpIHtcbiAgICAgIHRpbWVvdXQgPSBudWxsO1xuICAgICAgaWYgKGFyZ3MpIHJlc3VsdCA9IGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgfTtcblxuICAgIHZhciBkZWJvdW5jZWQgPSByZXN0QXJndW1lbnRzKGZ1bmN0aW9uKGFyZ3MpIHtcbiAgICAgIGlmICh0aW1lb3V0KSBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICBpZiAoaW1tZWRpYXRlKSB7XG4gICAgICAgIHZhciBjYWxsTm93ID0gIXRpbWVvdXQ7XG4gICAgICAgIHRpbWVvdXQgPSBzZXRUaW1lb3V0KGxhdGVyLCB3YWl0KTtcbiAgICAgICAgaWYgKGNhbGxOb3cpIHJlc3VsdCA9IGZ1bmMuYXBwbHkodGhpcywgYXJncyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aW1lb3V0ID0gXy5kZWxheShsYXRlciwgd2FpdCwgdGhpcywgYXJncyk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSk7XG5cbiAgICBkZWJvdW5jZWQuY2FuY2VsID0gZnVuY3Rpb24oKSB7XG4gICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICB0aW1lb3V0ID0gbnVsbDtcbiAgICB9O1xuXG4gICAgcmV0dXJuIGRlYm91bmNlZDtcbiAgfTtcblxuICAvLyBSZXR1cm5zIHRoZSBmaXJzdCBmdW5jdGlvbiBwYXNzZWQgYXMgYW4gYXJndW1lbnQgdG8gdGhlIHNlY29uZCxcbiAgLy8gYWxsb3dpbmcgeW91IHRvIGFkanVzdCBhcmd1bWVudHMsIHJ1biBjb2RlIGJlZm9yZSBhbmQgYWZ0ZXIsIGFuZFxuICAvLyBjb25kaXRpb25hbGx5IGV4ZWN1dGUgdGhlIG9yaWdpbmFsIGZ1bmN0aW9uLlxuICBfLndyYXAgPSBmdW5jdGlvbihmdW5jLCB3cmFwcGVyKSB7XG4gICAgcmV0dXJuIF8ucGFydGlhbCh3cmFwcGVyLCBmdW5jKTtcbiAgfTtcblxuICAvLyBSZXR1cm5zIGEgbmVnYXRlZCB2ZXJzaW9uIG9mIHRoZSBwYXNzZWQtaW4gcHJlZGljYXRlLlxuICBfLm5lZ2F0ZSA9IGZ1bmN0aW9uKHByZWRpY2F0ZSkge1xuICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiAhcHJlZGljYXRlLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgfTtcbiAgfTtcblxuICAvLyBSZXR1cm5zIGEgZnVuY3Rpb24gdGhhdCBpcyB0aGUgY29tcG9zaXRpb24gb2YgYSBsaXN0IG9mIGZ1bmN0aW9ucywgZWFjaFxuICAvLyBjb25zdW1pbmcgdGhlIHJldHVybiB2YWx1ZSBvZiB0aGUgZnVuY3Rpb24gdGhhdCBmb2xsb3dzLlxuICBfLmNvbXBvc2UgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgYXJncyA9IGFyZ3VtZW50cztcbiAgICB2YXIgc3RhcnQgPSBhcmdzLmxlbmd0aCAtIDE7XG4gICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGkgPSBzdGFydDtcbiAgICAgIHZhciByZXN1bHQgPSBhcmdzW3N0YXJ0XS5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgd2hpbGUgKGktLSkgcmVzdWx0ID0gYXJnc1tpXS5jYWxsKHRoaXMsIHJlc3VsdCk7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG4gIH07XG5cbiAgLy8gUmV0dXJucyBhIGZ1bmN0aW9uIHRoYXQgd2lsbCBvbmx5IGJlIGV4ZWN1dGVkIG9uIGFuZCBhZnRlciB0aGUgTnRoIGNhbGwuXG4gIF8uYWZ0ZXIgPSBmdW5jdGlvbih0aW1lcywgZnVuYykge1xuICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgIGlmICgtLXRpbWVzIDwgMSkge1xuICAgICAgICByZXR1cm4gZnVuYy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgfVxuICAgIH07XG4gIH07XG5cbiAgLy8gUmV0dXJucyBhIGZ1bmN0aW9uIHRoYXQgd2lsbCBvbmx5IGJlIGV4ZWN1dGVkIHVwIHRvIChidXQgbm90IGluY2x1ZGluZykgdGhlIE50aCBjYWxsLlxuICBfLmJlZm9yZSA9IGZ1bmN0aW9uKHRpbWVzLCBmdW5jKSB7XG4gICAgdmFyIG1lbW87XG4gICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKC0tdGltZXMgPiAwKSB7XG4gICAgICAgIG1lbW8gPSBmdW5jLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgICB9XG4gICAgICBpZiAodGltZXMgPD0gMSkgZnVuYyA9IG51bGw7XG4gICAgICByZXR1cm4gbWVtbztcbiAgICB9O1xuICB9O1xuXG4gIC8vIFJldHVybnMgYSBmdW5jdGlvbiB0aGF0IHdpbGwgYmUgZXhlY3V0ZWQgYXQgbW9zdCBvbmUgdGltZSwgbm8gbWF0dGVyIGhvd1xuICAvLyBvZnRlbiB5b3UgY2FsbCBpdC4gVXNlZnVsIGZvciBsYXp5IGluaXRpYWxpemF0aW9uLlxuICBfLm9uY2UgPSBfLnBhcnRpYWwoXy5iZWZvcmUsIDIpO1xuXG4gIF8ucmVzdEFyZ3VtZW50cyA9IHJlc3RBcmd1bWVudHM7XG5cbiAgLy8gT2JqZWN0IEZ1bmN0aW9uc1xuICAvLyAtLS0tLS0tLS0tLS0tLS0tXG5cbiAgLy8gS2V5cyBpbiBJRSA8IDkgdGhhdCB3b24ndCBiZSBpdGVyYXRlZCBieSBgZm9yIGtleSBpbiAuLi5gIGFuZCB0aHVzIG1pc3NlZC5cbiAgdmFyIGhhc0VudW1CdWcgPSAhe3RvU3RyaW5nOiBudWxsfS5wcm9wZXJ0eUlzRW51bWVyYWJsZSgndG9TdHJpbmcnKTtcbiAgdmFyIG5vbkVudW1lcmFibGVQcm9wcyA9IFsndmFsdWVPZicsICdpc1Byb3RvdHlwZU9mJywgJ3RvU3RyaW5nJyxcbiAgICAncHJvcGVydHlJc0VudW1lcmFibGUnLCAnaGFzT3duUHJvcGVydHknLCAndG9Mb2NhbGVTdHJpbmcnXTtcblxuICB2YXIgY29sbGVjdE5vbkVudW1Qcm9wcyA9IGZ1bmN0aW9uKG9iaiwga2V5cykge1xuICAgIHZhciBub25FbnVtSWR4ID0gbm9uRW51bWVyYWJsZVByb3BzLmxlbmd0aDtcbiAgICB2YXIgY29uc3RydWN0b3IgPSBvYmouY29uc3RydWN0b3I7XG4gICAgdmFyIHByb3RvID0gXy5pc0Z1bmN0aW9uKGNvbnN0cnVjdG9yKSAmJiBjb25zdHJ1Y3Rvci5wcm90b3R5cGUgfHwgT2JqUHJvdG87XG5cbiAgICAvLyBDb25zdHJ1Y3RvciBpcyBhIHNwZWNpYWwgY2FzZS5cbiAgICB2YXIgcHJvcCA9ICdjb25zdHJ1Y3Rvcic7XG4gICAgaWYgKGhhcyhvYmosIHByb3ApICYmICFfLmNvbnRhaW5zKGtleXMsIHByb3ApKSBrZXlzLnB1c2gocHJvcCk7XG5cbiAgICB3aGlsZSAobm9uRW51bUlkeC0tKSB7XG4gICAgICBwcm9wID0gbm9uRW51bWVyYWJsZVByb3BzW25vbkVudW1JZHhdO1xuICAgICAgaWYgKHByb3AgaW4gb2JqICYmIG9ialtwcm9wXSAhPT0gcHJvdG9bcHJvcF0gJiYgIV8uY29udGFpbnMoa2V5cywgcHJvcCkpIHtcbiAgICAgICAga2V5cy5wdXNoKHByb3ApO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICAvLyBSZXRyaWV2ZSB0aGUgbmFtZXMgb2YgYW4gb2JqZWN0J3Mgb3duIHByb3BlcnRpZXMuXG4gIC8vIERlbGVnYXRlcyB0byAqKkVDTUFTY3JpcHQgNSoqJ3MgbmF0aXZlIGBPYmplY3Qua2V5c2AuXG4gIF8ua2V5cyA9IGZ1bmN0aW9uKG9iaikge1xuICAgIGlmICghXy5pc09iamVjdChvYmopKSByZXR1cm4gW107XG4gICAgaWYgKG5hdGl2ZUtleXMpIHJldHVybiBuYXRpdmVLZXlzKG9iaik7XG4gICAgdmFyIGtleXMgPSBbXTtcbiAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSBpZiAoaGFzKG9iaiwga2V5KSkga2V5cy5wdXNoKGtleSk7XG4gICAgLy8gQWhlbSwgSUUgPCA5LlxuICAgIGlmIChoYXNFbnVtQnVnKSBjb2xsZWN0Tm9uRW51bVByb3BzKG9iaiwga2V5cyk7XG4gICAgcmV0dXJuIGtleXM7XG4gIH07XG5cbiAgLy8gUmV0cmlldmUgYWxsIHRoZSBwcm9wZXJ0eSBuYW1lcyBvZiBhbiBvYmplY3QuXG4gIF8uYWxsS2V5cyA9IGZ1bmN0aW9uKG9iaikge1xuICAgIGlmICghXy5pc09iamVjdChvYmopKSByZXR1cm4gW107XG4gICAgdmFyIGtleXMgPSBbXTtcbiAgICBmb3IgKHZhciBrZXkgaW4gb2JqKSBrZXlzLnB1c2goa2V5KTtcbiAgICAvLyBBaGVtLCBJRSA8IDkuXG4gICAgaWYgKGhhc0VudW1CdWcpIGNvbGxlY3ROb25FbnVtUHJvcHMob2JqLCBrZXlzKTtcbiAgICByZXR1cm4ga2V5cztcbiAgfTtcblxuICAvLyBSZXRyaWV2ZSB0aGUgdmFsdWVzIG9mIGFuIG9iamVjdCdzIHByb3BlcnRpZXMuXG4gIF8udmFsdWVzID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgdmFyIGtleXMgPSBfLmtleXMob2JqKTtcbiAgICB2YXIgbGVuZ3RoID0ga2V5cy5sZW5ndGg7XG4gICAgdmFyIHZhbHVlcyA9IEFycmF5KGxlbmd0aCk7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgdmFsdWVzW2ldID0gb2JqW2tleXNbaV1dO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWVzO1xuICB9O1xuXG4gIC8vIFJldHVybnMgdGhlIHJlc3VsdHMgb2YgYXBwbHlpbmcgdGhlIGl0ZXJhdGVlIHRvIGVhY2ggZWxlbWVudCBvZiB0aGUgb2JqZWN0LlxuICAvLyBJbiBjb250cmFzdCB0byBfLm1hcCBpdCByZXR1cm5zIGFuIG9iamVjdC5cbiAgXy5tYXBPYmplY3QgPSBmdW5jdGlvbihvYmosIGl0ZXJhdGVlLCBjb250ZXh0KSB7XG4gICAgaXRlcmF0ZWUgPSBjYihpdGVyYXRlZSwgY29udGV4dCk7XG4gICAgdmFyIGtleXMgPSBfLmtleXMob2JqKSxcbiAgICAgICAgbGVuZ3RoID0ga2V5cy5sZW5ndGgsXG4gICAgICAgIHJlc3VsdHMgPSB7fTtcbiAgICBmb3IgKHZhciBpbmRleCA9IDA7IGluZGV4IDwgbGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICB2YXIgY3VycmVudEtleSA9IGtleXNbaW5kZXhdO1xuICAgICAgcmVzdWx0c1tjdXJyZW50S2V5XSA9IGl0ZXJhdGVlKG9ialtjdXJyZW50S2V5XSwgY3VycmVudEtleSwgb2JqKTtcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdHM7XG4gIH07XG5cbiAgLy8gQ29udmVydCBhbiBvYmplY3QgaW50byBhIGxpc3Qgb2YgYFtrZXksIHZhbHVlXWAgcGFpcnMuXG4gIC8vIFRoZSBvcHBvc2l0ZSBvZiBfLm9iamVjdC5cbiAgXy5wYWlycyA9IGZ1bmN0aW9uKG9iaikge1xuICAgIHZhciBrZXlzID0gXy5rZXlzKG9iaik7XG4gICAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIHZhciBwYWlycyA9IEFycmF5KGxlbmd0aCk7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgcGFpcnNbaV0gPSBba2V5c1tpXSwgb2JqW2tleXNbaV1dXTtcbiAgICB9XG4gICAgcmV0dXJuIHBhaXJzO1xuICB9O1xuXG4gIC8vIEludmVydCB0aGUga2V5cyBhbmQgdmFsdWVzIG9mIGFuIG9iamVjdC4gVGhlIHZhbHVlcyBtdXN0IGJlIHNlcmlhbGl6YWJsZS5cbiAgXy5pbnZlcnQgPSBmdW5jdGlvbihvYmopIHtcbiAgICB2YXIgcmVzdWx0ID0ge307XG4gICAgdmFyIGtleXMgPSBfLmtleXMob2JqKTtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuZ3RoID0ga2V5cy5sZW5ndGg7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgcmVzdWx0W29ialtrZXlzW2ldXV0gPSBrZXlzW2ldO1xuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xuXG4gIC8vIFJldHVybiBhIHNvcnRlZCBsaXN0IG9mIHRoZSBmdW5jdGlvbiBuYW1lcyBhdmFpbGFibGUgb24gdGhlIG9iamVjdC5cbiAgLy8gQWxpYXNlZCBhcyBgbWV0aG9kc2AuXG4gIF8uZnVuY3Rpb25zID0gXy5tZXRob2RzID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgdmFyIG5hbWVzID0gW107XG4gICAgZm9yICh2YXIga2V5IGluIG9iaikge1xuICAgICAgaWYgKF8uaXNGdW5jdGlvbihvYmpba2V5XSkpIG5hbWVzLnB1c2goa2V5KTtcbiAgICB9XG4gICAgcmV0dXJuIG5hbWVzLnNvcnQoKTtcbiAgfTtcblxuICAvLyBBbiBpbnRlcm5hbCBmdW5jdGlvbiBmb3IgY3JlYXRpbmcgYXNzaWduZXIgZnVuY3Rpb25zLlxuICB2YXIgY3JlYXRlQXNzaWduZXIgPSBmdW5jdGlvbihrZXlzRnVuYywgZGVmYXVsdHMpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24ob2JqKSB7XG4gICAgICB2YXIgbGVuZ3RoID0gYXJndW1lbnRzLmxlbmd0aDtcbiAgICAgIGlmIChkZWZhdWx0cykgb2JqID0gT2JqZWN0KG9iaik7XG4gICAgICBpZiAobGVuZ3RoIDwgMiB8fCBvYmogPT0gbnVsbCkgcmV0dXJuIG9iajtcbiAgICAgIGZvciAodmFyIGluZGV4ID0gMTsgaW5kZXggPCBsZW5ndGg7IGluZGV4KyspIHtcbiAgICAgICAgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpbmRleF0sXG4gICAgICAgICAgICBrZXlzID0ga2V5c0Z1bmMoc291cmNlKSxcbiAgICAgICAgICAgIGwgPSBrZXlzLmxlbmd0aDtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgICB2YXIga2V5ID0ga2V5c1tpXTtcbiAgICAgICAgICBpZiAoIWRlZmF1bHRzIHx8IG9ialtrZXldID09PSB2b2lkIDApIG9ialtrZXldID0gc291cmNlW2tleV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBvYmo7XG4gICAgfTtcbiAgfTtcblxuICAvLyBFeHRlbmQgYSBnaXZlbiBvYmplY3Qgd2l0aCBhbGwgdGhlIHByb3BlcnRpZXMgaW4gcGFzc2VkLWluIG9iamVjdChzKS5cbiAgXy5leHRlbmQgPSBjcmVhdGVBc3NpZ25lcihfLmFsbEtleXMpO1xuXG4gIC8vIEFzc2lnbnMgYSBnaXZlbiBvYmplY3Qgd2l0aCBhbGwgdGhlIG93biBwcm9wZXJ0aWVzIGluIHRoZSBwYXNzZWQtaW4gb2JqZWN0KHMpLlxuICAvLyAoaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZG9jcy9XZWIvSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvT2JqZWN0L2Fzc2lnbilcbiAgXy5leHRlbmRPd24gPSBfLmFzc2lnbiA9IGNyZWF0ZUFzc2lnbmVyKF8ua2V5cyk7XG5cbiAgLy8gUmV0dXJucyB0aGUgZmlyc3Qga2V5IG9uIGFuIG9iamVjdCB0aGF0IHBhc3NlcyBhIHByZWRpY2F0ZSB0ZXN0LlxuICBfLmZpbmRLZXkgPSBmdW5jdGlvbihvYmosIHByZWRpY2F0ZSwgY29udGV4dCkge1xuICAgIHByZWRpY2F0ZSA9IGNiKHByZWRpY2F0ZSwgY29udGV4dCk7XG4gICAgdmFyIGtleXMgPSBfLmtleXMob2JqKSwga2V5O1xuICAgIGZvciAodmFyIGkgPSAwLCBsZW5ndGggPSBrZXlzLmxlbmd0aDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICBrZXkgPSBrZXlzW2ldO1xuICAgICAgaWYgKHByZWRpY2F0ZShvYmpba2V5XSwga2V5LCBvYmopKSByZXR1cm4ga2V5O1xuICAgIH1cbiAgfTtcblxuICAvLyBJbnRlcm5hbCBwaWNrIGhlbHBlciBmdW5jdGlvbiB0byBkZXRlcm1pbmUgaWYgYG9iamAgaGFzIGtleSBga2V5YC5cbiAgdmFyIGtleUluT2JqID0gZnVuY3Rpb24odmFsdWUsIGtleSwgb2JqKSB7XG4gICAgcmV0dXJuIGtleSBpbiBvYmo7XG4gIH07XG5cbiAgLy8gUmV0dXJuIGEgY29weSBvZiB0aGUgb2JqZWN0IG9ubHkgY29udGFpbmluZyB0aGUgd2hpdGVsaXN0ZWQgcHJvcGVydGllcy5cbiAgXy5waWNrID0gcmVzdEFyZ3VtZW50cyhmdW5jdGlvbihvYmosIGtleXMpIHtcbiAgICB2YXIgcmVzdWx0ID0ge30sIGl0ZXJhdGVlID0ga2V5c1swXTtcbiAgICBpZiAob2JqID09IG51bGwpIHJldHVybiByZXN1bHQ7XG4gICAgaWYgKF8uaXNGdW5jdGlvbihpdGVyYXRlZSkpIHtcbiAgICAgIGlmIChrZXlzLmxlbmd0aCA+IDEpIGl0ZXJhdGVlID0gb3B0aW1pemVDYihpdGVyYXRlZSwga2V5c1sxXSk7XG4gICAgICBrZXlzID0gXy5hbGxLZXlzKG9iaik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGl0ZXJhdGVlID0ga2V5SW5PYmo7XG4gICAgICBrZXlzID0gZmxhdHRlbihrZXlzLCBmYWxzZSwgZmFsc2UpO1xuICAgICAgb2JqID0gT2JqZWN0KG9iaik7XG4gICAgfVxuICAgIGZvciAodmFyIGkgPSAwLCBsZW5ndGggPSBrZXlzLmxlbmd0aDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIga2V5ID0ga2V5c1tpXTtcbiAgICAgIHZhciB2YWx1ZSA9IG9ialtrZXldO1xuICAgICAgaWYgKGl0ZXJhdGVlKHZhbHVlLCBrZXksIG9iaikpIHJlc3VsdFtrZXldID0gdmFsdWU7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH0pO1xuXG4gIC8vIFJldHVybiBhIGNvcHkgb2YgdGhlIG9iamVjdCB3aXRob3V0IHRoZSBibGFja2xpc3RlZCBwcm9wZXJ0aWVzLlxuICBfLm9taXQgPSByZXN0QXJndW1lbnRzKGZ1bmN0aW9uKG9iaiwga2V5cykge1xuICAgIHZhciBpdGVyYXRlZSA9IGtleXNbMF0sIGNvbnRleHQ7XG4gICAgaWYgKF8uaXNGdW5jdGlvbihpdGVyYXRlZSkpIHtcbiAgICAgIGl0ZXJhdGVlID0gXy5uZWdhdGUoaXRlcmF0ZWUpO1xuICAgICAgaWYgKGtleXMubGVuZ3RoID4gMSkgY29udGV4dCA9IGtleXNbMV07XG4gICAgfSBlbHNlIHtcbiAgICAgIGtleXMgPSBfLm1hcChmbGF0dGVuKGtleXMsIGZhbHNlLCBmYWxzZSksIFN0cmluZyk7XG4gICAgICBpdGVyYXRlZSA9IGZ1bmN0aW9uKHZhbHVlLCBrZXkpIHtcbiAgICAgICAgcmV0dXJuICFfLmNvbnRhaW5zKGtleXMsIGtleSk7XG4gICAgICB9O1xuICAgIH1cbiAgICByZXR1cm4gXy5waWNrKG9iaiwgaXRlcmF0ZWUsIGNvbnRleHQpO1xuICB9KTtcblxuICAvLyBGaWxsIGluIGEgZ2l2ZW4gb2JqZWN0IHdpdGggZGVmYXVsdCBwcm9wZXJ0aWVzLlxuICBfLmRlZmF1bHRzID0gY3JlYXRlQXNzaWduZXIoXy5hbGxLZXlzLCB0cnVlKTtcblxuICAvLyBDcmVhdGVzIGFuIG9iamVjdCB0aGF0IGluaGVyaXRzIGZyb20gdGhlIGdpdmVuIHByb3RvdHlwZSBvYmplY3QuXG4gIC8vIElmIGFkZGl0aW9uYWwgcHJvcGVydGllcyBhcmUgcHJvdmlkZWQgdGhlbiB0aGV5IHdpbGwgYmUgYWRkZWQgdG8gdGhlXG4gIC8vIGNyZWF0ZWQgb2JqZWN0LlxuICBfLmNyZWF0ZSA9IGZ1bmN0aW9uKHByb3RvdHlwZSwgcHJvcHMpIHtcbiAgICB2YXIgcmVzdWx0ID0gYmFzZUNyZWF0ZShwcm90b3R5cGUpO1xuICAgIGlmIChwcm9wcykgXy5leHRlbmRPd24ocmVzdWx0LCBwcm9wcyk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcblxuICAvLyBDcmVhdGUgYSAoc2hhbGxvdy1jbG9uZWQpIGR1cGxpY2F0ZSBvZiBhbiBvYmplY3QuXG4gIF8uY2xvbmUgPSBmdW5jdGlvbihvYmopIHtcbiAgICBpZiAoIV8uaXNPYmplY3Qob2JqKSkgcmV0dXJuIG9iajtcbiAgICByZXR1cm4gXy5pc0FycmF5KG9iaikgPyBvYmouc2xpY2UoKSA6IF8uZXh0ZW5kKHt9LCBvYmopO1xuICB9O1xuXG4gIC8vIEludm9rZXMgaW50ZXJjZXB0b3Igd2l0aCB0aGUgb2JqLCBhbmQgdGhlbiByZXR1cm5zIG9iai5cbiAgLy8gVGhlIHByaW1hcnkgcHVycG9zZSBvZiB0aGlzIG1ldGhvZCBpcyB0byBcInRhcCBpbnRvXCIgYSBtZXRob2QgY2hhaW4sIGluXG4gIC8vIG9yZGVyIHRvIHBlcmZvcm0gb3BlcmF0aW9ucyBvbiBpbnRlcm1lZGlhdGUgcmVzdWx0cyB3aXRoaW4gdGhlIGNoYWluLlxuICBfLnRhcCA9IGZ1bmN0aW9uKG9iaiwgaW50ZXJjZXB0b3IpIHtcbiAgICBpbnRlcmNlcHRvcihvYmopO1xuICAgIHJldHVybiBvYmo7XG4gIH07XG5cbiAgLy8gUmV0dXJucyB3aGV0aGVyIGFuIG9iamVjdCBoYXMgYSBnaXZlbiBzZXQgb2YgYGtleTp2YWx1ZWAgcGFpcnMuXG4gIF8uaXNNYXRjaCA9IGZ1bmN0aW9uKG9iamVjdCwgYXR0cnMpIHtcbiAgICB2YXIga2V5cyA9IF8ua2V5cyhhdHRycyksIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIGlmIChvYmplY3QgPT0gbnVsbCkgcmV0dXJuICFsZW5ndGg7XG4gICAgdmFyIG9iaiA9IE9iamVjdChvYmplY3QpO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBrZXkgPSBrZXlzW2ldO1xuICAgICAgaWYgKGF0dHJzW2tleV0gIT09IG9ialtrZXldIHx8ICEoa2V5IGluIG9iaikpIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH07XG5cblxuICAvLyBJbnRlcm5hbCByZWN1cnNpdmUgY29tcGFyaXNvbiBmdW5jdGlvbiBmb3IgYGlzRXF1YWxgLlxuICB2YXIgZXEsIGRlZXBFcTtcbiAgZXEgPSBmdW5jdGlvbihhLCBiLCBhU3RhY2ssIGJTdGFjaykge1xuICAgIC8vIElkZW50aWNhbCBvYmplY3RzIGFyZSBlcXVhbC4gYDAgPT09IC0wYCwgYnV0IHRoZXkgYXJlbid0IGlkZW50aWNhbC5cbiAgICAvLyBTZWUgdGhlIFtIYXJtb255IGBlZ2FsYCBwcm9wb3NhbF0oaHR0cDovL3dpa2kuZWNtYXNjcmlwdC5vcmcvZG9rdS5waHA/aWQ9aGFybW9ueTplZ2FsKS5cbiAgICBpZiAoYSA9PT0gYikgcmV0dXJuIGEgIT09IDAgfHwgMSAvIGEgPT09IDEgLyBiO1xuICAgIC8vIGBudWxsYCBvciBgdW5kZWZpbmVkYCBvbmx5IGVxdWFsIHRvIGl0c2VsZiAoc3RyaWN0IGNvbXBhcmlzb24pLlxuICAgIGlmIChhID09IG51bGwgfHwgYiA9PSBudWxsKSByZXR1cm4gZmFsc2U7XG4gICAgLy8gYE5hTmBzIGFyZSBlcXVpdmFsZW50LCBidXQgbm9uLXJlZmxleGl2ZS5cbiAgICBpZiAoYSAhPT0gYSkgcmV0dXJuIGIgIT09IGI7XG4gICAgLy8gRXhoYXVzdCBwcmltaXRpdmUgY2hlY2tzXG4gICAgdmFyIHR5cGUgPSB0eXBlb2YgYTtcbiAgICBpZiAodHlwZSAhPT0gJ2Z1bmN0aW9uJyAmJiB0eXBlICE9PSAnb2JqZWN0JyAmJiB0eXBlb2YgYiAhPSAnb2JqZWN0JykgcmV0dXJuIGZhbHNlO1xuICAgIHJldHVybiBkZWVwRXEoYSwgYiwgYVN0YWNrLCBiU3RhY2spO1xuICB9O1xuXG4gIC8vIEludGVybmFsIHJlY3Vyc2l2ZSBjb21wYXJpc29uIGZ1bmN0aW9uIGZvciBgaXNFcXVhbGAuXG4gIGRlZXBFcSA9IGZ1bmN0aW9uKGEsIGIsIGFTdGFjaywgYlN0YWNrKSB7XG4gICAgLy8gVW53cmFwIGFueSB3cmFwcGVkIG9iamVjdHMuXG4gICAgaWYgKGEgaW5zdGFuY2VvZiBfKSBhID0gYS5fd3JhcHBlZDtcbiAgICBpZiAoYiBpbnN0YW5jZW9mIF8pIGIgPSBiLl93cmFwcGVkO1xuICAgIC8vIENvbXBhcmUgYFtbQ2xhc3NdXWAgbmFtZXMuXG4gICAgdmFyIGNsYXNzTmFtZSA9IHRvU3RyaW5nLmNhbGwoYSk7XG4gICAgaWYgKGNsYXNzTmFtZSAhPT0gdG9TdHJpbmcuY2FsbChiKSkgcmV0dXJuIGZhbHNlO1xuICAgIHN3aXRjaCAoY2xhc3NOYW1lKSB7XG4gICAgICAvLyBTdHJpbmdzLCBudW1iZXJzLCByZWd1bGFyIGV4cHJlc3Npb25zLCBkYXRlcywgYW5kIGJvb2xlYW5zIGFyZSBjb21wYXJlZCBieSB2YWx1ZS5cbiAgICAgIGNhc2UgJ1tvYmplY3QgUmVnRXhwXSc6XG4gICAgICAvLyBSZWdFeHBzIGFyZSBjb2VyY2VkIHRvIHN0cmluZ3MgZm9yIGNvbXBhcmlzb24gKE5vdGU6ICcnICsgL2EvaSA9PT0gJy9hL2knKVxuICAgICAgY2FzZSAnW29iamVjdCBTdHJpbmddJzpcbiAgICAgICAgLy8gUHJpbWl0aXZlcyBhbmQgdGhlaXIgY29ycmVzcG9uZGluZyBvYmplY3Qgd3JhcHBlcnMgYXJlIGVxdWl2YWxlbnQ7IHRodXMsIGBcIjVcImAgaXNcbiAgICAgICAgLy8gZXF1aXZhbGVudCB0byBgbmV3IFN0cmluZyhcIjVcIilgLlxuICAgICAgICByZXR1cm4gJycgKyBhID09PSAnJyArIGI7XG4gICAgICBjYXNlICdbb2JqZWN0IE51bWJlcl0nOlxuICAgICAgICAvLyBgTmFOYHMgYXJlIGVxdWl2YWxlbnQsIGJ1dCBub24tcmVmbGV4aXZlLlxuICAgICAgICAvLyBPYmplY3QoTmFOKSBpcyBlcXVpdmFsZW50IHRvIE5hTi5cbiAgICAgICAgaWYgKCthICE9PSArYSkgcmV0dXJuICtiICE9PSArYjtcbiAgICAgICAgLy8gQW4gYGVnYWxgIGNvbXBhcmlzb24gaXMgcGVyZm9ybWVkIGZvciBvdGhlciBudW1lcmljIHZhbHVlcy5cbiAgICAgICAgcmV0dXJuICthID09PSAwID8gMSAvICthID09PSAxIC8gYiA6ICthID09PSArYjtcbiAgICAgIGNhc2UgJ1tvYmplY3QgRGF0ZV0nOlxuICAgICAgY2FzZSAnW29iamVjdCBCb29sZWFuXSc6XG4gICAgICAgIC8vIENvZXJjZSBkYXRlcyBhbmQgYm9vbGVhbnMgdG8gbnVtZXJpYyBwcmltaXRpdmUgdmFsdWVzLiBEYXRlcyBhcmUgY29tcGFyZWQgYnkgdGhlaXJcbiAgICAgICAgLy8gbWlsbGlzZWNvbmQgcmVwcmVzZW50YXRpb25zLiBOb3RlIHRoYXQgaW52YWxpZCBkYXRlcyB3aXRoIG1pbGxpc2Vjb25kIHJlcHJlc2VudGF0aW9uc1xuICAgICAgICAvLyBvZiBgTmFOYCBhcmUgbm90IGVxdWl2YWxlbnQuXG4gICAgICAgIHJldHVybiArYSA9PT0gK2I7XG4gICAgICBjYXNlICdbb2JqZWN0IFN5bWJvbF0nOlxuICAgICAgICByZXR1cm4gU3ltYm9sUHJvdG8udmFsdWVPZi5jYWxsKGEpID09PSBTeW1ib2xQcm90by52YWx1ZU9mLmNhbGwoYik7XG4gICAgfVxuXG4gICAgdmFyIGFyZUFycmF5cyA9IGNsYXNzTmFtZSA9PT0gJ1tvYmplY3QgQXJyYXldJztcbiAgICBpZiAoIWFyZUFycmF5cykge1xuICAgICAgaWYgKHR5cGVvZiBhICE9ICdvYmplY3QnIHx8IHR5cGVvZiBiICE9ICdvYmplY3QnKSByZXR1cm4gZmFsc2U7XG5cbiAgICAgIC8vIE9iamVjdHMgd2l0aCBkaWZmZXJlbnQgY29uc3RydWN0b3JzIGFyZSBub3QgZXF1aXZhbGVudCwgYnV0IGBPYmplY3RgcyBvciBgQXJyYXlgc1xuICAgICAgLy8gZnJvbSBkaWZmZXJlbnQgZnJhbWVzIGFyZS5cbiAgICAgIHZhciBhQ3RvciA9IGEuY29uc3RydWN0b3IsIGJDdG9yID0gYi5jb25zdHJ1Y3RvcjtcbiAgICAgIGlmIChhQ3RvciAhPT0gYkN0b3IgJiYgIShfLmlzRnVuY3Rpb24oYUN0b3IpICYmIGFDdG9yIGluc3RhbmNlb2YgYUN0b3IgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmlzRnVuY3Rpb24oYkN0b3IpICYmIGJDdG9yIGluc3RhbmNlb2YgYkN0b3IpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICYmICgnY29uc3RydWN0b3InIGluIGEgJiYgJ2NvbnN0cnVjdG9yJyBpbiBiKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICAgIC8vIEFzc3VtZSBlcXVhbGl0eSBmb3IgY3ljbGljIHN0cnVjdHVyZXMuIFRoZSBhbGdvcml0aG0gZm9yIGRldGVjdGluZyBjeWNsaWNcbiAgICAvLyBzdHJ1Y3R1cmVzIGlzIGFkYXB0ZWQgZnJvbSBFUyA1LjEgc2VjdGlvbiAxNS4xMi4zLCBhYnN0cmFjdCBvcGVyYXRpb24gYEpPYC5cblxuICAgIC8vIEluaXRpYWxpemluZyBzdGFjayBvZiB0cmF2ZXJzZWQgb2JqZWN0cy5cbiAgICAvLyBJdCdzIGRvbmUgaGVyZSBzaW5jZSB3ZSBvbmx5IG5lZWQgdGhlbSBmb3Igb2JqZWN0cyBhbmQgYXJyYXlzIGNvbXBhcmlzb24uXG4gICAgYVN0YWNrID0gYVN0YWNrIHx8IFtdO1xuICAgIGJTdGFjayA9IGJTdGFjayB8fCBbXTtcbiAgICB2YXIgbGVuZ3RoID0gYVN0YWNrLmxlbmd0aDtcbiAgICB3aGlsZSAobGVuZ3RoLS0pIHtcbiAgICAgIC8vIExpbmVhciBzZWFyY2guIFBlcmZvcm1hbmNlIGlzIGludmVyc2VseSBwcm9wb3J0aW9uYWwgdG8gdGhlIG51bWJlciBvZlxuICAgICAgLy8gdW5pcXVlIG5lc3RlZCBzdHJ1Y3R1cmVzLlxuICAgICAgaWYgKGFTdGFja1tsZW5ndGhdID09PSBhKSByZXR1cm4gYlN0YWNrW2xlbmd0aF0gPT09IGI7XG4gICAgfVxuXG4gICAgLy8gQWRkIHRoZSBmaXJzdCBvYmplY3QgdG8gdGhlIHN0YWNrIG9mIHRyYXZlcnNlZCBvYmplY3RzLlxuICAgIGFTdGFjay5wdXNoKGEpO1xuICAgIGJTdGFjay5wdXNoKGIpO1xuXG4gICAgLy8gUmVjdXJzaXZlbHkgY29tcGFyZSBvYmplY3RzIGFuZCBhcnJheXMuXG4gICAgaWYgKGFyZUFycmF5cykge1xuICAgICAgLy8gQ29tcGFyZSBhcnJheSBsZW5ndGhzIHRvIGRldGVybWluZSBpZiBhIGRlZXAgY29tcGFyaXNvbiBpcyBuZWNlc3NhcnkuXG4gICAgICBsZW5ndGggPSBhLmxlbmd0aDtcbiAgICAgIGlmIChsZW5ndGggIT09IGIubGVuZ3RoKSByZXR1cm4gZmFsc2U7XG4gICAgICAvLyBEZWVwIGNvbXBhcmUgdGhlIGNvbnRlbnRzLCBpZ25vcmluZyBub24tbnVtZXJpYyBwcm9wZXJ0aWVzLlxuICAgICAgd2hpbGUgKGxlbmd0aC0tKSB7XG4gICAgICAgIGlmICghZXEoYVtsZW5ndGhdLCBiW2xlbmd0aF0sIGFTdGFjaywgYlN0YWNrKSkgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBEZWVwIGNvbXBhcmUgb2JqZWN0cy5cbiAgICAgIHZhciBrZXlzID0gXy5rZXlzKGEpLCBrZXk7XG4gICAgICBsZW5ndGggPSBrZXlzLmxlbmd0aDtcbiAgICAgIC8vIEVuc3VyZSB0aGF0IGJvdGggb2JqZWN0cyBjb250YWluIHRoZSBzYW1lIG51bWJlciBvZiBwcm9wZXJ0aWVzIGJlZm9yZSBjb21wYXJpbmcgZGVlcCBlcXVhbGl0eS5cbiAgICAgIGlmIChfLmtleXMoYikubGVuZ3RoICE9PSBsZW5ndGgpIHJldHVybiBmYWxzZTtcbiAgICAgIHdoaWxlIChsZW5ndGgtLSkge1xuICAgICAgICAvLyBEZWVwIGNvbXBhcmUgZWFjaCBtZW1iZXJcbiAgICAgICAga2V5ID0ga2V5c1tsZW5ndGhdO1xuICAgICAgICBpZiAoIShoYXMoYiwga2V5KSAmJiBlcShhW2tleV0sIGJba2V5XSwgYVN0YWNrLCBiU3RhY2spKSkgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH1cbiAgICAvLyBSZW1vdmUgdGhlIGZpcnN0IG9iamVjdCBmcm9tIHRoZSBzdGFjayBvZiB0cmF2ZXJzZWQgb2JqZWN0cy5cbiAgICBhU3RhY2sucG9wKCk7XG4gICAgYlN0YWNrLnBvcCgpO1xuICAgIHJldHVybiB0cnVlO1xuICB9O1xuXG4gIC8vIFBlcmZvcm0gYSBkZWVwIGNvbXBhcmlzb24gdG8gY2hlY2sgaWYgdHdvIG9iamVjdHMgYXJlIGVxdWFsLlxuICBfLmlzRXF1YWwgPSBmdW5jdGlvbihhLCBiKSB7XG4gICAgcmV0dXJuIGVxKGEsIGIpO1xuICB9O1xuXG4gIC8vIElzIGEgZ2l2ZW4gYXJyYXksIHN0cmluZywgb3Igb2JqZWN0IGVtcHR5P1xuICAvLyBBbiBcImVtcHR5XCIgb2JqZWN0IGhhcyBubyBlbnVtZXJhYmxlIG93bi1wcm9wZXJ0aWVzLlxuICBfLmlzRW1wdHkgPSBmdW5jdGlvbihvYmopIHtcbiAgICBpZiAob2JqID09IG51bGwpIHJldHVybiB0cnVlO1xuICAgIGlmIChpc0FycmF5TGlrZShvYmopICYmIChfLmlzQXJyYXkob2JqKSB8fCBfLmlzU3RyaW5nKG9iaikgfHwgXy5pc0FyZ3VtZW50cyhvYmopKSkgcmV0dXJuIG9iai5sZW5ndGggPT09IDA7XG4gICAgcmV0dXJuIF8ua2V5cyhvYmopLmxlbmd0aCA9PT0gMDtcbiAgfTtcblxuICAvLyBJcyBhIGdpdmVuIHZhbHVlIGEgRE9NIGVsZW1lbnQ/XG4gIF8uaXNFbGVtZW50ID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgcmV0dXJuICEhKG9iaiAmJiBvYmoubm9kZVR5cGUgPT09IDEpO1xuICB9O1xuXG4gIC8vIElzIGEgZ2l2ZW4gdmFsdWUgYW4gYXJyYXk/XG4gIC8vIERlbGVnYXRlcyB0byBFQ01BNSdzIG5hdGl2ZSBBcnJheS5pc0FycmF5XG4gIF8uaXNBcnJheSA9IG5hdGl2ZUlzQXJyYXkgfHwgZnVuY3Rpb24ob2JqKSB7XG4gICAgcmV0dXJuIHRvU3RyaW5nLmNhbGwob2JqKSA9PT0gJ1tvYmplY3QgQXJyYXldJztcbiAgfTtcblxuICAvLyBJcyBhIGdpdmVuIHZhcmlhYmxlIGFuIG9iamVjdD9cbiAgXy5pc09iamVjdCA9IGZ1bmN0aW9uKG9iaikge1xuICAgIHZhciB0eXBlID0gdHlwZW9mIG9iajtcbiAgICByZXR1cm4gdHlwZSA9PT0gJ2Z1bmN0aW9uJyB8fCB0eXBlID09PSAnb2JqZWN0JyAmJiAhIW9iajtcbiAgfTtcblxuICAvLyBBZGQgc29tZSBpc1R5cGUgbWV0aG9kczogaXNBcmd1bWVudHMsIGlzRnVuY3Rpb24sIGlzU3RyaW5nLCBpc051bWJlciwgaXNEYXRlLCBpc1JlZ0V4cCwgaXNFcnJvciwgaXNNYXAsIGlzV2Vha01hcCwgaXNTZXQsIGlzV2Vha1NldC5cbiAgXy5lYWNoKFsnQXJndW1lbnRzJywgJ0Z1bmN0aW9uJywgJ1N0cmluZycsICdOdW1iZXInLCAnRGF0ZScsICdSZWdFeHAnLCAnRXJyb3InLCAnU3ltYm9sJywgJ01hcCcsICdXZWFrTWFwJywgJ1NldCcsICdXZWFrU2V0J10sIGZ1bmN0aW9uKG5hbWUpIHtcbiAgICBfWydpcycgKyBuYW1lXSA9IGZ1bmN0aW9uKG9iaikge1xuICAgICAgcmV0dXJuIHRvU3RyaW5nLmNhbGwob2JqKSA9PT0gJ1tvYmplY3QgJyArIG5hbWUgKyAnXSc7XG4gICAgfTtcbiAgfSk7XG5cbiAgLy8gRGVmaW5lIGEgZmFsbGJhY2sgdmVyc2lvbiBvZiB0aGUgbWV0aG9kIGluIGJyb3dzZXJzIChhaGVtLCBJRSA8IDkpLCB3aGVyZVxuICAvLyB0aGVyZSBpc24ndCBhbnkgaW5zcGVjdGFibGUgXCJBcmd1bWVudHNcIiB0eXBlLlxuICBpZiAoIV8uaXNBcmd1bWVudHMoYXJndW1lbnRzKSkge1xuICAgIF8uaXNBcmd1bWVudHMgPSBmdW5jdGlvbihvYmopIHtcbiAgICAgIHJldHVybiBoYXMob2JqLCAnY2FsbGVlJyk7XG4gICAgfTtcbiAgfVxuXG4gIC8vIE9wdGltaXplIGBpc0Z1bmN0aW9uYCBpZiBhcHByb3ByaWF0ZS4gV29yayBhcm91bmQgc29tZSB0eXBlb2YgYnVncyBpbiBvbGQgdjgsXG4gIC8vIElFIDExICgjMTYyMSksIFNhZmFyaSA4ICgjMTkyOSksIGFuZCBQaGFudG9tSlMgKCMyMjM2KS5cbiAgdmFyIG5vZGVsaXN0ID0gcm9vdC5kb2N1bWVudCAmJiByb290LmRvY3VtZW50LmNoaWxkTm9kZXM7XG4gIGlmICh0eXBlb2YgLy4vICE9ICdmdW5jdGlvbicgJiYgdHlwZW9mIEludDhBcnJheSAhPSAnb2JqZWN0JyAmJiB0eXBlb2Ygbm9kZWxpc3QgIT0gJ2Z1bmN0aW9uJykge1xuICAgIF8uaXNGdW5jdGlvbiA9IGZ1bmN0aW9uKG9iaikge1xuICAgICAgcmV0dXJuIHR5cGVvZiBvYmogPT0gJ2Z1bmN0aW9uJyB8fCBmYWxzZTtcbiAgICB9O1xuICB9XG5cbiAgLy8gSXMgYSBnaXZlbiBvYmplY3QgYSBmaW5pdGUgbnVtYmVyP1xuICBfLmlzRmluaXRlID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgcmV0dXJuICFfLmlzU3ltYm9sKG9iaikgJiYgaXNGaW5pdGUob2JqKSAmJiAhaXNOYU4ocGFyc2VGbG9hdChvYmopKTtcbiAgfTtcblxuICAvLyBJcyB0aGUgZ2l2ZW4gdmFsdWUgYE5hTmA/XG4gIF8uaXNOYU4gPSBmdW5jdGlvbihvYmopIHtcbiAgICByZXR1cm4gXy5pc051bWJlcihvYmopICYmIGlzTmFOKG9iaik7XG4gIH07XG5cbiAgLy8gSXMgYSBnaXZlbiB2YWx1ZSBhIGJvb2xlYW4/XG4gIF8uaXNCb29sZWFuID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgcmV0dXJuIG9iaiA9PT0gdHJ1ZSB8fCBvYmogPT09IGZhbHNlIHx8IHRvU3RyaW5nLmNhbGwob2JqKSA9PT0gJ1tvYmplY3QgQm9vbGVhbl0nO1xuICB9O1xuXG4gIC8vIElzIGEgZ2l2ZW4gdmFsdWUgZXF1YWwgdG8gbnVsbD9cbiAgXy5pc051bGwgPSBmdW5jdGlvbihvYmopIHtcbiAgICByZXR1cm4gb2JqID09PSBudWxsO1xuICB9O1xuXG4gIC8vIElzIGEgZ2l2ZW4gdmFyaWFibGUgdW5kZWZpbmVkP1xuICBfLmlzVW5kZWZpbmVkID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgcmV0dXJuIG9iaiA9PT0gdm9pZCAwO1xuICB9O1xuXG4gIC8vIFNob3J0Y3V0IGZ1bmN0aW9uIGZvciBjaGVja2luZyBpZiBhbiBvYmplY3QgaGFzIGEgZ2l2ZW4gcHJvcGVydHkgZGlyZWN0bHlcbiAgLy8gb24gaXRzZWxmIChpbiBvdGhlciB3b3Jkcywgbm90IG9uIGEgcHJvdG90eXBlKS5cbiAgXy5oYXMgPSBmdW5jdGlvbihvYmosIHBhdGgpIHtcbiAgICBpZiAoIV8uaXNBcnJheShwYXRoKSkge1xuICAgICAgcmV0dXJuIGhhcyhvYmosIHBhdGgpO1xuICAgIH1cbiAgICB2YXIgbGVuZ3RoID0gcGF0aC5sZW5ndGg7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGtleSA9IHBhdGhbaV07XG4gICAgICBpZiAob2JqID09IG51bGwgfHwgIWhhc093blByb3BlcnR5LmNhbGwob2JqLCBrZXkpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIG9iaiA9IG9ialtrZXldO1xuICAgIH1cbiAgICByZXR1cm4gISFsZW5ndGg7XG4gIH07XG5cbiAgLy8gVXRpbGl0eSBGdW5jdGlvbnNcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS1cblxuICAvLyBSdW4gVW5kZXJzY29yZS5qcyBpbiAqbm9Db25mbGljdCogbW9kZSwgcmV0dXJuaW5nIHRoZSBgX2AgdmFyaWFibGUgdG8gaXRzXG4gIC8vIHByZXZpb3VzIG93bmVyLiBSZXR1cm5zIGEgcmVmZXJlbmNlIHRvIHRoZSBVbmRlcnNjb3JlIG9iamVjdC5cbiAgXy5ub0NvbmZsaWN0ID0gZnVuY3Rpb24oKSB7XG4gICAgcm9vdC5fID0gcHJldmlvdXNVbmRlcnNjb3JlO1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIC8vIEtlZXAgdGhlIGlkZW50aXR5IGZ1bmN0aW9uIGFyb3VuZCBmb3IgZGVmYXVsdCBpdGVyYXRlZXMuXG4gIF8uaWRlbnRpdHkgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgIHJldHVybiB2YWx1ZTtcbiAgfTtcblxuICAvLyBQcmVkaWNhdGUtZ2VuZXJhdGluZyBmdW5jdGlvbnMuIE9mdGVuIHVzZWZ1bCBvdXRzaWRlIG9mIFVuZGVyc2NvcmUuXG4gIF8uY29uc3RhbnQgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9O1xuICB9O1xuXG4gIF8ubm9vcCA9IGZ1bmN0aW9uKCl7fTtcblxuICAvLyBDcmVhdGVzIGEgZnVuY3Rpb24gdGhhdCwgd2hlbiBwYXNzZWQgYW4gb2JqZWN0LCB3aWxsIHRyYXZlcnNlIHRoYXQgb2JqZWN04oCZc1xuICAvLyBwcm9wZXJ0aWVzIGRvd24gdGhlIGdpdmVuIGBwYXRoYCwgc3BlY2lmaWVkIGFzIGFuIGFycmF5IG9mIGtleXMgb3IgaW5kZXhlcy5cbiAgXy5wcm9wZXJ0eSA9IGZ1bmN0aW9uKHBhdGgpIHtcbiAgICBpZiAoIV8uaXNBcnJheShwYXRoKSkge1xuICAgICAgcmV0dXJuIHNoYWxsb3dQcm9wZXJ0eShwYXRoKTtcbiAgICB9XG4gICAgcmV0dXJuIGZ1bmN0aW9uKG9iaikge1xuICAgICAgcmV0dXJuIGRlZXBHZXQob2JqLCBwYXRoKTtcbiAgICB9O1xuICB9O1xuXG4gIC8vIEdlbmVyYXRlcyBhIGZ1bmN0aW9uIGZvciBhIGdpdmVuIG9iamVjdCB0aGF0IHJldHVybnMgYSBnaXZlbiBwcm9wZXJ0eS5cbiAgXy5wcm9wZXJ0eU9mID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgaWYgKG9iaiA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24oKXt9O1xuICAgIH1cbiAgICByZXR1cm4gZnVuY3Rpb24ocGF0aCkge1xuICAgICAgcmV0dXJuICFfLmlzQXJyYXkocGF0aCkgPyBvYmpbcGF0aF0gOiBkZWVwR2V0KG9iaiwgcGF0aCk7XG4gICAgfTtcbiAgfTtcblxuICAvLyBSZXR1cm5zIGEgcHJlZGljYXRlIGZvciBjaGVja2luZyB3aGV0aGVyIGFuIG9iamVjdCBoYXMgYSBnaXZlbiBzZXQgb2ZcbiAgLy8gYGtleTp2YWx1ZWAgcGFpcnMuXG4gIF8ubWF0Y2hlciA9IF8ubWF0Y2hlcyA9IGZ1bmN0aW9uKGF0dHJzKSB7XG4gICAgYXR0cnMgPSBfLmV4dGVuZE93bih7fSwgYXR0cnMpO1xuICAgIHJldHVybiBmdW5jdGlvbihvYmopIHtcbiAgICAgIHJldHVybiBfLmlzTWF0Y2gob2JqLCBhdHRycyk7XG4gICAgfTtcbiAgfTtcblxuICAvLyBSdW4gYSBmdW5jdGlvbiAqKm4qKiB0aW1lcy5cbiAgXy50aW1lcyA9IGZ1bmN0aW9uKG4sIGl0ZXJhdGVlLCBjb250ZXh0KSB7XG4gICAgdmFyIGFjY3VtID0gQXJyYXkoTWF0aC5tYXgoMCwgbikpO1xuICAgIGl0ZXJhdGVlID0gb3B0aW1pemVDYihpdGVyYXRlZSwgY29udGV4dCwgMSk7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBuOyBpKyspIGFjY3VtW2ldID0gaXRlcmF0ZWUoaSk7XG4gICAgcmV0dXJuIGFjY3VtO1xuICB9O1xuXG4gIC8vIFJldHVybiBhIHJhbmRvbSBpbnRlZ2VyIGJldHdlZW4gbWluIGFuZCBtYXggKGluY2x1c2l2ZSkuXG4gIF8ucmFuZG9tID0gZnVuY3Rpb24obWluLCBtYXgpIHtcbiAgICBpZiAobWF4ID09IG51bGwpIHtcbiAgICAgIG1heCA9IG1pbjtcbiAgICAgIG1pbiA9IDA7XG4gICAgfVxuICAgIHJldHVybiBtaW4gKyBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpO1xuICB9O1xuXG4gIC8vIEEgKHBvc3NpYmx5IGZhc3Rlcikgd2F5IHRvIGdldCB0aGUgY3VycmVudCB0aW1lc3RhbXAgYXMgYW4gaW50ZWdlci5cbiAgXy5ub3cgPSBEYXRlLm5vdyB8fCBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gIH07XG5cbiAgLy8gTGlzdCBvZiBIVE1MIGVudGl0aWVzIGZvciBlc2NhcGluZy5cbiAgdmFyIGVzY2FwZU1hcCA9IHtcbiAgICAnJic6ICcmYW1wOycsXG4gICAgJzwnOiAnJmx0OycsXG4gICAgJz4nOiAnJmd0OycsXG4gICAgJ1wiJzogJyZxdW90OycsXG4gICAgXCInXCI6ICcmI3gyNzsnLFxuICAgICdgJzogJyYjeDYwOydcbiAgfTtcbiAgdmFyIHVuZXNjYXBlTWFwID0gXy5pbnZlcnQoZXNjYXBlTWFwKTtcblxuICAvLyBGdW5jdGlvbnMgZm9yIGVzY2FwaW5nIGFuZCB1bmVzY2FwaW5nIHN0cmluZ3MgdG8vZnJvbSBIVE1MIGludGVycG9sYXRpb24uXG4gIHZhciBjcmVhdGVFc2NhcGVyID0gZnVuY3Rpb24obWFwKSB7XG4gICAgdmFyIGVzY2FwZXIgPSBmdW5jdGlvbihtYXRjaCkge1xuICAgICAgcmV0dXJuIG1hcFttYXRjaF07XG4gICAgfTtcbiAgICAvLyBSZWdleGVzIGZvciBpZGVudGlmeWluZyBhIGtleSB0aGF0IG5lZWRzIHRvIGJlIGVzY2FwZWQuXG4gICAgdmFyIHNvdXJjZSA9ICcoPzonICsgXy5rZXlzKG1hcCkuam9pbignfCcpICsgJyknO1xuICAgIHZhciB0ZXN0UmVnZXhwID0gUmVnRXhwKHNvdXJjZSk7XG4gICAgdmFyIHJlcGxhY2VSZWdleHAgPSBSZWdFeHAoc291cmNlLCAnZycpO1xuICAgIHJldHVybiBmdW5jdGlvbihzdHJpbmcpIHtcbiAgICAgIHN0cmluZyA9IHN0cmluZyA9PSBudWxsID8gJycgOiAnJyArIHN0cmluZztcbiAgICAgIHJldHVybiB0ZXN0UmVnZXhwLnRlc3Qoc3RyaW5nKSA/IHN0cmluZy5yZXBsYWNlKHJlcGxhY2VSZWdleHAsIGVzY2FwZXIpIDogc3RyaW5nO1xuICAgIH07XG4gIH07XG4gIF8uZXNjYXBlID0gY3JlYXRlRXNjYXBlcihlc2NhcGVNYXApO1xuICBfLnVuZXNjYXBlID0gY3JlYXRlRXNjYXBlcih1bmVzY2FwZU1hcCk7XG5cbiAgLy8gVHJhdmVyc2VzIHRoZSBjaGlsZHJlbiBvZiBgb2JqYCBhbG9uZyBgcGF0aGAuIElmIGEgY2hpbGQgaXMgYSBmdW5jdGlvbiwgaXRcbiAgLy8gaXMgaW52b2tlZCB3aXRoIGl0cyBwYXJlbnQgYXMgY29udGV4dC4gUmV0dXJucyB0aGUgdmFsdWUgb2YgdGhlIGZpbmFsXG4gIC8vIGNoaWxkLCBvciBgZmFsbGJhY2tgIGlmIGFueSBjaGlsZCBpcyB1bmRlZmluZWQuXG4gIF8ucmVzdWx0ID0gZnVuY3Rpb24ob2JqLCBwYXRoLCBmYWxsYmFjaykge1xuICAgIGlmICghXy5pc0FycmF5KHBhdGgpKSBwYXRoID0gW3BhdGhdO1xuICAgIHZhciBsZW5ndGggPSBwYXRoLmxlbmd0aDtcbiAgICBpZiAoIWxlbmd0aCkge1xuICAgICAgcmV0dXJuIF8uaXNGdW5jdGlvbihmYWxsYmFjaykgPyBmYWxsYmFjay5jYWxsKG9iaikgOiBmYWxsYmFjaztcbiAgICB9XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHByb3AgPSBvYmogPT0gbnVsbCA/IHZvaWQgMCA6IG9ialtwYXRoW2ldXTtcbiAgICAgIGlmIChwcm9wID09PSB2b2lkIDApIHtcbiAgICAgICAgcHJvcCA9IGZhbGxiYWNrO1xuICAgICAgICBpID0gbGVuZ3RoOyAvLyBFbnN1cmUgd2UgZG9uJ3QgY29udGludWUgaXRlcmF0aW5nLlxuICAgICAgfVxuICAgICAgb2JqID0gXy5pc0Z1bmN0aW9uKHByb3ApID8gcHJvcC5jYWxsKG9iaikgOiBwcm9wO1xuICAgIH1cbiAgICByZXR1cm4gb2JqO1xuICB9O1xuXG4gIC8vIEdlbmVyYXRlIGEgdW5pcXVlIGludGVnZXIgaWQgKHVuaXF1ZSB3aXRoaW4gdGhlIGVudGlyZSBjbGllbnQgc2Vzc2lvbikuXG4gIC8vIFVzZWZ1bCBmb3IgdGVtcG9yYXJ5IERPTSBpZHMuXG4gIHZhciBpZENvdW50ZXIgPSAwO1xuICBfLnVuaXF1ZUlkID0gZnVuY3Rpb24ocHJlZml4KSB7XG4gICAgdmFyIGlkID0gKytpZENvdW50ZXIgKyAnJztcbiAgICByZXR1cm4gcHJlZml4ID8gcHJlZml4ICsgaWQgOiBpZDtcbiAgfTtcblxuICAvLyBCeSBkZWZhdWx0LCBVbmRlcnNjb3JlIHVzZXMgRVJCLXN0eWxlIHRlbXBsYXRlIGRlbGltaXRlcnMsIGNoYW5nZSB0aGVcbiAgLy8gZm9sbG93aW5nIHRlbXBsYXRlIHNldHRpbmdzIHRvIHVzZSBhbHRlcm5hdGl2ZSBkZWxpbWl0ZXJzLlxuICBfLnRlbXBsYXRlU2V0dGluZ3MgPSB7XG4gICAgZXZhbHVhdGU6IC88JShbXFxzXFxTXSs/KSU+L2csXG4gICAgaW50ZXJwb2xhdGU6IC88JT0oW1xcc1xcU10rPyklPi9nLFxuICAgIGVzY2FwZTogLzwlLShbXFxzXFxTXSs/KSU+L2dcbiAgfTtcblxuICAvLyBXaGVuIGN1c3RvbWl6aW5nIGB0ZW1wbGF0ZVNldHRpbmdzYCwgaWYgeW91IGRvbid0IHdhbnQgdG8gZGVmaW5lIGFuXG4gIC8vIGludGVycG9sYXRpb24sIGV2YWx1YXRpb24gb3IgZXNjYXBpbmcgcmVnZXgsIHdlIG5lZWQgb25lIHRoYXQgaXNcbiAgLy8gZ3VhcmFudGVlZCBub3QgdG8gbWF0Y2guXG4gIHZhciBub01hdGNoID0gLyguKV4vO1xuXG4gIC8vIENlcnRhaW4gY2hhcmFjdGVycyBuZWVkIHRvIGJlIGVzY2FwZWQgc28gdGhhdCB0aGV5IGNhbiBiZSBwdXQgaW50byBhXG4gIC8vIHN0cmluZyBsaXRlcmFsLlxuICB2YXIgZXNjYXBlcyA9IHtcbiAgICBcIidcIjogXCInXCIsXG4gICAgJ1xcXFwnOiAnXFxcXCcsXG4gICAgJ1xccic6ICdyJyxcbiAgICAnXFxuJzogJ24nLFxuICAgICdcXHUyMDI4JzogJ3UyMDI4JyxcbiAgICAnXFx1MjAyOSc6ICd1MjAyOSdcbiAgfTtcblxuICB2YXIgZXNjYXBlUmVnRXhwID0gL1xcXFx8J3xcXHJ8XFxufFxcdTIwMjh8XFx1MjAyOS9nO1xuXG4gIHZhciBlc2NhcGVDaGFyID0gZnVuY3Rpb24obWF0Y2gpIHtcbiAgICByZXR1cm4gJ1xcXFwnICsgZXNjYXBlc1ttYXRjaF07XG4gIH07XG5cbiAgLy8gSmF2YVNjcmlwdCBtaWNyby10ZW1wbGF0aW5nLCBzaW1pbGFyIHRvIEpvaG4gUmVzaWcncyBpbXBsZW1lbnRhdGlvbi5cbiAgLy8gVW5kZXJzY29yZSB0ZW1wbGF0aW5nIGhhbmRsZXMgYXJiaXRyYXJ5IGRlbGltaXRlcnMsIHByZXNlcnZlcyB3aGl0ZXNwYWNlLFxuICAvLyBhbmQgY29ycmVjdGx5IGVzY2FwZXMgcXVvdGVzIHdpdGhpbiBpbnRlcnBvbGF0ZWQgY29kZS5cbiAgLy8gTkI6IGBvbGRTZXR0aW5nc2Agb25seSBleGlzdHMgZm9yIGJhY2t3YXJkcyBjb21wYXRpYmlsaXR5LlxuICBfLnRlbXBsYXRlID0gZnVuY3Rpb24odGV4dCwgc2V0dGluZ3MsIG9sZFNldHRpbmdzKSB7XG4gICAgaWYgKCFzZXR0aW5ncyAmJiBvbGRTZXR0aW5ncykgc2V0dGluZ3MgPSBvbGRTZXR0aW5ncztcbiAgICBzZXR0aW5ncyA9IF8uZGVmYXVsdHMoe30sIHNldHRpbmdzLCBfLnRlbXBsYXRlU2V0dGluZ3MpO1xuXG4gICAgLy8gQ29tYmluZSBkZWxpbWl0ZXJzIGludG8gb25lIHJlZ3VsYXIgZXhwcmVzc2lvbiB2aWEgYWx0ZXJuYXRpb24uXG4gICAgdmFyIG1hdGNoZXIgPSBSZWdFeHAoW1xuICAgICAgKHNldHRpbmdzLmVzY2FwZSB8fCBub01hdGNoKS5zb3VyY2UsXG4gICAgICAoc2V0dGluZ3MuaW50ZXJwb2xhdGUgfHwgbm9NYXRjaCkuc291cmNlLFxuICAgICAgKHNldHRpbmdzLmV2YWx1YXRlIHx8IG5vTWF0Y2gpLnNvdXJjZVxuICAgIF0uam9pbignfCcpICsgJ3wkJywgJ2cnKTtcblxuICAgIC8vIENvbXBpbGUgdGhlIHRlbXBsYXRlIHNvdXJjZSwgZXNjYXBpbmcgc3RyaW5nIGxpdGVyYWxzIGFwcHJvcHJpYXRlbHkuXG4gICAgdmFyIGluZGV4ID0gMDtcbiAgICB2YXIgc291cmNlID0gXCJfX3ArPSdcIjtcbiAgICB0ZXh0LnJlcGxhY2UobWF0Y2hlciwgZnVuY3Rpb24obWF0Y2gsIGVzY2FwZSwgaW50ZXJwb2xhdGUsIGV2YWx1YXRlLCBvZmZzZXQpIHtcbiAgICAgIHNvdXJjZSArPSB0ZXh0LnNsaWNlKGluZGV4LCBvZmZzZXQpLnJlcGxhY2UoZXNjYXBlUmVnRXhwLCBlc2NhcGVDaGFyKTtcbiAgICAgIGluZGV4ID0gb2Zmc2V0ICsgbWF0Y2gubGVuZ3RoO1xuXG4gICAgICBpZiAoZXNjYXBlKSB7XG4gICAgICAgIHNvdXJjZSArPSBcIicrXFxuKChfX3Q9KFwiICsgZXNjYXBlICsgXCIpKT09bnVsbD8nJzpfLmVzY2FwZShfX3QpKStcXG4nXCI7XG4gICAgICB9IGVsc2UgaWYgKGludGVycG9sYXRlKSB7XG4gICAgICAgIHNvdXJjZSArPSBcIicrXFxuKChfX3Q9KFwiICsgaW50ZXJwb2xhdGUgKyBcIikpPT1udWxsPycnOl9fdCkrXFxuJ1wiO1xuICAgICAgfSBlbHNlIGlmIChldmFsdWF0ZSkge1xuICAgICAgICBzb3VyY2UgKz0gXCInO1xcblwiICsgZXZhbHVhdGUgKyBcIlxcbl9fcCs9J1wiO1xuICAgICAgfVxuXG4gICAgICAvLyBBZG9iZSBWTXMgbmVlZCB0aGUgbWF0Y2ggcmV0dXJuZWQgdG8gcHJvZHVjZSB0aGUgY29ycmVjdCBvZmZzZXQuXG4gICAgICByZXR1cm4gbWF0Y2g7XG4gICAgfSk7XG4gICAgc291cmNlICs9IFwiJztcXG5cIjtcblxuICAgIC8vIElmIGEgdmFyaWFibGUgaXMgbm90IHNwZWNpZmllZCwgcGxhY2UgZGF0YSB2YWx1ZXMgaW4gbG9jYWwgc2NvcGUuXG4gICAgaWYgKCFzZXR0aW5ncy52YXJpYWJsZSkgc291cmNlID0gJ3dpdGgob2JqfHx7fSl7XFxuJyArIHNvdXJjZSArICd9XFxuJztcblxuICAgIHNvdXJjZSA9IFwidmFyIF9fdCxfX3A9JycsX19qPUFycmF5LnByb3RvdHlwZS5qb2luLFwiICtcbiAgICAgIFwicHJpbnQ9ZnVuY3Rpb24oKXtfX3ArPV9fai5jYWxsKGFyZ3VtZW50cywnJyk7fTtcXG5cIiArXG4gICAgICBzb3VyY2UgKyAncmV0dXJuIF9fcDtcXG4nO1xuXG4gICAgdmFyIHJlbmRlcjtcbiAgICB0cnkge1xuICAgICAgcmVuZGVyID0gbmV3IEZ1bmN0aW9uKHNldHRpbmdzLnZhcmlhYmxlIHx8ICdvYmonLCAnXycsIHNvdXJjZSk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgZS5zb3VyY2UgPSBzb3VyY2U7XG4gICAgICB0aHJvdyBlO1xuICAgIH1cblxuICAgIHZhciB0ZW1wbGF0ZSA9IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgIHJldHVybiByZW5kZXIuY2FsbCh0aGlzLCBkYXRhLCBfKTtcbiAgICB9O1xuXG4gICAgLy8gUHJvdmlkZSB0aGUgY29tcGlsZWQgc291cmNlIGFzIGEgY29udmVuaWVuY2UgZm9yIHByZWNvbXBpbGF0aW9uLlxuICAgIHZhciBhcmd1bWVudCA9IHNldHRpbmdzLnZhcmlhYmxlIHx8ICdvYmonO1xuICAgIHRlbXBsYXRlLnNvdXJjZSA9ICdmdW5jdGlvbignICsgYXJndW1lbnQgKyAnKXtcXG4nICsgc291cmNlICsgJ30nO1xuXG4gICAgcmV0dXJuIHRlbXBsYXRlO1xuICB9O1xuXG4gIC8vIEFkZCBhIFwiY2hhaW5cIiBmdW5jdGlvbi4gU3RhcnQgY2hhaW5pbmcgYSB3cmFwcGVkIFVuZGVyc2NvcmUgb2JqZWN0LlxuICBfLmNoYWluID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgdmFyIGluc3RhbmNlID0gXyhvYmopO1xuICAgIGluc3RhbmNlLl9jaGFpbiA9IHRydWU7XG4gICAgcmV0dXJuIGluc3RhbmNlO1xuICB9O1xuXG4gIC8vIE9PUFxuICAvLyAtLS0tLS0tLS0tLS0tLS1cbiAgLy8gSWYgVW5kZXJzY29yZSBpcyBjYWxsZWQgYXMgYSBmdW5jdGlvbiwgaXQgcmV0dXJucyBhIHdyYXBwZWQgb2JqZWN0IHRoYXRcbiAgLy8gY2FuIGJlIHVzZWQgT08tc3R5bGUuIFRoaXMgd3JhcHBlciBob2xkcyBhbHRlcmVkIHZlcnNpb25zIG9mIGFsbCB0aGVcbiAgLy8gdW5kZXJzY29yZSBmdW5jdGlvbnMuIFdyYXBwZWQgb2JqZWN0cyBtYXkgYmUgY2hhaW5lZC5cblxuICAvLyBIZWxwZXIgZnVuY3Rpb24gdG8gY29udGludWUgY2hhaW5pbmcgaW50ZXJtZWRpYXRlIHJlc3VsdHMuXG4gIHZhciBjaGFpblJlc3VsdCA9IGZ1bmN0aW9uKGluc3RhbmNlLCBvYmopIHtcbiAgICByZXR1cm4gaW5zdGFuY2UuX2NoYWluID8gXyhvYmopLmNoYWluKCkgOiBvYmo7XG4gIH07XG5cbiAgLy8gQWRkIHlvdXIgb3duIGN1c3RvbSBmdW5jdGlvbnMgdG8gdGhlIFVuZGVyc2NvcmUgb2JqZWN0LlxuICBfLm1peGluID0gZnVuY3Rpb24ob2JqKSB7XG4gICAgXy5lYWNoKF8uZnVuY3Rpb25zKG9iaiksIGZ1bmN0aW9uKG5hbWUpIHtcbiAgICAgIHZhciBmdW5jID0gX1tuYW1lXSA9IG9ialtuYW1lXTtcbiAgICAgIF8ucHJvdG90eXBlW25hbWVdID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBhcmdzID0gW3RoaXMuX3dyYXBwZWRdO1xuICAgICAgICBwdXNoLmFwcGx5KGFyZ3MsIGFyZ3VtZW50cyk7XG4gICAgICAgIHJldHVybiBjaGFpblJlc3VsdCh0aGlzLCBmdW5jLmFwcGx5KF8sIGFyZ3MpKTtcbiAgICAgIH07XG4gICAgfSk7XG4gICAgcmV0dXJuIF87XG4gIH07XG5cbiAgLy8gQWRkIGFsbCBvZiB0aGUgVW5kZXJzY29yZSBmdW5jdGlvbnMgdG8gdGhlIHdyYXBwZXIgb2JqZWN0LlxuICBfLm1peGluKF8pO1xuXG4gIC8vIEFkZCBhbGwgbXV0YXRvciBBcnJheSBmdW5jdGlvbnMgdG8gdGhlIHdyYXBwZXIuXG4gIF8uZWFjaChbJ3BvcCcsICdwdXNoJywgJ3JldmVyc2UnLCAnc2hpZnQnLCAnc29ydCcsICdzcGxpY2UnLCAndW5zaGlmdCddLCBmdW5jdGlvbihuYW1lKSB7XG4gICAgdmFyIG1ldGhvZCA9IEFycmF5UHJvdG9bbmFtZV07XG4gICAgXy5wcm90b3R5cGVbbmFtZV0gPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBvYmogPSB0aGlzLl93cmFwcGVkO1xuICAgICAgbWV0aG9kLmFwcGx5KG9iaiwgYXJndW1lbnRzKTtcbiAgICAgIGlmICgobmFtZSA9PT0gJ3NoaWZ0JyB8fCBuYW1lID09PSAnc3BsaWNlJykgJiYgb2JqLmxlbmd0aCA9PT0gMCkgZGVsZXRlIG9ialswXTtcbiAgICAgIHJldHVybiBjaGFpblJlc3VsdCh0aGlzLCBvYmopO1xuICAgIH07XG4gIH0pO1xuXG4gIC8vIEFkZCBhbGwgYWNjZXNzb3IgQXJyYXkgZnVuY3Rpb25zIHRvIHRoZSB3cmFwcGVyLlxuICBfLmVhY2goWydjb25jYXQnLCAnam9pbicsICdzbGljZSddLCBmdW5jdGlvbihuYW1lKSB7XG4gICAgdmFyIG1ldGhvZCA9IEFycmF5UHJvdG9bbmFtZV07XG4gICAgXy5wcm90b3R5cGVbbmFtZV0gPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBjaGFpblJlc3VsdCh0aGlzLCBtZXRob2QuYXBwbHkodGhpcy5fd3JhcHBlZCwgYXJndW1lbnRzKSk7XG4gICAgfTtcbiAgfSk7XG5cbiAgLy8gRXh0cmFjdHMgdGhlIHJlc3VsdCBmcm9tIGEgd3JhcHBlZCBhbmQgY2hhaW5lZCBvYmplY3QuXG4gIF8ucHJvdG90eXBlLnZhbHVlID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHRoaXMuX3dyYXBwZWQ7XG4gIH07XG5cbiAgLy8gUHJvdmlkZSB1bndyYXBwaW5nIHByb3h5IGZvciBzb21lIG1ldGhvZHMgdXNlZCBpbiBlbmdpbmUgb3BlcmF0aW9uc1xuICAvLyBzdWNoIGFzIGFyaXRobWV0aWMgYW5kIEpTT04gc3RyaW5naWZpY2F0aW9uLlxuICBfLnByb3RvdHlwZS52YWx1ZU9mID0gXy5wcm90b3R5cGUudG9KU09OID0gXy5wcm90b3R5cGUudmFsdWU7XG5cbiAgXy5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gU3RyaW5nKHRoaXMuX3dyYXBwZWQpO1xuICB9O1xuXG4gIC8vIEFNRCByZWdpc3RyYXRpb24gaGFwcGVucyBhdCB0aGUgZW5kIGZvciBjb21wYXRpYmlsaXR5IHdpdGggQU1EIGxvYWRlcnNcbiAgLy8gdGhhdCBtYXkgbm90IGVuZm9yY2UgbmV4dC10dXJuIHNlbWFudGljcyBvbiBtb2R1bGVzLiBFdmVuIHRob3VnaCBnZW5lcmFsXG4gIC8vIHByYWN0aWNlIGZvciBBTUQgcmVnaXN0cmF0aW9uIGlzIHRvIGJlIGFub255bW91cywgdW5kZXJzY29yZSByZWdpc3RlcnNcbiAgLy8gYXMgYSBuYW1lZCBtb2R1bGUgYmVjYXVzZSwgbGlrZSBqUXVlcnksIGl0IGlzIGEgYmFzZSBsaWJyYXJ5IHRoYXQgaXNcbiAgLy8gcG9wdWxhciBlbm91Z2ggdG8gYmUgYnVuZGxlZCBpbiBhIHRoaXJkIHBhcnR5IGxpYiwgYnV0IG5vdCBiZSBwYXJ0IG9mXG4gIC8vIGFuIEFNRCBsb2FkIHJlcXVlc3QuIFRob3NlIGNhc2VzIGNvdWxkIGdlbmVyYXRlIGFuIGVycm9yIHdoZW4gYW5cbiAgLy8gYW5vbnltb3VzIGRlZmluZSgpIGlzIGNhbGxlZCBvdXRzaWRlIG9mIGEgbG9hZGVyIHJlcXVlc3QuXG4gIGlmICh0eXBlb2YgZGVmaW5lID09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgIGRlZmluZSgndW5kZXJzY29yZScsIFtdLCBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBfO1xuICAgIH0pO1xuICB9XG59KCkpO1xuIl19
