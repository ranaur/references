#!/usr/bin/env node

var argv = require('minimist')(process.argv.slice(2));

if(argv["h"] != undefined || argv["_"].length != 2) {
    console.log('convert2md input.html output.md');
}

input = argv["_"][0];
output = argv["_"][1];

//console.dir(input);
//console.dir(output);
//console.dir(argv);

var fs = require('fs');

try {  
    var data = fs.readFileSync(input, 'utf8');
} catch(e) {
    console.log('Error reading input:', e.stack);
    process.exit(1);

}

// For Node.js
var TurndownService = require('turndown');
var turndownPluginGfm = require('turndown-plugin-gfm');
var gfm = turndownPluginGfm.gfm;
var converter = new TurndownService()
    .remove(['script', 'style', 'head', 'svg'])
;
converter.use(gfm);

outdata = converter.turndown(data);

try {  
    fs.writeFileSync(output, outdata);
} catch(e) {
    console.log('Error writing output:', e.stack);
    process.exit(1);
} 

process.exit(0);
