function download(data, filename, mimetype="application/octet-stream") {
	var blob = new Blob([data], {type: mimetype});
    var url = URL.createObjectURL(blob);
    chrome.downloads.download({
      url: url, 
	  filename: filename,
	  conflictAction: "uniquify",
	  saveAs: false
	});
}

function sendMessage(method, func) {
    chrome.tabs.query({
			active:true
			, currentWindow: true
			//, windowId: chrome.windows.WINDOW_ID_CURRENT
        }, 
        function(tabs) {
			if(tabs.length > 0) {
				chrome.tabs.sendMessage(
					tabs[0].id, 
					{method: method,
					url: tabs[0].url}, 
					func
				);
			}
        }
    );
}

function addElement(element, addInBody = true) {
	element.onload = function() {
		this.remove();
	};
	if(addInBody) {
		(document.body || document.documentElement).appendChild(element);
	} else {
		(document.head || document.documentElement).appendChild(element);
	}
}

function addElementDictionary(nameDotClass, attributes = {}, addInBody = true) {
	var nameClass = nameDotClass.split(".");
	var name = nameClass[0];
	var elementClass = nameClass[1];
	var element = document.createElement(name);
	
	if(elementClass != null) {
		element.className = elementClass;
	}

	for (var key in attributes) {
		element.setAttribute(key, attributes[key]);
	}

	addElement(element, addInBody);
}

function runScript(script) {
	var s = document.createElement('script');
	s.src = chrome.extension.getURL(script);
	addElement(s, false)
}

function toMD(html) {
	//var text = html2md(html);
	
	//var converter = new showdown.Converter();
	//converter.setOption('optionKey', 'value');
	//var text = converter.makeMarkdown(html);

	var converter = new TurndownService();
	var gfm = turndownPluginGfm.gfm;
	var converter = new TurndownService()
		.remove(['script', 'style', 'head', 'svg'])
	;
	converter.use(gfm);
	
	return converter.turndown(html);
}

runScript("lib/turndown.js");
runScript("lib/turndown-plugin-gfm.js");

function getClipboard() {
	addElementDictionary("textarea", {id: "clipArea"});
	var result = null;
	var textarea = document.getElementById('clipArea');
	textarea.value = '';
	textarea.select();

	if (document.execCommand('paste')) {
			result = textarea.value;
	} else {
			console.error('failed to get clipboard content');
	}

	textarea.value = '';
	return result;
}

function setClipboard(value) {
	addElementDictionary("textarea", {id: "clipArea"});
	var text = document.getElementById('clipArea'); 
	text.innerHTML = value;

	text.value = value;
	text.select();

	if (document.execCommand('copy')) {
			result = true;
	} else {
			console.error('failed to get clipboard content');
	}

	//text.value = '';
	return result;
}

function saveResponse(response) {
	if (typeof response === "undefined") {
		console.log("undefined response");
		return;
	}

	if(response.result != "OK") {
		console.log("Error: " + response.error);
		return false;
	}

	if(!("data" in response)) {
		console.log("empty response");
		return;
	}

	var mdData = "---\n";
	for(key in response) {
		if(key != "data" && key != "result" && key != "meta") {
			mdData += key + ": " + response[key] + "\n";
		}
	}
	for(element in response.meta) {
		if(element in ['author', 'keywords', 'description']) {
			mdData += element + ": " + response.meta[element] + "\n";
		}
	};
	mdData += "---\n";
	mdData += toMD(response.data);

	chrome.storage.sync.get(['options'], function(result) {
		switch(result.options.output) {
			case "output-clipboard":
				setClipboard(mdData);
				break;
			case "output-download":
				var url =  new URL(response.url).pathname;
				url = url.replace(/\/$/, "");
				var filename = url.substring(url.lastIndexOf('/')+1);
				filename = filename.replace(/\.[^/.]+$/, "");
				download(mdData, filename + ".md", "text/markdown; charset=UTF-8");
				break;
			case "output-googledrive":
				alert("Not implemented");
				break;
			default:
				console.log("invalid option");
		}
		
	});
	return true;
}

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
	if (typeof request === "undefined") {
		sendResponse({result:"ERROR", error:"undefined request"});
		return;
	}

	if(!("method" in request)) {
		sendResponse({result:"ERROR", error:"request without method"});
		return;
	}

	var response = {result:"EMPTY", error:"unhandled method"};
	switch (request.method) {
		case "copy-Selection":
			sendMessage("getSelection", saveResponse);
			response = {result:"OK"};
			break;
		case "copy-Element":
			sendMessage("getSelectedElement", saveResponse);
			response = {result:"OK"};
			break;
		case "copy-Document":
			sendMessage("getDocument", saveResponse);
			response = {result:"OK"};
			break;
		default:
			console.log("unknown method: " + request);
			response = {result:"ERROR", error:"unknown method: " + request};
	}
	sendResponse(response);
});

chrome.commands.onCommand.addListener(function(command) {
	switch(command) {
		case "copy-Selection":
			sendMessage("getSelection", saveResponse);
			break;
		case "copy-Element":
			sendMessage("getSelectedElement", saveResponse);
			break;
		case "copy-Document":
			sendMessage("getDocument", saveResponse);
			break;
		default:
			console.log("Invalid command: " + command);
	}
});