var manifestData = chrome.runtime.getManifest();
software_version = manifestData.name + " " + manifestData.version

// gets all metadata of the page
function getMetadata() {
	var res = {};

	const metas = document.getElementsByTagName('meta');
	for (let i = 0; i < metas.length; i++) {
		if (metas[i].getAttribute('name') !== null) {
			res[metas[i].getAttribute('name')] = metas[i].getAttribute('content')
		}
	}
	return res;
}

function makeMessage(method, html) {
	var now = new Date();
	var lastModified = new Date(window.document.lastModified);
	return {
		result: "OK",
		url: window.location.href,
		docurl: window.document.url,
		title: window.document.title,
		lastModified: lastModified.toISOString(),
		dateCaptured: now.toISOString(),
		software: software_version,
		capture: method,
		meta: getMetadata(),
		data: html
	};
}

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
	if(window.location.href == request.url) {
		switch (request.method) {
			case "getSelection":
				if (window.getSelection()+"" != "") {
					var fragment = window.getSelection().getRangeAt(0).cloneRange().cloneContents();
					var html = $('<div>').append(fragment).html();
					sendResponse(makeMessage("selection", html));
				} else {
					sendResponse({result: "ERROR", error: "no selection"});
				}
				break;
			case "getDocument":
				var html = $('body').html();
				sendResponse(makeMessage("document", html));
				break;
			case "getSelectedElement":
				if (window.getSelection().rangeCount > 0) {
				var fragment = window.getSelection().getRangeAt(0).commonAncestorContainer.cloneNode(true);
					var html = $('<div>').append(fragment).html();
					sendResponse(makeMessage("element", html));
				} else {
					sendResponse({result: "ERROR", error: "no selection"});
				}
				break;
			default:		
				sendResponse({result: "ERROR", error: "invalid command"}); // snub them.
		}
	} else {
		//console.log("Skipped " + request.method + " request " + request.URL + " != " + window.location.href);
	}
});
