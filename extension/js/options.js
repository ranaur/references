$( function() {
  $( "#tabs" ).tabs();
} );
$( function() {
    $( ".controlgroup" ).controlgroup()
    $( ".controlgroup-vertical" ).controlgroup({
      "direction": "vertical"
    });
} );
$( function() {
    $( ".output-checkboxradio" ).checkboxradio();
});

function saveOptions() {
    options = {
        output: $('input[name=output]:checked')[0].id,
    };
    console.log(options);
    chrome.storage.sync.set({options: options}, function() {
        console.log('Options saved');
    });
}

function loadOptions() {
    chrome.storage.sync.get(['options'], function(result) {
        console.log('Options loaded');
        //console.log(result.options.output);
        $('#'+result.options.output).attr( "checked", true );
        $( ".output-checkboxradio" ).checkboxradio( "refresh" );
//        console.log('Output = ' + result.output );
//        $('input[id='+result.output+']').checked();
    });
}

$( function() {
    $( "#save" ).button();
    $( "#save" ).click( function( event ) {
        console.log("Saving ...");
        event.preventDefault();
        saveOptions();
    } );
} );

loadOptions();
