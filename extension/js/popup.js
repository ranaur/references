function sendMessage(method) {
    var request = {method: method};
    chrome.runtime.sendMessage(
            request,
            function(response) {
                    console.log(response);
            }
    );
}

$( function() {
    $( ".widget, .widget button" ).button();
    
    $( "button" ).click( function( event ) {
      event.preventDefault();
    } );
  } );

$(function(){
    $('#copySelection').click(function() {
        sendMessage("copy-Selection");
    });
});
$(function(){
    $('#copyDocument').click(function() {
        sendMessage("copy-Document");
    });
});
$(function(){
    $('#copySelectedElement').click(function() {
        sendMessage("copy-Element");
    });
});