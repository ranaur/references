/*
 * html2md
 * (c) 2014 Beneath the Ink, Inc.
 * MIT License
 * Version 0.1.1
 */

(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.html2md = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * # HTML2MD
 * HTML2MD attempts to convert HTML into Markdown by reducing an HTML document into simple, Markdown-compatible parts. This library is compatible with both browsers and Node.js.
 *
 * To use, pass a string of HTML to the function.
 * 
 * ```javascript
 * var markdown = html2md("<h1>Hello World</h1>");
 * console.log(markdown); // -> # Hello World
 * ```
 */

var _ = require("underscore"),
	Entities = require("special-entities"),
	DOMUtils = require("bti-dom-utils");

function html2md(doc, options) {
	return html2md.toMarkdown(html2md.parse(doc, options));
}

module.exports = html2md;

var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;

var block_elements = [ "article", "aside", "blockquote", "body", "button", "canvas", "caption", "col", "colgroup", "dd", "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", "li", "map", "object", "ol", "output", "p", "pre", "progress", "section", "table", "tbody", "textarea", "tfoot", "th", "thead", "tr", "ul", "video" ];

var empty_tags = ([ "hr", "br", "img", "video", "audio" ]).join(", ");

var markdown_block_tags = [ "blockquote", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "li", "pre", "p" ];

var markdown_empty_blocks = [ "hr" ];

var markdown_inline_tags = {
	"b, strong": "bold",
	"i, em": "italic",
	"code": "code"
};

var markdown_syntax = {
	hr:         "- - -",
	br:         "  \n",
	h1:         "# ",
	h2:         "## ",
	h3:         "### ",
	h4:         "#### ",
	h5:         "##### ",
	h6:         "###### ",
	ul:         "* ",
	ol:         "1. ",
	blockquote: "> ",
	pre:        "  ",
	p:          "",
	bold:       "**",
	italic:     "_",
	code:       "`"
}

/**
 * ## parse()
 *
 * This is where the magic happens. This method takes a string of HTML (or an HTML Document) and returns a Markdown abstract syntax tree.
 * 
 * #### Arguments
 * 
 * - **html** _string | Document_ - A string of HTML or a Document instance provided by the DOM.
 * - **options** _object; optional_ - An object of options.
 *   - **options.window** _object_ - The window object to use while parsing. This is to gain access to some global methods needed while parsing. Usually this is not needed because its value can be inferred.
 */
html2md.parse = function(doc, options) {
	options = options || {};

	if (typeof doc === "string") {
		doc = html2md.toDOM(doc);
	}

	var win = options.window ? options.window :
		typeof window !== "undefined" ? window :
		doc.parentWindow || doc.defaultView;

	if (win == null) {
		throw new Error("Missing window reference.");
	}

	if (typeof DOMUtils == "function") {
		var utils = DOMUtils(win);
	} else { 
		var utils = DOMUtils;
	}
	/*
	 * #### STEP 1: Convert HTML into a set of basic blocks.
	 *
	 * Markdown organizes a document into a set of blocks and unlike HTML
	 * blocks, these cannot contain other blocks, only inline elements. This is
	 * accomplished by reducing the HTML to its smallest block parts, with the
	 * assumption that block elements usually define separate bodies of
	 * information within an HTML document.
	 *
	 * Each block is given a type based on the Markdown types. This type is
	 * determined from the closest ancestor with one of the following tags:
	 * `blockquote`, `pre`, `li`, `hr`, `h1-6`. All other blocks become
	 * paragraphs.
	 *
	 * This operates on a few assumptions, which outline its limitations:
	 *   - Inline elements do not contain block elements.
	 *   - Standard HTML block elements are used to define and separate content.
	 */

	var blocks = compactBlocks(extractBlocks(doc.body));
	
	function extractBlocks(node) {
		var currentBlock, blocks;

		blocks = [];
		extract(node);
		return blocks;

		function addInline(el) {
			if (currentBlock != null) {
				currentBlock.nodes.push(el);
			} else {
				blocks.push({ type: "p", nodes: [ el ] });
			}

			return blocks;
		}

		function extract(el) {
			if (el.nodeType !== 1) return addInline(el);

			var tag = el.tagName.toLowerCase();
			if (!_.contains(block_elements, tag)) return addInline(el);

			// remove the current block if it's empty
			if (currentBlock != null &&
				!currentBlock.nodes.length &&
				!_.contains(markdown_empty_blocks, currentBlock.type) &&
				_.last(blocks) === currentBlock) blocks.pop();

			// add a new block
			blocks.push(currentBlock = {
				type: _.contains(markdown_block_tags, tag) ? tag : "p",
				nodes: []
			});

			// process children
			_.each(el.childNodes, function(child) {
				extract(child);
			});

			// reset current block
			currentBlock = null;
		}
	}

	function compactBlocks(blocks) {
		return blocks.filter(function(b) {
			var emptyBlock = _.contains(markdown_empty_blocks, b.type);

			// delete nodes array if this is an empty block
			if (emptyBlock) delete b.nodes;

			// make sure the block isn't empty
			return emptyBlock || _.some(b.nodes, function(n) {
				return utils.getTextContent(n).trim() != "" ||
					(n.nodeType === 1 && (utils.matchesSelector(n, empty_tags) || n.querySelector(empty_tags)));
			});
		});
	}

	/*
	 * #### STEP 2: Convert inline HTML into inline Markdown.
	 *
	 * Basically we push each text node onto a stack, accounting for specific
	 * styling like italics and bold. Other inline elements like `br` and `img`
	 * are preserved, but everything else is thrown out.
	 */

	blocks.forEach(function(b) {
		if (_.contains(markdown_empty_blocks, b.type)) return;
		
		b.content = cleanInlines(b.nodes.reduce(function(m, n) {
			return extractInlines(n, m);
		}, []));
	});

	function extractInlines(el, inlines) {
		var lastInline, styles, content;

		if (inlines == null) inlines = [];

		switch (el.nodeType) {
			case 1:
				switch (el.tagName.toLowerCase()) {
					case "br":
						inlines.push({ type: "br" });
						break;

					case "img":
						inlines.push({
							type: "img",
							src: el.getAttribute("src")
						});
						break;

					default:
						_.each(el.childNodes, function(n) {
							extractInlines(n, inlines);
						});
						break;
				}

				break;

			case 3:
				lastInline = _.last(inlines);
				content = Entities.normalizeXML(utils.getTextContent(el), "html");
				styles = _.filter(markdown_inline_tags, function(s, sel) {
					return !!closest(el, sel);
				});

				if (lastInline && lastInline.content != null && _.isEqual(lastInline.styles, styles)) {
					lastInline.content += content;
				} else {
					inlines.push({
						type: "text",
						content: content,
						styles: styles
					});
				}

				break;
		}

		return inlines;
	}

	function cleanInlines(inlines) {
		// clean up whitespace and drop empty inlines
		inlines = inlines.reduce(function(m, inline, i) {
			if (inline.type !== "text") m.push(inline);
			else {
				var prev = i > 0 ? inlines[i-1] : null;

				// reduce multiple spaces to one
				inline.content = inline.content.replace(/\s+/g, " ");

				// remove leading space if previous inline has trailing space
				if (inline.content[0] === " " && prev && (
					prev.type === "br" || (
					prev.type === "text" &&
					prev.content[prev.content.length - 1] === " ")
				)) {
					inline.content = inline.content.substr(1);
				}

				// only add if this has real content
				if (inline.content) m.push(inline);
			}

			return m;
		}, []);

		// trim leading whitespace
		while (inlines.length && inlines[0].type === "text") {
			inlines[0].content = inlines[0].content.replace(/^\s+/, "");
			if (inlines[0].content) break;
			inlines.shift();
		}

		// trim trailing whitespace
		var lastInline;
		while (inlines.length && (lastInline = _.last(inlines)).type === "text") {
			lastInline.content = lastInline.content.replace(/\s+$/, "");
			if (lastInline.content) break;
			inlines.pop();
		}

		return inlines;
	}

	function closest(el, selector) {
		while (el != null) {
			if (el.nodeType === 1 && utils.matchesSelector(el, selector)) return el;
			el = el.parentNode;
		}

		return null;
	}

	/*
	 * #### STEP 3: Clean up
	 *
	 * The last step is to clean up the resulting AST before returning it.
	 */

	// cannot be empty unless otherwise specified
	blocks = blocks.filter(function(b) {
		return _.contains(markdown_empty_blocks, b.type) || b.content.length;
	});

	// remove DOM nodes reference to keep it clean
	blocks.forEach(function(b) { delete b.nodes; });

	return blocks;
}

/**
 * ## toMarkdown()
 *
 * This methods converts the output of `.parse()` into a string of Markdown.
 * 
 * #### Arguments
 * 
 * - **tree** _object_ - A Markdown AST object returned from `.parse()`.
 */
html2md.toMarkdown = function(tree) {
	return tree.map(function(block) {
		var activeStyles = [],
			content = "";

		if (block.content != null) {
			block.content.forEach(function(inline) {
				switch (inline.type) {
					case "text":
						updateStyles(inline.styles);
						content += inline.content;
						break;

					case "br":
						content += markdown_syntax.br;
						break;

					case "img":
						content += "![](" + inline.src + ")";
						break;
				}
			});
		
			updateStyles();
		}

		switch (block.type) {
			case "blockquote":
			case "pre":
			case "p":
			case "hr":
			case "h1":
			case "h2":
			case "h3":
			case "h4":
			case "h5":
			case "h6":
				return (markdown_syntax[block.type] || "") + content;
		}

		function updateStyles(styles) {
			if (styles == null) styles = [];

			// close active styles
			var close = _.difference(activeStyles, styles);
			activeStyles = _.without.apply(_, [ activeStyles ].concat(close));
			close.reverse().forEach(function(style) {
				content += markdown_syntax[style] || "";
			});

			// open new styles
			_.difference(styles, activeStyles).forEach(function(style) {
				activeStyles.push(style);
				content += markdown_syntax[style] || "";
			});
		}
	}).join("\n\n");	
}

/**
 * ## toDOM()
 *
 * A small utility that takes a string of HTML and returns a new HTMLDocument instance. In Node.js, `jsdom` is used to simulate the DOM.
 * 
 * #### Arguments
 * 
 * - **html** _string_ - A string of HTML.
 */
html2md.toDOM = function(html) {
	var doc;

	// clean html before we parse
	html = html.replace(SCRIPT_REGEX, '');
	html = Entities.normalizeXML(html, 'xhtml');

	// browsers
	if (typeof window !== "undefined" && window.document) {
		var doc = window.document.implementation.createHTMLDocument();
		doc.documentElement.innerHTML = html;
	}

	// nodejs
	//else {
	//	doc = require("jsdom").jsdom(html);
	//}

	return doc;
}

},{"bti-dom-utils":2,"special-entities":4,"underscore":5}],2:[function(require,module,exports){
(function (global){
/**
 * # DOM Utilities
 *
 * This is a collection of common utility methods for the DOM. While similar in nature to libraries like jQuery, this library aims to provide methods for unique and odd features.
 */

var Node = global.Node;
var Element = global.Element;

/**
 * ## isNode()
 *
 * Determines if a value is a DOM node.
 *
 * #### Arguments
 *
 * - **value** _mixed_ - A value to test as a DOM node.
 */
var isNode =
exports.isNode = function(node) {
	return node != null && typeof node.nodeType === "number";
}

/**
 * ## matchesSelector()
 *
 * A cross browser compatible solution to testing a DOM element against a CSS selector.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to test.
 * - **selector** _string_ - A CSS selector.
 */
var matchesSelector = typeof Element !== "undefined" ?
	Element.prototype.matches ||
	Element.prototype.webkitMatchesSelector ||
	Element.prototype.mozMatchesSelector ||
	Element.prototype.msMatchesSelector :
	function() { return false; };

exports.matchesSelector = function(node, selector) {
	return matchesSelector.call(node, selector)
}

/**
 * ## matches()
 *
 * Similar to `matchesSelector()`, this method will test a DOM node against CSS selectors, other DOM nodes and functions.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to test.
 * - **selector** _string | function | Node | Array_ - A CSS selector, a function (called with one argument, the node) or a DOM node. Pass an array of selectors to match any of them.
 */
var matches =
exports.matches = function(node, selector) {
	if (Array.isArray(selector)) return selector.some(function(s) {
		return matches(node, s);
	});

	if (isNode(selector)) {
		return node === selector;
	}
	
	if (typeof selector === "function") {
		return !!selector(node);
	}
	
	if (node.nodeType === Node.ELEMENT_NODE) {
		return matchesSelector.call(node, selector);
	}

	return false;
};

/**
 * ## closest()
 *
 * Stating at `elem`, this method traverses up the parent nodes and returns the first one that matches `selector`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to test.
 * - **selector** _string | function | Node_ - A CSS selector, a function (called with one argument, the node) or a DOM node.
 */
exports.closest = function(elem, selector) {
	while (elem != null) {
		if (elem.nodeType === 1 && matches(elem, selector)) return elem;
		elem = elem.parentNode;
	}

	return null;
};

/**
 * ## requestAnimationFrame()
 *
 * A cross-browser requestAnimationFrame. Fall back on `setTimeout()` if requestAnimationFrame isn't defined.
 *
 * #### Arguments
 *
 * - **fn** _function_ - A funciton to call on the next animation frame.
 */
exports.requestAnimationFrame =
	window.requestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	function (f) { setTimeout(f, 16); };

/**
 * ## getFirstLeafNode()
 *
 * Returns the first descendant node without children or `null` if doesn't exist.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to find the first leaf of.
 */
var getFirstLeafNode =
exports.getFirstLeafNode = function(node) {
	while (node.hasChildNodes()) node = node.firstChild;
	return node;
}

/**
 * ## getLastLeafNode()
 *
 * Returns the last descendant node without children or `null` if doesn't exist.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A DOM node to find the last leaf of.
 */
var getLastLeafNode =
exports.getLastLeafNode = function(node) {
	while (node.hasChildNodes()) node = node.lastChild;
	return node;
}

/**
 * ## getNextExtendedSibling()
 *
 * Returns the next sibling of this node, a direct ancestor node's next sibling, or `null`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the next extended sibling of.
 */
var getNextExtendedSibling =
exports.getNextExtendedSibling = function(node) {
	while (node != null) {
		if (node.nextSibling != null) return node.nextSibling;
		node = node.parentNode;
	}

	return null;
}

/**
 * ## getPreviousExtendedSibling()
 *
 * Returns the previous sibling of this node, a direct ancestor node's previous sibling, or `null`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the previous extended sibling of.
 */
var getPreviousExtendedSibling =
exports.getPreviousExtendedSibling = function(node) {
	while (node != null) {
		if (node.previousSibling != null) return node.previousSibling;
		node = node.parentNode;
	}

	return null;
}

/**
 * ## getNextNode()
 *
 * Gets the next node in the DOM tree. This is either the first child node, the next sibling node, a direct ancestor node's next sibling, or `null`.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the next node of.
 */
var getNextNode =
exports.getNextNode = function(node) {
	return node.hasChildNodes() ? node.firstChild : getNextExtendedSibling(node);
}

/**
 * ## getPreviousNode()
 *
 * Gets the previous node in the DOM tree. This will return the previous extended sibling's last, deepest leaf node or `null` if doesn't exist. This returns the exact opposite result of `getNextNode` (ie `getNextNode(getPreviousNode(node)) === node`).
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the previous node of.
 */
var getPreviousNode =
exports.getPreviousNode = function(node) {
	return node.previousSibling == null ? node.parentNode : getLastLeafNode(node.previousSibling);
}

/**
 * ## getTextContent()
 *
 * Gets the text content of a node and its descendants. This is the text content that is visible to a user viewing the HTML from browser. Hidden nodes, such as comments, are not included in the output.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node to get the text content of.
 */
var getTextContent =
exports.getTextContent = function(node) {
	if (Array.isArray(node)) return node.map(getTextContent).join("");

	switch(node.nodeType) {
		case Node.DOCUMENT_NODE:
		case Node.DOCUMENT_FRAGMENT_NODE:
			return getTextContent(Array.prototype.slice.call(node.childNodes, 0));

		case Node.ELEMENT_NODE:
			if (typeof node.innerText === "string") return node.innerText;		// webkit
			if (typeof node.textContent === "string") return node.textContent;	// firefox
			return getTextContent(Array.prototype.slice.call(node.childNodes, 0));// other
		
		case Node.TEXT_NODE:
			return node.nodeValue || "";

		default:
			return "";
	}
}

/**
 * ## getRootNode()
 *
 * Returns the root node of a DOM tree.
 *
 * #### Arguments
 *
 * - **node** _Node_ - A node in the DOM tree you need the root of.
 */
var getRootNode =
exports.getRootNode = function(node) {
	while (node.parentNode != null) {
		node = node.parentNode
	}
	
	return node;
}

/**
 * ## contains()
 *
 * Determines if a node is a direct ancestor of another node. This is the same syntax as jQuery's `$.contains()`.
 *
 * #### Arguments
 *
 * - **parent** _Node_ - The ancestor node.
 * - **node** _Node_ - The node which may or may not be a descendant of the parent.
 */
var contains =
exports.contains = function(parent, node) {
	while (node != null) {
		if (matches(node, parent)) return true;
		node = node.parentNode;
	}
	
	return false;
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],3:[function(require,module,exports){
module.exports={
	"Aacute": [ 193 ],
	"aacute": [ 225 ],
	"Abreve": [ 258 ],
	"abreve": [ 259 ],
	"ac": [ 8766 ],
	"acd": [ 8767 ],
	"acE": [ 8766, 819 ],
	"Acirc": [ 194 ],
	"acirc": [ 226 ],
	"acute": [ 180 ],
	"Acy": [ 1040 ],
	"acy": [ 1072 ],
	"AElig": [ 198 ],
	"aelig": [ 230 ],
	"af": [ 8289 ],
	"Afr": [ 120068 ],
	"afr": [ 120094 ],
	"Agrave": [ 192 ],
	"agrave": [ 224 ],
	"alefsym": [ 8501 ],
	"aleph": [ 8501 ],
	"Alpha": [ 913 ],
	"alpha": [ 945 ],
	"Amacr": [ 256 ],
	"amacr": [ 257 ],
	"amalg": [ 10815 ],
	"AMP": [ 38 ],
	"amp": [ 38 ],
	"And": [ 10835 ],
	"and": [ 8743 ],
	"andand": [ 10837 ],
	"andd": [ 10844 ],
	"andslope": [ 10840 ],
	"andv": [ 10842 ],
	"ang": [ 8736 ],
	"ange": [ 10660 ],
	"angle": [ 8736 ],
	"angmsd": [ 8737 ],
	"angmsdaa": [ 10664 ],
	"angmsdab": [ 10665 ],
	"angmsdac": [ 10666 ],
	"angmsdad": [ 10667 ],
	"angmsdae": [ 10668 ],
	"angmsdaf": [ 10669 ],
	"angmsdag": [ 10670 ],
	"angmsdah": [ 10671 ],
	"angrt": [ 8735 ],
	"angrtvb": [ 8894 ],
	"angrtvbd": [ 10653 ],
	"angsph": [ 8738 ],
	"angst": [ 197 ],
	"angzarr": [ 9084 ],
	"Aogon": [ 260 ],
	"aogon": [ 261 ],
	"Aopf": [ 120120 ],
	"aopf": [ 120146 ],
	"ap": [ 8776 ],
	"apacir": [ 10863 ],
	"apE": [ 10864 ],
	"ape": [ 8778 ],
	"apid": [ 8779 ],
	"apos": [ 39 ],
	"ApplyFunction": [ 8289 ],
	"approx": [ 8776 ],
	"approxeq": [ 8778 ],
	"Aring": [ 197 ],
	"aring": [ 229 ],
	"Ascr": [ 119964 ],
	"ascr": [ 119990 ],
	"Assign": [ 8788 ],
	"ast": [ 42 ],
	"asymp": [ 8776 ],
	"asympeq": [ 8781 ],
	"Atilde": [ 195 ],
	"atilde": [ 227 ],
	"Auml": [ 196 ],
	"auml": [ 228 ],
	"awconint": [ 8755 ],
	"awint": [ 10769 ],
	"backcong": [ 8780 ],
	"backepsilon": [ 1014 ],
	"backprime": [ 8245 ],
	"backsim": [ 8765 ],
	"backsimeq": [ 8909 ],
	"Backslash": [ 8726 ],
	"Barv": [ 10983 ],
	"barvee": [ 8893 ],
	"Barwed": [ 8966 ],
	"barwed": [ 8965 ],
	"barwedge": [ 8965 ],
	"bbrk": [ 9141 ],
	"bbrktbrk": [ 9142 ],
	"bcong": [ 8780 ],
	"Bcy": [ 1041 ],
	"bcy": [ 1073 ],
	"bdquo": [ 8222 ],
	"becaus": [ 8757 ],
	"Because": [ 8757 ],
	"because": [ 8757 ],
	"bemptyv": [ 10672 ],
	"bepsi": [ 1014 ],
	"bernou": [ 8492 ],
	"Bernoullis": [ 8492 ],
	"Beta": [ 914 ],
	"beta": [ 946 ],
	"beth": [ 8502 ],
	"between": [ 8812 ],
	"Bfr": [ 120069 ],
	"bfr": [ 120095 ],
	"bigcap": [ 8898 ],
	"bigcirc": [ 9711 ],
	"bigcup": [ 8899 ],
	"bigodot": [ 10752 ],
	"bigoplus": [ 10753 ],
	"bigotimes": [ 10754 ],
	"bigsqcup": [ 10758 ],
	"bigstar": [ 9733 ],
	"bigtriangledown": [ 9661 ],
	"bigtriangleup": [ 9651 ],
	"biguplus": [ 10756 ],
	"bigvee": [ 8897 ],
	"bigwedge": [ 8896 ],
	"bkarow": [ 10509 ],
	"blacklozenge": [ 10731 ],
	"blacksquare": [ 9642 ],
	"blacktriangle": [ 9652 ],
	"blacktriangledown": [ 9662 ],
	"blacktriangleleft": [ 9666 ],
	"blacktriangleright": [ 9656 ],
	"blank": [ 9251 ],
	"blk12": [ 9618 ],
	"blk14": [ 9617 ],
	"blk34": [ 9619 ],
	"block": [ 9608 ],
	"bne": [ 61, 8421 ],
	"bnequiv": [ 8801, 8421 ],
	"bNot": [ 10989 ],
	"bnot": [ 8976 ],
	"Bopf": [ 120121 ],
	"bopf": [ 120147 ],
	"bot": [ 8869 ],
	"bottom": [ 8869 ],
	"bowtie": [ 8904 ],
	"boxbox": [ 10697 ],
	"boxDL": [ 9559 ],
	"boxDl": [ 9558 ],
	"boxdL": [ 9557 ],
	"boxdl": [ 9488 ],
	"boxDR": [ 9556 ],
	"boxDr": [ 9555 ],
	"boxdR": [ 9554 ],
	"boxdr": [ 9484 ],
	"boxH": [ 9552 ],
	"boxh": [ 9472 ],
	"boxHD": [ 9574 ],
	"boxHd": [ 9572 ],
	"boxhD": [ 9573 ],
	"boxhd": [ 9516 ],
	"boxHU": [ 9577 ],
	"boxHu": [ 9575 ],
	"boxhU": [ 9576 ],
	"boxhu": [ 9524 ],
	"boxminus": [ 8863 ],
	"boxplus": [ 8862 ],
	"boxtimes": [ 8864 ],
	"boxUL": [ 9565 ],
	"boxUl": [ 9564 ],
	"boxuL": [ 9563 ],
	"boxul": [ 9496 ],
	"boxUR": [ 9562 ],
	"boxUr": [ 9561 ],
	"boxuR": [ 9560 ],
	"boxur": [ 9492 ],
	"boxV": [ 9553 ],
	"boxv": [ 9474 ],
	"boxVH": [ 9580 ],
	"boxVh": [ 9579 ],
	"boxvH": [ 9578 ],
	"boxvh": [ 9532 ],
	"boxVL": [ 9571 ],
	"boxVl": [ 9570 ],
	"boxvL": [ 9569 ],
	"boxvl": [ 9508 ],
	"boxVR": [ 9568 ],
	"boxVr": [ 9567 ],
	"boxvR": [ 9566 ],
	"boxvr": [ 9500 ],
	"bprime": [ 8245 ],
	"Breve": [ 728 ],
	"breve": [ 728 ],
	"brvbar": [ 166 ],
	"Bscr": [ 8492 ],
	"bscr": [ 119991 ],
	"bsemi": [ 8271 ],
	"bsim": [ 8765 ],
	"bsime": [ 8909 ],
	"bsol": [ 92 ],
	"bsolb": [ 10693 ],
	"bsolhsub": [ 10184 ],
	"bull": [ 8226 ],
	"bullet": [ 8226 ],
	"bump": [ 8782 ],
	"bumpE": [ 10926 ],
	"bumpe": [ 8783 ],
	"Bumpeq": [ 8782 ],
	"bumpeq": [ 8783 ],
	"Cacute": [ 262 ],
	"cacute": [ 263 ],
	"Cap": [ 8914 ],
	"cap": [ 8745 ],
	"capand": [ 10820 ],
	"capbrcup": [ 10825 ],
	"capcap": [ 10827 ],
	"capcup": [ 10823 ],
	"capdot": [ 10816 ],
	"CapitalDifferentialD": [ 8517 ],
	"caps": [ 8745, 65024 ],
	"caret": [ 8257 ],
	"caron": [ 711 ],
	"Cayleys": [ 8493 ],
	"ccaps": [ 10829 ],
	"Ccaron": [ 268 ],
	"ccaron": [ 269 ],
	"Ccedil": [ 199 ],
	"ccedil": [ 231 ],
	"Ccirc": [ 264 ],
	"ccirc": [ 265 ],
	"Cconint": [ 8752 ],
	"ccups": [ 10828 ],
	"ccupssm": [ 10832 ],
	"Cdot": [ 266 ],
	"cdot": [ 267 ],
	"cedil": [ 184 ],
	"Cedilla": [ 184 ],
	"cemptyv": [ 10674 ],
	"cent": [ 162 ],
	"CenterDot": [ 183 ],
	"centerdot": [ 183 ],
	"Cfr": [ 8493 ],
	"cfr": [ 120096 ],
	"CHcy": [ 1063 ],
	"chcy": [ 1095 ],
	"check": [ 10003 ],
	"checkmark": [ 10003 ],
	"Chi": [ 935 ],
	"chi": [ 967 ],
	"cir": [ 9675 ],
	"circ": [ 710 ],
	"circeq": [ 8791 ],
	"circlearrowleft": [ 8634 ],
	"circlearrowright": [ 8635 ],
	"circledast": [ 8859 ],
	"circledcirc": [ 8858 ],
	"circleddash": [ 8861 ],
	"CircleDot": [ 8857 ],
	"circledR": [ 174 ],
	"circledS": [ 9416 ],
	"CircleMinus": [ 8854 ],
	"CirclePlus": [ 8853 ],
	"CircleTimes": [ 8855 ],
	"cirE": [ 10691 ],
	"cire": [ 8791 ],
	"cirfnint": [ 10768 ],
	"cirmid": [ 10991 ],
	"cirscir": [ 10690 ],
	"ClockwiseContourIntegral": [ 8754 ],
	"CloseCurlyDoubleQuote": [ 8221 ],
	"CloseCurlyQuote": [ 8217 ],
	"clubs": [ 9827 ],
	"clubsuit": [ 9827 ],
	"Colon": [ 8759 ],
	"colon": [ 58 ],
	"Colone": [ 10868 ],
	"colone": [ 8788 ],
	"coloneq": [ 8788 ],
	"comma": [ 44 ],
	"commat": [ 64 ],
	"comp": [ 8705 ],
	"compfn": [ 8728 ],
	"complement": [ 8705 ],
	"complexes": [ 8450 ],
	"cong": [ 8773 ],
	"congdot": [ 10861 ],
	"Congruent": [ 8801 ],
	"Conint": [ 8751 ],
	"conint": [ 8750 ],
	"ContourIntegral": [ 8750 ],
	"Copf": [ 8450 ],
	"copf": [ 120148 ],
	"coprod": [ 8720 ],
	"Coproduct": [ 8720 ],
	"COPY": [ 169 ],
	"copy": [ 169 ],
	"copysr": [ 8471 ],
	"CounterClockwiseContourIntegral": [ 8755 ],
	"crarr": [ 8629 ],
	"Cross": [ 10799 ],
	"cross": [ 10007 ],
	"Cscr": [ 119966 ],
	"cscr": [ 119992 ],
	"csub": [ 10959 ],
	"csube": [ 10961 ],
	"csup": [ 10960 ],
	"csupe": [ 10962 ],
	"ctdot": [ 8943 ],
	"cudarrl": [ 10552 ],
	"cudarrr": [ 10549 ],
	"cuepr": [ 8926 ],
	"cuesc": [ 8927 ],
	"cularr": [ 8630 ],
	"cularrp": [ 10557 ],
	"Cup": [ 8915 ],
	"cup": [ 8746 ],
	"cupbrcap": [ 10824 ],
	"CupCap": [ 8781 ],
	"cupcap": [ 10822 ],
	"cupcup": [ 10826 ],
	"cupdot": [ 8845 ],
	"cupor": [ 10821 ],
	"cups": [ 8746, 65024 ],
	"curarr": [ 8631 ],
	"curarrm": [ 10556 ],
	"curlyeqprec": [ 8926 ],
	"curlyeqsucc": [ 8927 ],
	"curlyvee": [ 8910 ],
	"curlywedge": [ 8911 ],
	"curren": [ 164 ],
	"curvearrowleft": [ 8630 ],
	"curvearrowright": [ 8631 ],
	"cuvee": [ 8910 ],
	"cuwed": [ 8911 ],
	"cwconint": [ 8754 ],
	"cwint": [ 8753 ],
	"cylcty": [ 9005 ],
	"Dagger": [ 8225 ],
	"dagger": [ 8224 ],
	"daleth": [ 8504 ],
	"Darr": [ 8609 ],
	"dArr": [ 8659 ],
	"darr": [ 8595 ],
	"dash": [ 8208 ],
	"Dashv": [ 10980 ],
	"dashv": [ 8867 ],
	"dbkarow": [ 10511 ],
	"dblac": [ 733 ],
	"Dcaron": [ 270 ],
	"dcaron": [ 271 ],
	"Dcy": [ 1044 ],
	"dcy": [ 1076 ],
	"DD": [ 8517 ],
	"dd": [ 8518 ],
	"ddagger": [ 8225 ],
	"ddarr": [ 8650 ],
	"DDotrahd": [ 10513 ],
	"ddotseq": [ 10871 ],
	"deg": [ 176 ],
	"Del": [ 8711 ],
	"Delta": [ 916 ],
	"delta": [ 948 ],
	"demptyv": [ 10673 ],
	"dfisht": [ 10623 ],
	"Dfr": [ 120071 ],
	"dfr": [ 120097 ],
	"dHar": [ 10597 ],
	"dharl": [ 8643 ],
	"dharr": [ 8642 ],
	"DiacriticalAcute": [ 180 ],
	"DiacriticalDot": [ 729 ],
	"DiacriticalDoubleAcute": [ 733 ],
	"DiacriticalGrave": [ 96 ],
	"DiacriticalTilde": [ 732 ],
	"diam": [ 8900 ],
	"Diamond": [ 8900 ],
	"diamond": [ 8900 ],
	"diamondsuit": [ 9830 ],
	"diams": [ 9830 ],
	"die": [ 168 ],
	"DifferentialD": [ 8518 ],
	"digamma": [ 989 ],
	"disin": [ 8946 ],
	"div": [ 247 ],
	"divide": [ 247 ],
	"divideontimes": [ 8903 ],
	"divonx": [ 8903 ],
	"DJcy": [ 1026 ],
	"djcy": [ 1106 ],
	"dlcorn": [ 8990 ],
	"dlcrop": [ 8973 ],
	"dollar": [ 36 ],
	"Dopf": [ 120123 ],
	"dopf": [ 120149 ],
	"Dot": [ 168 ],
	"dot": [ 729 ],
	"DotDot": [ 8412 ],
	"doteq": [ 8784 ],
	"doteqdot": [ 8785 ],
	"DotEqual": [ 8784 ],
	"dotminus": [ 8760 ],
	"dotplus": [ 8724 ],
	"dotsquare": [ 8865 ],
	"doublebarwedge": [ 8966 ],
	"DoubleContourIntegral": [ 8751 ],
	"DoubleDot": [ 168 ],
	"DoubleDownArrow": [ 8659 ],
	"DoubleLeftArrow": [ 8656 ],
	"DoubleLeftRightArrow": [ 8660 ],
	"DoubleLeftTee": [ 10980 ],
	"DoubleLongLeftArrow": [ 10232 ],
	"DoubleLongLeftRightArrow": [ 10234 ],
	"DoubleLongRightArrow": [ 10233 ],
	"DoubleRightArrow": [ 8658 ],
	"DoubleRightTee": [ 8872 ],
	"DoubleUpArrow": [ 8657 ],
	"DoubleUpDownArrow": [ 8661 ],
	"DoubleVerticalBar": [ 8741 ],
	"DownArrow": [ 8595 ],
	"Downarrow": [ 8659 ],
	"downarrow": [ 8595 ],
	"DownArrowBar": [ 10515 ],
	"DownArrowUpArrow": [ 8693 ],
	"DownBreve": [ 785 ],
	"downdownarrows": [ 8650 ],
	"downharpoonleft": [ 8643 ],
	"downharpoonright": [ 8642 ],
	"DownLeftRightVector": [ 10576 ],
	"DownLeftTeeVector": [ 10590 ],
	"DownLeftVector": [ 8637 ],
	"DownLeftVectorBar": [ 10582 ],
	"DownRightTeeVector": [ 10591 ],
	"DownRightVector": [ 8641 ],
	"DownRightVectorBar": [ 10583 ],
	"DownTee": [ 8868 ],
	"DownTeeArrow": [ 8615 ],
	"drbkarow": [ 10512 ],
	"drcorn": [ 8991 ],
	"drcrop": [ 8972 ],
	"Dscr": [ 119967 ],
	"dscr": [ 119993 ],
	"DScy": [ 1029 ],
	"dscy": [ 1109 ],
	"dsol": [ 10742 ],
	"Dstrok": [ 272 ],
	"dstrok": [ 273 ],
	"dtdot": [ 8945 ],
	"dtri": [ 9663 ],
	"dtrif": [ 9662 ],
	"duarr": [ 8693 ],
	"duhar": [ 10607 ],
	"dwangle": [ 10662 ],
	"DZcy": [ 1039 ],
	"dzcy": [ 1119 ],
	"dzigrarr": [ 10239 ],
	"Eacute": [ 201 ],
	"eacute": [ 233 ],
	"easter": [ 10862 ],
	"Ecaron": [ 282 ],
	"ecaron": [ 283 ],
	"ecir": [ 8790 ],
	"Ecirc": [ 202 ],
	"ecirc": [ 234 ],
	"ecolon": [ 8789 ],
	"Ecy": [ 1069 ],
	"ecy": [ 1101 ],
	"eDDot": [ 10871 ],
	"Edot": [ 278 ],
	"eDot": [ 8785 ],
	"edot": [ 279 ],
	"ee": [ 8519 ],
	"efDot": [ 8786 ],
	"Efr": [ 120072 ],
	"efr": [ 120098 ],
	"eg": [ 10906 ],
	"Egrave": [ 200 ],
	"egrave": [ 232 ],
	"egs": [ 10902 ],
	"egsdot": [ 10904 ],
	"el": [ 10905 ],
	"Element": [ 8712 ],
	"elinters": [ 9191 ],
	"ell": [ 8467 ],
	"els": [ 10901 ],
	"elsdot": [ 10903 ],
	"Emacr": [ 274 ],
	"emacr": [ 275 ],
	"empty": [ 8709 ],
	"emptyset": [ 8709 ],
	"EmptySmallSquare": [ 9723 ],
	"emptyv": [ 8709 ],
	"EmptyVerySmallSquare": [ 9643 ],
	"emsp": [ 8195 ],
	"emsp13": [ 8196 ],
	"emsp14": [ 8197 ],
	"ENG": [ 330 ],
	"eng": [ 331 ],
	"ensp": [ 8194 ],
	"Eogon": [ 280 ],
	"eogon": [ 281 ],
	"Eopf": [ 120124 ],
	"eopf": [ 120150 ],
	"epar": [ 8917 ],
	"eparsl": [ 10723 ],
	"eplus": [ 10865 ],
	"epsi": [ 949 ],
	"Epsilon": [ 917 ],
	"epsilon": [ 949 ],
	"epsiv": [ 1013 ],
	"eqcirc": [ 8790 ],
	"eqcolon": [ 8789 ],
	"eqsim": [ 8770 ],
	"eqslantgtr": [ 10902 ],
	"eqslantless": [ 10901 ],
	"Equal": [ 10869 ],
	"equals": [ 61 ],
	"EqualTilde": [ 8770 ],
	"equest": [ 8799 ],
	"Equilibrium": [ 8652 ],
	"equiv": [ 8801 ],
	"equivDD": [ 10872 ],
	"eqvparsl": [ 10725 ],
	"erarr": [ 10609 ],
	"erDot": [ 8787 ],
	"Escr": [ 8496 ],
	"escr": [ 8495 ],
	"esdot": [ 8784 ],
	"Esim": [ 10867 ],
	"esim": [ 8770 ],
	"Eta": [ 919 ],
	"eta": [ 951 ],
	"ETH": [ 208 ],
	"eth": [ 240 ],
	"Euml": [ 203 ],
	"euml": [ 235 ],
	"euro": [ 8364 ],
	"excl": [ 33 ],
	"exist": [ 8707 ],
	"Exists": [ 8707 ],
	"expectation": [ 8496 ],
	"ExponentialE": [ 8519 ],
	"exponentiale": [ 8519 ],
	"fallingdotseq": [ 8786 ],
	"Fcy": [ 1060 ],
	"fcy": [ 1092 ],
	"female": [ 9792 ],
	"ffilig": [ 64259 ],
	"fflig": [ 64256 ],
	"ffllig": [ 64260 ],
	"Ffr": [ 120073 ],
	"ffr": [ 120099 ],
	"filig": [ 64257 ],
	"FilledSmallSquare": [ 9724 ],
	"FilledVerySmallSquare": [ 9642 ],
	"fjlig": [ 102, 106 ],
	"flat": [ 9837 ],
	"fllig": [ 64258 ],
	"fltns": [ 9649 ],
	"fnof": [ 402 ],
	"Fopf": [ 120125 ],
	"fopf": [ 120151 ],
	"ForAll": [ 8704 ],
	"forall": [ 8704 ],
	"fork": [ 8916 ],
	"forkv": [ 10969 ],
	"Fouriertrf": [ 8497 ],
	"fpartint": [ 10765 ],
	"frac12": [ 189 ],
	"frac13": [ 8531 ],
	"frac14": [ 188 ],
	"frac15": [ 8533 ],
	"frac16": [ 8537 ],
	"frac18": [ 8539 ],
	"frac23": [ 8532 ],
	"frac25": [ 8534 ],
	"frac34": [ 190 ],
	"frac35": [ 8535 ],
	"frac38": [ 8540 ],
	"frac45": [ 8536 ],
	"frac56": [ 8538 ],
	"frac58": [ 8541 ],
	"frac78": [ 8542 ],
	"frasl": [ 8260 ],
	"frown": [ 8994 ],
	"Fscr": [ 8497 ],
	"fscr": [ 119995 ],
	"gacute": [ 501 ],
	"Gamma": [ 915 ],
	"gamma": [ 947 ],
	"Gammad": [ 988 ],
	"gammad": [ 989 ],
	"gap": [ 10886 ],
	"Gbreve": [ 286 ],
	"gbreve": [ 287 ],
	"Gcedil": [ 290 ],
	"Gcirc": [ 284 ],
	"gcirc": [ 285 ],
	"Gcy": [ 1043 ],
	"gcy": [ 1075 ],
	"Gdot": [ 288 ],
	"gdot": [ 289 ],
	"gE": [ 8807 ],
	"ge": [ 8805 ],
	"gEl": [ 10892 ],
	"gel": [ 8923 ],
	"geq": [ 8805 ],
	"geqq": [ 8807 ],
	"geqslant": [ 10878 ],
	"ges": [ 10878 ],
	"gescc": [ 10921 ],
	"gesdot": [ 10880 ],
	"gesdoto": [ 10882 ],
	"gesdotol": [ 10884 ],
	"gesl": [ 8923, 65024 ],
	"gesles": [ 10900 ],
	"Gfr": [ 120074 ],
	"gfr": [ 120100 ],
	"Gg": [ 8921 ],
	"gg": [ 8811 ],
	"ggg": [ 8921 ],
	"gimel": [ 8503 ],
	"GJcy": [ 1027 ],
	"gjcy": [ 1107 ],
	"gl": [ 8823 ],
	"gla": [ 10917 ],
	"glE": [ 10898 ],
	"glj": [ 10916 ],
	"gnap": [ 10890 ],
	"gnapprox": [ 10890 ],
	"gnE": [ 8809 ],
	"gne": [ 10888 ],
	"gneq": [ 10888 ],
	"gneqq": [ 8809 ],
	"gnsim": [ 8935 ],
	"Gopf": [ 120126 ],
	"gopf": [ 120152 ],
	"grave": [ 96 ],
	"GreaterEqual": [ 8805 ],
	"GreaterEqualLess": [ 8923 ],
	"GreaterFullEqual": [ 8807 ],
	"GreaterGreater": [ 10914 ],
	"GreaterLess": [ 8823 ],
	"GreaterSlantEqual": [ 10878 ],
	"GreaterTilde": [ 8819 ],
	"Gscr": [ 119970 ],
	"gscr": [ 8458 ],
	"gsim": [ 8819 ],
	"gsime": [ 10894 ],
	"gsiml": [ 10896 ],
	"GT": [ 62 ],
	"Gt": [ 8811 ],
	"gt": [ 62 ],
	"gtcc": [ 10919 ],
	"gtcir": [ 10874 ],
	"gtdot": [ 8919 ],
	"gtlPar": [ 10645 ],
	"gtquest": [ 10876 ],
	"gtrapprox": [ 10886 ],
	"gtrarr": [ 10616 ],
	"gtrdot": [ 8919 ],
	"gtreqless": [ 8923 ],
	"gtreqqless": [ 10892 ],
	"gtrless": [ 8823 ],
	"gtrsim": [ 8819 ],
	"gvertneqq": [ 8809, 65024 ],
	"gvnE": [ 8809, 65024 ],
	"Hacek": [ 711 ],
	"hairsp": [ 8202 ],
	"half": [ 189 ],
	"hamilt": [ 8459 ],
	"HARDcy": [ 1066 ],
	"hardcy": [ 1098 ],
	"hArr": [ 8660 ],
	"harr": [ 8596 ],
	"harrcir": [ 10568 ],
	"harrw": [ 8621 ],
	"Hat": [ 94 ],
	"hbar": [ 8463 ],
	"Hcirc": [ 292 ],
	"hcirc": [ 293 ],
	"hearts": [ 9829 ],
	"heartsuit": [ 9829 ],
	"hellip": [ 8230 ],
	"hercon": [ 8889 ],
	"Hfr": [ 8460 ],
	"hfr": [ 120101 ],
	"HilbertSpace": [ 8459 ],
	"hksearow": [ 10533 ],
	"hkswarow": [ 10534 ],
	"hoarr": [ 8703 ],
	"homtht": [ 8763 ],
	"hookleftarrow": [ 8617 ],
	"hookrightarrow": [ 8618 ],
	"Hopf": [ 8461 ],
	"hopf": [ 120153 ],
	"horbar": [ 8213 ],
	"HorizontalLine": [ 9472 ],
	"Hscr": [ 8459 ],
	"hscr": [ 119997 ],
	"hslash": [ 8463 ],
	"Hstrok": [ 294 ],
	"hstrok": [ 295 ],
	"HumpDownHump": [ 8782 ],
	"HumpEqual": [ 8783 ],
	"hybull": [ 8259 ],
	"hyphen": [ 8208 ],
	"Iacute": [ 205 ],
	"iacute": [ 237 ],
	"ic": [ 8291 ],
	"Icirc": [ 206 ],
	"icirc": [ 238 ],
	"Icy": [ 1048 ],
	"icy": [ 1080 ],
	"Idot": [ 304 ],
	"IEcy": [ 1045 ],
	"iecy": [ 1077 ],
	"iexcl": [ 161 ],
	"iff": [ 8660 ],
	"Ifr": [ 8465 ],
	"ifr": [ 120102 ],
	"Igrave": [ 204 ],
	"igrave": [ 236 ],
	"ii": [ 8520 ],
	"iiiint": [ 10764 ],
	"iiint": [ 8749 ],
	"iinfin": [ 10716 ],
	"iiota": [ 8489 ],
	"IJlig": [ 306 ],
	"ijlig": [ 307 ],
	"Im": [ 8465 ],
	"Imacr": [ 298 ],
	"imacr": [ 299 ],
	"image": [ 8465 ],
	"ImaginaryI": [ 8520 ],
	"imagline": [ 8464 ],
	"imagpart": [ 8465 ],
	"imath": [ 305 ],
	"imof": [ 8887 ],
	"imped": [ 437 ],
	"Implies": [ 8658 ],
	"in": [ 8712 ],
	"incare": [ 8453 ],
	"infin": [ 8734 ],
	"infintie": [ 10717 ],
	"inodot": [ 305 ],
	"Int": [ 8748 ],
	"int": [ 8747 ],
	"intcal": [ 8890 ],
	"integers": [ 8484 ],
	"Integral": [ 8747 ],
	"intercal": [ 8890 ],
	"Intersection": [ 8898 ],
	"intlarhk": [ 10775 ],
	"intprod": [ 10812 ],
	"InvisibleComma": [ 8291 ],
	"InvisibleTimes": [ 8290 ],
	"IOcy": [ 1025 ],
	"iocy": [ 1105 ],
	"Iogon": [ 302 ],
	"iogon": [ 303 ],
	"Iopf": [ 120128 ],
	"iopf": [ 120154 ],
	"Iota": [ 921 ],
	"iota": [ 953 ],
	"iprod": [ 10812 ],
	"iquest": [ 191 ],
	"Iscr": [ 8464 ],
	"iscr": [ 119998 ],
	"isin": [ 8712 ],
	"isindot": [ 8949 ],
	"isinE": [ 8953 ],
	"isins": [ 8948 ],
	"isinsv": [ 8947 ],
	"isinv": [ 8712 ],
	"it": [ 8290 ],
	"Itilde": [ 296 ],
	"itilde": [ 297 ],
	"Iukcy": [ 1030 ],
	"iukcy": [ 1110 ],
	"Iuml": [ 207 ],
	"iuml": [ 239 ],
	"Jcirc": [ 308 ],
	"jcirc": [ 309 ],
	"Jcy": [ 1049 ],
	"jcy": [ 1081 ],
	"Jfr": [ 120077 ],
	"jfr": [ 120103 ],
	"jmath": [ 567 ],
	"Jopf": [ 120129 ],
	"jopf": [ 120155 ],
	"Jscr": [ 119973 ],
	"jscr": [ 119999 ],
	"Jsercy": [ 1032 ],
	"jsercy": [ 1112 ],
	"Jukcy": [ 1028 ],
	"jukcy": [ 1108 ],
	"Kappa": [ 922 ],
	"kappa": [ 954 ],
	"kappav": [ 1008 ],
	"Kcedil": [ 310 ],
	"kcedil": [ 311 ],
	"Kcy": [ 1050 ],
	"kcy": [ 1082 ],
	"Kfr": [ 120078 ],
	"kfr": [ 120104 ],
	"kgreen": [ 312 ],
	"KHcy": [ 1061 ],
	"khcy": [ 1093 ],
	"KJcy": [ 1036 ],
	"kjcy": [ 1116 ],
	"Kopf": [ 120130 ],
	"kopf": [ 120156 ],
	"Kscr": [ 119974 ],
	"kscr": [ 120000 ],
	"lAarr": [ 8666 ],
	"Lacute": [ 313 ],
	"lacute": [ 314 ],
	"laemptyv": [ 10676 ],
	"lagran": [ 8466 ],
	"Lambda": [ 923 ],
	"lambda": [ 955 ],
	"Lang": [ 10218 ],
	"lang": [ 10216 ],
	"langd": [ 10641 ],
	"langle": [ 10216 ],
	"lap": [ 10885 ],
	"Laplacetrf": [ 8466 ],
	"laquo": [ 171 ],
	"Larr": [ 8606 ],
	"lArr": [ 8656 ],
	"larr": [ 8592 ],
	"larrb": [ 8676 ],
	"larrbfs": [ 10527 ],
	"larrfs": [ 10525 ],
	"larrhk": [ 8617 ],
	"larrlp": [ 8619 ],
	"larrpl": [ 10553 ],
	"larrsim": [ 10611 ],
	"larrtl": [ 8610 ],
	"lat": [ 10923 ],
	"lAtail": [ 10523 ],
	"latail": [ 10521 ],
	"late": [ 10925 ],
	"lates": [ 10925, 65024 ],
	"lBarr": [ 10510 ],
	"lbarr": [ 10508 ],
	"lbbrk": [ 10098 ],
	"lbrace": [ 123 ],
	"lbrack": [ 91 ],
	"lbrke": [ 10635 ],
	"lbrksld": [ 10639 ],
	"lbrkslu": [ 10637 ],
	"Lcaron": [ 317 ],
	"lcaron": [ 318 ],
	"Lcedil": [ 315 ],
	"lcedil": [ 316 ],
	"lceil": [ 8968 ],
	"lcub": [ 123 ],
	"Lcy": [ 1051 ],
	"lcy": [ 1083 ],
	"ldca": [ 10550 ],
	"ldquo": [ 8220 ],
	"ldquor": [ 8222 ],
	"ldrdhar": [ 10599 ],
	"ldrushar": [ 10571 ],
	"ldsh": [ 8626 ],
	"lE": [ 8806 ],
	"le": [ 8804 ],
	"LeftAngleBracket": [ 10216 ],
	"LeftArrow": [ 8592 ],
	"Leftarrow": [ 8656 ],
	"leftarrow": [ 8592 ],
	"LeftArrowBar": [ 8676 ],
	"LeftArrowRightArrow": [ 8646 ],
	"leftarrowtail": [ 8610 ],
	"LeftCeiling": [ 8968 ],
	"LeftDoubleBracket": [ 10214 ],
	"LeftDownTeeVector": [ 10593 ],
	"LeftDownVector": [ 8643 ],
	"LeftDownVectorBar": [ 10585 ],
	"LeftFloor": [ 8970 ],
	"leftharpoondown": [ 8637 ],
	"leftharpoonup": [ 8636 ],
	"leftleftarrows": [ 8647 ],
	"LeftRightArrow": [ 8596 ],
	"Leftrightarrow": [ 8660 ],
	"leftrightarrow": [ 8596 ],
	"leftrightarrows": [ 8646 ],
	"leftrightharpoons": [ 8651 ],
	"leftrightsquigarrow": [ 8621 ],
	"LeftRightVector": [ 10574 ],
	"LeftTee": [ 8867 ],
	"LeftTeeArrow": [ 8612 ],
	"LeftTeeVector": [ 10586 ],
	"leftthreetimes": [ 8907 ],
	"LeftTriangle": [ 8882 ],
	"LeftTriangleBar": [ 10703 ],
	"LeftTriangleEqual": [ 8884 ],
	"LeftUpDownVector": [ 10577 ],
	"LeftUpTeeVector": [ 10592 ],
	"LeftUpVector": [ 8639 ],
	"LeftUpVectorBar": [ 10584 ],
	"LeftVector": [ 8636 ],
	"LeftVectorBar": [ 10578 ],
	"lEg": [ 10891 ],
	"leg": [ 8922 ],
	"leq": [ 8804 ],
	"leqq": [ 8806 ],
	"leqslant": [ 10877 ],
	"les": [ 10877 ],
	"lescc": [ 10920 ],
	"lesdot": [ 10879 ],
	"lesdoto": [ 10881 ],
	"lesdotor": [ 10883 ],
	"lesg": [ 8922, 65024 ],
	"lesges": [ 10899 ],
	"lessapprox": [ 10885 ],
	"lessdot": [ 8918 ],
	"lesseqgtr": [ 8922 ],
	"lesseqqgtr": [ 10891 ],
	"LessEqualGreater": [ 8922 ],
	"LessFullEqual": [ 8806 ],
	"LessGreater": [ 8822 ],
	"lessgtr": [ 8822 ],
	"LessLess": [ 10913 ],
	"lesssim": [ 8818 ],
	"LessSlantEqual": [ 10877 ],
	"LessTilde": [ 8818 ],
	"lfisht": [ 10620 ],
	"lfloor": [ 8970 ],
	"Lfr": [ 120079 ],
	"lfr": [ 120105 ],
	"lg": [ 8822 ],
	"lgE": [ 10897 ],
	"lHar": [ 10594 ],
	"lhard": [ 8637 ],
	"lharu": [ 8636 ],
	"lharul": [ 10602 ],
	"lhblk": [ 9604 ],
	"LJcy": [ 1033 ],
	"ljcy": [ 1113 ],
	"Ll": [ 8920 ],
	"ll": [ 8810 ],
	"llarr": [ 8647 ],
	"llcorner": [ 8990 ],
	"Lleftarrow": [ 8666 ],
	"llhard": [ 10603 ],
	"lltri": [ 9722 ],
	"Lmidot": [ 319 ],
	"lmidot": [ 320 ],
	"lmoust": [ 9136 ],
	"lmoustache": [ 9136 ],
	"lnap": [ 10889 ],
	"lnapprox": [ 10889 ],
	"lnE": [ 8808 ],
	"lne": [ 10887 ],
	"lneq": [ 10887 ],
	"lneqq": [ 8808 ],
	"lnsim": [ 8934 ],
	"loang": [ 10220 ],
	"loarr": [ 8701 ],
	"lobrk": [ 10214 ],
	"LongLeftArrow": [ 10229 ],
	"Longleftarrow": [ 10232 ],
	"longleftarrow": [ 10229 ],
	"LongLeftRightArrow": [ 10231 ],
	"Longleftrightarrow": [ 10234 ],
	"longleftrightarrow": [ 10231 ],
	"longmapsto": [ 10236 ],
	"LongRightArrow": [ 10230 ],
	"Longrightarrow": [ 10233 ],
	"longrightarrow": [ 10230 ],
	"looparrowleft": [ 8619 ],
	"looparrowright": [ 8620 ],
	"lopar": [ 10629 ],
	"Lopf": [ 120131 ],
	"lopf": [ 120157 ],
	"loplus": [ 10797 ],
	"lotimes": [ 10804 ],
	"lowast": [ 8727 ],
	"lowbar": [ 95 ],
	"LowerLeftArrow": [ 8601 ],
	"LowerRightArrow": [ 8600 ],
	"loz": [ 9674 ],
	"lozenge": [ 9674 ],
	"lozf": [ 10731 ],
	"lpar": [ 40 ],
	"lparlt": [ 10643 ],
	"lrarr": [ 8646 ],
	"lrcorner": [ 8991 ],
	"lrhar": [ 8651 ],
	"lrhard": [ 10605 ],
	"lrm": [ 8206 ],
	"lrtri": [ 8895 ],
	"lsaquo": [ 8249 ],
	"Lscr": [ 8466 ],
	"lscr": [ 120001 ],
	"Lsh": [ 8624 ],
	"lsh": [ 8624 ],
	"lsim": [ 8818 ],
	"lsime": [ 10893 ],
	"lsimg": [ 10895 ],
	"lsqb": [ 91 ],
	"lsquo": [ 8216 ],
	"lsquor": [ 8218 ],
	"Lstrok": [ 321 ],
	"lstrok": [ 322 ],
	"LT": [ 60 ],
	"Lt": [ 8810 ],
	"lt": [ 60 ],
	"ltcc": [ 10918 ],
	"ltcir": [ 10873 ],
	"ltdot": [ 8918 ],
	"lthree": [ 8907 ],
	"ltimes": [ 8905 ],
	"ltlarr": [ 10614 ],
	"ltquest": [ 10875 ],
	"ltri": [ 9667 ],
	"ltrie": [ 8884 ],
	"ltrif": [ 9666 ],
	"ltrPar": [ 10646 ],
	"lurdshar": [ 10570 ],
	"luruhar": [ 10598 ],
	"lvertneqq": [ 8808, 65024 ],
	"lvnE": [ 8808, 65024 ],
	"macr": [ 175 ],
	"male": [ 9794 ],
	"malt": [ 10016 ],
	"maltese": [ 10016 ],
	"Map": [ 10501 ],
	"map": [ 8614 ],
	"mapsto": [ 8614 ],
	"mapstodown": [ 8615 ],
	"mapstoleft": [ 8612 ],
	"mapstoup": [ 8613 ],
	"marker": [ 9646 ],
	"mcomma": [ 10793 ],
	"Mcy": [ 1052 ],
	"mcy": [ 1084 ],
	"mdash": [ 8212 ],
	"mDDot": [ 8762 ],
	"measuredangle": [ 8737 ],
	"MediumSpace": [ 8287 ],
	"Mellintrf": [ 8499 ],
	"Mfr": [ 120080 ],
	"mfr": [ 120106 ],
	"mho": [ 8487 ],
	"micro": [ 181 ],
	"mid": [ 8739 ],
	"midast": [ 42 ],
	"midcir": [ 10992 ],
	"middot": [ 183 ],
	"minus": [ 8722 ],
	"minusb": [ 8863 ],
	"minusd": [ 8760 ],
	"minusdu": [ 10794 ],
	"MinusPlus": [ 8723 ],
	"mlcp": [ 10971 ],
	"mldr": [ 8230 ],
	"mnplus": [ 8723 ],
	"models": [ 8871 ],
	"Mopf": [ 120132 ],
	"mopf": [ 120158 ],
	"mp": [ 8723 ],
	"Mscr": [ 8499 ],
	"mscr": [ 120002 ],
	"mstpos": [ 8766 ],
	"Mu": [ 924 ],
	"mu": [ 956 ],
	"multimap": [ 8888 ],
	"mumap": [ 8888 ],
	"nabla": [ 8711 ],
	"Nacute": [ 323 ],
	"nacute": [ 324 ],
	"nang": [ 8736, 8402 ],
	"nap": [ 8777 ],
	"napE": [ 10864, 824 ],
	"napid": [ 8779, 824 ],
	"napos": [ 329 ],
	"napprox": [ 8777 ],
	"natur": [ 9838 ],
	"natural": [ 9838 ],
	"naturals": [ 8469 ],
	"nbsp": [ 160 ],
	"nbump": [ 8782, 824 ],
	"nbumpe": [ 8783, 824 ],
	"ncap": [ 10819 ],
	"Ncaron": [ 327 ],
	"ncaron": [ 328 ],
	"Ncedil": [ 325 ],
	"ncedil": [ 326 ],
	"ncong": [ 8775 ],
	"ncongdot": [ 10861, 824 ],
	"ncup": [ 10818 ],
	"Ncy": [ 1053 ],
	"ncy": [ 1085 ],
	"ndash": [ 8211 ],
	"ne": [ 8800 ],
	"nearhk": [ 10532 ],
	"neArr": [ 8663 ],
	"nearr": [ 8599 ],
	"nearrow": [ 8599 ],
	"nedot": [ 8784, 824 ],
	"NegativeMediumSpace": [ 8203 ],
	"NegativeThickSpace": [ 8203 ],
	"NegativeThinSpace": [ 8203 ],
	"NegativeVeryThinSpace": [ 8203 ],
	"nequiv": [ 8802 ],
	"nesear": [ 10536 ],
	"nesim": [ 8770, 824 ],
	"NestedGreaterGreater": [ 8811 ],
	"NestedLessLess": [ 8810 ],
	"NewLine": [ 10 ],
	"nexist": [ 8708 ],
	"nexists": [ 8708 ],
	"Nfr": [ 120081 ],
	"nfr": [ 120107 ],
	"ngE": [ 8807, 824 ],
	"nge": [ 8817 ],
	"ngeq": [ 8817 ],
	"ngeqq": [ 8807, 824 ],
	"ngeqslant": [ 10878, 824 ],
	"nges": [ 10878, 824 ],
	"nGg": [ 8921, 824 ],
	"ngsim": [ 8821 ],
	"nGt": [ 8811, 8402 ],
	"ngt": [ 8815 ],
	"ngtr": [ 8815 ],
	"nGtv": [ 8811, 824 ],
	"nhArr": [ 8654 ],
	"nharr": [ 8622 ],
	"nhpar": [ 10994 ],
	"ni": [ 8715 ],
	"nis": [ 8956 ],
	"nisd": [ 8954 ],
	"niv": [ 8715 ],
	"NJcy": [ 1034 ],
	"njcy": [ 1114 ],
	"nlArr": [ 8653 ],
	"nlarr": [ 8602 ],
	"nldr": [ 8229 ],
	"nlE": [ 8806, 824 ],
	"nle": [ 8816 ],
	"nLeftarrow": [ 8653 ],
	"nleftarrow": [ 8602 ],
	"nLeftrightarrow": [ 8654 ],
	"nleftrightarrow": [ 8622 ],
	"nleq": [ 8816 ],
	"nleqq": [ 8806, 824 ],
	"nleqslant": [ 10877, 824 ],
	"nles": [ 10877, 824 ],
	"nless": [ 8814 ],
	"nLl": [ 8920, 824 ],
	"nlsim": [ 8820 ],
	"nLt": [ 8810, 8402 ],
	"nlt": [ 8814 ],
	"nltri": [ 8938 ],
	"nltrie": [ 8940 ],
	"nLtv": [ 8810, 824 ],
	"nmid": [ 8740 ],
	"NoBreak": [ 8288 ],
	"NonBreakingSpace": [ 160 ],
	"Nopf": [ 8469 ],
	"nopf": [ 120159 ],
	"Not": [ 10988 ],
	"not": [ 172 ],
	"NotCongruent": [ 8802 ],
	"NotCupCap": [ 8813 ],
	"NotDoubleVerticalBar": [ 8742 ],
	"NotElement": [ 8713 ],
	"NotEqual": [ 8800 ],
	"NotEqualTilde": [ 8770, 824 ],
	"NotExists": [ 8708 ],
	"NotGreater": [ 8815 ],
	"NotGreaterEqual": [ 8817 ],
	"NotGreaterFullEqual": [ 8807, 824 ],
	"NotGreaterGreater": [ 8811, 824 ],
	"NotGreaterLess": [ 8825 ],
	"NotGreaterSlantEqual": [ 10878, 824 ],
	"NotGreaterTilde": [ 8821 ],
	"NotHumpDownHump": [ 8782, 824 ],
	"NotHumpEqual": [ 8783, 824 ],
	"notin": [ 8713 ],
	"notindot": [ 8949, 824 ],
	"notinE": [ 8953, 824 ],
	"notinva": [ 8713 ],
	"notinvb": [ 8951 ],
	"notinvc": [ 8950 ],
	"NotLeftTriangle": [ 8938 ],
	"NotLeftTriangleBar": [ 10703, 824 ],
	"NotLeftTriangleEqual": [ 8940 ],
	"NotLess": [ 8814 ],
	"NotLessEqual": [ 8816 ],
	"NotLessGreater": [ 8824 ],
	"NotLessLess": [ 8810, 824 ],
	"NotLessSlantEqual": [ 10877, 824 ],
	"NotLessTilde": [ 8820 ],
	"NotNestedGreaterGreater": [ 10914, 824 ],
	"NotNestedLessLess": [ 10913, 824 ],
	"notni": [ 8716 ],
	"notniva": [ 8716 ],
	"notnivb": [ 8958 ],
	"notnivc": [ 8957 ],
	"NotPrecedes": [ 8832 ],
	"NotPrecedesEqual": [ 10927, 824 ],
	"NotPrecedesSlantEqual": [ 8928 ],
	"NotReverseElement": [ 8716 ],
	"NotRightTriangle": [ 8939 ],
	"NotRightTriangleBar": [ 10704, 824 ],
	"NotRightTriangleEqual": [ 8941 ],
	"NotSquareSubset": [ 8847, 824 ],
	"NotSquareSubsetEqual": [ 8930 ],
	"NotSquareSuperset": [ 8848, 824 ],
	"NotSquareSupersetEqual": [ 8931 ],
	"NotSubset": [ 8834, 8402 ],
	"NotSubsetEqual": [ 8840 ],
	"NotSucceeds": [ 8833 ],
	"NotSucceedsEqual": [ 10928, 824 ],
	"NotSucceedsSlantEqual": [ 8929 ],
	"NotSucceedsTilde": [ 8831, 824 ],
	"NotSuperset": [ 8835, 8402 ],
	"NotSupersetEqual": [ 8841 ],
	"NotTilde": [ 8769 ],
	"NotTildeEqual": [ 8772 ],
	"NotTildeFullEqual": [ 8775 ],
	"NotTildeTilde": [ 8777 ],
	"NotVerticalBar": [ 8740 ],
	"npar": [ 8742 ],
	"nparallel": [ 8742 ],
	"nparsl": [ 11005, 8421 ],
	"npart": [ 8706, 824 ],
	"npolint": [ 10772 ],
	"npr": [ 8832 ],
	"nprcue": [ 8928 ],
	"npre": [ 10927, 824 ],
	"nprec": [ 8832 ],
	"npreceq": [ 10927, 824 ],
	"nrArr": [ 8655 ],
	"nrarr": [ 8603 ],
	"nrarrc": [ 10547, 824 ],
	"nrarrw": [ 8605, 824 ],
	"nRightarrow": [ 8655 ],
	"nrightarrow": [ 8603 ],
	"nrtri": [ 8939 ],
	"nrtrie": [ 8941 ],
	"nsc": [ 8833 ],
	"nsccue": [ 8929 ],
	"nsce": [ 10928, 824 ],
	"Nscr": [ 119977 ],
	"nscr": [ 120003 ],
	"nshortmid": [ 8740 ],
	"nshortparallel": [ 8742 ],
	"nsim": [ 8769 ],
	"nsime": [ 8772 ],
	"nsimeq": [ 8772 ],
	"nsmid": [ 8740 ],
	"nspar": [ 8742 ],
	"nsqsube": [ 8930 ],
	"nsqsupe": [ 8931 ],
	"nsub": [ 8836 ],
	"nsubE": [ 10949, 824 ],
	"nsube": [ 8840 ],
	"nsubset": [ 8834, 8402 ],
	"nsubseteq": [ 8840 ],
	"nsubseteqq": [ 10949, 824 ],
	"nsucc": [ 8833 ],
	"nsucceq": [ 10928, 824 ],
	"nsup": [ 8837 ],
	"nsupE": [ 10950, 824 ],
	"nsupe": [ 8841 ],
	"nsupset": [ 8835, 8402 ],
	"nsupseteq": [ 8841 ],
	"nsupseteqq": [ 10950, 824 ],
	"ntgl": [ 8825 ],
	"Ntilde": [ 209 ],
	"ntilde": [ 241 ],
	"ntlg": [ 8824 ],
	"ntriangleleft": [ 8938 ],
	"ntrianglelefteq": [ 8940 ],
	"ntriangleright": [ 8939 ],
	"ntrianglerighteq": [ 8941 ],
	"Nu": [ 925 ],
	"nu": [ 957 ],
	"num": [ 35 ],
	"numero": [ 8470 ],
	"numsp": [ 8199 ],
	"nvap": [ 8781, 8402 ],
	"nVDash": [ 8879 ],
	"nVdash": [ 8878 ],
	"nvDash": [ 8877 ],
	"nvdash": [ 8876 ],
	"nvge": [ 8805, 8402 ],
	"nvgt": [ 62, 8402 ],
	"nvHarr": [ 10500 ],
	"nvinfin": [ 10718 ],
	"nvlArr": [ 10498 ],
	"nvle": [ 8804, 8402 ],
	"nvlt": [ 60, 8402 ],
	"nvltrie": [ 8884, 8402 ],
	"nvrArr": [ 10499 ],
	"nvrtrie": [ 8885, 8402 ],
	"nvsim": [ 8764, 8402 ],
	"nwarhk": [ 10531 ],
	"nwArr": [ 8662 ],
	"nwarr": [ 8598 ],
	"nwarrow": [ 8598 ],
	"nwnear": [ 10535 ],
	"Oacute": [ 211 ],
	"oacute": [ 243 ],
	"oast": [ 8859 ],
	"ocir": [ 8858 ],
	"Ocirc": [ 212 ],
	"ocirc": [ 244 ],
	"Ocy": [ 1054 ],
	"ocy": [ 1086 ],
	"odash": [ 8861 ],
	"Odblac": [ 336 ],
	"odblac": [ 337 ],
	"odiv": [ 10808 ],
	"odot": [ 8857 ],
	"odsold": [ 10684 ],
	"OElig": [ 338 ],
	"oelig": [ 339 ],
	"ofcir": [ 10687 ],
	"Ofr": [ 120082 ],
	"ofr": [ 120108 ],
	"ogon": [ 731 ],
	"Ograve": [ 210 ],
	"ograve": [ 242 ],
	"ogt": [ 10689 ],
	"ohbar": [ 10677 ],
	"ohm": [ 937 ],
	"oint": [ 8750 ],
	"olarr": [ 8634 ],
	"olcir": [ 10686 ],
	"olcross": [ 10683 ],
	"oline": [ 8254 ],
	"olt": [ 10688 ],
	"Omacr": [ 332 ],
	"omacr": [ 333 ],
	"Omega": [ 937 ],
	"omega": [ 969 ],
	"Omicron": [ 927 ],
	"omicron": [ 959 ],
	"omid": [ 10678 ],
	"ominus": [ 8854 ],
	"Oopf": [ 120134 ],
	"oopf": [ 120160 ],
	"opar": [ 10679 ],
	"OpenCurlyDoubleQuote": [ 8220 ],
	"OpenCurlyQuote": [ 8216 ],
	"operp": [ 10681 ],
	"oplus": [ 8853 ],
	"Or": [ 10836 ],
	"or": [ 8744 ],
	"orarr": [ 8635 ],
	"ord": [ 10845 ],
	"order": [ 8500 ],
	"orderof": [ 8500 ],
	"ordf": [ 170 ],
	"ordm": [ 186 ],
	"origof": [ 8886 ],
	"oror": [ 10838 ],
	"orslope": [ 10839 ],
	"orv": [ 10843 ],
	"oS": [ 9416 ],
	"Oscr": [ 119978 ],
	"oscr": [ 8500 ],
	"Oslash": [ 216 ],
	"oslash": [ 248 ],
	"osol": [ 8856 ],
	"Otilde": [ 213 ],
	"otilde": [ 245 ],
	"Otimes": [ 10807 ],
	"otimes": [ 8855 ],
	"otimesas": [ 10806 ],
	"Ouml": [ 214 ],
	"ouml": [ 246 ],
	"ovbar": [ 9021 ],
	"OverBar": [ 8254 ],
	"OverBrace": [ 9182 ],
	"OverBracket": [ 9140 ],
	"OverParenthesis": [ 9180 ],
	"par": [ 8741 ],
	"para": [ 182 ],
	"parallel": [ 8741 ],
	"parsim": [ 10995 ],
	"parsl": [ 11005 ],
	"part": [ 8706 ],
	"PartialD": [ 8706 ],
	"Pcy": [ 1055 ],
	"pcy": [ 1087 ],
	"percnt": [ 37 ],
	"period": [ 46 ],
	"permil": [ 8240 ],
	"perp": [ 8869 ],
	"pertenk": [ 8241 ],
	"Pfr": [ 120083 ],
	"pfr": [ 120109 ],
	"Phi": [ 934 ],
	"phi": [ 966 ],
	"phiv": [ 981 ],
	"phmmat": [ 8499 ],
	"phone": [ 9742 ],
	"Pi": [ 928 ],
	"pi": [ 960 ],
	"pitchfork": [ 8916 ],
	"piv": [ 982 ],
	"planck": [ 8463 ],
	"planckh": [ 8462 ],
	"plankv": [ 8463 ],
	"plus": [ 43 ],
	"plusacir": [ 10787 ],
	"plusb": [ 8862 ],
	"pluscir": [ 10786 ],
	"plusdo": [ 8724 ],
	"plusdu": [ 10789 ],
	"pluse": [ 10866 ],
	"PlusMinus": [ 177 ],
	"plusmn": [ 177 ],
	"plussim": [ 10790 ],
	"plustwo": [ 10791 ],
	"pm": [ 177 ],
	"Poincareplane": [ 8460 ],
	"pointint": [ 10773 ],
	"Popf": [ 8473 ],
	"popf": [ 120161 ],
	"pound": [ 163 ],
	"Pr": [ 10939 ],
	"pr": [ 8826 ],
	"prap": [ 10935 ],
	"prcue": [ 8828 ],
	"prE": [ 10931 ],
	"pre": [ 10927 ],
	"prec": [ 8826 ],
	"precapprox": [ 10935 ],
	"preccurlyeq": [ 8828 ],
	"Precedes": [ 8826 ],
	"PrecedesEqual": [ 10927 ],
	"PrecedesSlantEqual": [ 8828 ],
	"PrecedesTilde": [ 8830 ],
	"preceq": [ 10927 ],
	"precnapprox": [ 10937 ],
	"precneqq": [ 10933 ],
	"precnsim": [ 8936 ],
	"precsim": [ 8830 ],
	"Prime": [ 8243 ],
	"prime": [ 8242 ],
	"primes": [ 8473 ],
	"prnap": [ 10937 ],
	"prnE": [ 10933 ],
	"prnsim": [ 8936 ],
	"prod": [ 8719 ],
	"Product": [ 8719 ],
	"profalar": [ 9006 ],
	"profline": [ 8978 ],
	"profsurf": [ 8979 ],
	"prop": [ 8733 ],
	"Proportion": [ 8759 ],
	"Proportional": [ 8733 ],
	"propto": [ 8733 ],
	"prsim": [ 8830 ],
	"prurel": [ 8880 ],
	"Pscr": [ 119979 ],
	"pscr": [ 120005 ],
	"Psi": [ 936 ],
	"psi": [ 968 ],
	"puncsp": [ 8200 ],
	"Qfr": [ 120084 ],
	"qfr": [ 120110 ],
	"qint": [ 10764 ],
	"Qopf": [ 8474 ],
	"qopf": [ 120162 ],
	"qprime": [ 8279 ],
	"Qscr": [ 119980 ],
	"qscr": [ 120006 ],
	"quaternions": [ 8461 ],
	"quatint": [ 10774 ],
	"quest": [ 63 ],
	"questeq": [ 8799 ],
	"QUOT": [ 34 ],
	"quot": [ 34 ],
	"rAarr": [ 8667 ],
	"race": [ 8765, 817 ],
	"Racute": [ 340 ],
	"racute": [ 341 ],
	"radic": [ 8730 ],
	"raemptyv": [ 10675 ],
	"Rang": [ 10219 ],
	"rang": [ 10217 ],
	"rangd": [ 10642 ],
	"range": [ 10661 ],
	"rangle": [ 10217 ],
	"raquo": [ 187 ],
	"Rarr": [ 8608 ],
	"rArr": [ 8658 ],
	"rarr": [ 8594 ],
	"rarrap": [ 10613 ],
	"rarrb": [ 8677 ],
	"rarrbfs": [ 10528 ],
	"rarrc": [ 10547 ],
	"rarrfs": [ 10526 ],
	"rarrhk": [ 8618 ],
	"rarrlp": [ 8620 ],
	"rarrpl": [ 10565 ],
	"rarrsim": [ 10612 ],
	"Rarrtl": [ 10518 ],
	"rarrtl": [ 8611 ],
	"rarrw": [ 8605 ],
	"rAtail": [ 10524 ],
	"ratail": [ 10522 ],
	"ratio": [ 8758 ],
	"rationals": [ 8474 ],
	"RBarr": [ 10512 ],
	"rBarr": [ 10511 ],
	"rbarr": [ 10509 ],
	"rbbrk": [ 10099 ],
	"rbrace": [ 125 ],
	"rbrack": [ 93 ],
	"rbrke": [ 10636 ],
	"rbrksld": [ 10638 ],
	"rbrkslu": [ 10640 ],
	"Rcaron": [ 344 ],
	"rcaron": [ 345 ],
	"Rcedil": [ 342 ],
	"rcedil": [ 343 ],
	"rceil": [ 8969 ],
	"rcub": [ 125 ],
	"Rcy": [ 1056 ],
	"rcy": [ 1088 ],
	"rdca": [ 10551 ],
	"rdldhar": [ 10601 ],
	"rdquo": [ 8221 ],
	"rdquor": [ 8221 ],
	"rdsh": [ 8627 ],
	"Re": [ 8476 ],
	"real": [ 8476 ],
	"realine": [ 8475 ],
	"realpart": [ 8476 ],
	"reals": [ 8477 ],
	"rect": [ 9645 ],
	"REG": [ 174 ],
	"reg": [ 174 ],
	"ReverseElement": [ 8715 ],
	"ReverseEquilibrium": [ 8651 ],
	"ReverseUpEquilibrium": [ 10607 ],
	"rfisht": [ 10621 ],
	"rfloor": [ 8971 ],
	"Rfr": [ 8476 ],
	"rfr": [ 120111 ],
	"rHar": [ 10596 ],
	"rhard": [ 8641 ],
	"rharu": [ 8640 ],
	"rharul": [ 10604 ],
	"Rho": [ 929 ],
	"rho": [ 961 ],
	"rhov": [ 1009 ],
	"RightAngleBracket": [ 10217 ],
	"RightArrow": [ 8594 ],
	"Rightarrow": [ 8658 ],
	"rightarrow": [ 8594 ],
	"RightArrowBar": [ 8677 ],
	"RightArrowLeftArrow": [ 8644 ],
	"rightarrowtail": [ 8611 ],
	"RightCeiling": [ 8969 ],
	"RightDoubleBracket": [ 10215 ],
	"RightDownTeeVector": [ 10589 ],
	"RightDownVector": [ 8642 ],
	"RightDownVectorBar": [ 10581 ],
	"RightFloor": [ 8971 ],
	"rightharpoondown": [ 8641 ],
	"rightharpoonup": [ 8640 ],
	"rightleftarrows": [ 8644 ],
	"rightleftharpoons": [ 8652 ],
	"rightrightarrows": [ 8649 ],
	"rightsquigarrow": [ 8605 ],
	"RightTee": [ 8866 ],
	"RightTeeArrow": [ 8614 ],
	"RightTeeVector": [ 10587 ],
	"rightthreetimes": [ 8908 ],
	"RightTriangle": [ 8883 ],
	"RightTriangleBar": [ 10704 ],
	"RightTriangleEqual": [ 8885 ],
	"RightUpDownVector": [ 10575 ],
	"RightUpTeeVector": [ 10588 ],
	"RightUpVector": [ 8638 ],
	"RightUpVectorBar": [ 10580 ],
	"RightVector": [ 8640 ],
	"RightVectorBar": [ 10579 ],
	"ring": [ 730 ],
	"risingdotseq": [ 8787 ],
	"rlarr": [ 8644 ],
	"rlhar": [ 8652 ],
	"rlm": [ 8207 ],
	"rmoust": [ 9137 ],
	"rmoustache": [ 9137 ],
	"rnmid": [ 10990 ],
	"roang": [ 10221 ],
	"roarr": [ 8702 ],
	"robrk": [ 10215 ],
	"ropar": [ 10630 ],
	"Ropf": [ 8477 ],
	"ropf": [ 120163 ],
	"roplus": [ 10798 ],
	"rotimes": [ 10805 ],
	"RoundImplies": [ 10608 ],
	"rpar": [ 41 ],
	"rpargt": [ 10644 ],
	"rppolint": [ 10770 ],
	"rrarr": [ 8649 ],
	"Rrightarrow": [ 8667 ],
	"rsaquo": [ 8250 ],
	"Rscr": [ 8475 ],
	"rscr": [ 120007 ],
	"Rsh": [ 8625 ],
	"rsh": [ 8625 ],
	"rsqb": [ 93 ],
	"rsquo": [ 8217 ],
	"rsquor": [ 8217 ],
	"rthree": [ 8908 ],
	"rtimes": [ 8906 ],
	"rtri": [ 9657 ],
	"rtrie": [ 8885 ],
	"rtrif": [ 9656 ],
	"rtriltri": [ 10702 ],
	"RuleDelayed": [ 10740 ],
	"ruluhar": [ 10600 ],
	"rx": [ 8478 ],
	"Sacute": [ 346 ],
	"sacute": [ 347 ],
	"sbquo": [ 8218 ],
	"Sc": [ 10940 ],
	"sc": [ 8827 ],
	"scap": [ 10936 ],
	"Scaron": [ 352 ],
	"scaron": [ 353 ],
	"sccue": [ 8829 ],
	"scE": [ 10932 ],
	"sce": [ 10928 ],
	"Scedil": [ 350 ],
	"scedil": [ 351 ],
	"Scirc": [ 348 ],
	"scirc": [ 349 ],
	"scnap": [ 10938 ],
	"scnE": [ 10934 ],
	"scnsim": [ 8937 ],
	"scpolint": [ 10771 ],
	"scsim": [ 8831 ],
	"Scy": [ 1057 ],
	"scy": [ 1089 ],
	"sdot": [ 8901 ],
	"sdotb": [ 8865 ],
	"sdote": [ 10854 ],
	"searhk": [ 10533 ],
	"seArr": [ 8664 ],
	"searr": [ 8600 ],
	"searrow": [ 8600 ],
	"sect": [ 167 ],
	"semi": [ 59 ],
	"seswar": [ 10537 ],
	"setminus": [ 8726 ],
	"setmn": [ 8726 ],
	"sext": [ 10038 ],
	"Sfr": [ 120086 ],
	"sfr": [ 120112 ],
	"sfrown": [ 8994 ],
	"sharp": [ 9839 ],
	"SHCHcy": [ 1065 ],
	"shchcy": [ 1097 ],
	"SHcy": [ 1064 ],
	"shcy": [ 1096 ],
	"ShortDownArrow": [ 8595 ],
	"ShortLeftArrow": [ 8592 ],
	"shortmid": [ 8739 ],
	"shortparallel": [ 8741 ],
	"ShortRightArrow": [ 8594 ],
	"ShortUpArrow": [ 8593 ],
	"shy": [ 173 ],
	"Sigma": [ 931 ],
	"sigma": [ 963 ],
	"sigmaf": [ 962 ],
	"sigmav": [ 962 ],
	"sim": [ 8764 ],
	"simdot": [ 10858 ],
	"sime": [ 8771 ],
	"simeq": [ 8771 ],
	"simg": [ 10910 ],
	"simgE": [ 10912 ],
	"siml": [ 10909 ],
	"simlE": [ 10911 ],
	"simne": [ 8774 ],
	"simplus": [ 10788 ],
	"simrarr": [ 10610 ],
	"slarr": [ 8592 ],
	"SmallCircle": [ 8728 ],
	"smallsetminus": [ 8726 ],
	"smashp": [ 10803 ],
	"smeparsl": [ 10724 ],
	"smid": [ 8739 ],
	"smile": [ 8995 ],
	"smt": [ 10922 ],
	"smte": [ 10924 ],
	"smtes": [ 10924, 65024 ],
	"SOFTcy": [ 1068 ],
	"softcy": [ 1100 ],
	"sol": [ 47 ],
	"solb": [ 10692 ],
	"solbar": [ 9023 ],
	"Sopf": [ 120138 ],
	"sopf": [ 120164 ],
	"spades": [ 9824 ],
	"spadesuit": [ 9824 ],
	"spar": [ 8741 ],
	"sqcap": [ 8851 ],
	"sqcaps": [ 8851, 65024 ],
	"sqcup": [ 8852 ],
	"sqcups": [ 8852, 65024 ],
	"Sqrt": [ 8730 ],
	"sqsub": [ 8847 ],
	"sqsube": [ 8849 ],
	"sqsubset": [ 8847 ],
	"sqsubseteq": [ 8849 ],
	"sqsup": [ 8848 ],
	"sqsupe": [ 8850 ],
	"sqsupset": [ 8848 ],
	"sqsupseteq": [ 8850 ],
	"squ": [ 9633 ],
	"Square": [ 9633 ],
	"square": [ 9633 ],
	"SquareIntersection": [ 8851 ],
	"SquareSubset": [ 8847 ],
	"SquareSubsetEqual": [ 8849 ],
	"SquareSuperset": [ 8848 ],
	"SquareSupersetEqual": [ 8850 ],
	"SquareUnion": [ 8852 ],
	"squarf": [ 9642 ],
	"squf": [ 9642 ],
	"srarr": [ 8594 ],
	"Sscr": [ 119982 ],
	"sscr": [ 120008 ],
	"ssetmn": [ 8726 ],
	"ssmile": [ 8995 ],
	"sstarf": [ 8902 ],
	"Star": [ 8902 ],
	"star": [ 9734 ],
	"starf": [ 9733 ],
	"straightepsilon": [ 1013 ],
	"straightphi": [ 981 ],
	"strns": [ 175 ],
	"Sub": [ 8912 ],
	"sub": [ 8834 ],
	"subdot": [ 10941 ],
	"subE": [ 10949 ],
	"sube": [ 8838 ],
	"subedot": [ 10947 ],
	"submult": [ 10945 ],
	"subnE": [ 10955 ],
	"subne": [ 8842 ],
	"subplus": [ 10943 ],
	"subrarr": [ 10617 ],
	"Subset": [ 8912 ],
	"subset": [ 8834 ],
	"subseteq": [ 8838 ],
	"subseteqq": [ 10949 ],
	"SubsetEqual": [ 8838 ],
	"subsetneq": [ 8842 ],
	"subsetneqq": [ 10955 ],
	"subsim": [ 10951 ],
	"subsub": [ 10965 ],
	"subsup": [ 10963 ],
	"succ": [ 8827 ],
	"succapprox": [ 10936 ],
	"succcurlyeq": [ 8829 ],
	"Succeeds": [ 8827 ],
	"SucceedsEqual": [ 10928 ],
	"SucceedsSlantEqual": [ 8829 ],
	"SucceedsTilde": [ 8831 ],
	"succeq": [ 10928 ],
	"succnapprox": [ 10938 ],
	"succneqq": [ 10934 ],
	"succnsim": [ 8937 ],
	"succsim": [ 8831 ],
	"SuchThat": [ 8715 ],
	"Sum": [ 8721 ],
	"sum": [ 8721 ],
	"sung": [ 9834 ],
	"Sup": [ 8913 ],
	"sup": [ 8835 ],
	"sup1": [ 185 ],
	"sup2": [ 178 ],
	"sup3": [ 179 ],
	"supdot": [ 10942 ],
	"supdsub": [ 10968 ],
	"supE": [ 10950 ],
	"supe": [ 8839 ],
	"supedot": [ 10948 ],
	"Superset": [ 8835 ],
	"SupersetEqual": [ 8839 ],
	"suphsol": [ 10185 ],
	"suphsub": [ 10967 ],
	"suplarr": [ 10619 ],
	"supmult": [ 10946 ],
	"supnE": [ 10956 ],
	"supne": [ 8843 ],
	"supplus": [ 10944 ],
	"Supset": [ 8913 ],
	"supset": [ 8835 ],
	"supseteq": [ 8839 ],
	"supseteqq": [ 10950 ],
	"supsetneq": [ 8843 ],
	"supsetneqq": [ 10956 ],
	"supsim": [ 10952 ],
	"supsub": [ 10964 ],
	"supsup": [ 10966 ],
	"swarhk": [ 10534 ],
	"swArr": [ 8665 ],
	"swarr": [ 8601 ],
	"swarrow": [ 8601 ],
	"swnwar": [ 10538 ],
	"szlig": [ 223 ],
	"Tab": [ 9 ],
	"target": [ 8982 ],
	"Tau": [ 932 ],
	"tau": [ 964 ],
	"tbrk": [ 9140 ],
	"Tcaron": [ 356 ],
	"tcaron": [ 357 ],
	"Tcedil": [ 354 ],
	"tcedil": [ 355 ],
	"Tcy": [ 1058 ],
	"tcy": [ 1090 ],
	"tdot": [ 8411 ],
	"telrec": [ 8981 ],
	"Tfr": [ 120087 ],
	"tfr": [ 120113 ],
	"there4": [ 8756 ],
	"Therefore": [ 8756 ],
	"therefore": [ 8756 ],
	"Theta": [ 920 ],
	"theta": [ 952 ],
	"thetasym": [ 977 ],
	"thetav": [ 977 ],
	"thickapprox": [ 8776 ],
	"thicksim": [ 8764 ],
	"ThickSpace": [ 8287, 8202 ],
	"thinsp": [ 8201 ],
	"ThinSpace": [ 8201 ],
	"thkap": [ 8776 ],
	"thksim": [ 8764 ],
	"THORN": [ 222 ],
	"thorn": [ 254 ],
	"Tilde": [ 8764 ],
	"tilde": [ 732 ],
	"TildeEqual": [ 8771 ],
	"TildeFullEqual": [ 8773 ],
	"TildeTilde": [ 8776 ],
	"times": [ 215 ],
	"timesb": [ 8864 ],
	"timesbar": [ 10801 ],
	"timesd": [ 10800 ],
	"tint": [ 8749 ],
	"toea": [ 10536 ],
	"top": [ 8868 ],
	"topbot": [ 9014 ],
	"topcir": [ 10993 ],
	"Topf": [ 120139 ],
	"topf": [ 120165 ],
	"topfork": [ 10970 ],
	"tosa": [ 10537 ],
	"tprime": [ 8244 ],
	"TRADE": [ 8482 ],
	"trade": [ 8482 ],
	"triangle": [ 9653 ],
	"triangledown": [ 9663 ],
	"triangleleft": [ 9667 ],
	"trianglelefteq": [ 8884 ],
	"triangleq": [ 8796 ],
	"triangleright": [ 9657 ],
	"trianglerighteq": [ 8885 ],
	"tridot": [ 9708 ],
	"trie": [ 8796 ],
	"triminus": [ 10810 ],
	"TripleDot": [ 8411 ],
	"triplus": [ 10809 ],
	"trisb": [ 10701 ],
	"tritime": [ 10811 ],
	"trpezium": [ 9186 ],
	"Tscr": [ 119983 ],
	"tscr": [ 120009 ],
	"TScy": [ 1062 ],
	"tscy": [ 1094 ],
	"TSHcy": [ 1035 ],
	"tshcy": [ 1115 ],
	"Tstrok": [ 358 ],
	"tstrok": [ 359 ],
	"twixt": [ 8812 ],
	"twoheadleftarrow": [ 8606 ],
	"twoheadrightarrow": [ 8608 ],
	"Uacute": [ 218 ],
	"uacute": [ 250 ],
	"Uarr": [ 8607 ],
	"uArr": [ 8657 ],
	"uarr": [ 8593 ],
	"Uarrocir": [ 10569 ],
	"Ubrcy": [ 1038 ],
	"ubrcy": [ 1118 ],
	"Ubreve": [ 364 ],
	"ubreve": [ 365 ],
	"Ucirc": [ 219 ],
	"ucirc": [ 251 ],
	"Ucy": [ 1059 ],
	"ucy": [ 1091 ],
	"udarr": [ 8645 ],
	"Udblac": [ 368 ],
	"udblac": [ 369 ],
	"udhar": [ 10606 ],
	"ufisht": [ 10622 ],
	"Ufr": [ 120088 ],
	"ufr": [ 120114 ],
	"Ugrave": [ 217 ],
	"ugrave": [ 249 ],
	"uHar": [ 10595 ],
	"uharl": [ 8639 ],
	"uharr": [ 8638 ],
	"uhblk": [ 9600 ],
	"ulcorn": [ 8988 ],
	"ulcorner": [ 8988 ],
	"ulcrop": [ 8975 ],
	"ultri": [ 9720 ],
	"Umacr": [ 362 ],
	"umacr": [ 363 ],
	"uml": [ 168 ],
	"UnderBar": [ 95 ],
	"UnderBrace": [ 9183 ],
	"UnderBracket": [ 9141 ],
	"UnderParenthesis": [ 9181 ],
	"Union": [ 8899 ],
	"UnionPlus": [ 8846 ],
	"Uogon": [ 370 ],
	"uogon": [ 371 ],
	"Uopf": [ 120140 ],
	"uopf": [ 120166 ],
	"UpArrow": [ 8593 ],
	"Uparrow": [ 8657 ],
	"uparrow": [ 8593 ],
	"UpArrowBar": [ 10514 ],
	"UpArrowDownArrow": [ 8645 ],
	"UpDownArrow": [ 8597 ],
	"Updownarrow": [ 8661 ],
	"updownarrow": [ 8597 ],
	"UpEquilibrium": [ 10606 ],
	"upharpoonleft": [ 8639 ],
	"upharpoonright": [ 8638 ],
	"uplus": [ 8846 ],
	"UpperLeftArrow": [ 8598 ],
	"UpperRightArrow": [ 8599 ],
	"Upsi": [ 978 ],
	"upsi": [ 965 ],
	"upsih": [ 978 ],
	"Upsilon": [ 933 ],
	"upsilon": [ 965 ],
	"UpTee": [ 8869 ],
	"UpTeeArrow": [ 8613 ],
	"upuparrows": [ 8648 ],
	"urcorn": [ 8989 ],
	"urcorner": [ 8989 ],
	"urcrop": [ 8974 ],
	"Uring": [ 366 ],
	"uring": [ 367 ],
	"urtri": [ 9721 ],
	"Uscr": [ 119984 ],
	"uscr": [ 120010 ],
	"utdot": [ 8944 ],
	"Utilde": [ 360 ],
	"utilde": [ 361 ],
	"utri": [ 9653 ],
	"utrif": [ 9652 ],
	"uuarr": [ 8648 ],
	"Uuml": [ 220 ],
	"uuml": [ 252 ],
	"uwangle": [ 10663 ],
	"vangrt": [ 10652 ],
	"varepsilon": [ 1013 ],
	"varkappa": [ 1008 ],
	"varnothing": [ 8709 ],
	"varphi": [ 981 ],
	"varpi": [ 982 ],
	"varpropto": [ 8733 ],
	"vArr": [ 8661 ],
	"varr": [ 8597 ],
	"varrho": [ 1009 ],
	"varsigma": [ 962 ],
	"varsubsetneq": [ 8842, 65024 ],
	"varsubsetneqq": [ 10955, 65024 ],
	"varsupsetneq": [ 8843, 65024 ],
	"varsupsetneqq": [ 10956, 65024 ],
	"vartheta": [ 977 ],
	"vartriangleleft": [ 8882 ],
	"vartriangleright": [ 8883 ],
	"Vbar": [ 10987 ],
	"vBar": [ 10984 ],
	"vBarv": [ 10985 ],
	"Vcy": [ 1042 ],
	"vcy": [ 1074 ],
	"VDash": [ 8875 ],
	"Vdash": [ 8873 ],
	"vDash": [ 8872 ],
	"vdash": [ 8866 ],
	"Vdashl": [ 10982 ],
	"Vee": [ 8897 ],
	"vee": [ 8744 ],
	"veebar": [ 8891 ],
	"veeeq": [ 8794 ],
	"vellip": [ 8942 ],
	"Verbar": [ 8214 ],
	"verbar": [ 124 ],
	"Vert": [ 8214 ],
	"vert": [ 124 ],
	"VerticalBar": [ 8739 ],
	"VerticalLine": [ 124 ],
	"VerticalSeparator": [ 10072 ],
	"VerticalTilde": [ 8768 ],
	"VeryThinSpace": [ 8202 ],
	"Vfr": [ 120089 ],
	"vfr": [ 120115 ],
	"vltri": [ 8882 ],
	"vnsub": [ 8834, 8402 ],
	"vnsup": [ 8835, 8402 ],
	"Vopf": [ 120141 ],
	"vopf": [ 120167 ],
	"vprop": [ 8733 ],
	"vrtri": [ 8883 ],
	"Vscr": [ 119985 ],
	"vscr": [ 120011 ],
	"vsubnE": [ 10955, 65024 ],
	"vsubne": [ 8842, 65024 ],
	"vsupnE": [ 10956, 65024 ],
	"vsupne": [ 8843, 65024 ],
	"Vvdash": [ 8874 ],
	"vzigzag": [ 10650 ],
	"Wcirc": [ 372 ],
	"wcirc": [ 373 ],
	"wedbar": [ 10847 ],
	"Wedge": [ 8896 ],
	"wedge": [ 8743 ],
	"wedgeq": [ 8793 ],
	"weierp": [ 8472 ],
	"Wfr": [ 120090 ],
	"wfr": [ 120116 ],
	"Wopf": [ 120142 ],
	"wopf": [ 120168 ],
	"wp": [ 8472 ],
	"wr": [ 8768 ],
	"wreath": [ 8768 ],
	"Wscr": [ 119986 ],
	"wscr": [ 120012 ],
	"xcap": [ 8898 ],
	"xcirc": [ 9711 ],
	"xcup": [ 8899 ],
	"xdtri": [ 9661 ],
	"Xfr": [ 120091 ],
	"xfr": [ 120117 ],
	"xhArr": [ 10234 ],
	"xharr": [ 10231 ],
	"Xi": [ 926 ],
	"xi": [ 958 ],
	"xlArr": [ 10232 ],
	"xlarr": [ 10229 ],
	"xmap": [ 10236 ],
	"xnis": [ 8955 ],
	"xodot": [ 10752 ],
	"Xopf": [ 120143 ],
	"xopf": [ 120169 ],
	"xoplus": [ 10753 ],
	"xotime": [ 10754 ],
	"xrArr": [ 10233 ],
	"xrarr": [ 10230 ],
	"Xscr": [ 119987 ],
	"xscr": [ 120013 ],
	"xsqcup": [ 10758 ],
	"xuplus": [ 10756 ],
	"xutri": [ 9651 ],
	"xvee": [ 8897 ],
	"xwedge": [ 8896 ],
	"Yacute": [ 221 ],
	"yacute": [ 253 ],
	"YAcy": [ 1071 ],
	"yacy": [ 1103 ],
	"Ycirc": [ 374 ],
	"ycirc": [ 375 ],
	"Ycy": [ 1067 ],
	"ycy": [ 1099 ],
	"yen": [ 165 ],
	"Yfr": [ 120092 ],
	"yfr": [ 120118 ],
	"YIcy": [ 1031 ],
	"yicy": [ 1111 ],
	"Yopf": [ 120144 ],
	"yopf": [ 120170 ],
	"Yscr": [ 119988 ],
	"yscr": [ 120014 ],
	"YUcy": [ 1070 ],
	"yucy": [ 1102 ],
	"Yuml": [ 376 ],
	"yuml": [ 255 ],
	"Zacute": [ 377 ],
	"zacute": [ 378 ],
	"Zcaron": [ 381 ],
	"zcaron": [ 382 ],
	"Zcy": [ 1047 ],
	"zcy": [ 1079 ],
	"Zdot": [ 379 ],
	"zdot": [ 380 ],
	"zeetrf": [ 8488 ],
	"ZeroWidthSpace": [ 8203 ],
	"Zeta": [ 918 ],
	"zeta": [ 950 ],
	"Zfr": [ 8488 ],
	"zfr": [ 120119 ],
	"ZHcy": [ 1046 ],
	"zhcy": [ 1078 ],
	"zigrarr": [ 8669 ],
	"Zopf": [ 8484 ],
	"zopf": [ 120171 ],
	"Zscr": [ 119989 ],
	"zscr": [ 120015 ],
	"zwj": [ 8205 ],
	"zwnj": [ 8204 ]
}
},{}],4:[function(require,module,exports){
// the raw mapping of special names to unicode values
var by_name =
exports.by_name = require("../data/entities.json");

// index the reverse mappings immediately
var by_code = exports.by_code = {};
Object.keys(by_name).forEach(function(n) {
	by_name[n].forEach(function(c) {
		if (by_code[c] == null) by_code[c] = [];
		by_code[c].push(n);
	});
});

// a few regular expressions for parsing entities
var hex_value_regex = /^0?x([a-f0-9]+)$/i,
	entity_regex = /&(?:#x[a-f0-9]+|#[0-9]+|[a-z0-9]+);?/ig,
	dec_entity_regex = /^&#([0-9]+);?$/i,
	hex_entity_regex = /^&#x([a-f0-9]+);?$/i,
	spec_entity_regex = /^&([a-z0-9]+);?$/i;

// converts all entities found in string to specific format
var normalizeEntities =
exports.normalizeEntities = function(str, format) {
	return str.replace(entity_regex, function(entity) {
		return convert(entity, format) || entity;
	});
}

// converts all entities and higher order utf-8 chars to format
exports.normalizeXML = function(str, format) {
	var i, code, res;
	
	// convert entities first
	str = normalizeEntities(str, format);

	// find all characters above ASCII and replace, backwards
	for (i = str.length - 1; i >= 0; i--) {
		code = str.charCodeAt(i);
		if (code > 127) {
			res = convert(str[i], format);
			str = str.substr(0, i) + (res != null ? res : str[i]) + str.substr(i + 1);
		}
	}

	return str;
}

// converts single value into utf-8 character or entity equivalent
var convert =
exports.convert = function(s, format) {
	var code, name;
	if (format == null) format = "html";

	code = toCharCode(s);
	if (code === false) return null;

	switch(format) {
		// only decimal entities
		case "xml":
		case "xhtml":
		case "num":
		case "numeric":
			return toEntity(code);

		// only hex entities
		case "hex":
			return toEntity(code, true);

		// first special, then decimal
		case "html":
			return toEntity((name = cton(code)) != null ? name : code);

		// only special entities
		case "name":
		case "special":
			return (name = cton(code)) != null ? toEntity(name) : null;

		// utf-8 character
		case "char":
		case "character":
		case "utf-8":
		case "UTF-8":
			return String.fromCharCode(code);

		// regular number
		case "code":
			return code;
	}

	return null;
}

// code to name
var cton =
exports.codeToName = function(c) {
	var n = by_code[c.toString()];
	return n != null ? n[0] : null;
}

// name to code
var ntoc =
exports.nameToCode = function(n) {
	var c = by_name[n]
	return c != null ? c[0] : null;
}

// parse an array of inputs and returns the equivalent
// unicode decimal or false if invalid
var toCharCode =
exports.toCharCode = function(s) {
	var m, code;

	if (typeof s === "string") {
		if (s === "") return false;

		// regular char
		if (s.length === 1) return s.charCodeAt(0);

		// special entity
		if (m = spec_entity_regex.exec(s)) {
			return ntoc(m[1]) || false;
		}

		// decimal entity
		if (m = dec_entity_regex.exec(s)) {
			return parseInt(m[1], 10);
		}

		// hex entity
		if (m = hex_entity_regex.exec(s)) {
			return parseInt(m[1], 16);
		}

		// hex value
		if (m = hex_value_regex.exec(s)) {
			return parseInt(m[1], 16);
		}

		// otherwise look it up as a special entity name
		return ntoc(s) || false;
	}

	if (typeof s === "number") return s;

	return false;
}

// converts string or number to valid entity
var toEntity =
exports.toEntity = function(n, hex) {
	return "&" + (
		typeof n === "number" ?
		"#" + (hex ? "x" : "") +
		n.toString(hex ? 16 : 10).toUpperCase() : n
	) + ";";
}
},{"../data/entities.json":3}],5:[function(require,module,exports){
(function (global){
//     Underscore.js 1.9.1
//     http://underscorejs.org
//     (c) 2009-2018 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` (`self`) in the browser, `global`
  // on the server, or `this` in some virtual machines. We use `self`
  // instead of `window` for `WebWorker` support.
  var root = typeof self == 'object' && self.self === self && self ||
            typeof global == 'object' && global.global === global && global ||
            this ||
            {};

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype;
  var SymbolProto = typeof Symbol !== 'undefined' ? Symbol.prototype : null;

  // Create quick reference variables for speed access to core prototypes.
  var push = ArrayProto.push,
      slice = ArrayProto.slice,
      toString = ObjProto.toString,
      hasOwnProperty = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var nativeIsArray = Array.isArray,
      nativeKeys = Object.keys,
      nativeCreate = Object.create;

  // Naked function reference for surrogate-prototype-swapping.
  var Ctor = function(){};

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for their old module API. If we're in
  // the browser, add `_` as a global object.
  // (`nodeType` is checked to ensure that `module`
  // and `exports` are not HTML elements.)
  if (typeof exports != 'undefined' && !exports.nodeType) {
    if (typeof module != 'undefined' && !module.nodeType && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.9.1';

  // Internal function that returns an efficient (for current engines) version
  // of the passed-in callback, to be repeatedly applied in other Underscore
  // functions.
  var optimizeCb = function(func, context, argCount) {
    if (context === void 0) return func;
    switch (argCount == null ? 3 : argCount) {
      case 1: return function(value) {
        return func.call(context, value);
      };
      // The 2-argument case is omitted because we’re not using it.
      case 3: return function(value, index, collection) {
        return func.call(context, value, index, collection);
      };
      case 4: return function(accumulator, value, index, collection) {
        return func.call(context, accumulator, value, index, collection);
      };
    }
    return function() {
      return func.apply(context, arguments);
    };
  };

  var builtinIteratee;

  // An internal function to generate callbacks that can be applied to each
  // element in a collection, returning the desired result — either `identity`,
  // an arbitrary callback, a property matcher, or a property accessor.
  var cb = function(value, context, argCount) {
    if (_.iteratee !== builtinIteratee) return _.iteratee(value, context);
    if (value == null) return _.identity;
    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
    if (_.isObject(value) && !_.isArray(value)) return _.matcher(value);
    return _.property(value);
  };

  // External wrapper for our callback generator. Users may customize
  // `_.iteratee` if they want additional predicate/iteratee shorthand styles.
  // This abstraction hides the internal-only argCount argument.
  _.iteratee = builtinIteratee = function(value, context) {
    return cb(value, context, Infinity);
  };

  // Some functions take a variable number of arguments, or a few expected
  // arguments at the beginning and then a variable number of values to operate
  // on. This helper accumulates all remaining arguments past the function’s
  // argument length (or an explicit `startIndex`), into an array that becomes
  // the last argument. Similar to ES6’s "rest parameter".
  var restArguments = function(func, startIndex) {
    startIndex = startIndex == null ? func.length - 1 : +startIndex;
    return function() {
      var length = Math.max(arguments.length - startIndex, 0),
          rest = Array(length),
          index = 0;
      for (; index < length; index++) {
        rest[index] = arguments[index + startIndex];
      }
      switch (startIndex) {
        case 0: return func.call(this, rest);
        case 1: return func.call(this, arguments[0], rest);
        case 2: return func.call(this, arguments[0], arguments[1], rest);
      }
      var args = Array(startIndex + 1);
      for (index = 0; index < startIndex; index++) {
        args[index] = arguments[index];
      }
      args[startIndex] = rest;
      return func.apply(this, args);
    };
  };

  // An internal function for creating a new object that inherits from another.
  var baseCreate = function(prototype) {
    if (!_.isObject(prototype)) return {};
    if (nativeCreate) return nativeCreate(prototype);
    Ctor.prototype = prototype;
    var result = new Ctor;
    Ctor.prototype = null;
    return result;
  };

  var shallowProperty = function(key) {
    return function(obj) {
      return obj == null ? void 0 : obj[key];
    };
  };

  var has = function(obj, path) {
    return obj != null && hasOwnProperty.call(obj, path);
  }

  var deepGet = function(obj, path) {
    var length = path.length;
    for (var i = 0; i < length; i++) {
      if (obj == null) return void 0;
      obj = obj[path[i]];
    }
    return length ? obj : void 0;
  };

  // Helper for collection methods to determine whether a collection
  // should be iterated as an array or as an object.
  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
  var getLength = shallowProperty('length');
  var isArrayLike = function(collection) {
    var length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  };

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles raw objects in addition to array-likes. Treats all
  // sparse array-likes as if they were dense.
  _.each = _.forEach = function(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    var i, length;
    if (isArrayLike(obj)) {
      for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
      }
    } else {
      var keys = _.keys(obj);
      for (i = 0, length = keys.length; i < length; i++) {
        iteratee(obj[keys[i]], keys[i], obj);
      }
    }
    return obj;
  };

  // Return the results of applying the iteratee to each element.
  _.map = _.collect = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length,
        results = Array(length);
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      results[index] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Create a reducing function iterating left or right.
  var createReduce = function(dir) {
    // Wrap code that reassigns argument variables in a separate function than
    // the one that accesses `arguments.length` to avoid a perf hit. (#1991)
    var reducer = function(obj, iteratee, memo, initial) {
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length,
          index = dir > 0 ? 0 : length - 1;
      if (!initial) {
        memo = obj[keys ? keys[index] : index];
        index += dir;
      }
      for (; index >= 0 && index < length; index += dir) {
        var currentKey = keys ? keys[index] : index;
        memo = iteratee(memo, obj[currentKey], currentKey, obj);
      }
      return memo;
    };

    return function(obj, iteratee, memo, context) {
      var initial = arguments.length >= 3;
      return reducer(obj, optimizeCb(iteratee, context, 4), memo, initial);
    };
  };

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`.
  _.reduce = _.foldl = _.inject = createReduce(1);

  // The right-associative version of reduce, also known as `foldr`.
  _.reduceRight = _.foldr = createReduce(-1);

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, predicate, context) {
    var keyFinder = isArrayLike(obj) ? _.findIndex : _.findKey;
    var key = keyFinder(obj, predicate, context);
    if (key !== void 0 && key !== -1) return obj[key];
  };

  // Return all the elements that pass a truth test.
  // Aliased as `select`.
  _.filter = _.select = function(obj, predicate, context) {
    var results = [];
    predicate = cb(predicate, context);
    _.each(obj, function(value, index, list) {
      if (predicate(value, index, list)) results.push(value);
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, predicate, context) {
    return _.filter(obj, _.negate(cb(predicate)), context);
  };

  // Determine whether all of the elements match a truth test.
  // Aliased as `all`.
  _.every = _.all = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (!predicate(obj[currentKey], currentKey, obj)) return false;
    }
    return true;
  };

  // Determine if at least one element in the object matches a truth test.
  // Aliased as `any`.
  _.some = _.any = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (predicate(obj[currentKey], currentKey, obj)) return true;
    }
    return false;
  };

  // Determine if the array or object contains a given item (using `===`).
  // Aliased as `includes` and `include`.
  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
    if (!isArrayLike(obj)) obj = _.values(obj);
    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
    return _.indexOf(obj, item, fromIndex) >= 0;
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = restArguments(function(obj, path, args) {
    var contextPath, func;
    if (_.isFunction(path)) {
      func = path;
    } else if (_.isArray(path)) {
      contextPath = path.slice(0, -1);
      path = path[path.length - 1];
    }
    return _.map(obj, function(context) {
      var method = func;
      if (!method) {
        if (contextPath && contextPath.length) {
          context = deepGet(context, contextPath);
        }
        if (context == null) return void 0;
        method = context[path];
      }
      return method == null ? method : method.apply(context, args);
    });
  });

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, _.property(key));
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs) {
    return _.filter(obj, _.matcher(attrs));
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
    return _.find(obj, _.matcher(attrs));
  };

  // Return the maximum element (or element-based computation).
  _.max = function(obj, iteratee, context) {
    var result = -Infinity, lastComputed = -Infinity,
        value, computed;
    if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value > result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
          result = v;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iteratee, context) {
    var result = Infinity, lastComputed = Infinity,
        value, computed;
    if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value < result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed < lastComputed || computed === Infinity && result === Infinity) {
          result = v;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Shuffle a collection.
  _.shuffle = function(obj) {
    return _.sample(obj, Infinity);
  };

  // Sample **n** random values from a collection using the modern version of the
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
  // If **n** is not specified, returns a single random element.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
    if (n == null || guard) {
      if (!isArrayLike(obj)) obj = _.values(obj);
      return obj[_.random(obj.length - 1)];
    }
    var sample = isArrayLike(obj) ? _.clone(obj) : _.values(obj);
    var length = getLength(sample);
    n = Math.max(Math.min(n, length), 0);
    var last = length - 1;
    for (var index = 0; index < n; index++) {
      var rand = _.random(index, last);
      var temp = sample[index];
      sample[index] = sample[rand];
      sample[rand] = temp;
    }
    return sample.slice(0, n);
  };

  // Sort the object's values by a criterion produced by an iteratee.
  _.sortBy = function(obj, iteratee, context) {
    var index = 0;
    iteratee = cb(iteratee, context);
    return _.pluck(_.map(obj, function(value, key, list) {
      return {
        value: value,
        index: index++,
        criteria: iteratee(value, key, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior, partition) {
    return function(obj, iteratee, context) {
      var result = partition ? [[], []] : {};
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index) {
        var key = iteratee(value, index, obj);
        behavior(result, value, key);
      });
      return result;
    };
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, value, key) {
    if (has(result, key)) result[key].push(value); else result[key] = [value];
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, value, key) {
    result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, value, key) {
    if (has(result, key)) result[key]++; else result[key] = 1;
  });

  var reStrSymbol = /[^\ud800-\udfff]|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff]/g;
  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (_.isString(obj)) {
      // Keep surrogate pair characters together
      return obj.match(reStrSymbol);
    }
    if (isArrayLike(obj)) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
  };

  // Split a collection into two arrays: one whose elements all satisfy the given
  // predicate, and one whose elements all do not satisfy the predicate.
  _.partition = group(function(result, value, pass) {
    result[pass ? 0 : 1].push(value);
  }, true);

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null || array.length < 1) return n == null ? void 0 : [];
    if (n == null || guard) return array[0];
    return _.initial(array, array.length - n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array.
  _.last = function(array, n, guard) {
    if (array == null || array.length < 1) return n == null ? void 0 : [];
    if (n == null || guard) return array[array.length - 1];
    return _.rest(array, Math.max(0, array.length - n));
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, n == null || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, Boolean);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, strict, output) {
    output = output || [];
    var idx = output.length;
    for (var i = 0, length = getLength(input); i < length; i++) {
      var value = input[i];
      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
        // Flatten current level of array or arguments object.
        if (shallow) {
          var j = 0, len = value.length;
          while (j < len) output[idx++] = value[j++];
        } else {
          flatten(value, shallow, strict, output);
          idx = output.length;
        }
      } else if (!strict) {
        output[idx++] = value;
      }
    }
    return output;
  };

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, false);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = restArguments(function(array, otherArrays) {
    return _.difference(array, otherArrays);
  });

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // The faster algorithm will not work with an iteratee if the iteratee
  // is not a one-to-one function, so providing an iteratee will disable
  // the faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
    if (!_.isBoolean(isSorted)) {
      context = iteratee;
      iteratee = isSorted;
      isSorted = false;
    }
    if (iteratee != null) iteratee = cb(iteratee, context);
    var result = [];
    var seen = [];
    for (var i = 0, length = getLength(array); i < length; i++) {
      var value = array[i],
          computed = iteratee ? iteratee(value, i, array) : value;
      if (isSorted && !iteratee) {
        if (!i || seen !== computed) result.push(value);
        seen = computed;
      } else if (iteratee) {
        if (!_.contains(seen, computed)) {
          seen.push(computed);
          result.push(value);
        }
      } else if (!_.contains(result, value)) {
        result.push(value);
      }
    }
    return result;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = restArguments(function(arrays) {
    return _.uniq(flatten(arrays, true, true));
  });

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var result = [];
    var argsLength = arguments.length;
    for (var i = 0, length = getLength(array); i < length; i++) {
      var item = array[i];
      if (_.contains(result, item)) continue;
      var j;
      for (j = 1; j < argsLength; j++) {
        if (!_.contains(arguments[j], item)) break;
      }
      if (j === argsLength) result.push(item);
    }
    return result;
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = restArguments(function(array, rest) {
    rest = flatten(rest, true, true);
    return _.filter(array, function(value){
      return !_.contains(rest, value);
    });
  });

  // Complement of _.zip. Unzip accepts an array of arrays and groups
  // each array's elements on shared indices.
  _.unzip = function(array) {
    var length = array && _.max(array, getLength).length || 0;
    var result = Array(length);

    for (var index = 0; index < length; index++) {
      result[index] = _.pluck(array, index);
    }
    return result;
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = restArguments(_.unzip);

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values. Passing by pairs is the reverse of _.pairs.
  _.object = function(list, values) {
    var result = {};
    for (var i = 0, length = getLength(list); i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // Generator function to create the findIndex and findLastIndex functions.
  var createPredicateIndexFinder = function(dir) {
    return function(array, predicate, context) {
      predicate = cb(predicate, context);
      var length = getLength(array);
      var index = dir > 0 ? 0 : length - 1;
      for (; index >= 0 && index < length; index += dir) {
        if (predicate(array[index], index, array)) return index;
      }
      return -1;
    };
  };

  // Returns the first index on an array-like that passes a predicate test.
  _.findIndex = createPredicateIndexFinder(1);
  _.findLastIndex = createPredicateIndexFinder(-1);

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iteratee, context) {
    iteratee = cb(iteratee, context, 1);
    var value = iteratee(obj);
    var low = 0, high = getLength(array);
    while (low < high) {
      var mid = Math.floor((low + high) / 2);
      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
    }
    return low;
  };

  // Generator function to create the indexOf and lastIndexOf functions.
  var createIndexFinder = function(dir, predicateFind, sortedIndex) {
    return function(array, item, idx) {
      var i = 0, length = getLength(array);
      if (typeof idx == 'number') {
        if (dir > 0) {
          i = idx >= 0 ? idx : Math.max(idx + length, i);
        } else {
          length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
        }
      } else if (sortedIndex && idx && length) {
        idx = sortedIndex(array, item);
        return array[idx] === item ? idx : -1;
      }
      if (item !== item) {
        idx = predicateFind(slice.call(array, i, length), _.isNaN);
        return idx >= 0 ? idx + i : -1;
      }
      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
        if (array[idx] === item) return idx;
      }
      return -1;
    };
  };

  // Return the position of the first occurrence of an item in an array,
  // or -1 if the item is not included in the array.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (stop == null) {
      stop = start || 0;
      start = 0;
    }
    if (!step) {
      step = stop < start ? -1 : 1;
    }

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var range = Array(length);

    for (var idx = 0; idx < length; idx++, start += step) {
      range[idx] = start;
    }

    return range;
  };

  // Chunk a single array into multiple arrays, each containing `count` or fewer
  // items.
  _.chunk = function(array, count) {
    if (count == null || count < 1) return [];
    var result = [];
    var i = 0, length = array.length;
    while (i < length) {
      result.push(slice.call(array, i, i += count));
    }
    return result;
  };

  // Function (ahem) Functions
  // ------------------

  // Determines whether to execute a function as a constructor
  // or a normal function with the provided arguments.
  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
    var self = baseCreate(sourceFunc.prototype);
    var result = sourceFunc.apply(self, args);
    if (_.isObject(result)) return result;
    return self;
  };

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = restArguments(function(func, context, args) {
    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
    var bound = restArguments(function(callArgs) {
      return executeBound(func, bound, context, this, args.concat(callArgs));
    });
    return bound;
  });

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context. _ acts
  // as a placeholder by default, allowing any combination of arguments to be
  // pre-filled. Set `_.partial.placeholder` for a custom placeholder argument.
  _.partial = restArguments(function(func, boundArgs) {
    var placeholder = _.partial.placeholder;
    var bound = function() {
      var position = 0, length = boundArgs.length;
      var args = Array(length);
      for (var i = 0; i < length; i++) {
        args[i] = boundArgs[i] === placeholder ? arguments[position++] : boundArgs[i];
      }
      while (position < arguments.length) args.push(arguments[position++]);
      return executeBound(func, bound, this, this, args);
    };
    return bound;
  });

  _.partial.placeholder = _;

  // Bind a number of an object's methods to that object. Remaining arguments
  // are the method names to be bound. Useful for ensuring that all callbacks
  // defined on an object belong to it.
  _.bindAll = restArguments(function(obj, keys) {
    keys = flatten(keys, false, false);
    var index = keys.length;
    if (index < 1) throw new Error('bindAll must be passed function names');
    while (index--) {
      var key = keys[index];
      obj[key] = _.bind(obj[key], obj);
    }
  });

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memoize = function(key) {
      var cache = memoize.cache;
      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
      if (!has(cache, address)) cache[address] = func.apply(this, arguments);
      return cache[address];
    };
    memoize.cache = {};
    return memoize;
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = restArguments(function(func, wait, args) {
    return setTimeout(function() {
      return func.apply(null, args);
    }, wait);
  });

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = _.partial(_.delay, _, 1);

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
    var timeout, context, args, result;
    var previous = 0;
    if (!options) options = {};

    var later = function() {
      previous = options.leading === false ? 0 : _.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };

    var throttled = function() {
      var now = _.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };

    throttled.cancel = function() {
      clearTimeout(timeout);
      previous = 0;
      timeout = context = args = null;
    };

    return throttled;
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, result;

    var later = function(context, args) {
      timeout = null;
      if (args) result = func.apply(context, args);
    };

    var debounced = restArguments(function(args) {
      if (timeout) clearTimeout(timeout);
      if (immediate) {
        var callNow = !timeout;
        timeout = setTimeout(later, wait);
        if (callNow) result = func.apply(this, args);
      } else {
        timeout = _.delay(later, wait, this, args);
      }

      return result;
    });

    debounced.cancel = function() {
      clearTimeout(timeout);
      timeout = null;
    };

    return debounced;
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return _.partial(wrapper, func);
  };

  // Returns a negated version of the passed-in predicate.
  _.negate = function(predicate) {
    return function() {
      return !predicate.apply(this, arguments);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var args = arguments;
    var start = args.length - 1;
    return function() {
      var i = start;
      var result = args[start].apply(this, arguments);
      while (i--) result = args[i].call(this, result);
      return result;
    };
  };

  // Returns a function that will only be executed on and after the Nth call.
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Returns a function that will only be executed up to (but not including) the Nth call.
  _.before = function(times, func) {
    var memo;
    return function() {
      if (--times > 0) {
        memo = func.apply(this, arguments);
      }
      if (times <= 1) func = null;
      return memo;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = _.partial(_.before, 2);

  _.restArguments = restArguments;

  // Object Functions
  // ----------------

  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
    'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

  var collectNonEnumProps = function(obj, keys) {
    var nonEnumIdx = nonEnumerableProps.length;
    var constructor = obj.constructor;
    var proto = _.isFunction(constructor) && constructor.prototype || ObjProto;

    // Constructor is a special case.
    var prop = 'constructor';
    if (has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

    while (nonEnumIdx--) {
      prop = nonEnumerableProps[nonEnumIdx];
      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
        keys.push(prop);
      }
    }
  };

  // Retrieve the names of an object's own properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`.
  _.keys = function(obj) {
    if (!_.isObject(obj)) return [];
    if (nativeKeys) return nativeKeys(obj);
    var keys = [];
    for (var key in obj) if (has(obj, key)) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve all the property names of an object.
  _.allKeys = function(obj) {
    if (!_.isObject(obj)) return [];
    var keys = [];
    for (var key in obj) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // Returns the results of applying the iteratee to each element of the object.
  // In contrast to _.map it returns an object.
  _.mapObject = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = _.keys(obj),
        length = keys.length,
        results = {};
    for (var index = 0; index < length; index++) {
      var currentKey = keys[index];
      results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Convert an object into a list of `[key, value]` pairs.
  // The opposite of _.object.
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`.
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // An internal function for creating assigner functions.
  var createAssigner = function(keysFunc, defaults) {
    return function(obj) {
      var length = arguments.length;
      if (defaults) obj = Object(obj);
      if (length < 2 || obj == null) return obj;
      for (var index = 1; index < length; index++) {
        var source = arguments[index],
            keys = keysFunc(source),
            l = keys.length;
        for (var i = 0; i < l; i++) {
          var key = keys[i];
          if (!defaults || obj[key] === void 0) obj[key] = source[key];
        }
      }
      return obj;
    };
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = createAssigner(_.allKeys);

  // Assigns a given object with all the own properties in the passed-in object(s).
  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
  _.extendOwn = _.assign = createAssigner(_.keys);

  // Returns the first key on an object that passes a predicate test.
  _.findKey = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = _.keys(obj), key;
    for (var i = 0, length = keys.length; i < length; i++) {
      key = keys[i];
      if (predicate(obj[key], key, obj)) return key;
    }
  };

  // Internal pick helper function to determine if `obj` has key `key`.
  var keyInObj = function(value, key, obj) {
    return key in obj;
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = restArguments(function(obj, keys) {
    var result = {}, iteratee = keys[0];
    if (obj == null) return result;
    if (_.isFunction(iteratee)) {
      if (keys.length > 1) iteratee = optimizeCb(iteratee, keys[1]);
      keys = _.allKeys(obj);
    } else {
      iteratee = keyInObj;
      keys = flatten(keys, false, false);
      obj = Object(obj);
    }
    for (var i = 0, length = keys.length; i < length; i++) {
      var key = keys[i];
      var value = obj[key];
      if (iteratee(value, key, obj)) result[key] = value;
    }
    return result;
  });

  // Return a copy of the object without the blacklisted properties.
  _.omit = restArguments(function(obj, keys) {
    var iteratee = keys[0], context;
    if (_.isFunction(iteratee)) {
      iteratee = _.negate(iteratee);
      if (keys.length > 1) context = keys[1];
    } else {
      keys = _.map(flatten(keys, false, false), String);
      iteratee = function(value, key) {
        return !_.contains(keys, key);
      };
    }
    return _.pick(obj, iteratee, context);
  });

  // Fill in a given object with default properties.
  _.defaults = createAssigner(_.allKeys, true);

  // Creates an object that inherits from the given prototype object.
  // If additional properties are provided then they will be added to the
  // created object.
  _.create = function(prototype, props) {
    var result = baseCreate(prototype);
    if (props) _.extendOwn(result, props);
    return result;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Returns whether an object has a given set of `key:value` pairs.
  _.isMatch = function(object, attrs) {
    var keys = _.keys(attrs), length = keys.length;
    if (object == null) return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      if (attrs[key] !== obj[key] || !(key in obj)) return false;
    }
    return true;
  };


  // Internal recursive comparison function for `isEqual`.
  var eq, deepEq;
  eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a === 1 / b;
    // `null` or `undefined` only equal to itself (strict comparison).
    if (a == null || b == null) return false;
    // `NaN`s are equivalent, but non-reflexive.
    if (a !== a) return b !== b;
    // Exhaust primitive checks
    var type = typeof a;
    if (type !== 'function' && type !== 'object' && typeof b != 'object') return false;
    return deepEq(a, b, aStack, bStack);
  };

  // Internal recursive comparison function for `isEqual`.
  deepEq = function(a, b, aStack, bStack) {
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className !== toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
      case '[object RegExp]':
      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return '' + a === '' + b;
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive.
        // Object(NaN) is equivalent to NaN.
        if (+a !== +a) return +b !== +b;
        // An `egal` comparison is performed for other numeric values.
        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a === +b;
      case '[object Symbol]':
        return SymbolProto.valueOf.call(a) === SymbolProto.valueOf.call(b);
    }

    var areArrays = className === '[object Array]';
    if (!areArrays) {
      if (typeof a != 'object' || typeof b != 'object') return false;

      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                               _.isFunction(bCtor) && bCtor instanceof bCtor)
                          && ('constructor' in a && 'constructor' in b)) {
        return false;
      }
    }
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

    // Initializing stack of traversed objects.
    // It's done here since we only need them for objects and arrays comparison.
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] === a) return bStack[length] === b;
    }

    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);

    // Recursively compare objects and arrays.
    if (areArrays) {
      // Compare array lengths to determine if a deep comparison is necessary.
      length = a.length;
      if (length !== b.length) return false;
      // Deep compare the contents, ignoring non-numeric properties.
      while (length--) {
        if (!eq(a[length], b[length], aStack, bStack)) return false;
      }
    } else {
      // Deep compare objects.
      var keys = _.keys(a), key;
      length = keys.length;
      // Ensure that both objects contain the same number of properties before comparing deep equality.
      if (_.keys(b).length !== length) return false;
      while (length--) {
        // Deep compare each member
        key = keys[length];
        if (!(has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return true;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
    return _.keys(obj).length === 0;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) === '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError, isMap, isWeakMap, isSet, isWeakSet.
  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error', 'Symbol', 'Map', 'WeakMap', 'Set', 'WeakSet'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) === '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE < 9), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return has(obj, 'callee');
    };
  }

  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
  // IE 11 (#1621), Safari 8 (#1929), and PhantomJS (#2236).
  var nodelist = root.document && root.document.childNodes;
  if (typeof /./ != 'function' && typeof Int8Array != 'object' && typeof nodelist != 'function') {
    _.isFunction = function(obj) {
      return typeof obj == 'function' || false;
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return !_.isSymbol(obj) && isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`?
  _.isNaN = function(obj) {
    return _.isNumber(obj) && isNaN(obj);
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, path) {
    if (!_.isArray(path)) {
      return has(obj, path);
    }
    var length = path.length;
    for (var i = 0; i < length; i++) {
      var key = path[i];
      if (obj == null || !hasOwnProperty.call(obj, key)) {
        return false;
      }
      obj = obj[key];
    }
    return !!length;
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iteratees.
  _.identity = function(value) {
    return value;
  };

  // Predicate-generating functions. Often useful outside of Underscore.
  _.constant = function(value) {
    return function() {
      return value;
    };
  };

  _.noop = function(){};

  // Creates a function that, when passed an object, will traverse that object’s
  // properties down the given `path`, specified as an array of keys or indexes.
  _.property = function(path) {
    if (!_.isArray(path)) {
      return shallowProperty(path);
    }
    return function(obj) {
      return deepGet(obj, path);
    };
  };

  // Generates a function for a given object that returns a given property.
  _.propertyOf = function(obj) {
    if (obj == null) {
      return function(){};
    }
    return function(path) {
      return !_.isArray(path) ? obj[path] : deepGet(obj, path);
    };
  };

  // Returns a predicate for checking whether an object has a given set of
  // `key:value` pairs.
  _.matcher = _.matches = function(attrs) {
    attrs = _.extendOwn({}, attrs);
    return function(obj) {
      return _.isMatch(obj, attrs);
    };
  };

  // Run a function **n** times.
  _.times = function(n, iteratee, context) {
    var accum = Array(Math.max(0, n));
    iteratee = optimizeCb(iteratee, context, 1);
    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };

  // A (possibly faster) way to get the current timestamp as an integer.
  _.now = Date.now || function() {
    return new Date().getTime();
  };

  // List of HTML entities for escaping.
  var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
  };
  var unescapeMap = _.invert(escapeMap);

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  var createEscaper = function(map) {
    var escaper = function(match) {
      return map[match];
    };
    // Regexes for identifying a key that needs to be escaped.
    var source = '(?:' + _.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
      string = string == null ? '' : '' + string;
      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
  };
  _.escape = createEscaper(escapeMap);
  _.unescape = createEscaper(unescapeMap);

  // Traverses the children of `obj` along `path`. If a child is a function, it
  // is invoked with its parent as context. Returns the value of the final
  // child, or `fallback` if any child is undefined.
  _.result = function(obj, path, fallback) {
    if (!_.isArray(path)) path = [path];
    var length = path.length;
    if (!length) {
      return _.isFunction(fallback) ? fallback.call(obj) : fallback;
    }
    for (var i = 0; i < length; i++) {
      var prop = obj == null ? void 0 : obj[path[i]];
      if (prop === void 0) {
        prop = fallback;
        i = length; // Ensure we don't continue iterating.
      }
      obj = _.isFunction(prop) ? prop.call(obj) : prop;
    }
    return obj;
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate: /<%([\s\S]+?)%>/g,
    interpolate: /<%=([\s\S]+?)%>/g,
    escape: /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'": "'",
    '\\': '\\',
    '\r': 'r',
    '\n': 'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escapeRegExp = /\\|'|\r|\n|\u2028|\u2029/g;

  var escapeChar = function(match) {
    return '\\' + escapes[match];
  };

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  // NB: `oldSettings` only exists for backwards compatibility.
  _.template = function(text, settings, oldSettings) {
    if (!settings && oldSettings) settings = oldSettings;
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset).replace(escapeRegExp, escapeChar);
      index = offset + match.length;

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      } else if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      } else if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }

      // Adobe VMs need the match returned to produce the correct offset.
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + 'return __p;\n';

    var render;
    try {
      render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled source as a convenience for precompilation.
    var argument = settings.variable || 'obj';
    template.source = 'function(' + argument + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function. Start chaining a wrapped Underscore object.
  _.chain = function(obj) {
    var instance = _(obj);
    instance._chain = true;
    return instance;
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var chainResult = function(instance, obj) {
    return instance._chain ? _(obj).chain() : obj;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    _.each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return chainResult(this, func.apply(_, args));
      };
    });
    return _;
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
      return chainResult(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  _.each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return chainResult(this, method.apply(this._wrapped, arguments));
    };
  });

  // Extracts the result from a wrapped and chained object.
  _.prototype.value = function() {
    return this._wrapped;
  };

  // Provide unwrapping proxy for some methods used in engine operations
  // such as arithmetic and JSON stringification.
  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

  _.prototype.toString = function() {
    return String(this._wrapped);
  };

  // AMD registration happens at the end for compatibility with AMD loaders
  // that may not enforce next-turn semantics on modules. Even though general
  // practice for AMD registration is to be anonymous, underscore registers
  // as a named module because, like jQuery, it is a base library that is
  // popular enough to be bundled in a third party lib, but not be part of
  // an AMD load request. Those cases could generate an error when an
  // anonymous define() is called outside of a loader request.
  if (typeof define == 'function' && define.amd) {
    define('underscore', [], function() {
      return _;
    });
  }
}());

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1])(1)
});
