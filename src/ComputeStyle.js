let computed_styles = ["display",
        "font-size",  // fontSize
        "font-stretch", // fontStretch
        "font-style", // fontStyle
        "font-weight", // fontWeight
        "font-variant", // fontVariant
        "font-variant-caps", // fontVariantCaps
        "font", // font
        "float", // float
        "cssFloat", // cssFloat
        "color", // color
        "background-color", // backgroundColor
        "visibility" // visible
];

/*
 * Set the computed styles as atributes of the HTML
 */
function computeStyles(node, styles) {
    let allElements = node.querySelectorAll('*');
    let i;
    for(i = 0; i < allElements.length; i += 1) {
        styles.forEach((item) => {
            let style =  document.defaultView.getComputedStyle(allElements[i]).getPropertyValue(item);
            allElements[i].setAttribute("_css_" + item, style);
        });
    }
}

module.exports = {
    computed_styles:computed_styles,
    computeStyles:computeStyles,
};