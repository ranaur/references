let html = require('./../html.js');

function getClipboard() {
    html.addElementDictionary("textarea", {
        id: "clipArea"
    });
    var result = null;
    var textarea = document.getElementById('clipArea');
    textarea.value = '';
    textarea.select();

    if (document.execCommand('paste')) {
        result = textarea.value;
    } else {
        console.error('failed to get clipboard content');
    }

    html.delElementByID('clipArea');
    return result;
}

function setClipboard(value) {
    html.addElementDictionary("textarea", {
        id: "clipArea"
    });
    var text = document.getElementById('clipArea');
    text.innerHTML = value;

    text.value = value;
    text.select();

    if (document.execCommand('copy')) {
        result = true;
    } else {
        console.error('failed to get clipboard content');
    }

    html.delElementByID('clipArea');
    //text.value = '';
    return result;
}

function save(text, options) 
{
    let previousText;
    if(options.hasOwnProperty("appendClipboard") ? options.appendClipboard : false) {
        previousText = getClipboard() + "\n";
    } else {
        previousText = "";
    }
    setClipboard(previousText + text);
}

module.exports = { save: save };