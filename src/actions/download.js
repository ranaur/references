var util = require('./../util.js');

function save(text, options, url) {
    let downloadDir = options.hasOwnProperty("downloadDir") ? options.downloadDir.trim() : "";
    downloadDir = downloadDir[downloadDir.length-1] == '/' ? downloadDir.substr(0,downloadDir.length-1) : downloadDir;

    let filename = util.makeFilename(url, "md");

    util.download(text, downloadDir + "/" + filename, "text/markdown; charset=UTF-8");
}

module.exports = { save:save };