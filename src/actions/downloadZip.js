/* jshint esversion: 8 */
let util = require('./../util.js');

async function getImages(zip, html, base, md) {
    //let newHtml = html.cloneNode(true);
    let dom = new DOMParser().parseFromString(html, "text/html");
    let imgs = dom.getElementsByTagName("img");

    for (let i = 0; i < imgs.length; i++) {
        let imgURL = new URL(imgs[i].src, base);
        if (imgURL.protocol == "chrome-extension:") {
            console.log("chrome extension");
            imgURL.protocol = "http";
        }
        let ext = util.getExtension(imgURL.pathname);
        let filename = "img" + util.num2seq(i) + "." + ext;

        try {
            console.log("Fetch: ", imgURL.toString());
            let blob = await fetch(imgURL.toString());
            blob = blob.blob();
            //console.log("Filename: ", filename);
            zip.file(filename, blob, {
                base64: false,
                binary: true
            });
            md = md.replace(imgs[i].src, filename);
        } catch (e) {
            console.log(e);
        }
    }

    return md;
}

async function downloadZip(url, md, html, directory) {
    let zip = new JSZip();
    let filenameZIP = util.makeFilename(url, "zip");
    directory = directory[directory.length-1] == '/' ? directory.substr(0,directory.length-1) : directory;
    //let filenameMD = makeFilename(url, "md");
    md = await getImages(zip, html, url, md);

    zip.file("index.md", md, {
        base64: false,
        binary: false
    });
    let content = await zip.generateAsync({
        type: "blob"
    });
    util.download(content, directory + "/" + filenameZIP, "application/zip");
}

function save(md, html, options, url) {
    let downloadZipDir = options.hasOwnProperty("zipDir") ? options.zipDir.trim() : "";
    downloadZip(url, md, html, downloadZipDir);
}

module.exports = { save: save };