let html = require('./html.js');

html.runScript("lib/jquery-3.3.1.min.js");
html.runScript("lib/jszip.min.js");
html.runScript("lib/turndown.js");
html.runScript("lib/turndown-plugin-gfm.js");

function setBadge(text, ok = true, timeout = 3) {
    let color;
    if(ok) {
        color = [0x8b, 0xc3, 0x4a, 255]; // #8bc34a
    } else {
        color = [0xf4, 0x43, 0x36, 255]; // #f44336
    }
    chrome.browserAction.setBadgeBackgroundColor({ color: color });
    chrome.browserAction.setBadgeText({text: text});
    setTimeout(function (){
        chrome.browserAction.setBadgeText({text: ""});
    }, timeout * 1000);
}

function sendMessage(method, func, window_id) {
    let query_data;
    
    if(typeof window_id === "undefined") {
        query_data = {
            active: true,
            //currentWindow: true,
            windowId: chrome.windows.WINDOW_ID_CURRENT
        };
    } else {
        query_data = {
            active: true,
            windowId: window_id
        };
    }

    let computeStyles = document.options.cleanupComputeCSSStyles ? document.options.cleanupComputeStyles : false;
    
    chrome.tabs.query(query_data,
        function(tabs) {
            if (tabs.length > 0) {
                chrome.tabs.sendMessage(
                    tabs[0].id, {
                        method: method,
                        url: tabs[0].url,
                        computeStyles: computeStyles
                    },
                    func
                );
            }
        }
    );
}

function saveResponse(response) {
    if (typeof response === "undefined") {
        console.log("undefined response");
        return;
    }

    if (response.result != "OK") {
        console.log("Error: " + response.error);
        return false;
    }

    if (!("html" in response)) {
        console.log("empty response");
        return;
    }

    chrome.storage.sync.get(['options'], function(result) {
        let options = result.options;

        response.options = options;
        response = require('./converters/index.js')(response);
        let mdData = response.headerMD + response.bodyMD;
        if(response.hasOwnProperty("inspectMD")) mdData += "\n\n---\n" + response.inspectMD;

        if(options.hasOwnProperty("outputToClipboard") ? options.outputToClipboard : true) {
            require("./actions/clipboard.js").save(mdData, options);

            if(options.hasOwnProperty("clipboardShowBadge") ? options.clipboardShowBadge : true) {
                let showBadgeValue = options.hasOwnProperty("clipboardShowBadgeSeconds") ? options.clipboardShowBadgeSeconds : 5;
                setBadge("  ", showBadgeValue);
            }
        }
            
        if(options.hasOwnProperty("outputToDownload") ? options.outputToDownload : false) {
            require("./actions/download.js").save(mdData, options, response.captureMeta.url);
        }
        
        if(options.hasOwnProperty("outputToZip") ? options.outputToZip : false) {
            require("./actions/downloadZip.js").save(mdData, response.originalHTML, options, response.captureMeta.url);
        }
    });
    return true;
}

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    if (typeof request === "undefined") {
        sendResponse({
            result: "ERROR",
            error: "undefined request"
        });
        return;
    }

    if (!("method" in request)) {
        sendResponse({
            result: "ERROR",
            error: "request without method"
        });
        return;
    }

    var response = {
        result: "EMPTY",
        error: "unhandled method"
    };
    switch (request.method) {
        case "copy-Selection":
            sendMessage("getSelection", saveResponse);
            response = {
                result: "OK"
            };
            break;
        case "copy-Element":
            sendMessage("getSelectedElement", saveResponse);
            response = {
                result: "OK"
            };
            break;
        case "copy-Document":
            sendMessage("getDocument", saveResponse);
            response = {
                result: "OK"
            };
            break;
        default:
            console.log("unknown method: " + request);
            response = {
                result: "ERROR",
                error: "unknown method: " + request
            };
    }
    sendResponse(response);
});

// Command Listener
chrome.commands.onCommand.addListener(function(command) {
    switch (command) {
        case "copy-Selection":
            sendMessage("getSelection", saveResponse);
            break;
        case "copy-Element":
            sendMessage("getSelectedElement", saveResponse);
            break;
        case "copy-Document":
            sendMessage("getDocument", saveResponse);
            break;
        default:
            console.log("Invalid command: " + command);
    }
});

// Context Menus
chrome.contextMenus.removeAll(function() {
    chrome.contextMenus.create({
        title: "Copy document as MD",
        id: "getDocument",
        contexts: ['page'],
    });
    
    chrome.contextMenus.create({
        title: "Copy selection as MD",
        id: "getSelection",
        contexts: ['selection'],
    });
    
    chrome.contextMenus.create({
        title: "Copy selected Element as MD",
        id: "getSelectedElement",
        contexts: ['selection'],
    });
    
    chrome.contextMenus.onClicked.addListener(function (info, tab) {
        { 
            sendMessage(info.menuItemId, saveResponse, tab.windowId);
        }
    });
});

// load options
chrome.storage.sync.get(null, function(data) {
    // XXX treat when there is no data
    document.options = data.options;
});