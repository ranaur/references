let manifestData = chrome.runtime.getManifest();
let software_version = manifestData.name + " " + manifestData.version;
let computeStyles = require('./ComputeStyle.js');

// cssText grid
// gets all metadata of the page
function getMetadata() {
    let i;
    let res = {};

    const metas = document.getElementsByTagName('meta');
    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute('name') !== null) {
            res[metas[i].getAttribute('name')] = metas[i].getAttribute('content');
        }
    }
    return res;
}

function makeMessage(method, html) {
    let now = new Date();
    let lastModified = new Date(window.document.lastModified);
    return {
        result: "OK",
        captureMeta: { 
            url: window.location.href,
            docurl: window.document.url,
            title: window.document.title,
            lastModified: lastModified.toISOString(),
            dateCaptured: now.toISOString(),
            software: software_version,
            capture: method,
        },
        documentMeta: getMetadata(),
        html: html
    };
}

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    if (window.location.href == request.url) {
        if(Array.isArray(request.computeStyles)) {
            computeStyles.computeStyles(document.body, request.computeStyles);
        }
        switch (request.method) {
            case "hasSelection":
                if (window.getSelection() + "" != "") {
                    sendResponse({
                        result: "OK",
                        hasSelection: true
                    });
                } else {
                    sendResponse({
                        result: "OK",
                        hasSelection: false
                    });
                }
            break;

        case "getSelection":
                if (window.getSelection() + "" != "") {
                    var fragment = window.getSelection().getRangeAt(0).cloneRange().cloneContents();
                    var html = $('<body>').append(fragment).html();
                    sendResponse(makeMessage("selection", html));
                } else {
                    sendResponse({
                        result: "ERROR",
                        error: "no selection"
                    });
                }
                break;
            case "getDocument":
                sendResponse(makeMessage("document", $('body').html()));
                break;
            case "getSelectedElement":
                if (window.getSelection().rangeCount > 0) {
                    let frag = window.getSelection().getRangeAt(0).commonAncestorContainer.cloneNode(true);
                    sendResponse(makeMessage("element", $('<body>').append(frag).html()));
                } else {
                    sendResponse({
                        result: "ERROR",
                        error: "no selection"
                    });
                }
                break;
            default:
                sendResponse({
                    result: "ERROR",
                    error: "invalid command"
                }); // snub them.
        }
    } else {
        //console.log("Skipped " + request.method + " request " + request.URL + " != " + window.location.href);
    }
});
