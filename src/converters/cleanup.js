var Cleanup = require("./modules/Cleanup.js");

function execute(data) {
    if(data.options.hasOwnProperty("useCleanup") ? data.options.useCleanup : false) {
        data.html = Cleanup.Cleanup(data.html, data.options.cleanupRules, data.captureMeta.url);
    }
    return data;
}

module.exports = execute;