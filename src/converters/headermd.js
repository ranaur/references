function execute(data) {
    let mdHeader = "---\n";
    for (let key in data.captureMeta) {
        mdHeader += key + ": " + data.captureMeta[key] + "\n";
    }
    for (let element in data.documentMeta) {
        if (element in ['author', 'keywords', 'description']) {
            mdHeader += element + ": " + data.documentMeta[element] + "\n";
        }
    }
    mdHeader += "---\n";

    data.headerMD = mdHeader;
    return data;
}

module.exports = execute;

