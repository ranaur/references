let converters = [
    require('./headermd.js'),
    require('./cleanup.js'), 
    require('./inspect.js'), 
    require('./readability.js'),
    require('./turndown.js')
];

function process(data) {
    data.originalHTML = data.html;

    converters.forEach(function (converter) {
        //console.log(converter);
        data = converter(data);
    });
    return data;
}

module.exports = process;