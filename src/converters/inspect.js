var Inspect = require("./modules/Inspect.js");

function array2md(object, title = "", heading = "***") {
    let res = "\n";

    if(title != "") {
        res += heading + " " + title + "\n\n";
    }
    Object.keys(object).forEach((key) => {
        res += "\t- " + object[key].name + ": " + object[key].count + "\n";
        
    });
    return res;
}

function execute(data) {
    if(data.options.hasOwnProperty("useInspect") ? data.options.useInspect : false) {
        let attributes, classes, tags, IDs;
        const uniqueTag = 'inspect';
        var element = document.createElement(uniqueTag);
        try {
            element.innerHTML = data.html;
        } catch (e) {
            console.log(e);
        }

        if(data.options.inspectAttributes) {
            attributes = Inspect.getAllAttributes(element, data.options.inspectAttributeList, data.options.inspectAttributePrefix);
        }
        if(data.options.inspectClasses) {
            classes = Inspect.getAllClasses(element);
        }
        if(data.options.inspectTags) {
            tags = Inspect.getAllTags(element);
        }
        if(data.options.inspectIDs) {
            IDs = Inspect.getAllIDs(element);
        }

        var result = "** Inspect Output\n\n";
        if(attributes) {
            result += "*** Attributes and values\n";
            Object.keys(attributes).forEach((key) => {
                result += array2md(attributes[key].itens, attributes[key].name, "h3");
            });
        }
        if(classes) {
            result += array2md(classes, "Classes");
        }
        if(tags) {
            result += array2md(tags, "Tags");
        }
        if(IDs) {
            result += array2md(IDs, "IDs");
        }
        data.inspectMD = result;
    }
    return data;
}

module.exports = execute;