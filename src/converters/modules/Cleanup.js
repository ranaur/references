// needs jQuery
let default_rules = [
    { action: "remove", selector: [
        'head', 'header', 'script', 'nav', 'svg', 'footer', 'menu', 
        '[id^=nav-]', '#sidebar', '[class$=-actions]', '[id^=newsletter-]', 
        '#toc'
    ]},
    //{ action: "replaceAttribute", selector: ['[_css_display=none]'], newAttribute: "i-am-a-block", newValue:"yes" },
    { action: "replaceTag", selector: ['main', 'article'], newTag: "div" }
]; 
/* Cleanup rules:

Array [] of {} => executes in order
    {
        url: (optional) matches rule only when regular expression matches (or array)
        action: <remove, replaceTag, replaceAttribute or function(html) {}>
        selector: Array of [] jquery selectors strings or function(element) {}
        newTag: on replaceTag the new tag name
        newAttribute: on replaceTag the new attribute name
        newValue: on replaceAttribute the new value
    }
*/

function Cleanup(html, rules, sourceURL) {
     /*jshint unused:false*/
    const uniqueTag = 'cleanup';
    if(typeof rules=== "undefined") {
        rules = default_rules;
    }
    function remove(elem, tag, newTag, newAttribute, newValue) {
        elem.remove();
    }
    function replaceTag(elem, tag, newTag, newAttribute, newValue) {
        elem.replaceWith($('<'+newTag+'/>').html(elem.html()));
    }
    function replaceAttribute(elem, tag, newTag, newAttribute, newValue) {
        if(typeof newValue === 'undefined') {
            elem.removeAttr(newAttribute);
        } else {
            elem.attr(newAttribute, newValue);
        }
    }
    /*jshint unused:true*/
    if(typeof html === "string") {
        var a = document.createElement(uniqueTag);
        try {
            a.innerHTML = html;
        } catch (e) {
            console.log(e);
        }
        html = a;
    } else {
        html = html.cloneNode(true);
    }
    if(!Array.isArray(rules)) {
        throw "rules must be an array";
    }

    html.normalize();
    document.body.appendChild(html);

    rules.forEach(function (rule) {
        let action = rule.action;
        let newTag, newAttribute, newValue;

        let urlMatches = false;
        if(typeof sourceURL === "undefined") { urlMatches = true; }
        else if (typeof rule.when === "undefined") { urlMatches = true; }
        else if(typeof rule.when === "string") {
            urlMatches = new RegExp(rule.when).exec(sourceURL) != null;
        } else if(Array.isArray(rule.when)) {
            rule.when.forEach(function (url) {
                let re = new RegExp(url);
                if (re.exec(sourceURL) != null) {
                    urlMatches = true;
                }
            });
        } else {
            throw "invalid url parameter in rule";
        }

        if(urlMatches) {
            if(action == "remove") { action = remove; }
            if(action == "replaceTag") { action = replaceTag; newTag = rule.newTag; }
            if(action == "replaceAttribute") { action = replaceAttribute; newAttribute = rule.newAttribute; newValue = rule.newValue; }
            
            let selector = rule.selector;
            if(Array.isArray(selector)) {
                selector.forEach(function (tag) {
                    var a = $(tag);
                    if(!!a) action(a, tag, newTag, newAttribute, newValue);
                });
            } if(typeof selector === "string") {
                var a = $(selector);
                if(!!a) action(a, null, newTag, newAttribute, newValue);
            } else if (typeof selector === "function") {
                document.querySelectorAll('*').forEach(function(node) {
                    if(selector(node)) {
                        var a = $(node);
                        if(!!a) action(a, null, newTag, newAttribute, newValue);
                    }
                });
            }
        }
    });

    let res = document.getElementsByTagName(uniqueTag)[0].innerHTML;
    document.getElementsByTagName(uniqueTag)[0].remove();
    return res;
}

Cleanup.prototype.default_rules = default_rules;

if (typeof module === "object") {
    module.exports = { Cleanup:Cleanup, default_rules:default_rules };
}