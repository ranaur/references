function getAllAttributes(node, attributes, prefix = "") {
    function getAttribute (item) {
        let value = allElements[i].getAttribute(prefix + item);
        let val = allAttributes[item].itens[value] || {value:value, count:0};
        val.count++;
        allAttributes[item].itens[value] = val;
    }

    let allAttributes = {};
    let i;
    if(typeof attributes == "string") { attributes = [attributes]; }
    attributes.forEach(function (item) {
        allAttributes[item] = {name:item, itens:{}};
    });
    let allElements = node.querySelectorAll('*');

    for (i = 0; i < allElements.length; i += 1) {
        attributes.forEach(getAttribute);
    }
    return allAttributes;
}

function getAllClasses(node) {
    let allClasses = {};
    let allElements = node.querySelectorAll('*');
    let i, j;

    for(i = 0; i < allElements.length; i += 1) {
        let classes = allElements[i].className.toString().split(/\s+/);
        for (j = 0; j < classes.length; j += 1) {
            let cls = classes[j];
            let _class = allClasses[cls] || {name: cls, count:0};
            _class.count++;

            allClasses[cls] = _class;
        }
    }
    return allClasses;
}

function getAllTags(node) {
    let allTags = {};
    let i;
    let allElements = node.querySelectorAll('*');

    for (i = 0; i < allElements.length; i += 1) {
        let element = allTags[allElements[i].tagName] || {name: allElements[i].tagName, count:0};
        element.count++;
        allTags[allElements[i].tagName] = element;
    }
    return allTags;
}

function getAllIDs(node) {
    let ids = {};
    let i;
    let allElements = node.querySelectorAll('*');

    for (i = 0; i < allElements.length; i += 1) {
        let id = ids[allElements[i].id] || {name: allElements[i].id, count:0};
        id.count++;
        ids[allElements[i].name] = id;
    }
    return ids;
}

/*
function analyze(node) {
    let elements = {};

    let allElements = node.querySelectorAll('*');

    for (let i = 0; i < allElements.length; i++) {
        let element = elements[allElements[i].name] || {};
        elelemt[count] = elelemt[count]++ || 1;

        let classes = allElements[i].className.toString().split(/\s+/);
        for (let j = 0; j < classes.length; j++) {
            let cls = classes[j];
            if (cls && allClasses.indexOf(cls) === -1)
            allClasses.push(cls);
        }
    }
    return allClasses;
}
*/

module.exports = {
    getAllAttributes:getAllAttributes,
    getAllIDs:getAllIDs,
    getAllTags:getAllTags,
    getAllClasses:getAllClasses
};
