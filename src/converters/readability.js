let Readability = require("./modules/Readability.js");

function execute(data) {
    if(data.options.useReadability) {
        let readabilityNode = document.createElement('readability');
        readabilityNode.innerHTML = data.html;
        document.body.appendChild(readabilityNode);
        Readability(document);
        Readability.prototype.parse();
        data.html = readabilityNode.innerHTML;
        document.getElementsByTagName("readability")[0].remove();
    } 
    return data;
}

module.exports = execute;

