function execute(data) {

    var converter = new TurndownService(data.options.turndownOptions)
        .remove(data.options.turndownClassesToRemove);
    
    if(data.options.turndownUseGFM) {
        var gfm = turndownPluginGfm.gfm;
        converter.use(gfm);
    }

    data.bodyMD = converter.turndown(data.html);
    return data;
}

module.exports = execute;
