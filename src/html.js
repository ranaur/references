function addElement(element, addInBody = true) {
    element.onload = function() {
        this.remove();
    };
    if (addInBody) {
        (document.body || document.documentElement).appendChild(element);
    } else {
        (document.head || document.documentElement).appendChild(element);
    }
}

function addElementDictionary(nameDotClass, attributes = {}, addInBody = true) {
    var nameClass = nameDotClass.split(".");
    var name = nameClass[0];
    var elementClass = nameClass[1];
    var element = document.createElement(name);

    if (elementClass != null) {
        element.className = elementClass;
    }

    for (var key in attributes) {
        element.setAttribute(key, attributes[key]);
    }

    addElement(element, addInBody);
}

function runScript(script) {
    var s = document.createElement('script');
    s.id = script;
    s.src = chrome.extension.getURL(script);
    if (document.getElementById(script) == null) {
        addElement(s, false)
    }
}

function delElementByID(id) {
    document.getElementById(id).remove();
}

module.exports = {addElement:addElement, addElementDictionary:addElementDictionary, delElementByID:delElementByID, runScript:runScript};
