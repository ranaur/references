let default_rules = require("./converters/modules/Cleanup.js").default_rules;
let computed_styles = require("./ComputeStyle.js").computed_styles;
let YAML = require("../assets/lib/js-yaml.min.js");

//yaml = require('js-yaml');
let defaultOptions = {
    useReadability: false,
    useCleanup: true,
    cleanupComputeCSSStyles: true,
    cleanupComputeStyles: computed_styles,
    cleanupRules: default_rules, // '[{"action":"remove","selector":["head","header","script","nav","svg","footer","menu","[id^=nav-]","#sidebar","[class$=-actions]","[id^=newsletter-]","#toc"]},{"action":"replaceTag","selector":["main","article"],"newTag":"div"}]'
    outputToClipboard: true,
    clipboardAppend: false,
    clipboardShowBadge: true,
    clipboardShowBadgeSeconds: 5,
    saveToClipboard: true,
    appendClipboard: false,
    outputToDownload: false,
    downloadDir: "references",
    outputToZip: false,
    zipDir: "referencesZip",
    turndownUseGFM: true,
    turndownClassesToRemove: ['script', 'style', 'head', 'svg'],
    turndownOptions: {codeBlockStyle:"fenced"},

    useInspect: false,
    inspectAttributes: true,
    inspectAttributeList: "",
    inspectAttributePrefix: "",
    inspectClasses: true,
    inspectTags: true,
    inspectIDs: true,

};

function saveOptions() {
    let options = {
        useReadability: getCheck("useReadability"),
        useCleanup: getCheck("useCleanup"),
        cleanupComputeCSSStyles:getCheck("cleanupComputeCSSStyles"),
        cleanupComputeStyles:getEditor("cleanupComputeStyles"),
        cleanupRules:getEditor("cleanupRules"),
        outputToClipboard:getCheck("outputToClipboard"),
        clipboardAppend:getCheck("clipboardAppend"),
        clipboardShowBadge: getCheck("clipboardShowBadge"),
        clipboardShowBadgeSeconds: getValue("clipboardShowBadgeSeconds"),
        outputToDownload: getCheck("outputToDownload"),
        downloadDir: getValue("downloadDir"),
        outputToZip: getCheck("outputToZip"),
        zipDir: getValue("zipDir"),
        turndownUseGFM: getCheck("turndownUseGFM"),
        turndownClassesToRemove: getEditor("turndownClassesToRemove"),
        turndownOptions: getEditor("turndownOptions"),
        useInspect: getCheck("useInspect"),
        inspectAttributes: getCheck("inspectAttributes"),
        inspectAttributeList: getValue("inspectAttributeList"),
        inspectAttributePrefix: getValue("inspectAttributePrefix"),
        inspectClasses: getCheck("inspectClasses"),
        inspectTags: getCheck("inspectTags"),
        inspectIDs: getCheck("inspectIDs"),
        };
    $("#clipboardShowBadgeSecondsValue").prop("disabled", !$("#clipboardShowBadgeCheck").is(":checked"));
    console.log("Saving ... ", options);
    chrome.storage.sync.set({
        options: options
    }, function() {
        console.log('Options saved ...');
    });
}

function getCheck(selector) {
    return !!$('#'+selector+'Check')[0].checked;
}
function fillCheck(selector, options) {
    $('#'+selector+'Check')[0].checked = options.hasOwnProperty(selector) ? options[selector] : defaultOptions[selector];
}

function getValue(selector) {
    return $('#'+selector+'Value')[0].value;
}   
function fillValue(selector, options) {
    $('#'+selector+'Value')[0].value = options.hasOwnProperty(selector) ? options[selector] : defaultOptions[selector];
}

function getEditor(selector) {
    //return JSON.parse(editors[selector].getValue());
    return YAML.safeLoad(editors[selector].getValue());
}
function fillEditor(selector, options) {
    let text = options.hasOwnProperty(selector) ? options[selector] : defaultOptions[selector];
    //editors[selector].setValue(JSON.stringify(text, null, '\t'), -1);
    editors[selector].setValue(YAML.safeDump(text), -1);
}

function setOptions(options) {
    console.log("Loading ...", options);
    fillCheck("useReadability", options);
    fillCheck("useCleanup", options);
    fillCheck("cleanupComputeCSSStyles", options);
    fillEditor("cleanupComputeStyles", options);
    fillCheck("outputToClipboard", options);
    fillCheck("clipboardAppend", options);
    fillEditor("cleanupRules", options);
    fillCheck("clipboardShowBadge", options);
    fillCheck("clipboardShowBadge", options);
    fillValue("clipboardShowBadgeSeconds", options);
    $("#clipboardShowBadgeSecondsValue").prop("disabled", !$("#clipboardShowBadgeCheck").is(":checked"));
    fillCheck("outputToDownload", options);
    fillValue("downloadDir", options);
    fillCheck("outputToZip", options);
    fillValue("zipDir", options);
    fillCheck("turndownUseGFM", options);
    fillEditor("turndownClassesToRemove", options);
    fillEditor("turndownOptions", options);
    fillCheck("useInspect", options),
    fillCheck("inspectAttributes", options),
    fillValue("inspectAttributeList", options),
    fillValue("inspectAttributePrefix", options),
    fillCheck("inspectClasses", options),
    fillCheck("inspectClasses", options),
    fillCheck("inspectTags", options),
    fillCheck("inspectIDs", options),

    $('label[data-toggle=collapse]').each(function(index, element) {
        let target = element.getAttribute("data-target");
        let input = element.getAttribute("for");

        if ($("#" + input)[0].checked) {
            $(target).collapse('show');
        } else {
            $(target).collapse('hide');
        }
    });
    $('label[data-toggle=collapse]').each(function(index, element) {
        let target = element.getAttribute("data-target");
        let input = element.getAttribute("for");

        if ($("#" + input)[0].checked) {
            $(target).collapse('show');
        } else {
            $(target).collapse('hide');
        }
    });
}

$('.editor').keypress(saveOptions);
$('input').change(saveOptions);
$('input[number]').keypress(saveOptions);
$('input[text]').keypress(saveOptions);

let editors = {};
function makeEditor(selector, mode = "yaml") {
    var editor = ace.edit(selector + "Editor");
    editor.setTheme("ace/theme/chrome");
    editor.session.setMode("ace/mode/" + mode);
    editors[selector] = editor;
    enableDisableEditor(selector, true);
}
function enableDisableEditor(selector, enable) {
    let editor = editors[selector];
    if(!!enable) {
        editor.setOptions({
            readOnly: false,
            highlightActiveLine: true,
            highlightGutterLine: true
        });
        editor.setTheme("ace/theme/chrome");
    } else {
        editor.setOptions({
            readOnly: true,
            highlightActiveLine: false,
            highlightGutterLine: false
        });
        editor.setTheme("ace/theme/kuroir");
    }
}

makeEditor("cleanupComputeStyles");
makeEditor("cleanupRules");
makeEditor("turndownClassesToRemove", "text");
makeEditor("turndownOptions");

chrome.storage.sync.get(['options'], function(result) {
    try {
        setOptions(result.options);
    } catch (e) {
        setOptions({});
        console.log('Error loading options: ' + e);
    }
});
/*
function clearOptions() {
    chrome.storage.sync.clear();
}
clearOptions();
*/
