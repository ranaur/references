function sendMessage(method) {
    var request = {
        method: method
    };
    chrome.runtime.sendMessage(
        request,
        function(response) {
            console.log(response);
        }
    );
}

function sendMessageContent(method, func) {
    chrome.tabs.query({
            active: true,
            currentWindow: true
            //, windowId: chrome.windows.WINDOW_ID_CURRENT
        },
        function(tabs) {
            if (tabs.length > 0 ) {
                if(tabs[0].url.startsWith("chrome://")) {
                    func();
                } else {
                    chrome.tabs.sendMessage(
                        tabs[0].id, {
                            method: method,
                            url: tabs[0].url
                        },
                        func
                    );
                }
            }
        }
    );
}

$(function() {
    $(".widget, .widget button").button();

    $("button").click(function(event) {
        event.preventDefault();
    });
});

$(function() {
    $('#copySelection').click(function() {
        sendMessage("copy-Selection");
    });
});
$(function() {
    $('#copyDocument').click(function() {
        sendMessage("copy-Document");
    });
});
$(function() {
    $('#copySelectedElement').click(function() {
        sendMessage("copy-Element");
    });
});

sendMessageContent("hasSelection", function(response) {
    if(typeof response === 'undefined') {
        $('#copyDocument').attr('disabled','disabled');
        $('#copySelection').hide();
        $('#copySelectedElement').hide();
    } else if(response.hasSelection) {
        $('#copyDocument').removeAttr('disabled');
        $('#copySelection').show();
        $('#copySelectedElement').show();
    } else {
        $('#copyDocument').removeAttr('disabled');
        $('#copySelection').hide();
        $('#copySelectedElement').hide();
    }
});
