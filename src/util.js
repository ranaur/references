/*
 * Generate a sequence number. If the imput number I is less than 999999 (if
 * ndigits = 6), it returns ia string with the number padded with 0 of ndigits
 * caracters). If it is greater than this, it return A00000 up to ZZZZZZ. If
 * it is a negative number or a greter number, it throws an exception.
 *
 * This function is useful to generate sequencial file names that works on 
 * lexicographical order.
 *
 */
function num2seq(i, ndigits = 6) {
    const limit10 = Math.pow(10, ndigits);
    const start36 = parseInt("A" + "0".repeat(ndigits - 1), 36);
    const limit36 = Math.pow(36, ndigits) - start36 + limit10; // 1573120576;
    if (i < 0) {
        throw "invalid number";
    } else if (i < limit10) {
        return ("0".repeat(ndigits) + i).slice(-ndigits);
    } else if (i < limit36) {
        i = i - limit10 + start36;
        return i.toString(36).toUpperCase();
    } else {
        throw "invalid number";
    }
}

function getExtension(path) {
    var basename = path.split(/[\\/]/).pop(), // extract file name from full path ...
        // (supports `\\` and `/` separators)
        pos = basename.lastIndexOf("."); // get last position of `.`

    if (basename === "" || pos < 1) // if file name is empty or ...
        return ""; //  `.` not found (-1) or comes first (0)

    return basename.slice(pos + 1); // extract extension ignoring `.`
}

function makeFilename(urlstr, extension = null) {
    var url = new URL(urlstr).pathname;
    url = url.replace(/\/$/, "");
    var filename = url.substring(url.lastIndexOf('/') + 1);
    return filename.replace(/\.[^/.]+$/, "") + (!!extension ? "." + extension : "");
}

/*
 * Downloads a file
 *
 */
function download(data, filename, mimetype = "application/octet-stream") {
    var blob = new Blob([data], {
        type: mimetype
    });
    var url = URL.createObjectURL(blob);
    chrome.downloads.download({
        url: url,
        filename: filename,
        conflictAction: "uniquify",
        saveAs: false
    });
}

module.exports = { num2seq:num2seq, getExtension:getExtension, makeFilename:makeFilename, download:download };